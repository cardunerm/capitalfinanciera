﻿using System.Collections.Generic;

namespace DataAccess.Pages
{
    public class PageResult<T> where T : class
    {
        public int Count { get; set; }
        public IList<T> Result { get; set; }
    }
}