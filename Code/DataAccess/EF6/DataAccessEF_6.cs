﻿using DataAccess.Pages;
using Numero3.EntityFramework.Implementation;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;

namespace DataAccess.EF6
{
    public class DataAccessEF_6<TContext> : Interface.IDataAccess
           where TContext : DbContext
    {
        ///// <summary>
        ///// Dll que se inyecta con la dll del contexto correspondiente
        ///// </summary>
        //public string DllIject { get; set; }

        //private string server;
        //private string dataBase;
        //private string user;
        //private string password;

        private readonly IAmbientDbContextLocator _contextLocator = new AmbientDbContextLocator();

        /// <summary>
        /// Dll que se inyecta con la dll del contexto correspondiente
        /// </summary>
        public string DllIject { get; set; }

        private DbContext context;
        private bool createContext = true;

        /// <summary>
        /// Context de EF 6
        /// </summary>
        public DbContext Context
        {
            get
            {
                
                if (_contextLocator == null)
                    throw new Exception();

                if (createContext)
                {
                    context = _contextLocator.Get<TContext>();
                }

                return context;
                //= _contextLocator.Get<TContext>();
                // _context.Configuration.LazyLoadingEnabled = false;
                //return _context ?? (_context = _contextLocator.Get<TContext>());
            }
        }

        public DataAccessEF_6() { }

        //public DataAccessEF_6(string server, string dataDase, string userName, string password)
        //{
        //    context = CreateContext(server, dataDase, userName, password);
        //    createContext = false;
        //}

        public void SetLazy(bool lazy)
        {
            Context.Configuration.LazyLoadingEnabled = lazy;
        }

        public void UseProxy(bool proxy)
        {
            Context.Configuration.ProxyCreationEnabled = proxy;
        }

        /// <summary>
        /// Crea el contexto dependiendo de la DLL inyectada
        /// </summary>
        /// <returns></returns>
        private DbContext CreateContext()
        {
            var obj = Activator.CreateInstance(Type.GetType(DllIject, true));

            return obj as DbContext;
        }

        //private DbContext CreateContext(string server, string dataDase, string userName, string password)
        //{
        //    var obj = Activator.CreateInstance(Type.GetType("Luma.Sarmiento.Model.External.ICRModelContext, Luma.Sarmiento.Model.External", true), server, dataDase, userName, password);

        //    return obj as DbContext;
        //}

        ///// <summary>
        ///// Crea el contexto dependiendo de la DLL inyectada
        ///// </summary>
        ///// <returns></returns>
        //private DbContext CreateContext()
        //{
        //    try
        //    {
        //        object obj;

        //        if (string.IsNullOrEmpty(server))
        //        {
        //            obj = Activator.CreateInstance(Type.GetType(DllIject, true));
        //        }
        //        else
        //        {
        //            string connStr = ConfigurationManager.ConnectionStrings["BAS_ICR_ConnStr"].ConnectionString;
        //            connStr = string.Format(connStr, server, dataBase, user, password);

        //            DbConnection dbConn = new System.Data.SqlClient.SqlConnection(connStr);
        //            obj = new DbContext(dbConn, true);
        //        }

        //        return obj as DbContext;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public DataAccessEF_6(string server = null, string dataBase = null, string user = null, string password = null)
        //{
        //    this.server = server;
        //    this.dataBase = dataBase;
        //    this.user = user;
        //    this.password = password;
        //}

        /// <summary>
        /// Obtiene una entidad por ID
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetById<T>(long id) where T : class
        {
            return Context.Set<T>().Find(id);
        }        

        public List<T> GetAll<T>() where T : class
        {
            IQueryable<T> query = Context.Set<T>();

            return query.ToList();
        }

        public void Update<T>(T entity) where T : class
        {
            if (Context.Entry(entity).State == EntityState.Added)
            {

            }
        }

        public void SaveOrUpdate<T>(T entity) where T : class
        {

        }

        public void Delete<T>(T entity) where T : class
        {
            Context.Set<T>().Remove(entity);
        }

        public void DeleteList<T>(IEnumerable<T> list) where T : class
        {
            Context.Set<T>().RemoveRange(list);
        }

        public void SaveChanges()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbEntityValidationException vex)
            {
                throw vex;
            }
        }

        [Obsolete("Usa linq en memoria no en la base de datos")]
        public List<T> GetByConditionsNoSQL<T>(Func<T, bool> conditions, params Expression<Func<T, object>>[] includes) where T : class
        {
            IQueryable<T> query = Context.Set<T>();

            if (includes != null)
                query = includes.Aggregate(query,
                   (current, include) => current.Include(include));

            return query.Where(conditions).ToList();
        }

        public List<T> GetByConditions<T>(Expression<Func<T, bool>> conditions) where T : class
        {
            IQueryable<T> query = Context.Set<T>();
            return ExecuteConditions<T>(query, conditions: conditions);
        }

        public void Save<T>(T entity) where T : class
        {
            Context.Set<T>().Add(entity);
        }

        public List<T> GetByConditions<T>(Func<T, T> columns, Expression<Func<T, bool>> conditions) where T : class
        {
            IQueryable<T> query = Context.Set<T>();
            return ExecuteConditions<T>(query, columns, conditions);
        }

        public List<T> GetByConditions<T>(Func<T, T> columns, Expression<Func<T, bool>> conditions, params Expression<Func<T, object>>[] includes) where T : class
        {
            IQueryable<T> query = Context.Set<T>();
            return ExecuteConditions<T>(query, columns, conditions, includes);
        }


        public List<T> GetByConditions<T>(Expression<Func<T, bool>> conditions, List<string> includes) where T : class
        {
            IQueryable<T> query = Context.Set<T>();

            foreach (string include in includes)
            {
                query = query.Include(include);
            }

            return query.Where(conditions).ToList();
        }

        public List<T> GetByConditions<T>(Expression<Func<T, bool>> conditions, params Expression<Func<T, object>>[] includes) where T : class
        {
            IQueryable<T> query = Context.Set<T>();

            return ExecuteConditions<T>(query, null, conditions, includes);
        }

        private List<T> ExecuteConditions<T>(IQueryable<T> query, Func<T, T> columns = null, Expression<Func<T, bool>> conditions = null, params Expression<Func<T, object>>[] includes) where T : class
        {
            if (conditions != null)
            {
                query = query.Where(conditions);
            }

            if (includes != null)
            {
                query = includes.Aggregate(query,
                   (current, include) => current.Include(include));
            }

            if (columns != null)
            {
                query.Select(columns).ToList();
            }

            return query.ToList();
        }

        public PageResult<T> GetByConditions<T>(Expression<Func<T, bool>> conditions, int page, int size, Expression<Func<T, string>> order) where T : class
        {
            IQueryable<T> query = Context.Set<T>();

            var result = new PageResult<T>
            {
                Count = query.Where(conditions).Count(),
                Result = query.Where(conditions)
                                    .OrderBy(order)
                                    .Skip((page) * size)
                                    .Take(size).ToList()
            };

            return result;
        }


        public PageResult<T> GetByConditions<T, TProperty>(Expression<Func<T, bool>> conditions, int skip, int size, Expression<Func<T, TProperty>> order, bool desc = false) where T : class
        {
            IQueryable<T> query = Context.Set<T>();

            if (desc)
                query = query.OrderByDescending(order);
            else
                query = query.OrderBy(order);

            var result = new PageResult<T>
            {
                Count = query.Where(conditions).Count(),
                Result = query.Where(conditions)                                 
                                    .Skip(skip)
                                    .Take(size).ToList()
            };

            return result;
        }


        public PageResult<T> GetByConditions<T>(Func<T, T> columns, Expression<Func<T, bool>> conditions, int page, int size, Expression<Func<T, string>> order) where T : class
        {
            IQueryable<T> query = Context.Set<T>();

            if (columns != null)
            {
                var result = new PageResult<T>
                {
                    Count = query.Where(conditions).Count(),
                    Result = query.Where(conditions)
                                        .OrderBy(order)
                                        .Skip((page) * size)
                                        .Take(size)
                                        .Select(columns).ToList()
                };

                return result;
            }
            else
            {
                var result = new PageResult<T>
                {
                    Count = query.Where(conditions).Count(),
                    Result = query.Where(conditions)
                                        .OrderBy(order)
                                        .Skip((page) * size)
                                        .Take(size).ToList()
                };

                return result;
            }
        }

        public PageResult<T> GetByConditions<T>(Expression<Func<T, bool>> conditions, List<string> includes, int page, int size, Expression<Func<T, string>> order) where T : class
        {
            IQueryable<T> query = Context.Set<T>();

            foreach (string include in includes)
            {
                query = query.Include(include);
            }

            var result = new PageResult<T>
            {
                Count = query.Where(conditions).Count(),
                Result = query.Where(conditions)
                                    .OrderBy(order)
                                    .Skip((page) * size)
                                    .Take(size).ToList()
            };
            return result;
        }

        public List<T> MapView<T>(string query, object[] parameters = null) where T : class
        {
            Context.Database.CommandTimeout = int.MaxValue;

            if (parameters != null)
            {
                return Context.Database.SqlQuery<T>(query, parameters).ToList();
            }
            else
            {
                return Context.Database.SqlQuery<T>(query).ToList();
            }
        }

        public void ExcecuteQuery(string query, object[] parameters = null)
        {
            if (parameters != null)
            {
                Context.Database.ExecuteSqlCommand(query, parameters);
            }
            else
            {
                Context.Database.ExecuteSqlCommand(query);
            }
        }

        public IQueryable<T> GetQueryable<T>() where T : class
        {
            return Context.Set<T>();
        }

        public object GetContext()
        {
            return Context;
        }

        public bool Any<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return Context.Set<T>().Any(predicate);
        }

        public void Remove<T>(T entity) where T : class
        {
            // verifico si hace falta agregarla la instancia del contexto
            bool isDetached = Context.Entry(entity).State == EntityState.Detached;
            if (isDetached)
                // cargo la entidad en la misma instancia del conxtexto, para luego poder eliminarla.
                Context.Set<T>().Attach(entity);

            Context.Set<T>().Remove(entity);
        }
    }
}