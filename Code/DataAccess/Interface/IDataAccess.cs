﻿using DataAccess.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DataAccess.Interface
{
    public interface IDataAccess
    {
        object GetContext();

        void Remove<T>(T entity) where T : class;

        T GetById<T>(long id) where T : class;

        List<T> GetAll<T>() where T : class;

        void Save<T>(T entity) where T : class;

        void Update<T>(T entity) where T : class;

        void SaveOrUpdate<T>(T entity) where T : class;

        void Delete<T>(T entity) where T : class;

        void DeleteList<T>(IEnumerable<T> list) where T : class;



        void SaveChanges();

        /*Old Method*/
        List<T> GetByConditionsNoSQL<T>(Func<T, bool> conditions, params Expression<Func<T, object>>[] includes) where T : class;

        List<T> GetByConditions<T>(Expression<Func<T, bool>> conditions) where T : class;

        List<T> GetByConditions<T>(Func<T, T> columns, Expression<Func<T, bool>> conditions) where T : class;

        List<T> GetByConditions<T>(Expression<Func<T, bool>> conditions, List<string> includes) where T : class;

        List<T> GetByConditions<T>(Func<T, T> columns, Expression<Func<T, bool>> conditions, params Expression<Func<T, object>>[] includes) where T : class;

        List<T> GetByConditions<T>(Expression<Func<T, bool>> conditions, params Expression<Func<T, object>>[] includes) where T : class;

        PageResult<T> GetByConditions<T>(Expression<Func<T, bool>> conditions, int page, int size, Expression<Func<T, string>> order) where T : class;

        PageResult<T> GetByConditions<T, TProperty>(Expression<Func<T, bool>> conditions, int page, int size, Expression<Func<T, TProperty>> order, bool desc = false) where T : class;

        PageResult<T> GetByConditions<T>(Func<T, T> columns, Expression<Func<T, bool>> conditions, int page, int size, Expression<Func<T, string>> order) where T : class;

        PageResult<T> GetByConditions<T>(Expression<Func<T, bool>> conditions, List<string> includes, int page, int size, Expression<Func<T, string>> order) where T : class;

        bool Any<T>(Expression<Func<T, bool>> predicate) where T : class;

        void SetLazy(bool lazy);

        void UseProxy(bool proxy);

        IQueryable<T> GetQueryable<T>() where T : class;

        //T GetById<T>(long id) where T : class;

        // List<T> GetAll<T>() where T : class;

        // List<T> GetByConditions<T>(Func<T, bool> conditions) where T : class;

        // List<TEntity> GetByConditions<TEntity>(Func<TEntity, TEntity> columns,Func<TEntity, bool> conditions)
        //     where TEntity : class;

        List<T> MapView<T>(string query, object[] parameters = null) where T : class;

        //void Save<T>(T entity) where T : class;

        //void Update<T>(T entity) where T : class;

        //void SaveOrUpdate<T>(T entity) where T : class;

        //void Delete<T>(T entity) where T : class;

        //void DeleteList<T>(IEnumerable<T> list) where T : class;

        void ExcecuteQuery(string query, object[] parameters = null);

        //void SaveChanges();
    }
}