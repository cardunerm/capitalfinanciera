﻿using DataAccess.Interface;

namespace DataAccess
{
    public class DataAccessManager<T>
    {
        public static IDataAccess Instance { get; set; }

        static DataAccessManager()
        {
            Instance = AOPCastle.CreateObject<IDataAccess>(typeof(T).FullName);
        }
    }
}