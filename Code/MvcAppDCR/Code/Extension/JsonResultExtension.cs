﻿using Newtonsoft.Json;

namespace System.Web.Mvc
{
    public static class JsonResultExtension
    {
        public static JsonResult JsonSerialize(this Controller obj, object data, JsonRequestBehavior jsonRequesBehavior)
        {
            var result = new JsonResult
            {
                Data = JsonConvert.SerializeObject(data, Formatting.Indented,
                      new JsonSerializerSettings
                      {
                          ReferenceLoopHandling = ReferenceLoopHandling.Ignore,


                      }),
                JsonRequestBehavior = jsonRequesBehavior
            };
            result.MaxJsonLength = Int32.MaxValue;
            return result;
        }
    }
}