﻿using Business;
using Luma.Utils.Security;
using System.Web.Mvc;
using System.Web.Security;

namespace MvcAppDCR.Code
{
    public class CustomView<T> : WebViewPage<T>
    {
        public CustomMembershipProvider<UsuarioBusiness> CustomMembership
        {
            get
            {
                return (CustomMembershipProvider<UsuarioBusiness>)Membership.Provider;
            }
        }

        public override void Execute()
        {

        }
    }
}