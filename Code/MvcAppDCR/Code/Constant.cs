﻿using System;
using System.Configuration;

namespace MvcAppDCR.Code
{
    public class Constant
    {
        public class Grid
        {
            public static short PageSize { get { return Convert.ToInt16(ConfigurationManager.AppSettings["PageSize"] ?? "15"); } }
            public static short PageSizeLower { get { return Convert.ToInt16(ConfigurationManager.AppSettings["PageSizeLower"] ?? "5"); } }
        }

        public class Ajax
        {
            public const string Post = "Post";
            public const string Get = "Get";
            public const string JsOnBeginAjax = "onBeginAjax();";
            public const string JsOnSuccessAjax = "onSuccessAjax(data, status, xhr);";
            public const string JsOnError = "onErrorAjax();";
        }

        public class Page
        {
            public const string WaitingPopUp = "wpopup";
        }

        public class Bundle
        {
            public const string Script_SyncfusionAll = "~/script/sync/all";
            public const string Script_JQueryAll = "~/script/jquery/all";
            public const string Script_TemplateAll = "~/script/template/all";
            public const string Script_Site = "~/script/site/all";
            public const string Script_JQuery = "~/script/jquery";
            public const string Script_Bootstrap = "~/script/bootstrap";
            public const string Script_Globalize = "~/script/jquery/globalize";
            public const string Script_Banco = "~/script/site/banco";
            public const string Script_Operacion = "~/script/site/operacion";
            public const string Script_UploadFile = "~/script/site/adjuntos";
            public const string Script_OperacionInversor = "~/script/site/operacioninversor";
            public const string Script_OperacionAprobacion = "~/script/site/operacionAprobacion";
            public const string Script_OperacionBandeja = "~/script/site/operacionBandeja";
            public const string Script_OperacionBandejaInversor = "~/script/site/operacioninversorBandeja";
            public const string Script_OperacionBandejaAprobacion = "~/script/site/aprobacionBandeja";
            public const string Script_BandejaOperacion=  "~/script/site/bandejaOperacion";
            public const string Script_Autocomplete = "~/script/site/autocomplete";
            public const string Script_DateTimeFormat = "~/script/jquery/datetimeformat";
            public const string Script_Caja = "~/script/site/caja";
            public const string Script_Inversor = "~/script/site/inversores";
            public const string Script_DepositoEfectivo = "~/script/site/depositoEfectivo";
            public const string Script_ExtraccionFondos = "~/script/site/extraccionFondos";
            public const string Script_DepositoCheque = "~/script/site/depositoCheque";
            public const string Script_FacturaOperacion = "~/script/site/FacturaOperacion";


            public const string CSS_Site = "~/Content/site";
            public const string CSS_BootstrapAll = "~/Content/bootstrap/all";
            public const string CSS_JQuery = "~/Content/jquery";
            public const string CSS_Template = "~/Content/template/";
            public const string CSS_SyncfusionAll = "~/Content/sync";
            public const string CSS_Operaciones = "~/Content/operaciones/";
        }

        public class Layout
        {
            public static string Master { get { return ConfigurationManager.AppSettings["MasterLayout"]; } }
        }

        public class Sections
        {
            public const string Header = "HeadSection";
            public const string Script = "ScriptSection";
            public const string Filter = "Filter";
        }

        public class Partial
        {
            public const string IDStandarTemplateCommand = "standarTemplateCommand";
        }

        public class SuccessMessages
        {
            public const string MESSAGE_OK = "1";
            public const string MESSAGE_ERROR = "0";
            public const string MESSAGE_PARTIAL = "2";
            public const string ERROR_VAR = "error_model";
            public const string ERROR_VAR_MODAL = "error_model_modal";
        }
    }
}