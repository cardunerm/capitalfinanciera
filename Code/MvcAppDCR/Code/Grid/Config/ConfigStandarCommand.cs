﻿
namespace MvcAppDCR.Code.Grid.Config
{
    public class ConfigStandarCommand
    {
        public bool ShowEdit { get; set; }
        public bool ShowDelete { get; set; }
        public string CustomeTemplate { get; set; }

        /// <summary>
        /// Format "/URL/{{:columnId-table}}"
        /// </summary>
        public string UrlEdit { get; set; }

        /// <summary>
        /// Format "/URL/Delete/{{:columnId-table}}"
        /// </summary>
        public string UrlDelete { get; set; }

        public string ActionDelete { get; set; }

        public string ActionEstado { get; set; }

        public string ControllerDelete { get; set; }

        public string ControllerEstado { get; set; }

        public string IdDelete { get; set; }

        public string Grid { get; set; }

        public string ButtonFilter { get; set; }

        public string IdEstadoOperacion { get; set; }


        public bool DescargarContrato { get; set; }

        
    }
}