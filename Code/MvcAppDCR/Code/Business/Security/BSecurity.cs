﻿using Enumerations;
using Business;
using CurrentContext;
using Domine;
using MvcAppDCR.Model;
using MvcAppDCR.Model.Security;
using Luma.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MvcAppDCR.Code.Business.Security
{
    public class BSecurity
    {
        internal UsuarioModel GetUsuarioModel(long? id)
        {
            var usuario = (id == null || id == 0) ? new Usuario() : UsuarioBusiness.Instance.GetEntity(id.Value);
            var UsuarioPerfils = new List<PerfilModel>();

            PerfilBusiness.Instance.GetAllActives(pr => new Perfil { IdPerfil = pr.IdPerfil, Nombre = pr.Nombre })
                .ForEach(p => UsuarioPerfils.Add(new PerfilModel
                {
                    IdPerfil = p.IdPerfil,
                    Nombre = p.Nombre,
                    Selected = usuario.UsuarioPerfil.Any(up => up.IdPerfil == p.IdPerfil &&
                                                            up.Estado == (int)EntityStatus.Active)
                })
            );


            return new UsuarioModel
            {
                PerfilMod = UsuarioPerfils,
                UsuarioMod = usuario
            };
        }

        internal void SaveOrUpdate(UsuarioModel usuarioModel, List<Perfil> selectedPerfiles, bool changecontrasenia = false)
        {
            Usuario entity;
            //IEnumerable<UsuarioPerfils> oldPerfiles = null;

            if (usuarioModel.UsuarioMod.IdUsuario > 0)
            {
                entity = UsuarioBusiness.Instance.GetEntity(usuarioModel.UsuarioMod.IdUsuario);

                if (changecontrasenia)
                {
                    entity.Contrasenia = usuarioModel.UsuarioMod.Contrasenia;
                }

                entity.FechaEdicion = DateTime.Now;

            }
            else
            {
                entity = new Usuario
                {
                    Contrasenia = UtilHelper.GetMD5String(usuarioModel.UsuarioMod.Contrasenia),
                    FechaCreacion = DateTime.Now,
                    Estado = (int)EntityStatus.Active,
                    IdEmpresa = usuarioModel.UsuarioMod.IdEmpresa
                };
            }

            entity.Nombre = usuarioModel.UsuarioMod.Nombre;
            entity.Apellido = usuarioModel.UsuarioMod.Apellido;
            entity.NombreUsuario = usuarioModel.UsuarioMod.NombreUsuario;
            entity.Email = usuarioModel.UsuarioMod.Email;

            if (selectedPerfiles != null)
            {
                if (entity.IdUsuario > 0)
                {
                    UsuarioBusiness.Instance.DeletePerfilesUsuarios(entity.IdUsuario);
                }

                foreach (var item in selectedPerfiles)
                {
                    entity.UsuarioPerfil.Add(new UsuarioPerfil
                    {
                        IdPerfil = item.IdPerfil,
                        FechaCreacion = DateTime.Now,
                        Estado = (int)EntityStatus.Active
                    });
                }
            }

            UsuarioBusiness.Instance.SaveOrUpdateUsuario(entity, changecontrasenia);

            if (entity.IdUsuario == SessionContext.UsuarioActual.IdUsuario)
            {
                SessionContext.UsuarioActual.UsuarioPerfil = entity.UsuarioPerfil;
            }

            AuditoriaBusiness.Instance.AuditoriaUsuario(usuarioModel.UsuarioMod.IdUsuario > 0 ? AuditAction.Edit : AuditAction.Create, entity);
            //AuditoriaBusiness.Instance.AuditoriaUsuario(usuarioModel.UsuarioMod.IdUsuario > 0 ? AuditAction.Edit : AuditAction.Create, entity, oldPerfiles);
        }

        internal List<SucursalModel> GetSucursalOperacionModels(Usuario usuario)
        {
            var sucursalModels = SucursalBusiness.Instance.GetSucursalByUsuarioEmpresas(usuario).Select(p => new SucursalModel
            {
                IdSucursal = p.IdSucursal,
                EmpresaNombre = p.Empresa.Nombre,
                Nombre = p.Descripcion,
                Selected = usuario.SucursalUsuario.Any(pm => pm.IdSucursal == p.IdSucursal)
            });

            return sucursalModels.ToList();
        }

        internal List<PerfilModel> GetPerfilModel(long? idusuario)
        {
            Usuario usuario = idusuario == null ? null : UsuarioBusiness.Instance.GetEntity(Convert.ToInt64(idusuario));

            var perfilModels = PerfilBusiness.Instance.GetPerfilByUsuario().Select(p => new PerfilModel
            {
                IdPerfil = p.IdPerfil,
                Nombre = p.Nombre,
                Selected = usuario == null ? false : usuario.UsuarioPerfil.Any(pm => pm.IdPerfil == p.IdPerfil)
            });

            return perfilModels.ToList();
        }

        internal List<ModuloModel> GetModuloModels(Perfil perfil)
        {
            var moduloModels = ModuloBusiness.Instance.GetByConditions(m => m.IdModulo != 100 &&  m.IdModulo != 200 && m.IdModulo != 300 && m.IdModulo != 400 && m.IdModulo != 500 && m.IdModulo != 600).Select(p => new ModuloModel
            {
                IdModulo = p.IdModulo,
                Nombre = p.Nombre,
                Selected = perfil.PerfilModulo.Any(pm => pm.IdModulo == p.IdModulo &&
                                                            pm.Estado == (int)EntityStatus.Active),
                ViewSelected = perfil.PerfilModulo.Any(pm => pm.IdModulo == p.IdModulo &&
                                                            pm.Estado == (int)EntityStatus.Active &&
                                                            pm.Permiso == (int)Permission.View),
                CreateSelected = perfil.PerfilModulo.Any(pm => pm.IdModulo == p.IdModulo &&
                                                            pm.Estado == (int)EntityStatus.Active &&
                                                            pm.Permiso == (int)Permission.Create),
                EditSelected = perfil.PerfilModulo.Any(pm => pm.IdModulo == p.IdModulo &&
                                                            pm.Estado == (int)EntityStatus.Active &&
                                                            pm.Permiso == (int)Permission.Edit),
                DeleteSelected = perfil.PerfilModulo.Any(pm => pm.IdModulo == p.IdModulo &&
                                                            pm.Estado == (int)EntityStatus.Active &&
                                                            pm.Permiso == (int)Permission.Delete)
            });

            return moduloModels.ToList();
        }

        internal List<ValidacionModel> GetValidacionModels(Perfil perfil)
        {
            var validacionModels = DCREnumerationsHelper.ObtenerValidaciones().Select(p => new ValidacionModel
            {
                Validacion = int.Parse(p.Value),
                Nombre = p.Text,
                Selected = perfil.PerfilValidacion.Any(pv => pv.Validacion == int.Parse(p.Value) &&
                                                             pv.Estado == (int)EntityStatus.Active)
            });

            return validacionModels.ToList();
        }
    }
}