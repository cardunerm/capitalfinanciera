﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MvcAppDCR.SignalRhubs.Startup))]
namespace MvcAppDCR.SignalRhubs
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }

    public class NotificationHub : Hub
    {

        public void NotificationSend(string connection, string msg)
        {
            //Clients.User(connection).NotificationSend("",msg);
        }

        public void GroupSend(string msg)
        {
            //Clients.Group("Aprobadores").
        }

        public static void SendToEvaluador(string user, string msg)
        {
            var ctx = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            ctx.Clients.Group(user).NotificationSend(msg);
        }

        public static void SendToGroup(string msg)
        {
            var ctx = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            ctx.Clients.Group("Aprobadores").NotificationSend(msg);
        }

        public void joinAprobadores(string connectionid)
        {
            Groups.Add(connectionid, "Aprobadores");
        }

        public void leaveAprobadores(string connectionid)
        {
            Groups.Remove(connectionid, "Aprobadores");
        }

        public void joinOperador(string connectionid, string id)
        {
            Groups.Add(connectionid, id);
        }

        public void leaveOperador(string connectionid, string id)
        {
            Groups.Remove(connectionid, id);
        }

    }
}
