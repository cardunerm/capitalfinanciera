﻿var tmpclient = "";



function autocompleteInversorSelectEvent(args) {
    $("#IdInversor").val(args.key);
    tmpclient = args.value;

    getCuentasCorrientes(args.key);
    
}

function autocompleteClientLostFocustEvent(args) {
    if (args.value !== tmpclient) {
        $("#IdInversor").val("");
        $("#acInversor").val("");
        tmpclient = "";
    }
}

function getCuentasCorrientes(idInversor, dontUseOverlay) {
    var url = '/DepositoEfectivo/GetCuentasCorrientesByIdInversor';

    if (dontUseOverlay === undefined || dontUseOverlay === null) {
        showOverlay();
    }
    
    $.ajax({
        type: "GET",
        url: url,
        data: { idInversor: idInversor },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }
            
            if (result.success) {
                var dropdownCuentas = $('#IdCuentaCorrienteInversor');
                dropdownCuentas.empty();

                $.each(result.data, function (i, cuenta) {
                    $('<option>', {
                        value: cuenta.Id
                    }).html(cuenta.NumeroTipo).appendTo(dropdownCuentas);
                });
            } else {
                popupGenerico("error", "Error", result.msg);
            }            
        },
        error: function () {
            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }
            popupGenerico("error", "Error", "Error al obtener las cuentas");
        }
    });
}

var onBegin = function () {
    showOverlay();
}

var onSuccess = function (data) {
    if (data.success) {
        //$.ajax({
        //    type: "GET",
        //    url: '/DepositoEfectivo/GenerarDeposito',
        //    data: { dtoReporteDeposito: data.data },
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "action",
        //})
        generarReporte(data.data);
        hideOverlay();
        popupGenerico("info", "Información", data.msg);
        resetForm();
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
    
}

function generarReporte(data, dontUseOverlay) {
    
    var url = encodeURI($('#depositoEfectivoInfo').data('report-url') + '?paramDtoReporteDeposito=' + JSON.stringify(data));
    ShowReportModal(url, $('#depositoEfectivoInfo').data('nombre-reporte'));

}

var onError = function () {
    hideOverlay();
    popupGenerico("error", "Error", "Error al realizar la operación");
}

function resetForm() {
    $("#IdInversor").val("");
    $("#acInversor").val("");
    tmpclient = "";
    $('#IdCuentaCorrienteInversor').empty();
    $("#Monto").val("");
    $("#Observacion").val("");
}

function createInversor() {
    openPopUp();
}

function openPopUp() {
    modalDisplay("#modal-inversor-edit", false);
}

function cancelPopUp() {
    closePopUp();
    clearFormClient();
}

function closePopUp() {
    modalDisplay("#modal-inversor-edit", true);
}

function clearFormClient() {
    $("#form-cliente input[type=text]").each(function () { $(this).val('') });
    $.validator.unobtrusive.parse($("#form-cliente"))
    $("#form-cliente").validate().resetForm();
}

function successPopUp(args) {
    if (args.status !== undefined && args.status === "0") {
        hideOverlay();
        popupGenerico("error", "Error", args.msg);
        return;
    }

    popupGenerico("info", "Información", args.msg);

    $("#IdInversor").val(args.id);
    $("#acInversor").val(args.name);

    getCuentasCorrientes(args.id, true);

    hideOverlay();
    closePopUp();
}


