﻿
var searchUrl = '';

$(function () {
    searchUrl = $('#cuentaCorrienteinfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?cliente=' + $('#txtNombre').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#cuentaCorrienteGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#cuentaCorrienteGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});

function imprimirResumen() {

    var fechaDesde = $('#txtFechaDesde').val()
    var fechaHasta = $('#txtFechaHasta').val()    

    var url = $('#cuentaCorrienteinfo').data('report-url') + '?fechaDesde=' + $('#txtFechaDesde').val() + '&fechaHasta=' + $('#txtFechaHasta').val() + '&idCuentaCorriente=' + $('#hiddenIdCtaCte').val();

    ShowReportModal(url, $('#cuentaCorrienteinfo').data('nombre-reporte'));
}

function abrirReporte(idCuentaCorriente) {
    $('#hiddenIdCtaCte').val(idCuentaCorriente);
    modalDisplay("#modal-reporte", false);
}

function closePopUp() {
    modalDisplay("#modal-reporte", true);
}

$(document).ready
    (
        function () {
            var date = $('#txtFechaDesde');
            var date2 = $('#txtFechaHasta');

            date.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
                date.datepicker('hide');
            });

            date2.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
                    date.datepicker('hide');
            });
        }
    );