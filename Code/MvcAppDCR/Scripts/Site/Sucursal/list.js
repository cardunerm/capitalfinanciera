﻿var searchUrl = '';

$(function () {
    searchUrl = $('#sucursalInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?numero=' + $('#txtNumero').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#sucursalGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#sucursalGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});