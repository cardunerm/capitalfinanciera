﻿
var searchUrl = '';
ej.widget.init();
$(function () {
    searchUrl = $('#chequeRechazadoinfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?numero=' + $('#txtNumero').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#cheques").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });
        $("#cheques").ejGrid('dataSource', dataManager);
        CleanContainerFields();
    });
});

function createChequeRechazado() {
    var json = [];
    var obj, value;

    $("input:radio:checked").each(function () {
        obj = {
            IdCheque: this.id
        };

        json.push(obj);
    });

    $('#cheques').val(JSON.stringify(json));
}

function onSuccessSendChequeRechazado(data) {
    if (data.success) {
        document.location.href = data.url;
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
}