﻿
var searchUrl = '';

$(function () {

    Date.prototype.withoutTime = function () {
        var d = new Date(this);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    searchUrl = $('#proyeccionFinancieraInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?idInversor=' + $('#IdInversor').val() +
                     '&idCuenta=' + $('#IdCuentaCorrienteInversor').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#ProyeccionFinancieraGrid").ejGrid('dataSource', dataManager);
    });
   

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#ProyeccionFinancieraGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });

    $("#IdCuentaCorrienteInversor").change(function () {
        $("#ProyeccionFinancieraGrid").ejGrid("option", { dataSource: [] });
        $("#ProyeccionFinancieraGrid").ejGrid("instance").refreshContent();
    });


    //$('#btnSearchHome').click(function () {
    //    debugger;
    //    var params = '?idCuenta=' + $('#ddlComponeAElemento').val() +
    //        '&';

    //    var dataManager = ej.DataManager({
    //        url: searchUrl + params
    //    });

    //    showOverlay();
    //    $("#ProyeccionFinancieraGrid").ejGrid('dataSource', dataManager);
    //    hideOverlay();
    //});


    //$('#btnRestoreHome').click(function () {
    //    var dataManager = ej.DataManager({
    //        url: searchUrl
    //    });

    //    $("#ProyeccionFinancieraGrid").ejGrid('dataSource', dataManager);

    //    CleanContainerFields();
    //});

});


var tmpclient = "";

function autocompleteInversorSelectEvent(args) {
    $("#IdInversor").val(args.key);
    tmpclient = args.value;

    getCuentasCorrientes(args.key);

}

function autocompleteClientLostFocustEvent(args) {
    if (args.value !== tmpclient) {
        $("#IdInversor").val("");
        $("#acInversor").val("");
        tmpclient = "";
    }
}

function getCuentasCorrientes(idInversor, dontUseOverlay) {
    var url = '/DepositoEfectivo/GetCuentasCorrientesByIdInversor';

    if (dontUseOverlay === undefined || dontUseOverlay === null) {
        showOverlay();
    }

    $.ajax({
        type: "GET",
        url: url,
        data: { idInversor: idInversor },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }

            if (result.success) {
                var dropdownCuentas = $('#IdCuentaCorrienteInversor');
                dropdownCuentas.empty();

                $.each(result.data, function (i, cuenta) {
                    $('<option>', {
                        value: cuenta.Id
                    }).html(cuenta.NumeroTipo).appendTo(dropdownCuentas);
                });
            } else {
                popupGenerico("error", "Error", result.msg);
            }
            $("#ProyeccionFinancieraGrid").ejGrid("option", { dataSource: [] });
            $("#ProyeccionFinancieraGrid").ejGrid("instance").refreshContent();
        },
        error: function () {
            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }
            popupGenerico("error", "Error", "Error al obtener las cuentas");
            $("#ProyeccionFinancieraGrid").ejGrid("option", { dataSource: [] });
            $("#ProyeccionFinancieraGrid").ejGrid("instance").refreshContent();
        }
    });
}

function querycellinfo(args) {

    var today = new Date();

    if (args.column.field == "Fecha" && (args.data.Fecha.withoutTime().toString() == today.withoutTime().toString()))
    {
        $($(args.cell).parent()).css("backgroundColor", "#428bca");
        $($(args.cell).parent()).css("color", "#ffffff");
    }
        

}


function BuscarDetalle() {

    searchUrl = $('#proyeccionFinancieraInfo').data('search-url');   
    var params = '?idCuenta=' + $('#ddlComponeAElemento').val() +
        '&';

    var dataManager = ej.DataManager({
        url: searchUrl + params
    });

    showOverlay();
    $("#ProyeccionFinancieraGrid").ejGrid('dataSource', dataManager);
    hideOverlay();

}

function DescargarContrato(Id) {
   
    var url = searchUrl = $('#proyeccionFinancieraInfo').data('print-url'); 
    url = url + "?idInversor= " + Id;
    ShowReportModal(url, "Contrato");
}

