﻿var searchUrl = '';

$(function () {

    Date.prototype.withoutTime = function () {
        var d = new Date(this);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    searchUrl = $('#proyeccionFinancieraInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?idInversor=' + $('#IdInversor').val() +
            '&idCuenta=' + $('#IdCuentaCorrienteInversor').val() +
            '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#ProyeccionFinancieraGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#ProyeccionFinancieraGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });

    $('#btnMoreInfo').click(function () {
        var url = '/ProyeccionFinancieraBBB/GetCheques'
        var obj = $("#ChequesEnCartera").data("ejGrid");
        var idCuentaSeleccionada = $('#IdCuentaCorrienteInversor').val();
        $.ajax({
            url: url,
            type: "get",
            data: { idCuenta: idCuentaSeleccionada },
        }).done(function (data) {
            obj.dataSource(data.cheques);
            $("#montoMaxRetiro").text(data.montoMaxRetiro);
            $("#montoOperativo").text(data.montoOperativo);
            $("#montoGarantia").text(data.montoGarantia);
        }).fail(function () {
            popupGenerico("error", "Error", "Error al obtener los cheques");
        });

    });



    $("#IdCuentaCorrienteInversor").change(function () {
        $("#ProyeccionFinancieraGrid").ejGrid("option", { dataSource: [] });
        $("#ProyeccionFinancieraGrid").ejGrid("instance").refreshContent();
    });
});


var tmpclient = "";

function autocompleteInversorSelectEvent(args) {
    $("#IdInversor").val(args.key);
    tmpclient = args.value;

    getCuentasCorrientes(args.key);

}

function autocompleteClientLostFocustEvent(args) {
    if (args.value !== tmpclient) {
        $("#IdInversor").val("");
        $("#acInversor").val("");
        tmpclient = "";
    }
}

function getCuentasCorrientes(idInversor, dontUseOverlay) {
    var url = '/DepositoEfectivo/GetCuentasCorrientesByIdInversor';

    if (dontUseOverlay === undefined || dontUseOverlay === null) {
        showOverlay();
    }

    $.ajax({
        type: "GET",
        url: url,
        data: { idInversor: idInversor },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }

            if (result.success) {
                var dropdownCuentas = $('#IdCuentaCorrienteInversor');
                dropdownCuentas.empty();

                $.each(result.data, function (i, cuenta) {
                    $('<option>', {
                        value: cuenta.Id
                    }).html(cuenta.NumeroTipo).appendTo(dropdownCuentas);
                });
            } else {
                popupGenerico("error", "Error", result.msg);
            }
            $("#ProyeccionFinancieraGrid").ejGrid("option", { dataSource: [] });
            $("#ProyeccionFinancieraGrid").ejGrid("instance").refreshContent();
        },
        error: function () {
            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }
            popupGenerico("error", "Error", "Error al obtener las cuentas");
            $("#ProyeccionFinancieraGrid").ejGrid("option", { dataSource: [] });
            $("#ProyeccionFinancieraGrid").ejGrid("instance").refreshContent();
        }
    });
}

function querycellinfo(args) {

    var today = new Date();

    if (args.column.field == "Fecha" && (args.data.Fecha.withoutTime().toString() == today.withoutTime().toString())) {
        $($(args.cell).parent()).css("backgroundColor", "#428bca");
        $($(args.cell).parent()).css("color", "#ffffff");
    }
}

function CalcularTotal() {

    if ($('#IdCuentaCorrienteInversor').val() == "") {
        alert("Debe seleccionar un Inversor");
        return;
    }

    var url = '/ProyeccionFinancieraBBB/CalcularTotal'
    var idCuentaSeleccionada = $('#IdCuentaCorrienteInversor').val();
    $.ajax({
        url: url,
        type: "get",
        data: { idCuenta: idCuentaSeleccionada }
    }).done(function (data) {
        $("#montochequeOperacion").text(data.montochequeOperacion);
        $("#montoporcentajeGarantia").text(data.montochequeGarantia);
        $("#montototalRetirar").text(data.montototalRetirar);
        $('#modalCalcular').modal({ show: 'true' });
    }).fail(function () {
        popupGenerico("error", "Error", "Error al obtener los cheques");
    });

}

function AceptarOperacion() {


    var url = '/ProyeccionFinancieraBBB/ExtraerFondo'
    var idCuentaSeleccionada = $('#IdCuentaCorrienteInversor').val();
    var monto = $('#montototalRetirar').text();
    $.ajax({
        url: url,
        type: "get",
        data: { idCuenta: idCuentaSeleccionada, monto: monto },
    }).done(function (data) {
        debugger;
        $('#modalCalcular').modal('toggle');
        generarReporte(data.data);
    }).fail(function () {
        popupGenerico("error", "Error", "Error al obtener los cheques");
    });
}
function generarReporte(data) {
    debugger;
    var ur = $('#proyeccionFinancieraInfo').data('extraccion-url');
    var url = encodeURI($('#proyeccionFinancieraInfo').data('extraccion-url') + '?paramDtoReporteDeposito=' + JSON.stringify(data));
    ShowReportModal(url, 'Egreso de Operación');
}
function CancelarOperacion() {
    $('#modalCalcular').modal('toggle');
}
