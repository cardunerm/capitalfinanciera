﻿var tmpclient = "";
var ctasctes = null;

function autocompleteInversorSelectEvent(args) {
    $("#IdInversor").val(args.key);
    tmpclient = args.value;
    getCuentasCorrientes(args.key);
    
}

function autocompleteClientLostFocustEvent(args) {
    if (args.value !== tmpclient) {
        $("#IdInversor").val("");
        $("#acInversor").val("");
        tmpclient = "";
    }
}

$(function () {
    $('#IdCuentaCorrienteInversor').change(function () {
        if ((ctasctes[0]).ClasificacionInversor == 2) {
            SetLimiteDescubierto();
        }
    });
});

function getCuentasCorrientes(idInversor, dontUseOverlay) {
    var url = '/ExtraccionFondos/GetCuentasCorrientesByIdInversor';

    if (dontUseOverlay === undefined || dontUseOverlay === null) {
        showOverlay();
    }
    
    $.ajax({
        type: "GET",
        url: url,
        data: { idInversor: idInversor },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }
            
            if (result.success) {

                var dropdownCuentas = $('#IdCuentaCorrienteInversor');
                dropdownCuentas.empty();
                $.each(result.data, function (i, cuenta) {
                    $('<option>', {
                        value: cuenta.Id
                    }).html(cuenta.NumeroTipo).appendTo(dropdownCuentas);
                });

                ctasctes = result.data;

                if ((result.data[0]).ClasificacionInversor == 2) 
                {
                    SetLimiteDescubierto();
                }

            } else {
                popupGenerico("error", "Error", result.msg);
            }            
        },
        error: function () {
            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }
            popupGenerico("error", "Error", "Error al obtener las cuentas");
        }
    });
}

var SetLimiteDescubierto = function () {
    var idCuenta = $('#IdCuentaCorrienteInversor').val();
    var cuentaSeleccionada = SearchCtaCte(ctasctes, idCuenta);
    var limiteDescubierto = cuentaSeleccionada.LimiteDescubierto;
    //$('#Monto').prop('max', limiteDescubierto);
    //$('#Monto').val(limiteDescubierto);
}

var SearchCtaCte = function (cuentas, id) {
    var i = null;
    for (i = 0; cuentas.length > i; ++i) {
        if (cuentas[i].Id == id) {
            return cuentas[i];
        }
    }
    return null;
};

var onBegin = function () {
    showOverlay();
}

var onSuccess = function (data) {
    if (data.success) {
        generarReporte(data.data);
        hideOverlay();
        popupGenerico("info", "Información", data.msg);
        resetForm();
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
    
}

function generarReporte(data) {
    var url = encodeURI($('#extraccionFondosInfo').data('report-url') + '?paramDtoReporteDeposito=' + JSON.stringify(data));
    ShowReportModal(url, $('#extraccionFondosInfo').data('nombre-reporte'));
}

var onError = function () {
    hideOverlay();
    popupGenerico("error", "Error", "Error al realizar la operación");
}

function resetForm() {
    $("#IdInversor").val("");
    $("#acInversor").val("");
    tmpclient = "";
    $('#IdCuentaCorrienteInversor').empty();
    $("#Monto").val("");
    $("#Observacion").val("");
}


