﻿$(function () {
    $(':checkbox[data-selected=true]').each(function () { this.checked = true; });

    $('.e-headertemplate').html('<div class="e-headercelldiv" style="text-align: center;"><input type="checkbox" id="chkAll" onchange="CheckAllModules();" /></div>');
});

function CheckAllModules() {
    $('.row-header').attr('checked', true);
}

function onSuccessSendAssignSucursal(data) {
    if (data.success) {
        window.location.href = '/Usuario/Index';
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
}

function addSucursal() {
    var json = [];
    var obj;

    $(':checkbox:checked').each(function () {
        obj = {
            IdSucursal: this.id
        };

        json.push(obj);
    });

    $('#sucursales').val(JSON.stringify(json));
}