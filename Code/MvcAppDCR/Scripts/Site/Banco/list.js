﻿var searchUrl = '';

$(function () {
    searchUrl = $('#bancoInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?nombre=' + $('#txtNombre').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#bancoGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#bancoGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});