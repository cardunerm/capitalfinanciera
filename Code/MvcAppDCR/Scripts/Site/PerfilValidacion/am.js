﻿$(function () {
    $(':checkbox[data-selected=true]').each(function () { this.checked = true; });

    $('.e-headertemplate').html('<div class="e-headercelldiv" style="text-align: center;"><input type="checkbox" id="chkAll" onchange="CheckAllModules();" /></div>');
});

function CheckAllModules() {
    $('.row-header').attr('checked', true);
}

function addValidacion() {
    var json = [];

    $(':checkbox:checked[validacion=true]').each(function () {
        json.push(parseInt(this.id));
    });

    $('#validaciones').val(JSON.stringify(json));
}