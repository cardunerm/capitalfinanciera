﻿function ShowReportModal(urlReport, title) {
    $('#divIframeReportModal').html('');

    title = (title ? title : '');

    var divDownloadFileFrame = '<iframe id="iframeReportModal" src="' + urlReport + '" height="100%" width="100%"></iframe>';

    $('#divIframeReportModal').html(divDownloadFileFrame);

    $('#reportModal h4.modal-title').html(title);

    $('#reportModal').modal({ show: true });
}

function DownloadExcelReport(urlReport) {
    var divDownloadFileFrame = '<iframe src="' + urlReport + '"></iframe>';

    $('#divDownloadFileFrame').html(divDownloadFileFrame);
}