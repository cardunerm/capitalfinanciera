﻿
var searchUrl = '';

function createMovimiento(idCaja, tipoMovimiento) {
    if (tipoMovimiento == "1") {
        openPopUpIngreso();
    } else {
        openPopUpEgreso();
    }
   
    $('#txtIdCaja').val(idCaja)
}

function createMovimientoCliente(idCaja) {
    popUpCliente();
    $('#txtIdCaja').val(idCaja)
}

function popUpCliente(show) {
    modalDisplay("#modal-cliente", false);
}

function cerrarCaja(idCaja) {
    $('#hiddenCaja').val(idCaja);
    openPopUpCierreCaja();
}

function handleError(args) {
    var response = args.get_response();
    var statusCode = response.get_statusCode();
    alert("Sorry, the request failed with status code " + statusCode);
}

function abrirCaja() {
    modalDisplay("#modal-abrir-caja", false);
} 

function successPopUp(args) {
    if (args.status == 0) {
        popupError("Error al crear movimiento",args.msg);
        hideOverlay();
    }
    hideOverlay();
    closePopUpIngreso();
    closePopUpEgreso();
    closePopUpCierreCaja();
    $('#btnRestore').click();
}

function successPopUpAbrirCaja(args) {
    hideOverlay();
    closePopUpAbrirCaja();
    window.location.reload();
}

function openPopUpCierreCaja() {
    modalDisplay("#modal-cerrar-caja", false);
}

function openPopUpEgreso() {
    modalDisplay("#modal-egreso-create", false);
}

function openPopUpIngreso() {
    modalDisplay("#modal-ingreso-create", false);
}

function closePopUpIngreso() {
    modalDisplay("#modal-ingreso-create", true);
}

function closePopUpEgreso() {
    modalDisplay("#modal-egreso-create", true);
}

function closePopUpCierreCaja() {
    modalDisplay("#modal-cerrar-caja", true);
}

function closePopUpAbrirCaja() {
    modalDisplay("#modal-abrir-caja", true);
}
function cancelPopUp() {
    closePopUpIngreso();
    closePopUpEgreso();
}

function clearFormClient() {
    $("#form-operacionCaja input[type=text]").each(function () { $(this).val('') });
    $.validator.unobtrusive.parse($("#form-operacionCaja"))
    $("#form-operacionCaja").validate().resetForm();
}

$(function () {
    searchUrl = $('#cajainfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?fecha=' + $('#txtFechaAperturaFiltro').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#cajas").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });
        $("#cajas").ejGrid('dataSource', dataManager);
        //CleanContainerFields();
    });
});

$(document).ready
    (
        function () {
            var date = $('#txtFechaAperturaFiltro');
            date.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
                date.datepicker('hide');
            });           
        }
    );

