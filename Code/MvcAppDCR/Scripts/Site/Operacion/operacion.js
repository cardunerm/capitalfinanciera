﻿var tmpclient = "";

function autocompleteClientSelectEvent(args) {

    $("#IdCliente").val(args.key);   
    $("#DetalleOperacion_IdCliente").val(args.key);   
    tmpclient = args.value;
    getResumenCuentaCorriente(args.key);
}
function changeColor(id, value) {
    if (value < 0) {
        $(id).css("color", "red");
    }
    else
        $(id).css("color", "black");
}
function getResumenCuentaCorriente(key) {
    showOverlay();
    var securityToken = $('[name=__RequestVerificationToken]').val();
    $.ajax({
        url: '/Operacion/GetResumenCuenta',  //Server script to process data
        type: 'POST',
        data: { key: key, __RequestVerificationToken: securityToken },
    }).done(function (data) {
        if (data.success) {
            $("#label-saldo").text("$" + data.datos.Saldo);
            changeColor("#label-saldo", parseFloat(data.datos.Saldo));

            $("#label-cheques-rechazados").text("$" + data.datos.MontoRechazos);
            changeColor("#label-cheques-rechazados", parseFloat(data.datos.MontoRechazos));

            $("#label-cheques-cantidad-rechazados").text(data.datos.CantidadRechazos);
            $("#label-tasa").text(data.tasa + "%");
            reloadGrid();
            hideOverlay();
        }
        else {
            onErrorAjax();
        }
    }).error(function () {
        onErrorAjax();
    });
}

function autocompleteClientLostFocustEvent(args) {
    if (args.value !== tmpclient) {
        $("#IdCliente").val("");
        $("#acCliente").val("");
        tmpclient = "";
    }
}
function createClient() {
    openPopUp();
}

function cancelPopUp() {
    closePopUp();
    clearFormClient();
}

function openPopUp() {
    modalDisplay("#modal-client-edit", false);
}

function closePopUp() {
    modalDisplay("#modal-client-edit", true);
}

function openPopUpImages() {
    modalDisplay("#modal-expand-image", false);
}

function closePopUpImages() {
    modalDisplay("#modal-expand-image", true);
}


function successPopUp(args) {
    if (args.status !== undefined && args.status === "0") {
        hideOverlay();
        popupGenerico("error", "Error", args.msg);
        return;
    }


    $("#IdCliente").val(args.id);
    $("#acCliente").val(args.name);
    hideOverlay();
    closePopUp();
}

function clearFormClient() {
    $("#form-cliente input[type=text]").each(function () { $(this).val('') });
    $.validator.unobtrusive.parse($("#form-cliente"))
    $("#form-cliente").validate().resetForm();
}

var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

$(document).ready
    (
    function () {
        $(function () {
            var date = $('#DetalleOperacion_FechaVencimiento');
            date.datepicker({
                format: "dd/mm/yyyy", onRender: function (date) {
                    //return date.valueOf() < now.valueOf() ? 'disabled' : '';
                    return '';
                }
            }).on('changeDate', function (ev) {
                date.datepicker('hide');
            }).on('blur', function (ev) {
                try {
                    if (moment(date.val(), "DD/MM/YYYY").toDate() < now) {
                        date.val('');
                    }
                }
                catch (e) {
                    date.val('');
                }
            });

            $("#file").on('change', function () {
                if ($("#file").val() !== "") {
                    if ($.fn.validateExtension($("#file").val()))
                        sendFrontImage();
                    else
                        $("#file").val('');
                }
            });

            $("#fileBack").on('change', function () {
                if ($("#fileBack").val() !== "") {
                    if ($.fn.validateExtension($("#fileBack").val()))
                        sendBackImage();
                    else
                        $("#fileBack").val('');
                }
            });

        });
        $("#DetalleOperacion_Banco").val($("#DetalleOperacion_IdBanco :selected").text());

        $("#IdEstadoFiltro").val("");
    }

    );

function addOperacionToGrid(data) {
    if (data.success) {
        reloadGrid();
        clearFormCheque();
        hideOverlay();
    }
    else {
        popupError("Error", data.msg);
        hideOverlay();
    }
}

function reloadGrid() {
    $("#cheques").ejGrid("refreshContent");
}

function onChangeBanco() {
    $("#DetalleOperacion_Banco").val($("#DetalleOperacion_IdBanco :selected").text());
}

function clearFormCheque() {
    $("#DetalleOperacion_UniqueId").val('');
    $("#form-cheques input[type=text]").each(function () { $(this).val('') });
    $("#form-cheques input[type=number]").each(function () { $(this).val('') });
    $("#form-cheques select").each(function () { $(this).val('') });
    $('#imgCheque1').prop('src', '/UploadImages/Cheques/no-Imagen.png');
    $('#imgCheque2').prop('src', '/UploadImages/Cheques/no-Imagen.png');
    $("#DetalleOperacion_ImagenFront").val('');
    $("#DetalleOperacion_ImagenBack").val('');
}

function onSuccessDeleteItemOp(data) {
    if (data.success) {
        reloadGrid();
        hideOverlay();
    }
    else {
        onErrorAjax();
    }
}

function expandImage(id) {
    $("#imgExpand").prop("src", $(id).prop("src"));
    openPopUpImages();
}

function onSuccessEditItemOp(data) {
    if (data.success) {
        $("#DetalleOperacion_Numero").val(data.result.Numero);
        $("#DetalleOperacion_Banco").val(data.result.Banco);
        $("#DetalleOperacion_UniqueId").val(data.result.UniqueId);
        $("#DetalleOperacion_IdBanco").val(data.result.IdBanco);
        $("#DetalleOperacion_IdBanco").val(data.result.IdBanco);
        $("#DetalleOperacion_TitularCuenta").val(data.result.TitularCuenta);
        $("#DetalleOperacion_NumeroCuenta").val(data.result.NumeroCuenta);
        $("#DetalleOperacion_SucursalEmision").val(data.result.SucursalEmision);
        $("#DetalleOperacion_FechaVencimiento").val(dateTimeReviver(data.result.FechaVencimiento).format("dd/mm/yyyy"));
        $("#DetalleOperacion_MontoStr").val(data.result.Monto);
        $("#DetalleOperacion_Cp").val(data.result.Cp);
        $("#DetalleOperacion_CMC7").val(data.result.CMC7);

        $("#DetalleOperacion_ImagenFront").val(data.result.ImagenFront);
        $("#DetalleOperacion_ImagenBack").val(data.result.ImagenBack);

        $('#imgCheque1').prop('src', '/UploadImages/Cheques/' + (data.result.ImagenFront == null ? "no-Imagen.png" : data.result.ImagenFront));
        $('#imgCheque2').prop('src', '/UploadImages/Cheques/' + (data.result.ImagenBack == null ? "no-Imagen.png" : data.result.ImagenBack));

        hideOverlay();
    }
}
function deleteIamge(id, defaultimage, idHiden) {
    $(id).prop("src", defaultimage);
    $(idHiden).val('no-Imagen.png');
}

function onSuccessSendOperation(data) {
    if (data.success) {
        document.location.href = data.url;
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
}

function IsValidData() {
    if ($("#IdCliente").val() === "0" || $("#IdCliente").val() === "") {
        return false;
    }
    if ($("#cheques").data("ejGrid").model.currentViewData.length == 0) {
        return false;
    }
    return true;
}

function beforeSenData() {
    if (IsValidData()) {
        return true;
    }
    hideOverlay();
    popupGenerico("error", "Error", "Complete los campos obligatorios.");
    return false;
}

function actionComplete(model) {
    if (model.type === "actionComplete") {
        setResumenData(model.model.currentViewData);
    }
}

function setResumenData(items) {
    var cheques = items.length;
    var importe = 0;
    var importeAprobado = 0;

    for (var i = 0; i < items.length; i++) {
        if (!isNaN(parseFloat(items[i].Importe)))
            importe += parseFloat(items[i].Importe);
        if (!isNaN(parseFloat(items[i].ImporteApr)))
            importeAprobado += parseFloat(items[i].ImporteApr);
    }


    $("#label-cheques").text(cheques);
    $("#label-importe").text("$" + importe.toFixed(2));
    $("#label-importeApr").text("$" + importeAprobado.toFixed(2));
}

function uploadFile(front) {
    if (front)
        $("#file").click();
    else
        $("#fileBack").click();
}

function sendFrontImage(id) {
    var formData = new FormData($('#frmUplaodFileAdd')[0]);
    showOverlay();
    $.ajax({
        url: '/Operacion/UploadFront',  //Server script to process data
        type: 'POST',
        data: formData,
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        if (data.success) {
            $('#imgCheque1').prop('src', data.src);
            $('#DetalleOperacion_ImagenFront').val(data.image);
            hideOverlay();
        }
        else {
            $('#imgCheque1').prop('src', '/UploadImages/Cheques/no-Imagen.png');
            $('#DetalleOperacion_ImagenFront').val('/UploadImages/Cheques/no-Imagen.png');
            hideOverlay();
        }
    }).error(function () {
        onErrorAjax();
    });
}

function sendBackImage(id) {
    var formData = new FormData($('#frmUplaodFileAddBack')[0]);
    showOverlay();
    $.ajax({
        url: '/Operacion/UploadBack',  //Server script to process data
        type: 'POST',
        data: formData,
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        if (data.success) {
            $('#imgCheque2').prop('src', data.src);
            $('#DetalleOperacion_ImagenBack').val(data.image);
            hideOverlay();
        }
        else {
            $('#imgCheque2').prop('src', '/UploadImages/Cheques/no-Imagen.png');
            $('#DetalleOperacion_ImagenBack').val('/UploadImages/Cheques/no-Imagen.png');
            hideOverlay();
        }
    });
}

function scanCheque() {
    showOverlay();
    $.ajax({
        url: 'http://localhost:8000/ScannerWCF/Get',  //Server script to process data
        type: 'GET',
        contentType: "application/json",
        crossDomain: true,
        dataType: "json",
    }).done(function (data) {
        if (data !== null) {
            setDataCheque(data);
            if (data.ChequeResponse.Base64Back !== null && data.ChequeResponse.Base64Front !== null) {
                $('#imgCheque1').prop('src', data.Scanner.FrontUrl);
                $('#DetalleOperacion_ImagenFront').val(data.Scanner.FrontName);

                $('#imgCheque2').prop('src', data.Scanner.BackUrl);
                $('#DetalleOperacion_ImagenBack').val(data.Scanner.BackName);

                //var securityToken = $('[name=__RequestVerificationToken]').val();
                //$.ajax({
                //    url: '/Operacion/CreateImagesScan',  //Server script to process data
                //    type: 'POST',
                //    contentType: "application/json",
                //    crossDomain: true,
                //    dataType: "json",
                //    data: JSON.stringify({
                //        front: data.ChequeResponse.Base64Front,
                //        back: data.ChequeResponse.Base64Back
                //    })
                //}).done(function (data) {
                //    if (data !== null) {
                //        if (data.success) {
                //            $('#imgCheque1').prop('src', data.frontSrc);
                //            $('#DetalleOperacion_ImagenFront').val(data.frontName);

                //            $('#imgCheque2').prop('src', data.backSrc);
                //            $('#DetalleOperacion_ImagenBack').val(data.backName);
                //        }
                //        else {
                //            $('#imgCheque1').prop('src', '/UploadImages/Cheques/no-Imagen.png');
                //            $('#DetalleOperacion_ImagenFront').val('/UploadImages/Cheques/no-Imagen.png');

                //            $('#imgCheque2').prop('src', '/UploadImages/Cheques/no-Imagen.png');
                //            $('#DetalleOperacion_ImagenBack').val('/UploadImages/Cheques/no-Imagen.png');
                //        }
                //    }                  
                //    hideOverlay();
                //}).error(function () {
                //    onErrorAjax();            
                //});
            }
            else {
                $('#imgCheque1').prop('src', '/UploadImages/Cheques/no-Imagen.png');
                $('#DetalleOperacion_ImagenFront').val('/UploadImages/Cheques/no-Imagen.png');

                $('#imgCheque2').prop('src', '/UploadImages/Cheques/no-Imagen.png');
                $('#DetalleOperacion_ImagenBack').val('/UploadImages/Cheques/no-Imagen.png');
            }
        }
        else {
            hideOverlay();
        }
    }).error(function () {
        onErrorAjax();
    });
}

function setDataCheque(data) {

    clearFormCheque();
    try {
        if (data != null && data.ChequeResponse != null) {
            $("#DetalleOperacion_IdBanco [data-code='" + data.ChequeResponse.Banck + "']").prop("selected", "selected");
            onChangeBanco();
            $("#DetalleOperacion_Numero").val(getIntValue(data.ChequeResponse.Number));
            $("#DetalleOperacion_NumeroCuenta").val(getIntValue(data.ChequeResponse.Account));
            $("#DetalleOperacion_Cp").val(getIntValue(data.ChequeResponse.Cp));
            $("#DetalleOperacion_SucursalEmision").val(getIntValue(data.ChequeResponse.Suc));
            $("#DetalleOperacion_CMC7").val(data.ChequeResponse.Banck + data.ChequeResponse.Suc + data.ChequeResponse.Cp + data.ChequeResponse.Number + data.ChequeResponse.Account);

        }
    } catch (e) {

    }
    hideOverlay();
}

function getIntValue(value) {
    try {
        var ret = parseInt(value);
        if (!isNaN(ret))
            return ret;
    } catch (e) { }
    return "";
}

