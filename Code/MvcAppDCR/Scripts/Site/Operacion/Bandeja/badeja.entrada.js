﻿var tmpclient = "";

function autocompleteClientSelectEvent(args) {
    $("#IdCliente").val(args.key);
    tmpclient = args.value;
}

function autocompleteClientLostFocustEvent(args) {
    if (args.value !== tmpclient) {
        $("#IdCliente").val("");
        $("#acCliente").val("");
        tmpclient = "";
    }
}

function printOperacion(id) {

    $.confirm({
        title: 'Imprimir Detalle? ',
        content: '',
        buttons: {
            Si: function () {
               // params = params + "&garantia=1"
                $.ajax({
                    url: 'PrintOperacion',
                    type: "GET",
                    dataType: "html",
                    data: { idoperacion: encodeURI(id), idgarantia: 1 },
                    cache: false,
                    success: function (data) {
                        var printWindow = window.open('', '', 'height=400,width=800');
                        printWindow.document.write(data);
                        printWindow.document.close();
                        printWindow.print();
                        printWindow.close();
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            },
            No: function () {
                
                $.ajax({
                    url: 'PrintOperacion',
                    type: "GET",
                    dataType: "html",
                    data: { idoperacion: encodeURI(id), idgarantia: 0},
                    cache: false,
                    success: function (data) {
                        var printWindow = window.open('', '', 'height=400,width=800');
                        printWindow.document.write(data);
                        printWindow.document.close();
                        printWindow.print();
                        printWindow.close();
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }

        }
    });


    //$.ajax({
    //    url: 'PrintOperacion',
    //    type: "GET",
    //    dataType: "html",
    //    data: { idoperacion: encodeURI(id) },
    //    cache: false,
    //    success: function (data) {
    //        var printWindow = window.open('', '', 'height=400,width=800');
    //        printWindow.document.write(data);
    //        printWindow.document.close();
    //        printWindow.print();
    //        printWindow.close();
    //    },
    //    error: function (xhr, status, error) {
    //        alert(xhr.responseText);
    //    }
    //});

}

var searchUrl = '';

$(function () {
    searchUrl = $('#bandejaOperadorinfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?fecha=' + $('#txtFechaFiltro').val() +
					 '&' +
                     'cliente=' + $('#IdCliente').val() +
                     '&' +
                     'estado=' + $('#IdEstadoFiltro').val() +
                     '&' +
                     'op=' + $('#txtNroOpFiltro').val() +
                     '&';


        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#bandejaOperaciones").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });
        $("#bandejaOperaciones").ejGrid('dataSource', dataManager);
        CleanContainerFields();
    });
});

function queryCellInfo(args) {
    if (args.column.field == "EstadoStr") {
        if (args.data.Estado == 1)
            $($(args.cell).parent()).css("backgroundColor", "#DED9A2").css("color", "black");
        if (args.data.Estado == 2 || args.data.Estado == 3)
            $($(args.cell).parent()).css("backgroundColor", "#9EDEA5").css("color", "black");
        if (args.data.Estado == 4)
            $($(args.cell).parent()).css("backgroundColor", "#DC9696").css("color", "black");
    }
}

$(document).ready
    (
        function () {
            var date = $('#txtFechaFiltro');
            date.datepicker({ format: "dd/mm/yyyy", language: "es-AR" }).on('changeDate', function (ev) {
                date.datepicker('hide');
            });
            setInterval(function () { $('#btnSearch').click() }, 1000 * 60 * 10);
        }
    );

function queryCellInfo(args) {
    if (args.column.field == "EstadoStr") {
        if (args.data.Estado == 1)
            $($(args.cell).parent()).css("backgroundColor", "#DED9A2").css("color", "black");

        if (args.data.Estado == 2 || args.data.Estado == 3)
            $($(args.cell).parent()).css("backgroundColor", "#9EDEA5").css("color", "black");

        if (args.data.Estado == 4)
            $($(args.cell).parent()).css("backgroundColor", "#DC9696").css("color", "black");

        if (args.data.Estado == 5 || args.data.Estado == 6)
            $($(args.cell).parent()).css("backgroundColor", "#00FB9E").css("color", "black");
    }
}

function ajaxOperacion(id, url, onCallback, confirm) {
    var token = $("*[name=__RequestVerificationToken]").val();
    if (confirm) {
        confirmChangeStatus(function () { ajaxOperacion(id, url, onCallback, false); });
        return false;
    }
    else {
        $.ajax({
            url: url,
            data:
            {
                __RequestVerificationToken: token,
                IDKey: id
            },
            type: 'POST',
            async: false,
            success: function (data) {
                if (data.success) {
                    onCallback(data);
                }
                else {
                    popupGenerico("error", "Error", data.msg);
                }
            },
            error: function () {
                onErrorAjax();
            }
        });
    }
}

function redirectOperacion(id, url) {
    var form = $("<form/>",
                { action: url, method: "post" }
           );
    $(form).append("<input type='hiden' value='" + $("*[name=__RequestVerificationToken]").val() + "' name='__RequestVerificationToken'/>");
    $(form).append("<input type='hiden' value='" + encodeURI(id) + "' name='IDKey'/>");
    $(form).append("<input type='hiden' value='bandejaoperador' name='Url'/>");
    $(document.body).append(form);
    $(form).submit();
}

function confirmChangeStatus(funct) {
    confirm("Confimación", "¿Esta seguro que desea cambiar el estado de la operación?", funct);
    return false;
}