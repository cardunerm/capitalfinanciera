﻿$(function () {
    //$('#NombreUsuario').change(function () {
    //    GetSucursalesByNombreUsuario('IdSucursal', 'NombreUsuario');
    //});

    $('#btnLogin').click(function () {
    });
});

function GetSucursalesByNombreUsuario(ddlSucursal, txtNombreUsuario) {
    $.ajax({
        url: $('#loginInfo').data('get-sucursales-url'),
        type: 'POST',
        data: { nombreUsuario: $('#' + txtNombreUsuario).val() },
        dataType: 'json',
        success: function (data) {
            var control = $('#' + ddlSucursal);
            control.empty();

            if (data.status == '1') {
                if (data.Options.length > 0) {
                    $.each(data.Options, function (i, o) {
                        control.append($('<option/>')
								.attr('value', o.Value)
								.text(o.Text)
						);

                        $('#IdEmpresa').val(data.Empresa)
                    });
                }
                else {
                    popupError($('#loginInfo').data('error-text'),"El usuario no tiene sucursales asignadas");
                }
            }
            else {
                popupError($('#loginInfo').data('error-text'),"El usuario no tiene sucursales asignadas");
            }
        }
    });
}

function onRestablecerContraseniaSuccessAjax(data, status, xhr) {
    hideOverlay();

    if (status == 'success') {
        if (data.status == '1') {
            $('#changeContraseniaModal').modal('hide');
        }
        else {
            $(data.error).text(data.msg);
        }
    }
}

//function EnterPressed(event) {
//	var key = window.event ? event.keyCode : event.which;

//	if (key == 13) {
//		ResetContrasenia();
//	}
//}