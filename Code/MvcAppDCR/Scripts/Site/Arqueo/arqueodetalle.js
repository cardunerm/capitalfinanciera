﻿function createArqueo() {
    var json = [];
    var obj, value;

    $("input:checkbox:checked").each(function () {
        obj = {
            IdCheque: this.id
        };

        json.push(obj);
    });
    
    $('#cheques').val(JSON.stringify(json));
}

function onSuccessSendArqueo(data) {
    if (data.success) {
        document.location.href = data.url;
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
}

function onSuccessEditArqueo(data) {
    if (data.success) {
        window.location.reload();        
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
}
