﻿$(function () {
    $(':checkbox[data-selected=true]').each(function () { this.checked = true; });

    $('.e-headertemplate').html('<div class="e-headercelldiv" style="text-align: center;"><input type="checkbox" id="chkAll" onclick="CheckAllModules();" /></div>');
});

function addModulo() {
    var json = [];
    var obj;

    $(':checkbox:checked[modulo=true]').each(function () {
        if ($('#view_' + this.id).is(':checked')) {
            obj = {
                IdModulo: this.id,
                Permiso: $('#perfilInfo').data('view-value')
            };

            json.push(obj);
        }

        if ($('#create_' + this.id).is(':checked')) {
            obj = {
                IdModulo: this.id,
                Permiso: $('#perfilInfo').data('create-value')
            };

            json.push(obj);
        }

        if ($('#edit_' + this.id).is(':checked')) {
            obj = {
                IdModulo: this.id,
                Permiso: $('#perfilInfo').data('edit-value')
            };

            json.push(obj);
        }

        if ($('#delete_' + this.id).is(':checked')) {
            obj = {
                IdModulo: this.id,
                Permiso: $('#perfilInfo').data('delete-value')
            };

            json.push(obj);
        }
    });

    $('#modulos').val(JSON.stringify(json));
}

function CheckAllModules() {
    if ($(':checkbox[id=chkAll]').is(':checked')) {
        $('.row-header').attr('checked', true);
        $('.row-check').attr('checked', true);
    }
    else {
        $('.row-header').removeAttr('checked');
        $('.row-check').removeAttr('checked');
    }
}

function CheckAllRow(idModulo) {
    if ($(':checkbox[id=' + idModulo + ']').is(':checked')) {
        $('#view_' + idModulo).attr('checked', true);
        $('#create_' + idModulo).attr('checked', true);
        $('#edit_' + idModulo).attr('checked', true);
        $('#delete_' + idModulo).attr('checked', true);
    }
    else {
        $('#view_' + idModulo).removeAttr('checked');
        $('#create_' + idModulo).removeAttr('checked');
        $('#edit_' + idModulo).removeAttr('checked');
        $('#delete_' + idModulo).removeAttr('checked');
    }
}