﻿var searchUrl = '';

$(function () {
    searchUrl = $('#perfilInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?nombre=' + $('#txtNombre').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#perfilGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#perfilGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});