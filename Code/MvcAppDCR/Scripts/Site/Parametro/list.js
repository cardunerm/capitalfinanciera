﻿var searchUrl = '';

$(function () {
    searchUrl = $('#parametroInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?nombre=' + $('#txtNombre').val() +
            '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#parametroGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#parametroGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});