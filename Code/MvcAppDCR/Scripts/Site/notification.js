﻿$(function () {
    if (window.notification == undefined) {
        window.notification = $.connection.notificationHub;
        $.connection.hub.start().done(function () {
            notification.server.joinOperador(notification.connection.id, notificationID);
            notification.server.leaveAprobadores(notification.connection.id);

            if (registerAdmin) {
                notification.server.joinAprobadores(notification.connection.id);
            }
        });


        notification.client.NotificationSend = function (message) {
            showNotification(message)
        };
    }
});

function showNotification(msg) {
    notify({
        type: "success", //alert | success | error | warning | info
        title: "Notificación",
        position: {
            x: "left", //right | left | center
            y: "top" //top | bottom | center
        },
        icon: '<img src="/content/images/info.png" />',
        message: msg
    });
}