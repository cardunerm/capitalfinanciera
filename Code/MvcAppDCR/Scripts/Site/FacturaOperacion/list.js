﻿var searchUrl = '';

$(function () {
    searchUrl = $('#operacionInfo').data('search-url');

    

    $('#btnSearch').click(function () {
        var params = '?numero=' + $('#txtFacturaNumero').val() +
            '&cliente=' + $('#ddlCliente').val() +
            '&deudor=' + $('#ddlDeudor').val() +
            '&estado=' + $('#ddlEstado').val() +
            '&';

        

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#facturaoperacionGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#facturaoperacionGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });

   
    var dateFacturaCobro = $('#FacturaFechaCobro');
    dateFacturaCobro.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
        dateFacturaCobro.datepicker('hide');
        IncidenciaIntereses();
    });

    var dateFacturaFecha = $('#FacturaFecha');
    dateFacturaFecha.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
        dateFacturaFecha.datepicker('hide');
        IncidenciaIntereses();
    });

    var dateDesembolso = $('#FechaDesembolso');
    dateDesembolso.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
        dateDesembolso.datepicker('hide');
    });

    var dateCobro = $('#FechaCobro');
    dateCobro.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
        dateCobro.datepicker('hide');
    });


    var dateTasa = $('#FechaTasa');
    dateTasa.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
        dateTasa.datepicker('hide');
    });






    $("#FacturaNumero").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#FacturaFecha").focus();
        }
    });

    $("#FacturaFecha").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#FacturaFecha").datepicker('hide');
            $("#FacturaMonto").focus();
        }
    });

    $("#FacturaMonto").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#FacturaRetencion").focus();
        }
    });

    $("#FacturaRetencion").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#FacturaFechaCobro").focus();
        }
    });

    $("#FacturaFechaCobro").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#FacturaFechaCobro").datepicker('hide');
            $("#FacturaImagen").focus();
        }
    });

    $("#FacturaImagen").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#Escribania").focus();
        }
    });

    $("#Escribania").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#Transcripcion").focus();
        }
    });

    $("#Transcripcion").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#Protocolizacion").focus();
        }
    });

    $("#Protocolizacion").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#Sello").focus();
        }
    });

    $("#Sello").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#DiasDemora").focus();
        }
    });

    $("#DiasDemora").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#ImpCheque").focus();
        }
    });

    $("#ImpCheque").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#GastoOtorgamiento").focus();
        }
    });

    $("#GastoOtorgamiento").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#TasaDiaria").focus();
        }
    });

    $("#TasaDiaria").keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $("#btnSave").focus();
        }
    });
});

function CalcularTotalGasto() {

   
    var escribania = 0; 
    var transcripcion = 0; 
    var protocolizacion = 0;
    var sello = 0;

    if ($('#Escribania').val() != "")
        escribania = parseFloat($('#Escribania').val());

    if ($('#Transcripcion').val() != "")
        transcripcion = parseFloat($('#Transcripcion').val());

    if ($('#Protocolizacion').val() != "")
        protocolizacion = parseFloat($('#Protocolizacion').val());

    if ($('#Sello').val() != "")
        sello = parseFloat($('#Sello').val());

    var total = escribania + transcripcion + protocolizacion +  sello;

    $('#TotalGasto').val(total);

  //  IncidenciaGasto();
    GastoCesion();
    GastoOtorgamiento();
    GastoImpuesto();
    GastoTotal();
    GastoIvaInteres();
    GastoIvaGasto();
    Garantia1();
}


function CalcularDemoraPosible() {

    var tasadiaria = 0;
    var diasmora = 0;  
    var importefactura = 0;
    var interescalculado = 0;
    var retencion = 0;
    var demoraposible = 0;

  

    if ($('#TasaDiaria').val() != "")
        tasadiaria = parseFloat($('#TasaDiaria').val());

    if ($('#DiasDemora').val() != "")
        diasmora = parseFloat($('#DiasDemora').val());

    if ($('#FacturaMonto').val() != "")
        importefactura = parseFloat($('#FacturaMonto').val());

    if ($('#FacturaRetencion').val() != "")
        retencion = parseFloat($('#FacturaRetencion').val());

    interescalculado = importefactura * (1 - (retencion / 100));
    demoraposible = (((Math.pow((1 + (tasadiaria/100)), diasmora) * interescalculado) - interescalculado) * 1.21) / importefactura;

    $('#DemoraPosible').val(parseFloat(demoraposible * 100).toFixed(2));

    Incidencia();
    IncidenciaIntereses();
    SaldoInicial();
    
}


function Incidencia() {
  
    var totalGasto = 0;
    var gastofijo = 0;
    var importefactura = 0;
    var interescalculado = 0;
    var retencion = 0;
    var incidencia = 0;

    if ($('#FacturaMonto').val() != "")
        importefactura = parseFloat($('#FacturaMonto').val());

    if ($('#FacturaRetencion').val() != "")
        retencion = parseFloat($('#FacturaRetencion').val());

    if ($('#TotalGasto').val() != "")
        totalGasto = parseFloat($('#TotalGasto').val());

    if ($('#GastoFijo').val() != "")
        gastofijo = parseFloat($('#GastoFijo').val());



    interescalculado = importefactura * (1 - (retencion / 100));
    incidencia = ((totalGasto + ((gastofijo /100) * interescalculado)) * 1.21) / importefactura;
    $('#IncidenciaGasto').val(parseFloat(incidencia * 100).toFixed(2));

    CalcularTotalOforo();

}


function IncidenciaIntereses() {
    var tasadiaria = 0;
    
    var diasmora = 0;
    var importefactura = 0;
    var interescalculado = 0;
    var retencion = 0;
    var demoraposible = 0;

  

    //var dateCobro = $('#FacturaFechaCobro');
    //dateCobro.datepicker('hide');
   

    if ($('#TasaDiaria').val() != "")
        tasadiaria = parseFloat($('#TasaDiaria').val());

    
    var a = moment($('#FacturaFecha').val(), "DD/MM/YYYY");
    var b = moment($('#FacturaFechaCobro').val(), "DD/MM/YYYY");
    var diasmora = b.diff(a, 'days')  

       
      

    if ($('#FacturaMonto').val() != "")
        importefactura = parseFloat($('#FacturaMonto').val());

    if ($('#FacturaRetencion').val() != "")
        retencion = parseFloat($('#FacturaRetencion').val());





    interescalculado = importefactura * (1 - (retencion / 100));
    demoraposible = (((Math.pow((1 + (tasadiaria/100) ), diasmora) * interescalculado) - interescalculado) * 1.21) / importefactura;

    $('#IncidenciaInteres').val(parseFloat(demoraposible * 100).toFixed(2));

    CalcularTotalOforo();
}


function CalcularTotalOforo() {

    var retencion = 0;
    var demora = 0;
    var incidenciagasto = 0;
    var incidenciainteres = 0;
    var totalaforo = 0;

    if ($('#FacturaRetencion').val() != "")
        retencion = parseFloat($('#FacturaRetencion').val(),5);

    if ($('#DemoraPosible').val() != "")
        demora = parseFloat($('#DemoraPosible').val(),5);

    if ($('#IncidenciaGasto').val() != "")
        incidenciagasto = parseFloat($('#IncidenciaGasto').val(),5);

    if ($('#IncidenciaInteres').val() != "")
        incidenciainteres = parseFloat($('#IncidenciaInteres').val(),5);

    totalaforo = retencion + demora + incidenciagasto + incidenciainteres;

    $('#TotalAforo').val(parseFloat(totalaforo).toFixed(5));

    SaldoInicial();
}


function SaldoInicial() {
   
    var totalaforo = 0;
    var importefactura = 0;
    var saldoinicial = 0;

    totalaforo = $('#TotalAforo').val();
    importefactura = $('#FacturaMonto').val();
    saldoinicial = importefactura * (1 - (totalaforo/100));
    $('#SaldoInicial').val(parseFloat(saldoinicial).toFixed(2));
    InteresOperacion();
    GastoCesion();
    GastoOtorgamiento();
    GastoImpuesto();
    GastoTotal();
    CalcularTotalGasto();
}

function InteresOperacion() {

    var montodesembolso = 0;
    var tasadiaria = 0;
    var diasmora = 0;
    var interesoperacion = 0;


    if ($('#SaldoInicial').val() != "")
        montodesembolso = parseFloat($('#SaldoInicial').val());

    if ($('#TasaDiaria').val() != "")
        tasadiaria = parseFloat($('#TasaDiaria').val());


    var a = moment($('#FacturaFecha').val(), "DD/MM/YYYY");
    var b = moment($('#FacturaFechaCobro').val(), "DD/MM/YYYY");
    diasmora = b.diff(a, 'days')  
    interesoperacion = montodesembolso * (Math.pow((1 + (tasadiaria / 100)), diasmora) - 1);
    $('#Intereses').val(parseFloat(interesoperacion).toFixed(2));

}


function GastoCesion() {

    $('#GastoCesion').val(parseFloat($('#TotalGasto').val()).toFixed(2));

}

function GastoOtorgamiento() {
    var importefactura = 0;
    var retencion = 0;
    var gastoOtorgamiento = 0;


    if ($('#FacturaMonto').val() != "")
        importefactura = parseFloat($('#FacturaMonto').val());

    if ($('#FacturaRetencion').val() != "")
        retencion = parseFloat($('#FacturaRetencion').val());

    if ($('#GastoOtorgamiento').val() != "")
        gastoOtorgamiento = parseFloat($('#GastoOtorgamiento').val());


    gastocesion = importefactura * (1 - (retencion / 100)) * (gastoOtorgamiento/100);

    $('#GastoOtorga').val(parseFloat(gastocesion).toFixed(2));
}

function GastoImpuesto() {

    var importefactura = 0;
    var retencion = 0;
    var impcheque = 0;
    var gastoimpuesto = 0;

    if ($('#FacturaMonto').val() != "")
        importefactura = parseFloat($('#FacturaMonto').val());

    if ($('#FacturaRetencion').val() != "")
        retencion = parseFloat($('#FacturaRetencion').val());

    if ($('#ImpCheque').val() != "")
        impcheque = parseFloat($('#ImpCheque').val());


    gastoimpuesto = importefactura * (1 - (retencion / 100)) * (impcheque / 100);
    $('#Impuestos').val(parseFloat(gastoimpuesto).toFixed(2));

   

}

function GastoTotal() {

    var gastocesion = 0;
    var gastootorgamiento = 0;
    var gastoimpuesto = 0;
    var totalgasto = 0;

    if ($('#GastoCesion').val() != "")
        gastocesion = parseFloat($('#GastoCesion').val());

    if ($('#GastoOtorga').val() != "")
        gastootorgamiento = parseFloat($('#GastoOtorga').val());

    if ($('#Impuestos').val() != "")
        gastoimpuesto = parseFloat($('#Impuestos').val());

    totalgasto = gastocesion + gastootorgamiento + gastoimpuesto;
    $('#Iva').val(parseFloat(totalgasto).toFixed(2));
       
}

function GastoIvaInteres() {
    var intereses = 0;
    var ivainteres = 0;

    if ($('#Intereses').val() != "")
        intereses = parseFloat($('#Intereses').val());

    ivainteres = intereses * 0.21;
    $('#Ivainteres').val(parseFloat(ivainteres).toFixed(2));

}


function GastoIvaGasto() {
    var gastos = 0;
    var gastoImpuesto = 0;
    var ivagasto = 0;

    if ($('#GastoOtorga').val() != "")
        gastos = parseFloat($('#GastoOtorga').val());

    if ($('#Impuestos').val() != "")
        gastoImpuesto = parseFloat($('#Impuestos').val());
    

    ivagasto = (gastos + gastoImpuesto)* 0.21;
    $('#Ivagasto').val(parseFloat(ivagasto).toFixed(2));

}

function Garantia1() {
    var importefactura = 0;
    var retencion = 0;
    var tasa = 0;
    var garantia1 = 0;

    if ($('#FacturaMonto').val() != "")
        importefactura = parseFloat($('#FacturaMonto').val());

    if ($('#FacturaRetencion').val() != "")
        retencion = parseFloat($('#FacturaRetencion').val());

    if ($('#TasaDiaria').val() != "")
        tasa = parseFloat($('#TasaDiaria').val());
    
    garantia1 = importefactura * (1 - (retencion / 100)) * ((Math.pow((1 + (tasa / 100)), 30)) - 1) * 1.21;

    $('#Garantia1').val(parseFloat(garantia1).toFixed(2));

    Garantia2();
}

function Garantia2() {
    var importefactura = 0;
    var retencion = 0;
    var tasa = 0;
    var garantia1 = 0;
    var garantia2 = 0;

    if ($('#FacturaMonto').val() != "")
        importefactura = parseFloat($('#FacturaMonto').val());

    if ($('#FacturaRetencion').val() != "")
        retencion = parseFloat($('#FacturaRetencion').val());

    if ($('#TasaDiaria').val() != "")
        tasa = parseFloat($('#TasaDiaria').val());

    if ($('#Garantia1').val() != "")
        garantia1 = parseFloat($('#Garantia1').val());
   
    garantia2 = (importefactura * (1 - (retencion / 100)) * ((Math.pow((1 + (tasa / 100)), 60) - 1) * 1.21) - garantia1);

    $('#Garantia2').val(parseFloat(garantia2).toFixed(2));

    Garantia3();
}

function Garantia3() {
    var importefactura = 0;
    var retencion = 0;
    var tasa = 0;
    var garantia1 = 0;
    var garantia2 = 0;
    var garantia3 = 0;

    if ($('#FacturaMonto').val() != "")
        importefactura = parseFloat($('#FacturaMonto').val());

    if ($('#FacturaRetencion').val() != "")
        retencion = parseFloat($('#FacturaRetencion').val());

    if ($('#TasaDiaria').val() != "")
        tasa = parseFloat($('#TasaDiaria').val());

    if ($('#Garantia2').val() != "")
        garantia2 = parseFloat($('#Garantia2').val());

    if ($('#Garantia1').val() != "")
        garantia1 = parseFloat($('#Garantia1').val());

    garantia3 = (importefactura * (1 - (retencion / 100)) * ((Math.pow((1 + (tasa / 100)), 90) - 1) * 1.21) - garantia2 - garantia1);

    $('#Garantia3').val(parseFloat(garantia3).toFixed(2));


}

function VerDesembolsos(id) {

   
    var searchUrl = $('#operacionInfo').data('getdesembolso-url');
    var params = '?idFacturaOperacion=' + id + '&';

    var dataManager = ej.DataManager({ url: searchUrl + params });

    $('#desembolsoGrid').ejGrid('dataSource', dataManager);
    $('#ModalVerDesembolso').modal('show');
    

}

function VerCobros(id) {

    var searchUrl = $('#operacionInfo').data('getcobro-url');
    var params = '?idFacturaOperacion=' + id + '&';

    var dataManager = ej.DataManager({ url: searchUrl + params });

    $('#cobroGrid').ejGrid('dataSource', dataManager);   
    $('#ModalVerCobros').modal('show');

}

function VerTasas(id) {

    var searchUrl = $('#operacionInfo').data('gettasas-url');
    var params = '?idFacturaOperacion=' + id + '&';

    var dataManager = ej.DataManager({ url: searchUrl + params });

    $('#tasaGrid').ejGrid('dataSource', dataManager);
    $('#ModalVerTasas').modal('show');

}

function AvanceOperacion(id) {

    var searchUrl = $('#operacionInfo').data('getoperaciones-url');
    var params = '?idFacturaOperacion=' + id + '&';

    var dataManager = ej.DataManager({ url: searchUrl + params });

    $('#OperacionGrid').ejGrid('dataSource', dataManager);
    $('#ModalVerAvances').modal('show');

}


function CargarDesembolso(id) {
    $('#IdFacturaOperacionDesembolso').val(id);
    $('#ModalCargarDesembolso').modal('show');

}

function CargarCobros(id) {

    $('#IdFacturaOperacionCobro').val(id);
    $('#ModalCargarCobros').modal('show');
}

function CargarTasas(id) {

    $('#IdFacturaOperacionCobro').val(id);
    $('#ModalCargarTasas').modal('show');
}


function GuardarDesembolso() {

    var url = searchUrl = $('#operacionInfo').data('guardardesembolso-url');

    var idfacturaoperacion = $('#IdFacturaOperacionDesembolso').val();
    var fechadesembolso = $('#FechaDesembolso').val();
    var monto = $('#ImporteDesembolso').val();

    $.ajax({      
        url: url,
        data: { idfacturaoperacion: idfacturaoperacion, fecha: fechadesembolso, monto: monto},       
        dataType: "json",
        success: function (result) {         
            $('#ModalCargarDesembolso').modal('hide');
            if (result.status) {               
                $("#facturaoperacionGrid").ejGrid("refreshContent");
                $('#ImporteDesembolso').val('0.00');
            } else {
                popupGenerico("error", "Error", result.msg);
            }
        },
        error: function () {
                      popupGenerico("error", "Error", "Error al obtener las cuentas");
        }
    });

}

function GuardarCobro() {

    var url = searchUrl = $('#operacionInfo').data('guardarcobro-url');

    var idfacturaoperacion = $('#IdFacturaOperacionCobro').val();
    var fechacobro = $('#FechaCobro').val();
    var monto = $('#ImporteCobro').val();

    $.ajax({
        url: url,
        data: { idfacturaoperacion: idfacturaoperacion, fecha: fechacobro, monto: monto },
        dataType: "json",
        success: function (result) {
            $('#ModalCargarCobros').modal('hide');
            if (result.status) {
                $("#facturaoperacionGrid").ejGrid("refreshContent");
                $('#ImporteCobro').val('0.00');
            } else {
                popupGenerico("error", "Error", result.msg);
            }
        },
        error: function () {
            popupGenerico("error", "Error", "Error al obtener las cuentas");
        }
    });

}

function GuardarTasa() {

    var url = searchUrl = $('#operacionInfo').data('guardartasa-url');

    var idfacturaoperacion = $('#IdFacturaOperacionCobro').val();
    var fechatasa = $('#FechaTasa').val();
    var monto = $('#ImporteTasa').val();

    $.ajax({
        url: url,
        data: { idfacturaoperacion: idfacturaoperacion, fecha: fechatasa, monto: monto },
        dataType: "json",
        success: function (result) {
            $('#ModalCargarTasas').modal('hide');
            if (result.status) {
                $("#facturaoperacionGrid").ejGrid("refreshContent");
                $('#ImporteTasa').val('0.00');
            } else {
                popupGenerico("error", "Error", result.msg);
            }
        },
        error: function () {
            popupGenerico("error", "Error", "Error al obtener las cuentas");
        }
    });

}

function VerFactura(id) {

    var url = searchUrl = $('#operacionInfo').data('getfile-url');
    var params = '/' + id;
    DownloadFile(url + params);

   

}




function GastosFijos() {

    var gastootorgamiento = 0;
    var impcheque = 0;
        
    if ($('#GastoOtorgamiento').val() != "")
        gastootorgamiento = parseFloat($('#GastoOtorgamiento').val());


    if ($('#ImpCheque').val() != "")
        impcheque = parseFloat($('#ImpCheque').val());



    $('#GastoFijo').val(parseFloat(gastootorgamiento + impcheque).toFixed(2));

}


function EliminarDesembolso(id) {

    var url = searchUrl = $('#operacionInfo').data('eliminardesembolso-url');       
    $.ajax({
        url: url,
        data: { idfacturadesembolso: id},
        dataType: "json",
        success: function (result) {
            
            if (result.status) {
                $("#desembolsoGrid").ejGrid("refreshContent");                
            } else {
                popupGenerico("error", "Error", result.msg);
            }
        },
        error: function () {
            popupGenerico("error", "Error", "Error al obtener las cuentas");
        }
    });
}

function EliminarCobro(id) {

    var url = searchUrl = $('#operacionInfo').data('eliminarcobro-url');
    $.ajax({
        url: url,
        data: { idfacturacobro: id },
        dataType: "json",
        success: function (result) {

            if (result.status) {
                $("#cobroGrid").ejGrid("refreshContent");
            } else {
                popupGenerico("error", "Error", result.msg);
            }
        },
        error: function () {
            popupGenerico("error", "Error", "Error al obtener las cuentas");
        }
    });
}


function EliminarTasa(id) {

    var url = searchUrl = $('#operacionInfo').data('eliminartasa-url');
    $.ajax({
        url: url,
        data: { idfacturatasa: id },
        dataType: "json",
        success: function (result) {

            if (result.status) {
                $("#tasaGrid").ejGrid("refreshContent");
            } else {
                popupGenerico("error", "Error", result.msg);
            }
        },
        error: function () {
            popupGenerico("error", "Error", "Error al obtener las cuentas");
        }
    });
}

function ImprimirComprobante(id) {

    var url = searchUrl = $('#operacionInfo').data('print-url');
    var params = '?Id=' + id;

    $.confirm({
        title: 'Imprimir Garantias? ',
        content: '',
        buttons: {
            Si: function () {
                params = params + "&garantia=1"
                ShowReportModal(url + params, "Comprobante");
            },
            No: function () {
                params = params + "&garantia=0"
                ShowReportModal(url + params, "Comprobante");
            }

        }
    });
}

function ImprimirComprobanteAprobado(id) {

    var url = searchUrl = $('#operacionInfo').data('printaprobado-url');
    var params = '?Id=' + id;
    ShowReportModal(url + params, "Comprobante");
  
}


function ImprimirComprobanteTerminado(id) {

    var url = searchUrl = $('#operacionInfo').data('printterminado-url');
    var params = '?Id=' + id;
    ShowReportModal(url + params, "Comprobante");   
}


function queryCellInfo(args) {
   // if (args.column.field == "IdEstadoEstados") {
        if (args.data.IdFacturaEstado == 3)
            $($(args.cell).parent()).css("backgroundColor", "#DC9696").css("color", "black");
        //if (args.data.Estado == 2 || args.data.Estado == 3)
        //    $($(args.cell).parent()).css("backgroundColor", "#9EDEA5").css("color", "black");
        //if (args.data.Estado == 4)
        //    $($(args.cell).parent()).css("backgroundColor", "#DC9696").css("color", "black");
    //}
}