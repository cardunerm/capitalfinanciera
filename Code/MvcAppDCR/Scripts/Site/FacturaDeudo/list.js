﻿var searchUrl = '';

$(function () {
    searchUrl = $('#deudoInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?nombre=' + $('#txtNombre').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#facturadeudoGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#facturadeudoGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});