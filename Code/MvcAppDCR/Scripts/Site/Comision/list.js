﻿var searchUrl = '';

$(document).ready
    (
    function () {
        var datedesde = $('#FechaDesde');
        var datehasta = $('#FechaHasta');


        datedesde.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
            date.datepicker('hide');
        });

        datehasta.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
            date.datepicker('hide');
        });
    }




);

$(function () {
    searchUrl = $('#clienteInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?nombre=' + $('#ddlVendedor').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#comisionGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#comisionGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});


function Buscar() {  

    var _vendedor = $('#IdVendedor').val();

    if (_vendedor == "0") {
        alert("Debe Seleccionar un Vendedor");
        return;
    }


    searchUrl = $('#comisionInfo').data('search-url');

    var params = '?fechadesde=' + $('#FechaDesde').val() 
        + '&fechahasta=' + $('#FechaHasta').val() 
        + '&vendedor=' + $('#IdVendedor').val() 
		+ '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#comisionesGrid").ejGrid('dataSource', dataManager);
}


function abrirReporte(id) {

    debugger;
    var url = searchUrl = $('#clienteInfo').data('print-url');
    var params = '?Id=' + id + '&';

    ShowReportModal(url + params, "Comision");

}