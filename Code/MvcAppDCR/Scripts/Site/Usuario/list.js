﻿var searchUrl = '';
permiso
function permiso(idUsuario) {

    window.location.href = '/SucursalUsuario/AssignSucursal/?idUsuario=' + idUsuario;
}

$(function () {
    searchUrl = $('#usuarioInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?nombre=' + $('#txtNombre').val() +
					 '&apellido=' + $('#txtApellido').val() +
					 '&nombreUsuario=' + $('#txtNombreUsuario').val() +
					 '&idPerfil=' + $('#ddlPerfil').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#usuarioGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#usuarioGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});