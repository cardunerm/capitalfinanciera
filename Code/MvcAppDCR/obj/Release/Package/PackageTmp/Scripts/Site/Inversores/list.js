﻿var searchUrl = '';

function autocompleteInversorSelectEvent(args) {
    
    $("#IdInversor").val(args.key);
    tmpclient = args.value;

}

function autocompleteClientLostFocustEvent(args) {
    //if (args.value !== tmpclient) {
    //    $("#IdInversor").val("");
    //    $("#acInversor").val("");
    //    tmpclient = "";
    //}
}


function DescargarContrato() {

    var url = searchUrl = $('#proyeccionFinancieraInfo').data('print-url');
    ShowReportModal(url, "Contrato");
}



$(function () {
    searchUrl = $('#inversoresInfo').data('search-url');

    $('#btnSearch').click(function () {

        var nombre;
        if ($("#acInversor").ejGrid().val().split('-')[1] !== undefined) {
            nombre = $("#acInversor").ejGrid().val().split('-')[1].trim();
        } else {
            nombre = '';
        }

        var params = '?inversor=' + nombre +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#inversorGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#inversorGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});

function DescargarContrato(Id) {

    var url = searchUrl = $('#inversoresInfo').data('print-url');
    url = url + "?idInversor= " + Id;
    ShowReportModal(url, "Contrato");
}