﻿
function autocompleteClientSelectEvent(args) {
    $("#IdCliente").val(args.key);
    tmpclient = args.value;
}

function autocompleteClientLostFocustEvent(args) {
    if (args.value !== tmpclient) {
        $("#IdCliente").val("");
        $("#acCliente").val("");
        tmpclient = "";
    }
}

var searchUrl = '';

$(function () {
    searchUrl = $('#bandejaAprobacioninfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?fecha=' + $('#txtFechaFiltro').val() +
					 '&' +
                     'cliente=' + $('#IdCliente').val() +
                     '&' +
                     'estado=' + $('#IdEstadoFiltro').val() +
                     '&' +
                     'op=' + $('#txtNroOpFiltro').val() +
                     '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#bandejaAprobacion").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });
        $("#bandejaAprobacion").ejGrid('dataSource', dataManager);
        CleanContainerFields();
    });
});

function queryCellInfo(args) {
    if (args.column.field == "EstadoStr") {
        if (args.data.Estado == 1)
            $($(args.cell).parent()).css("backgroundColor", "#DED9A2").css("color", "black");

        if (args.data.Estado == 2 || args.data.Estado == 3)
            $($(args.cell).parent()).css("backgroundColor", "#9EDEA5").css("color", "black");

        if (args.data.Estado == 4)
            $($(args.cell).parent()).css("backgroundColor", "#DC9696").css("color", "black");

        if (args.data.Estado == 5 || args.data.Estado == 6)
            $($(args.cell).parent()).css("backgroundColor", "#00FB9E").css("color", "black");
    }
}

$(document).ready
    (
        function () {
            var date = $('#txtFechaFiltro');
            date.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
                date.datepicker('hide');
            });
            setInterval(function () { $('#btnSearch').click() }, 10000);
        }

    );

function redirectOperacion(id, url) {
    var form = $("<form/>",
                { action: url, method: "post" }
           );
    $(form).append("<input type='hiden' value='" + $("*[name=__RequestVerificationToken]").val() + "' name='__RequestVerificationToken'/>");
    $(form).append("<input type='hiden' value='" + id + "' name='IDKey'/>");
    $(form).append("<input type='hiden' value='bandejaaprobacion' name='Url'/>");
    $(document.body).append(form);
    $(form).submit();
}

function ajaxOperacion(id, url, onCallback, confirm) {
    var token = $("*[name=__RequestVerificationToken]").val();
    if (confirm) {
        confirmChangeStatus(function () { ajaxOperacion(id, url, onCallback, false); });
        return false;
    }
    else {
        $.ajax({
            url: url,
            data:
            {
                __RequestVerificationToken: token,
                IDKey: id
            },
            type: 'POST',
            async: false,
            success: function (data) {
                if (data.success) {
                    onCallback(data);
                }
                else {
                    popupGenerico("error", "Error", data.msg);
                }
            },
            error: function () {
                onErrorAjax();
            }
        });
    }
}

function confirmChangeStatus(funct) {
    confirm("Confimación", "¿Esta seguro que desea cambiar el estado de la operación?", funct);
    return false;
}