﻿var searchUrl = '';

$(function () {
    searchUrl = $('#clienteInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?nombre=' + $('#txtNombre').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#clienteGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#clienteGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});