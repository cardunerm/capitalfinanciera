﻿
var searchUrl = '';

function imprimirArqueo(idArqueo) {
    var url = $('#arqueoinfo').data('report-url') + '?idArqueo=' + idArqueo;

    ShowReportModal(url, $('#arqueoinfo').data('nombre-reporte'));
}

$(function () {
    searchUrl = $('#arqueoinfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?fecha=' + $('#txtFechaFiltro').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#arqueos").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });
        $("#arqueos").ejGrid('dataSource', dataManager);
        CleanContainerFields();
    });
});

$(document).ready
    (
        function () {
            var date = $('#txtFechaFiltro');
            date.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
                date.datepicker('hide');
            });
        }
    );

function detalle(idArqueo) {
    window.location.href = '/Arqueo/EditArqueo/?idArqueo=' + idArqueo;
}