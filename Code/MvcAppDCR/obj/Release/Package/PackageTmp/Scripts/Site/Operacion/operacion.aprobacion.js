﻿var tmpclient = "";
var enumSatus =
    {
        aprobado: 3,
        cancelado: 4
    };
function autocompleteClientSelectEvent(args) {
    $("#IdCliente").val(args.key);
    tmpclient = args.value;
}

function autocompleteClientLostFocustEvent(args) {
    if (args.value !== tmpclient) {
        $("#IdCliente").val("");
        $("#acCliente").val("");
        tmpclient = "";
    }
}

function createClient() {
    openPopUp();
}

function cancelPopUp() {
    closePopUp();
    clearFormClient();
}

function openPopUp() {
    modalDisplay("#modal-client-edit", false);
}

function closePopUp() {
    modalDisplay("#modal-client-edit", true);
}

function openPopUpImages() {
    modalDisplay("#modal-expand-image", false);
}

function closePopUpImages() {
    modalDisplay("#modal-expand-image", true);
}

function successPopUp(args) {
    $("#IdCliente").val(args.id);
    $("#acCliente").val(args.name);
    hideOverlay();
    closePopUp();
}

function clearFormClient() {
    $("#form-cliente input[type=text]").each(function () { $(this).val('') });
    $.validator.unobtrusive.parse($("#form-cliente"))
    $("#form-cliente").validate().resetForm();
}

$(document).ready
    (
        function () {
            var date = $('#DetalleOperacion_FechaVencimiento');
            // date.on("onchange", function () { onchageMontoCalculado(); });
            date.datepicker({
                format: "dd/mm/yyyy", onRender: function (date) {
                    return date.valueOf() < new Date() ? 'disabled' : '';
                }
            }).on('changeDate', function (ev) {
                date.datepicker('hide');
                onchageMontoCalculado(true);
            }).on('blur', function (ev) {
                try {
                    if (moment(date.val(), "DD/MM/YYYY").toDate() < new Date()) {
                        date.val('');
                    }
                }
                catch (e) {
                    date.val('');
                }
            });

            $("#DetalleOperacion_TasaStr").on("blur", function () { onchageMontoCalculado(); });
            $("#DetalleOperacion_MontoStr").on("blur", function () { onchageMontoCalculado(); });
            $("#DetalleOperacion_GastoStr").on("blur", function () { onchageMontoCalculado(); });
            $("#DetalleOperacion_DiasClearing").on("blur", function () { onchageMontoCalculado(); });
            
            enablePanelAprobation(false);

            $('input.importe').maskMoney({
                thousands: '',
                decimal: ','
            });
        }
    );

function addOperacionToGrid(data) {
    if (data.success) {
        reloadGrid();
        clearFormCheque();
        hideOverlay();
    }
    else {
        onErrorAjax();
    }
}

function reloadGrid() {
    $("#cheques").ejGrid("refreshContent");
}

function onChangeBanco() {
    $("#DetalleOperacion_Banco").val($("#DetalleOperacion_IdBanco :selected").text());
}

function clearFormCheque() {
    $("#DetalleOperacion_UniqueId").val('');
    $("#form-cheques input[type=text]").each(function () { $(this).val('') });
    $("#form-cheques input[type=number]").each(function () { $(this).val('') });
    $("#form-cheques select").each(function () { $(this).val('') });
    $('#imgCheque1').prop('src', '/UploadImages/Cheques/no-Imagen.png');
    $('#imgCheque2').prop('src', '/UploadImages/Cheques/no-Imagen.png');
}

function onSuccessDeleteItemOp(data) {
    if (data.success) {
        reloadGrid();
        hideOverlay();
    }
    else {
        onErrorAjax();
    }
}

function expandImage(id) {
    $("#imgExpand").prop("src", $(id).prop("src"));
    openPopUpImages();
}

function onSuccessEditItemOp(data) {
    if (data.success) {
        $("#DetalleOperacion_Numero").val(data.result.Numero);
        $("#DetalleOperacion_Banco").val(data.result.Banco);
        $("#DetalleOperacion_Id").val(data.result.Id);
        $("#DetalleOperacion_IdBanco").val(data.result.IdBanco);
        $("#DetalleOperacion_TitularCuenta").val(data.result.TitularCuenta);
        $("#DetalleOperacion_NumeroCuenta").val(data.result.NumeroCuenta);
        $("#DetalleOperacion_SucursalEmision").val(data.result.SucursalEmision);
        $("#DetalleOperacion_FechaVencimiento").val(dateTimeReviver(data.result.FechaVencimiento).format("dd/mm/yyyy"));
        $("#DetalleOperacion_MontoStr").val(data.result.Monto);
        $("#det-calculo").html(data.result.DetalleCalculo);
        $("#DetalleOperacion_DiasClearing").val(data.result.DiasClearing);
        //$("#DetalleOperacion_Gastos").val(data.result.Gastos);
        $("#DetalleOperacion_GastoStr").val(data.result.GastoStr);
        //$("#DetalleOperacion_Tasa").val(data.result.Tasa);
        $("#DetalleOperacion_TasaStr").val(data.result.TasaStr);
        $("#DetalleOperacion_MontoAprobado").val(data.result.MontoAprobado);
        $("#DetalleOperacion_Observacion").val(data.result.Observacion);
        $("#DetalleOperacion_Cp").val(data.result.Cp);
        $("#DetalleOperacion_CMC7").val(data.result.CMC7);
        if (data.result.ImagenFront !== null)
            $('#imgCheque1').prop('src', '/UploadImages/Cheques/' + data.result.ImagenFront);
        else
            $('#imgCheque1').prop('src', '/UploadImages/Cheques/no-Imagen.png');

        if (data.result.ImagenBack !== null)
            $('#imgCheque2').prop('src', '/UploadImages/Cheques/' + data.result.ImagenBack);
        else
            $('#imgCheque2').prop('src', '/UploadImages/Cheques/no-Imagen.png');
        enablePanelAprobation(true);
        enablePanelDatosCheque(true);
    }
    hideOverlay();
}

function enablePanelAprobation(enable) {
    $("#DetalleOperacion_GastoStr").each(function () { $(this).prop("disabled", !enable); })
    //$("#DetalleOperacion_DiasClearing").each(function () { $(this).prop("disabled", !enable); })
    $("#DetalleOperacion_TasaStr").each(function () { $(this).prop("disabled", !enable); })
    $("#data-aprobacion button").each(function () { $(this).prop("disabled", !enable); })
    $("#data-aprobacion textarea").each(function () { $(this).prop("disabled", !enable); })
    $("#enable-edit").prop("disabled", !enable);

    if (enable === false) {
        enablePanelDatosCheque(false);
    }
}

function onSuccessFinishOperation(data) {
    if (data.success) {
        document.location.href = data.url;
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
}

function onchageMontoCalculado(calculateCl) {
    var monto = parseFloat($("#DetalleOperacion_MontoStr").val());
    //var gasto = parseFloat($("#DetalleOperacion_GastoStr").val());
    var gasto = $("#DetalleOperacion_GastoStr").val();
    var fecha = moment($("#Fecha").val(), "DD/MM/YYYY")._i;
    var vencimiento = moment($("#DetalleOperacion_FechaVencimiento").val(), "DD/MM/YYYY")._i;
    //var tasa = parseFloat($("#DetalleOperacion_TasaStr").val());
    var tasa = $("#DetalleOperacion_TasaStr").val();
    var estado = parseFloat($("#DetalleOperacion_Estado").val());
    var diasC = parseFloat($("#DetalleOperacion_DiasClearing").val());
    var token = $("*[name=__RequestVerificationToken]").val();

    showOverlay();
    $.ajax({
        url: '/Operacion/CalcularMontoCheque',  //Server script to process data
        type: 'POST',
        data: {
            vencimiento: vencimiento,
            fecha: fecha,
            tasa: tasa,
            monto: monto,
            gasto: gasto,
            diasClearing: diasC,
            estado: isNaN(estado) ? 1 : estado,
            __RequestVerificationToken: token,
            calculateCl: calculateCl !== undefined ? calculateCl : false
        },
    }).done(function (data) {
        hideOverlay();
        $("#DetalleOperacion_MontoAprobado").val(data.amount);
        $("#DetalleOperacion_DiasClearing").val(data.dc);
    }).error(function () {
        onErrorAjax();
    });
}

function updateGrid(data) {
    if (data.success) {
        reloadGrid();
        clearFormCheque();
        hideOverlay();
        enablePanelAprobation(false);
    }
    else {
        onErrorAjax();
    }
}


function actionComplete(model) {
    if (model.type === "actionComplete") {
        setResumenData(model.model.currentViewData);
    }
}

function setResumenData(items) {
    var cheques = items.length;
    var importe = 0;
    var importeAprobado = 0;

    for (var i = 0; i < items.length; i++) {
        if (!isNaN(parseFloat(items[i].Monto)))
            importe += parseFloat(items[i].Monto);
        if (!isNaN(parseFloat(items[i].MontoAprobado)))
            importeAprobado += parseFloat(items[i].MontoAprobado);
    }

    $("#label-cheques").text(cheques);
    $("#label-importe").text("$" + importe.toFixed(2));
    $("#label-importeApr").text("$" + importeAprobado.toFixed(2));
}

function confirmChangeStatus() {
    var newStatus = parseInt($("#IdEstado").val());
    if (!isNaN(newStatus)) {
        if (enumSatus.aprobado == newStatus || newStatus == enumSatus.cancelado) {
            confirm("Confimación", "Una vez finalizada la operación no podra volver a modifcarla. ¿Desea continuar?", changeStatusCallback);
            return false;
        }
    }
    return true;
}
function showMotivos(id) {
    searchUrl = $('#motivos-rechazos').data('search-url');

    var params = '?IDKey=' + id + "&";

    var dataManager = ej.DataManager({
        url: searchUrl + params
    });

    $("#motivosRechazos").ejGrid('dataSource', dataManager);

    $("#modal-motivos").modal("show");
}
function changeStatusCallback() {
    $("#frmSave").submit();
}

function showImagen(front, back) {
    if (front != null && front != undefined) {
        $("#imgFront").prop("src", front);
        $("#imgFront").show();
    }

    if (back != null && back != undefined) {
        $("#imgBack").prop("src", back);
        $("#imgBack").show();
    }

    $("#modal-expand-image").modal("show");
}


function activateEdit() {
    var token = $("*[name=__RequestVerificationToken]").val();
    var data = {
        __RequestVerificationToken: token
    };
    $.fn.CallAjax("/operacion/activaredicion", function (data) { enablePanelDatosCheque(true); }, data);
}

function enablePanelDatosCheque(enable) {
    $("#data-cheque input").each(function () { $(this).prop("disabled", !enable); })
    $("#data-cheque select").each(function () { $(this).prop("disabled", !enable); })
    $("#data-cheque button").each(function () { $(this).prop("disabled", !enable); })
    $("#data-cheque textarea").each(function () { $(this).prop("disabled", !enable); })
}