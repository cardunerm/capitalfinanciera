﻿var searchUrl = '';

function autocompleteInversorSelectEvent(args) {

    $("#IdInversor").val(args.key);
    tmpclient = args.value;

}

function autocompleteClientLostFocustEvent(args) {
    //if (args.value !== tmpclient) {
    //    $("#IdInversor").val("");
    //    $("#acInversor").val("");
    //    tmpclient = "";
    //}
}

//function getResumenCuentaCorriente(key) {
//    showOverlay();
//    var securityToken = $('[name=__RequestVerificationToken]').val();
//    $.ajax({
//        url: '/Operacion/GetResumenCuenta',  //Server script to process data
//        type: 'POST',
//        data: { key: key, __RequestVerificationToken: securityToken },
//    }).done(function (data) {
//        if (data.success) {
//            $("#label-saldo").text("$" + data.datos.Saldo);
//            changeColor("#label-saldo", parseFloat(data.datos.Saldo));

//            $("#label-cheques-rechazados").text("$" + data.datos.MontoRechazos);
//            changeColor("#label-cheques-rechazados", parseFloat(data.datos.MontoRechazos));

//            $("#label-cheques-cantidad-rechazados").text(data.datos.CantidadRechazos);
//            $("#label-tasa").text(data.tasa + "%");
//            reloadGrid();
//            hideOverlay();
//        }
//        else {
//            onErrorAjax();
//        }
//    }).error(function () {
//        onErrorAjax();
//    });
//}

function imprimirDeposito(IdOperacionInversor) {
    var url = $('#bandejaOperacionInfo').data('report-url') + '?IdOperacionInversor=' + IdOperacionInversor;
    ShowReportModal(url, $('#bandejaOperacionInfo').data('nombre-reporte'));
}

function imprimirDepositoCheque(IdOperacionInversor) {
    var url = $('#bandejaOperacionInfo').data('report-urlch') + '?IdOperacionInversor=' + IdOperacionInversor;
    ShowReportModal(url, $('#bandejaOperacionInfo').data('nombre-reportech'));
}

function imprimirExtraccionFondos(IdOperacionInversor) {
    var url = $('#bandejaOperacionInfo').data('report-fondos') + '?IdOperacionInversor=' + IdOperacionInversor;

    ShowReportModal(url, $('#bandejaOperacionInfo').data('nombre-fondos'));
}

function cancelarOperacion(IdOperacionInversor) {

    var params = '?IdOperacionInversor=' + IdOperacionInversor +
                '&';

    cancelar = $('#bandejaOperacionInfo').data('cancelar');

    var token = $("*[name=__RequestVerificationToken]").val();

    var url = cancelar + params;

    $.ajax({
        url: url,
        data:
            {
                __RequestVerificationToken: token,
                IdOperacionInversor: IdOperacionInversor
            },
        type: 'POST',
        async: false,
        success: function (data) {
            if (data.status == "1") {
                
                $("#btnSearch").click();
                popupGenerico("info", "Operacion", "Operacion Cancelada");
            }
            else {
                popupGenerico("error", "Error", data.msg);
            }
        },
        error: function () {
            onErrorAjax();
        }
    });

}

function rechazarCheque(IdCheque) {

    var params = '?IdCheque=' + IdCheque +
            '&';

    rechazar = $('#bandejaChequesInfo').data('rechazar');
    var token = $("*[name=__RequestVerificationToken]").val();

    var url = rechazar + params;

    $.ajax({
        url: url,
        data:
        {
            __RequestVerificationToken: token,
            IdCheque: IdCheque
        },
        type: 'POST',
        async: false,
        success: function (data) {

            if (data.status == "1") {

                $('#modalCheques').modal('hide')
            }
            else {
                popupGenerico("error", "Error", data.msg);
            }
        },
        error: function () {
            onErrorAjax();
        }
    });
}

function vistaCheques(IdOperacionInversor) {

    var params = '?IdOperacionInversor=' + IdOperacionInversor +
					 '&';

    searchCheques = $('#bandejaChequesInfo').data('search-url');

    var dataManager = ej.DataManager({
        url: searchCheques + params
    });

    $("#bandejaCheque").ejGrid('dataSource', dataManager);



    $('#modalCheques').modal('show')


}

//function printOperacion(id) {
//    $.ajax({
//        url: 'ReporteDeposito',
//        type: "GET",
//        dataType: "html",
//        data: { idoperacion: encodeURI(id) },
//        cache: false,
//        success: function (data) {
//            var printWindow = window.open('', '', 'height=400,width=800');
//            printWindow.document.write(data);
//            printWindow.document.close();
//            printWindow.print();
//            printWindow.close();
//        },
//        error: function (xhr, status, error) {
//            alert(xhr.responseText);
//        }
//    });

//}

var searchUrl = '';

$(function () {
    searchUrl = $('#bandejaOperacionInfo').data('search-url');



    $('#btnSearch').click(function () {



        var nombre;
        if ($("#acInversor").ejGrid().val().split('-')[1] !== undefined) {
            nombre = $("#acInversor").ejGrid().val().split('-')[1].trim();
        } else {
            nombre = '';
        }
        //var nombre = $("#acInversor").ejGrid().val().split('-')[1].trim();


        var params = '?inversor=' + nombre +
                      '&' +
                     'fechaDesde=' + $('#txtFechaDesde').val() +
					 '&' +
                     'fechaHasta=' + $('#txtFechaHasta').val() +
                     '&' +
                     'tipoMoneda=' + $('#txtTipoMoneda').val() +
                     '&' +
                     'condicion=' + $('#txtCondicion').val() +
                     '&' +
                     'idEstado=' + $('#IdEstadoFiltro').val() +
                     '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#bandejaOperaciones").ejGrid('dataSource', dataManager);

        getDataInversor($('#IdInversor').val(), $('#txtTipoMoneda').val());

    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });
        $("#bandejaOperaciones").ejGrid('dataSource', dataManager);
        CleanContainerFields();

        $('#txtTipoMoneda').val(1);
        $('#IdEstadoFiltro').val("");
        $('#txtCondicion').val("");

        limpiarGrilla();
    });
});

function limpiarGrilla() {
    $("#label-nombre").text("");
    $("#label-cuit").text("");
    $("#label-numero").text("");
    $("#label-saldo").text("");
}

function getDataInversor(idInversor, tipoMoneda) {
    showOverlay();
    var securityToken = $('[name=__RequestVerificationToken]').val();
    $.ajax({
        url: '/BandejaOperacion/GetDataInversor',  //Server script to process data
        type: 'POST',
        data: { idInversor: idInversor, tipoMoneda: tipoMoneda, __RequestVerificationToken: securityToken },
    }).done(function (data) {
        if (data.success) {

            $("#label-nombre").text(data.nombre);
            $("#label-cuit").text("N°" + data.cuit);
            $("#label-numero").text(data.numero);
            $("#label-saldo").text("$" + data.saldo);
            hideOverlay();
        }
        else {
            onErrorAjax();
        }
    }).error(function () {
        onErrorAjax();
    });



}

function reloadGrid() {
    $("#bandejaOperaciones").ejGrid("refreshContent");
}

//function queryCellInfo(args) {
//    if (args.column.field == "ClasificacionMovimientoStr") {
//        if (args.data.ClasificacionMovimiento == 1)
//            $($(args.cell).parent()).css("backgroundColor", "#DED9A2").css("color", "black");

//    }
//}

$(document).ready
    (
        function () {
            var dateDesde = $('#txtFechaDesde');
            dateDesde.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
                dateDesde.datepicker('hide');
            });

            var dateHasta = $('#txtFechaHasta');
            dateHasta.datepicker({ format: "dd/mm/yyyy" }).on('changeDate', function (ev) {
                dateHasta.datepicker('hide');
            });

            setInterval(function () { $('#btnSearch').click() }, 1000 * 60 * 10);

            $('#txtTipoMoneda').val(1);
            $('#IdEstadoFiltro').val("");
            $('#txtCondicion').val("");

            $("#IdEstadoFiltro option[value='1']").remove();

            $("#IdEstadoFiltro option[value='5']").remove();

            $("#IdEstadoFiltro option[value='6']").remove();

            $("#IdEstadoFiltro option[value='8']").remove();

            $("#IdEstadoFiltro option[value='4']").remove();
        }
    );



//function queryCellInfo(args) {
//    if (args.column.field == "ClasificacionMovimientoStr") {
//        if (args.data.ClasificacionMovimiento == 1)
//            $($(args.cell).parent()).css("backgroundColor", "#DED9A2").css("color", "black");


//    }
//}

//function ajaxOperacion(id, url, onCallback, confirm) {
//    var token = $("*[name=__RequestVerificationToken]").val();
//    if (confirm) {
//        confirmChangeStatus(function () { ajaxOperacion(id, url, onCallback, false); });
//        return false;
//    }
//    else {
//        $.ajax({
//            url: url,
//            data:
//            {
//                __RequestVerificationToken: token,
//                IDKey: id
//            },
//            type: 'POST',
//            async: false,
//            success: function (data) {
//                if (data.success) {
//                    onCallback(data);
//                }
//                else {
//                    popupGenerico("error", "Error", data.msg);
//                }
//            },
//            error: function () {
//                onErrorAjax();
//            }
//        });
//    }
//}

//function redirectOperacion(id, url) {
//    var form = $("<form/>",
//                { action: url, method: "post" }
//           );
//    $(form).append("<input type='hiden' value='" + $("*[name=__RequestVerificationToken]").val() + "' name='__RequestVerificationToken'/>")
//    $(form).append("<input type='hiden' value='" + encodeURI(id) + "' name='IDKey'/>")
//    $(form).append("<input type='hiden' value='bandejaoperador' name='Url'/>")
//    $(form).submit();
//}

//function confirmChangeStatus(funct) {
//    confirm("Confimación", "¿Esta seguro que desea cambiar el estado de la operación?", funct);
//    return false;
//}