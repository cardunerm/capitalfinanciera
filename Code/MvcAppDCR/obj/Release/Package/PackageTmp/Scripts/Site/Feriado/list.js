﻿var searchUrl = '';

$(function () {
    searchUrl = $('#FeriadoInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?fechaStr=' + $('#txtFecha').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#FeriadoGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#FeriadoGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });
});