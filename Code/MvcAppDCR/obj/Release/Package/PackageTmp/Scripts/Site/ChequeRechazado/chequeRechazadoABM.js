﻿
var searchUrl = '';
//ej.widget.init();

$(document).ready(function () {

    searchUrl = $('#chequeRechazadoinfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?numero=' + $('#txtNumero').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#gridcheques").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });
        $("#gridcheques").ejGrid('dataSource', dataManager);
        CleanContainerFields();
    });

});

function createChequeRechazado() {
    var json = [];
    var obj, value;

    $("input:radio:checked").each(function () {
        obj = {
            IdCheque: this.id
        };

        json.push(obj);
    });

    $('#cheques').val(JSON.stringify(json));
}

function onSuccessSendChequeRechazado(data) {
    if (data.success) {
        document.location.href = data.url;
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
}
function queryCellInfo(args) {
    if (args.column.field == "EstadoStr") {
        if (args.data.Estado == 1)
            $($(args.cell).parent()).css("backgroundColor", "#DED9A2").css("color", "black");
        if (args.data.Estado == 2 || args.data.Estado == 3)
            $($(args.cell).parent()).css("backgroundColor", "#9EDEA5").css("color", "black");
        if (args.data.Estado == 4)
            $($(args.cell).parent()).css("backgroundColor", "#DC9696").css("color", "black");
    }
}