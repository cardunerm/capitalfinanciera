﻿
var searchUrl = '';

function createChequeRechazado() {
    var json = [];
    var obj, value;

    $("input:radio:checked").each(function () {
        obj = {
            IdCheque: this.id
        };

        json.push(obj);
    });

    $('#cheques').val(JSON.stringify(json));
}

function cancelPopUp() {
    modalDisplay("#modal-imagen-cheque", true);
}
function verChequeRechazado(idCheque) {
    //abrir popup
    modalDisplay("#modal-imagen-cheque", false);
    var securityToken = $('[name=__RequestVerificationToken]').val();
    //cargar imagenes   
    $.ajax({
        url: '/Cheque/GetCheque',  
        type: 'POST',
        data:
             {
                 __RequestVerificationToken: securityToken,
                 id: idCheque
             },       
    }).done(function (data) {
        if (data.success) {
            if (data.ImagenFront !== null)
                $('#imgCheque1').prop('src', '/UploadImages/Cheques/' + data.ImagenFront);
            else
                $('#imgCheque1').prop('src', '/UploadImages/Cheques/no-Imagen.png');

            if (data.ImagenBack !== null)
                $('#imgCheque2').prop('src', '/UploadImages/Cheques/' + data.ImagenBack);
            else
                $('#imgCheque2').prop('src', '/UploadImages/Cheques/no-Imagen.png');
            hideOverlay();
        }
        else {
            $('#imgCheque1').prop('src', '/UploadImages/Cheques/no-Imagen.png');
            $('#imgCheque2').prop('src', '/UploadImages/Cheques/no-Imagen.png');
            hideOverlay();
        }
    });   
}

$(function () {
    searchUrl = $('#chequeInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?numero=' + $('#txtNumero').val() + '&banco=' + $('#ddlBanco').val() + '&estado=' + $('#ddlEstado').val() + '&';
        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#chequeGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });
        $("#chequeGrid").ejGrid('dataSource', dataManager);
        CleanContainerFields();
    });
});

function onSuccessSendChequeRechazado(data) {
    if (data.success) {
        document.location.href = data.url;
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
}

function showModalRechazoCheque(idCheque) {
    $('#IdCheque').val(idCheque);
    $('#modalRechazoCheque').modal('show');
}

function ActualizarGrilla() {
    $('#modalRechazoCheque').modal('hide');
}