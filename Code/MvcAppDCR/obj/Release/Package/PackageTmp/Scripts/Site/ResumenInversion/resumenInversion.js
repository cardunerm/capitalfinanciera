﻿
var searchUrl = '';

$(function () {

    Date.prototype.withoutTime = function () {
        var d = new Date(this);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    searchUrl = $('#resumenInversionInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?IdTipoMoneda=' + $('#ddlTipoMoneda').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#ResumenInversionGrid").ejGrid('dataSource', dataManager);
    });

    //$('#btnRestore').click(function () {
    //    var dataManager = ej.DataManager({
    //        url: searchUrl
    //    });

    //    $("#ProyeccionFinancieraGrid").ejGrid('dataSource', dataManager);

    //    CleanContainerFields();
    //});

    //$("#IdCuentaCorrienteInversor").change(function () {
    //    $("#ProyeccionFinancieraGrid").ejGrid("option", { dataSource: [] });
    //    $("#ProyeccionFinancieraGrid").ejGrid("instance").refreshContent();
    //});

});


var tmpclient = "";

function autocompleteInversorSelectEvent(args) {
    $("#IdInversor").val(args.key);
    tmpclient = args.value;

    getCuentasCorrientes(args.key);

}

function autocompleteClientLostFocustEvent(args) {
    if (args.value !== tmpclient) {
        $("#IdInversor").val("");
        $("#acInversor").val("");
        tmpclient = "";
    }
}

function getCuentasCorrientes(idInversor, dontUseOverlay) {
    var url = '/DepositoEfectivo/GetCuentasCorrientesByIdInversor';

    if (dontUseOverlay === undefined || dontUseOverlay === null) {
        showOverlay();
    }

    $.ajax({
        type: "GET",
        url: url,
        data: { idInversor: idInversor },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }

            if (result.success) {
                var dropdownCuentas = $('#IdCuentaCorrienteInversor');
                dropdownCuentas.empty();

                $.each(result.data, function (i, cuenta) {
                    $('<option>', {
                        value: cuenta.Id
                    }).html(cuenta.NumeroTipo).appendTo(dropdownCuentas);
                });
            } else {
                popupGenerico("error", "Error", result.msg);
            }
            $("#ProyeccionFinancieraGrid").ejGrid("option", { dataSource: [] });
            $("#ProyeccionFinancieraGrid").ejGrid("instance").refreshContent();
        },
        error: function () {
            if (dontUseOverlay === undefined || dontUseOverlay === null) {
                hideOverlay();
            }
            popupGenerico("error", "Error", "Error al obtener las cuentas");
            $("#ProyeccionFinancieraGrid").ejGrid("option", { dataSource: [] });
            $("#ProyeccionFinancieraGrid").ejGrid("instance").refreshContent();
        }
    });
}

function querycellinfo(args) {

    var today = new Date();

    if (args.column.field == "Fecha" && (args.data.Fecha.withoutTime().toString() == today.withoutTime().toString()))
    {
        $($(args.cell).parent()).css("backgroundColor", "#428bca");
        $($(args.cell).parent()).css("color", "#ffffff");
    }
        

}


function VerDetalle(fecha) {
  
    $('#hdFecha').val(fecha);
    $('#ModalVerDetalle').modal('show');


}


function BuscarDetalle() {
    
    var _fecha = $('#hdFecha').val();
    var _idInversor = $('#ddlInversores option:selected').val();
    var _tipoMoneda = $('#ddlTipoMoneda option:selected').val();
   
    var url = searchUrl = $('#resumenInversionInfo').data('searchinversor-url');
    $.ajax({
        url: url,
        data: { idTipomoneda: _tipoMoneda, IdInversor: _idInversor, Fecha: _fecha},
        dataType: "json",
        success: function (result) {

            if (result.status) {
                $("#verdetalleGrid").ejGrid("refreshContent");
            } else {
                popupGenerico("error", "Error", result.msg);
            }
        },
        error: function () {
            popupGenerico("error", "Error", "Error al obtener las cuentas");
        }
    });
}