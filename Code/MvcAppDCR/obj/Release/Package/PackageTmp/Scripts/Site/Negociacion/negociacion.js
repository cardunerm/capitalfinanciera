﻿
var searchUrl = '';
var searchUrlDetalle = '';
var idsCheques = new Array();
var _idNegociacion = 0;

$(function () {

    searchUrl = $('#negociacioninfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?tiponegociacion=' + $('#ddlTipoNegociacion option:selected').text() +
					 '&banco=' + $('#ddlBanco option:selected').text() +
                     '&mayorista=' + $('#ddlMayorista option:selected').text() +
                     '&proveedor=' + $('#ddlProveedor option:selected').text() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#negociacionGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var dataManager = ej.DataManager({
            url: searchUrl
        });
        $("#negociacionGrid").ejGrid('dataSource', dataManager);
        CleanContainerFields();
    });




    $('#btnSearchCheque').click(function () {
       
        searchUrlDetalle = $('#negociacionDetalleinfo').data('searchcheque-url');

        var params = '?numero=' + $('#txtNumero').val() + '&';

        var dataManager = ej.DataManager({
            url: searchUrlDetalle + params
        });

        $("#chequesGrid").ejGrid('dataSource', dataManager);


    });
    

   

    

});

function onSuccessSendChequeRechazado(data) {
    if (data.status == 1) {
        document.location.href = data.url;
    }
    else {
        hideOverlay();
        popupGenerico("error", "Error", data.msg);
    }
}


function seleccionarTipoNegociacion(value)
{
    var id = $('#IdTipoOperacion').val();

    if(id== 1)
    {
        $('#divBanco').show();
       // $('#divMayorista').hide();
        $('#divProveedor').hide();
    }

    if (id == 2)
    {
        $('#divBanco').hide();
       // $('#divMayorista').hide();
        $('#divProveedor').show();
    }

    if (id == 3)
    {
        $('#divBanco').hide();
        //$('#divMayorista').show();
        $('#divProveedor').hide ();
    }

}


function SeleccionarCheque()
{
 
    $('#modal-cheque').modal({
        show: 'true'
    });

}


function ActualizarGrillaChequeSeleccionados()
{
    searchUrlDetalle = $('#negociacionDetalleinfo').data('search-url');
    var dataManager = ej.DataManager({
        url: searchUrlDetalle
    });

    $("#negociacionDetalleGrid").ejGrid('dataSource', dataManager);
}

function GuardarSeleccionCheque()
{

    showOverlay();
    var Id = idsCheques;
    $.ajax({
        url: '/Negociacion/AgregarCheque',  //Server script to process data
        type: 'POST',
        data: {
            IdsCheque: Id
        },
    }).done(function (data) {
        hideOverlay();
        $('#modal-cheque').modal('hide');
        ActualizarGrillaChequeSeleccionados();
    }).error(function () {
        onErrorAjax();
    });

}



function CheckRow(Id)
{

    if (idsCheques.indexOf(Id) == -1)
        idsCheques.push(Id);
    else
    {
        var posicion = idsCheques.indexOf(Id)
        idsCheques.splice(posicion, 1);
    }


}



function ViewCancelarPopup(Id)
{    
    _idNegociacion = Id;
    $('#modal-cancelarnegociacion').modal('show');
}

function CancelarNegociacion(tipo)
{

    if(tipo == 1)
    {    showOverlay();
  
    $.ajax({
        url: '/Negociacion/CancelarNegociacion',  //Server script to process data
        type: 'POST',
        data: {
            IdNegociacion: _idNegociacion
        },
    }).done(function (data) {
        hideOverlay();     
        $('#btnSearch').click();
        $('#modal-cancelarnegociacion').modal('hide');

    }).error(function () {
        onErrorAjax();
    });
    }else{
        $('#modal-cancelarnegociacion').modal('hide');
    }


}

       



function ViewCerrarNegociacion(Id,Valor) {

    $('#lblnumeronegociacion').text(Id);
    $('#txtMontoAprobado').val(Valor);    
    $('#modal-cierrenegociacion').modal('show');
  


}

function CerrarNegociacion() {

    showOverlay();

    var id = $('#lblnumeronegociacion').text();
    var monto = $('#txtMontoAprobado').val();

    $.ajax({
        url: '/Negociacion/CerrarNegociacion',  //Server script to process data
        type: 'POST',
        data: {
            IdNegociacion: id,
            Monto: monto
        },
    }).done(function (data) {
        hideOverlay();
        $('#modal-cierrenegociacion').modal('hide');
        $('#btnSearch').click();

    }).error(function () {
        onErrorAjax();
    });
}



function DeleteChequeSeleccionado(Id)
{

    showOverlay();

   

    $.ajax({
        url: '/Negociacion/DeleteChequeSeleccionado',  //Server script to process data
        type: 'POST',
        data: {
            IdNegociacionDetalle: Id         
        },
    }).done(function (data) {
        hideOverlay();       
        ActualizarGrillaChequeSeleccionados();

    }).error(function () {
        onErrorAjax();
    });

}