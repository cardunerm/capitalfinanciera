﻿var OVERLAY = $('<div id="overlay" class="e-waitpopup-pane e-widget"><div><span class="e-image"></span><p id="msgoverlay"></p></div></div>');

//var dualListBoxSettings = {
//	filterTextClear: 'mostrat todo',
//	filterPlaceHolder: 'Filtro',
//	moveSelectedLabel: 'Mover seleccionados',
//	moveAllLabel: 'Mover todos',
//	removeSelectedLabel: 'Remover seleccionados',
//	removeAllLabel: 'Remover todos',
//	selectedListLabel: 'Seleccionados',
//	removeAllLabel: 'Remover todos',
//	nonSelectedListLabel: 'No seleccionados',
//	infoText: 'Mostrando todos - {0}',
//	infoTextFiltered: '<span class="label label-warning">Filtrado</span> {0} de {1}',
//	infoTextEmpty: 'Lista vacía',
//	moveOnSelect: false,
//	showFilterInputs: false
//};

$(function () {
    $('.tools.pull-right').children('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');

    $.validator.methods.date = function (value, element) {
        Globalize.culture("es-AR");
        // you can alternatively pass the culture to parseDate instead of
        // setting the culture above, like so:
        // parseDate(value, null, "es-AR")
        return this.optional(element) || Globalize.parseDate(value) !== null;
    }

    /*
    * Boton para mostrar u ocultar los filtros en los formularios de busqueda
    */

    // $("#standarTemplateCommand").html($("#standarTemplateCommand").html().replace("template__", $("#test").html()));

    $('.date-picker').datepicker({
        format: 'dd/mm/yyyy'
    }).on('changeDate', function (e) {
        $(this).datepicker('hide');
    });

    $('.time-picker').timepicker({
        minuteStep: 1
    }).on('changeDate', function (e) {
        $(this).datepicker('hide');
    });

    $('.btn-toggle').click(function () {
        $(this).find('.btn').toggleClass('active');

        if ($(this).find('.btn-primary').size() > 0) {
            $(this).find('.btn').toggleClass('btn-primary');
        }

        if ($(this).find('.btn-danger').size() > 0) {
            $(this).find('.btn').toggleClass('btn-danger');
        }

        if ($(this).find('.btn-success').size() > 0) {
            $(this).find('.btn').toggleClass('btn-success');
        }

        if ($(this).find('.btn-info').size() > 0) {
            $(this).find('.btn').toggleClass('btn-info');
        }

        $(this).find('.btn').toggleClass('btn-default');
    });
});

function onBeginAjax() {
    showOverlay();
}

function onSuccessAjax(data, status, xhr) {
    debugger;
    hideOverlay();

    if (status == 'success') {
        if (data.status == '1' && data.url != null && data.url != undefined) {
            window.location.href = data.url;
        }
        else {
            $(data.error).html(data.msg);
        }
    }
}

function onErrorAjax() {
    hideOverlay();
    popupError("Error", "Se ha producido un error al realizar la operación");
}

/*
* Overlay functions
*/

function showOverlay() {
    OVERLAY.appendTo(document.body);
    $('#msgoverlay').text(msgoverlay);
}

function hideOverlay() {
    OVERLAY.remove();
}

/*
* Popup de información
*/
function popupInfo(titulo, mensaje) {
    (!titulo || titulo == undefined) ? titulo = $('#masterInfo').data('information') : '';

    popupGenerico('info', titulo, mensaje);
}

/*
* Popup de información con callback
*/
function popupInfoCallback(titulo, mensaje, callback, data) {
    (!titulo || titulo == undefined) ? titulo = $('#masterInfo').data('information') : '';

    var divModal = popupGenerico('info', titulo, mensaje);

    divModal.find('#btnAcceptDialog').click(function () {
        callback ? callback(data) : '';
        divModal.modal('hide');
    });
}

/*
* Popup de información
*/
function popupAdvertencia(titulo, mensaje) {
    (!titulo || titulo == undefined) ? titulo = $('#masterInfo').data('warning') : '';

    popupGenerico('warning', titulo, mensaje);
}

/*
* Popup de error
*/
function popupError(titulo, mensaje) {
    (!titulo || titulo == undefined) ? titulo = $('#masterInfo').data('error') : '';
    (mensaje == undefined || mensaje == null) ? mensaje = $('#masterInfo').data('an-error-has-occurred') : '';

    popupGenerico('error', titulo, mensaje);
}

function popupGenerico(tipo, titulo, mensaje) {
    if (tipo != 'info' && tipo != 'warning' && tipo != 'error')
        tipo = 'info';

    var divModal = $('<div class="modal fade" id="popupAlerta" tabindex="-1" role="dialog" aria-hidden="true" style="overflow: hidden;">' +
                '<div class="modal-dialog" style="width: 350px;">' +
                  '<div class="modal-content">' +
                    '<div class="modal-header">' +
                      '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                      '<h5 class="modal-title">' + titulo + '</h5>' +
                    '</div>' +
                    '<div class="modal-body">' +
                        '<div style="display: inline-block"><img src="' + $('#masterInfo').data('content-url') + 'Images/' + tipo + '.png" alt="alerta" style="height: 40px;" /></div>' +
                        '<div style="display: inline-block;padding-left: 20px; width: 240px;">' + mensaje + '</div>' +
                        '<div class="clear"></div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                      '<button type="button" class="btn btn-primary" id="btnAcceptDialog" data-dismiss="modal">' + $('#masterInfo').data('accept') + '</button>' +
                    '</div>' +
                  '</div>' +
                '</div>' +
              '</div>');

    divModal.modal('show');

    return divModal;
}

function deleteRegister(data) {
    $(data.form).submit();
}

function deleteResult(data, btn) {
    hideOverlay();

    if (data.status == 1) {
        filterTable(btn);
    }
    else {
        popupError(null, data.msg);
    }
}

function filterTable(btn) {
    hideOverlay();
    $(btn).click();
}

function confirm(titulo, mensaje, callback, data) {
    var confirmModal = $('<div class="modal fade" id="popupConfirmacion" tabindex="-1" role="dialog" aria-hidden="true" style="overflow: hidden;">' +
                '<div class="modal-dialog" style="width: 350px;">' +
                  '<div class="modal-content">' +
                    '<div class="modal-header">' +
                      '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                      '<h5 class="modal-title">' + titulo + '</h5>' +
                    '</div>' +
                    '<div class="modal-body" style="height:100px">' +
                        '<div class="fl" style="display: inline;width:20%;float:left"><img src="' + $('#masterInfo').data('content-url') + 'Images/warning.png" alt="alerta" style="height: 40px;" /></div>' +
                        '<div class="fl" style="padding-left: 20px;float:right; width: 80%;display: inline;">' + mensaje + '</div>' +
                        '<div class="clear"></div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                      '<button id="confirmBtn" type="button" class="btn btn-primary">' + $('#masterInfo').data('accept') + '</button>' +
                      '<button id="cancelBtn" type="button" data-dismiss="modal" class="btn btn-default" data-dismiss="modal">' + $('#masterInfo').data('cancel') + '</button>' +
                    '</div>' +
                  '</div>' +
                '</div>' +
              '</div>');

    confirmModal.find('#confirmBtn').click(function () {
        callback ? callback(data) : '';
        confirmModal.modal('hide');
    });

    confirmModal.modal('show');
}

function confirmCancelCallback(titulo, mensaje, callback, data, cancelCallback, cancelData) {
    var confirmModal = $('<div class="modal fade" id="popupConfirmacion" tabindex="-1" role="dialog" aria-hidden="true" style="overflow: hidden;">' +
                '<div class="modal-dialog" style="width: 350px;">' +
                  '<div class="modal-content">' +
                    '<div class="modal-header">' +
                      '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                      '<h5 class="modal-title">' + titulo + '</h5>' +
                    '</div>' +
                    '<div class="modal-body" style="height:100px">' +
                        '<div class="fl" style="display: inline;width:20%;float:left"><img src="' + $('#masterInfo').data('content-url') + 'Images/warning.png" alt="alerta" style="height: 40px;" /></div>' +
                        '<div class="fl" style="padding-left: 20px;float:right; width: 80%;display: inline;">' + mensaje + '</div>' +
                        '<div class="clear"></div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                      '<button id="confirmBtn" type="button" class="btn btn-primary">' + $('#masterInfo').data('accept') + '</button>' +
                      '<button id="cancelBtn" type="button" data-dismiss="modal" class="btn btn-default" data-dismiss="modal">' + $('#masterInfo').data('cancel') + '</button>' +
                    '</div>' +
                  '</div>' +
                '</div>' +
              '</div>');

    confirmModal.find('#confirmBtn').click(function () {
        callback ? callback(data) : '';
        confirmModal.modal('hide');
    });

    confirmModal.find('#cancelBtn').click(function () {
        cancelCallback ? cancelCallback(cancelData) : '';
        confirmModal.modal('hide');
    });

    confirmModal.modal('show');
}

/* Deshabilita todos los campos de un formulario */
function DisableFields(idForm) {
    $('#' + idForm).find('input, textarea, button, select, a, span').attr('disabled', true);
}

/* Habilita todos los campos de un formulario */
function EnableFields(idForm) {
    $('#' + idForm).find('input, textarea, button, select, a, span').attr('disabled', false);
}

function CleanContainerFields() {
    $('.panel-body').find('input, textarea').val('');
    $('.panel-body').find('select').val(-1);

    if ($('.panel-body').find('input.e-dropdownlist').length > 0) {
        $('.panel-body').find('input.e-dropdownlist').data('ejDropDownList').unCheckAll()
    }
}

function ShowChangePassword() {
    var changeModal = $('<div class="modal fade" id="popupChangePassword" tabindex="-1" role="dialog" aria-hidden="true" style="overflow: hidden;">' +
							'<div class="modal-dialog" style="width: 350px;">' +
								'<div class="modal-content">' +
								'<div class="modal-header">' +
									'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
									'<h5 class="modal-title">' + $('#masterInfo').data('change-password-text') + '</h5>' +
								'</div>' +
								'<div class="modal-body">' +
									'<div class="fl" style="padding-left: 20px; width: 240px;">' +
										'<label name="lbNewPassword_changen" id="lbNewPassword_changen"class="modal-title"> Contraseña Actual<label/>' +
									'</div>'
                                +
									'<div class="fl" style="padding-left: 20px; width: 240px;">' +
										'<input autocomplete="new-password" type="password" name="txtActualPassword_changen" id="txtActualPassword_changen"class="form-control" maxlength="32" pattern=".{6,}" value=""  />' +
									'</div>' +
									'<div class="clear"></div>'
                                    +
									'<div class="fl" style="padding-left: 20px; width: 240px;">' +
										'<label name="lbNewPassword_changen" id="lbNewPassword_changen"class="modal-title"> Contraseña Nueva<label/>' +
									'</div>'
                                +
									'<div class="fl" style="padding-left: 20px; width: 240px;">' +
										'<input type="password" name="txtNewPassword_changen" id="txtNewPassword_change"class="form-control" maxlength="32" pattern=".{6,}" value=""  />' +
									'</div>' +
									'<div class="clear"></div>'
                                    +
									'<div class="fl" style="padding-left: 20px; width: 240px;">' +
										'<label name="lbNewPassword_changen" id="lbNewPassword_changen"class="modal-title"> Confirmar Contraseña Nueva<label/>' +
									'</div>'
                                +
									'<div class="fl" style="padding-left: 20px; width: 240px;">' +
										'<input type="password" name="txtNewPasswordConfirm_changen" id="txtNewPasswordConfirm_changen"class="form-control" maxlength="32" pattern=".{6,}" value=""  />' +
									'</div>' +
									'<div class="clear"></div>' +
								'</div>'
                                +
								'<div class="modal-footer">' +
									'<button id="btConfirmChangePassword" type="button" class="btn btn-primary" onclick="ConfirmChangePassword();">' + $('#masterInfo').data('change-password-confirm') + '</button>' +
									'<button id="btCancelChangePassword" type="button" data-dismiss="modal" class="btn btn-default" data-dismiss="modal" onclick="">' + $('#masterInfo').data('change-password-cancel') + '</button>' +
								'</div>' +
								'</div>' +
							'</div>' +
						'</div>');

    changeModal.modal('show');
}

function ConfirmChangePassword() {
    if ($('#txtNewPassword_change').val().trim() != '' && $('#txtActualPassword_changen').val().trim() != '' && $('#txtNewPasswordConfirm_changen').val().trim() != '') {
        if ($('#txtNewPassword_change').val().length < 6 || $('#txtNewPasswordConfirm_changen').val().length < 6) {
            popupAdvertencia(null, "La contraseña debe tener 6 caracteres como minimo");
        }
        else {
            $.ajax({
                url: $('#masterInfo').data('change-password-url'),
                data: JSON.stringify(
                {
                    actual: $('#txtActualPassword_changen').val(),
                    contrasenia: $('#txtNewPassword_change').val(),
                    confirmacion: $('#txtNewPasswordConfirm_changen').val()
                }),
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                success: function (data) {
                    if (data.status == 1) {
                        popupAdvertencia(null, $('#masterInfo').data('success-change-message'));
                        $('#btCancelChangePassword').click();
                    }
                    else {
                        popupError(null, data.msg);
                    }
                }
            });
        }
    }
    else {
        popupAdvertencia(null, $('#masterInfo').data('required'));
    }
}

function DownloadFile(url) {
    var iframeHtml = '<iframe src="' + url + '"></iframe>';

    $('#divDownloadFileFrame').html(iframeHtml);
}

function ShowChangeOperationBranch() {
    $.ajax({
        url: $('#masterInfo').data('get-user-operation-branches-url'),
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        async: false,
        success: function (data) {
            if (data.status == 1) {
                var branches = '';

                $.each(data.Options, function (i, o) {
                    branches += '<option value="' + o.Value + '">' + o.Text + '</option>';
                });

                var changeModal = $('<div class="modal fade" id="popupChangeOperationBranch" tabindex="-1" role="dialog" aria-hidden="true" style="overflow: hidden;">' +
										'<div class="modal-dialog" style="width: 350px;">' +
											'<div class="modal-content">' +
											'<div class="modal-header">' +
												'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
												'<h5 class="modal-title">' + $('#masterInfo').data('change-operation-branch-text') + '</h5>' +
											'</div>' +
											'<div class="modal-body">' +
												'<div class="fl" style="padding-left: 20px; width: 240px;">' +
													'<select name="ddlOperationBranch_change" id="ddlOperationBranch_change" class="form-control">' + branches + '</select>' +
												'</div>' +
												'<div class="clear"></div>' +
											'</div>' +
											'<div class="modal-footer">' +
												'<button id="btConfirmChangeOperationBranch" type="button" class="btn btn-primary" onclick="ConfirmChangeOperationBranch();">' + $('#masterInfo').data('change-operation-branch-confirm') + '</button>' +
												'<button id="btCancelChangeOperationBranch" type="button" data-dismiss="modal" class="btn btn-default" data-dismiss="modal" onclick="">' + $('#masterInfo').data('change-operation-branch-cancel') + '</button>' +
											'</div>' +
											'</div>' +
										'</div>' +
									'</div>');

                changeModal.modal('show');
            }
            else {
                popupError(null, data.msg);
            }
        }
    });
}

function ConfirmChangeOperationBranch() {
    $.ajax({
        url: $('#masterInfo').data('change-operation-branch-url'),
        data: JSON.stringify(
		{
		    idSucursal: $('#ddlOperationBranch_change').val()
		}),
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        async: false,
        success: function (data) {
            if (data.status == 1) {
                window.location = '';
            }
            else {
                popupError(null, data.msg);
            }
        }
    });
}

function ValidateNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 9 ||
		event.keyCode == 13 || event.keyCode == 37 ||
		event.keyCode == 39 || event.keyCode == 46 ||
		key == 8 || key == 9 ||
		key == 13 || key == 37 ||
		key == 39 || key == 46) {
        return true;
    }
    else if (key < 48 || key > 57) {
        return false;
    }
    else {
        return true;
    }
}

function ValidateOnlyNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 9 ||
		event.keyCode == 13 || event.keyCode == 37 ||
		event.keyCode == 39 ||
		key == 8 || key == 9 ||
		key == 13 || key == 37 ||
		key == 39) {
        return true;
    }
    else if (key < 48 || key > 57) {
        return false;
    }
    else {
        return true;
    }
}
function modalDisplay(id, hide) {
    if (hide)
        $(id).modal('hide');
    else
        $(id).modal('show');
}


$(document).ready
    (
       setMaxlength
    );

function setMaxlength() {
    $("input[data-val-length-max]").each
        (
            function () {
                var ctr = $(this);
                ctr.attr("maxlength", ctr.attr("data-val-length-max"));
            }
        );

    $("textarea[data-val-length-max]").each
        (
            function () {
                var ctr = $(this);
                ctr.attr("maxlength", ctr.attr("data-val-length-max"));
            }
        );
}

var dateTimeReviver = function (value) {
    var a;
    if (typeof value === 'string') {
        a = /\/Date\((\d*)\)\//.exec(value);
        if (a) {
            return new Date(+a[1]);
        }
    }
    return value;
}

$(
    $.fn.CallAjax = function (url, callback, data, config, error) {
        if (config === undefined)
            config = {
                method: "POST"
            };

        config.url = url;
        config.data = data;

        $.ajax(config)
            .done(
                function (data) {
                    if (callback !== undefined)
                        callback(data);
                }
            )
        .error(
             function (data) {
                 if (error !== undefined)
                     error(data);
             }
        );
    }

);

(
    $.fn.validateExtension = function (filename) {
        var ext = (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
        return ext !== undefined && (ext.toString().toLowerCase() === "jpg" || ext.toString().toLowerCase() === "png");
    }

);