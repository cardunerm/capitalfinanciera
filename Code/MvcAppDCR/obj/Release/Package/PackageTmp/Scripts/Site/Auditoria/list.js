﻿var searchUrl = '';

$(function () {
    searchUrl = $('#auditoriaInfo').data('search-url');

    $('#btnSearch').click(function () {
        var params = '?idUsuario=' + $('#ddlUsuario').val() +
					 '&ipDomicilio=' + $('#txtIPDomicilio').val() +
					 '&accionValue=' + $('#ddlAccion').val() +
					 '&idModulo=' + $('#ddlModulo').val() +
					 '&fechaDesde=' + $('#dpFechaDesde').val() +
					 '&fechaHasta=' + $('#dpFechaHasta').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl + params
        });

        $("#auditoriaGrid").ejGrid('dataSource', dataManager);
    });

    $('#btnRestore').click(function () {
        var params = '?fechaDesde=' + $('#dpFechaDesde').val() +
					 '&fechaHasta=' + $('#dpFechaHasta').val() +
					 '&';

        var dataManager = ej.DataManager({
            url: searchUrl
        });

        $("#auditoriaGrid").ejGrid('dataSource', dataManager);

        CleanContainerFields();
    });

    $('#btnShowPDFReport').click(function () {
        var url = GetReportUrl($('#auditoriaInfo').data('pdf-format-value'));

        ShowReportModal(url, $('#auditoriaInfo').data('auditoria-nombre'));
    });

    $('#btnShowExcelReport').click(function () {
        var url = GetReportUrl($('#auditoriaInfo').data('excel-format-value'));

        DownloadExcelReport(url);
    });
});

function GetReportUrl(reportFormat) {
    var reportUrl = $('#auditoriaInfo').data('report-url');

    var params = '?idUsuario=' + $('#ddlUsuario').val() +
				 '&ipDomicilio=' + $('#txtIPDomicilio').val() +
				 '&accionValue=' + $('#ddlAccion').val() +
				 '&idModulo=' + $('#ddlModulo').val() +
				 '&fechaDesde=' + $('#dpFechaDesde').val() +
				 '&fechaHasta=' + $('#dpFechaHasta').val() +
				 '&formatoReporte=' + reportFormat;

    var urlReport = reportUrl + params;

    return urlReport;
}