﻿//Load attachments modal
function UploadModal(dataid, urlstr) {
    var url = urlstr
    var id = dataid;
    $.get(url + '/' + id, function (data) {
        $('#uploadModalContainer').html(data);
        $('#uploadFilesModal').modal('show');
    });
}

//Success attachments modal
function success(data) {
    $('#uploadFilesModal').modal('hide');
    $('#uploadModalContainer').html("");

    popupGenerico("info", "Información", data.msg);
}