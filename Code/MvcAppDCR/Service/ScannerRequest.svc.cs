﻿using System;
using System.Configuration;
using System.IO;
using System.Web;

namespace MvcAppDCR.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ScannerRequest" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ScannerRequest.svc or ScannerRequest.svc.cs at the Solution Explorer and start debugging.
    public class ScannerRequest : IScannerRequest
    {
        public ScannerResult SaveImage(string front, string back)
        {
            var frontName = CreateImage(front, "front");
            var backName = CreateImage(back, "back");

            return new ScannerResult
            {
                BackUrl = VirtualPathUtility.ToAbsolute("~/UploadImages/Cheques/") + backName,
                FrontUrl = VirtualPathUtility.ToAbsolute("~/UploadImages/Cheques/") + frontName,
                FrontName = frontName,
                BackName = backName
            };
        }

        private string CreateImage(string imageBase64, string prefix)
        {
            try
            {
                if (!string.IsNullOrEmpty(imageBase64))
                {
                    var fileName = prefix + DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ".jpg";
                    byte[] imageBytes = Convert.FromBase64String(imageBase64);
                    var path = Path.Combine(ConfigurationManager.AppSettings["ImagesPath"], fileName);
                    System.IO.File.WriteAllBytes(path, imageBytes);
                    return fileName;
                }
            }
            catch
            { }
            return "no-Imagen.png";

        }
    }
}
