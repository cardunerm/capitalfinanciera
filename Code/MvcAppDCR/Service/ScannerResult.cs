﻿using System.Runtime.Serialization;

namespace MvcAppDCR.Service
{
    [DataContract]
    public class ScannerResult
    {
        [DataMember]
        public string FrontName { get; set; }
        [DataMember]
        public string BackName { get; set; }

        [DataMember]
        public string FrontUrl { get; set; }
        [DataMember]
        public string BackUrl { get; set; }
    }
}