﻿using Business;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controllers
{
    public class ChequeController : BaseController
    {
        //
        // GET: /Cheque/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetCheques(string numero = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = ChequeBusiness.Instance.GetByFilters(numero, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetChequesDisponiblesArqueo([Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = ChequeBusiness.Instance.GetDisponiblesArqueo(skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetChequesByFilters(string numero = "",[Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = ChequeBusiness.Instance.GetChequesByFilters(numero, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list.Result, count = list.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpGet]
        public ActionResult ABMCheque(long? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new Cheque());
            }

            var entity = ChequeBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMCheque(Cheque Cheque)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ChequeBusiness.Instance.SaveOrUpdateCheque(Cheque);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                ChequeBusiness.Instance.DeleteLogic(id, ChequeBusiness.Instance.ValidateDelete, AuditoriaBusiness.Instance.AuditoriaCheque);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCheque(int id)
        {
            try
            {
                var ImagenFront = ChequeBusiness.Instance.GetCheque(id).ImagenFront;
                var ImagenBack = ChequeBusiness.Instance.GetCheque(id).ImagenBack;

                return Json(new { success = true, ImagenFront = ImagenFront,ImagenBack =ImagenBack, status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
        //[HttpPost]
        //public JsonResult ValidateCheque(string nombre = "", string cuit = "")
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(nombre))
        //        {
        //            var entity = ChequeBusiness.Instance.ChequeExist(nombre, cuit);

        //            if (entity != null)
        //            {
        //                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true, Id = entity.IdCheque });
        //            }
        //            else
        //            {
        //                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
        //            }
        //        }
        //        else
        //        {
        //            return ExceptionManager.Instance.JsonError("Requerido");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return ExceptionManager.Instance.JsonError(ex);
        //    }
        //}

    }
}
