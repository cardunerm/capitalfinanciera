﻿using Business;
using Controllers;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class FacturaDeudoController : BaseController
    {
        //
        // GET: /Cliente/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetFacturaDeudos(string nombre = "", string cuit = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = FacturaDeudoBusiness.Instance.GetByFilters(nombre, cuit, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMFacturaDeudo(long? id, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
            ;
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new FacturaDeudo());
            }

            var entity = FacturaDeudoBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

       

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMFacturaDeudo(FacturaDeudo deudo, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);            
            try
            {
                if (ViewBag.PopUp)
                    ModelState.Remove("IdTasa");

                if (ModelState.IsValid)
                {
                    deudo.Activo = true;

                    //Valido si es una actualización o una nuevo
                    if (deudo.IdFacturaDeudo > 0)
                    {
                        FacturaDeudo _entidad = FacturaDeudoBusiness.Instance.GetEntity(long.Parse(deudo.IdFacturaDeudo.ToString()));
                        _entidad.Nombre = deudo.Nombre;
                        _entidad.CUIT = deudo.CUIT;
                        _entidad.Domicilio = deudo.Domicilio;
                        _entidad.Email = deudo.Email;
                        _entidad.TelefonoFijo = deudo.TelefonoFijo;
                        _entidad.TelefonoMovil = deudo.TelefonoMovil;
                        FacturaDeudoBusiness.Instance.SaveOrUpdate(_entidad);
                    }
                    else {
                        FacturaDeudoBusiness.Instance.SaveOrUpdate(deudo);
                    }
                 

                    return Json(
                        new
                        {
                            id = deudo.IdFacturaDeudo,
                            name = deudo.Nombre,
                            msg = Resources.Resource.SaveSuccess,
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                FacturaDeudo entidad = FacturaDeudoBusiness.Instance.GetEntity(id);
                entidad.Activo = false;
                FacturaDeudoBusiness.Instance.SaveOrUpdate(entidad);
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}