﻿using Business;
using CurrentContext;
using Domine;
using Luma.Utils;
using Luma.Utils.Attributes.Security;
using Luma.Utils.Logs;
using Luma.Utils.Security;
using Numero3.EntityFramework.Implementation;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcAppDCR.Utils;
using Resources;
using System;
using System.Threading;
using System.Globalization;
using MvcAppDCR.Context;

namespace Controllers
{
    [CustomAuthorize(typeof(UsuarioBusiness))]
    public class BaseController : Controller
    {
        private static DbContextScopeFactory factory = new DbContextScopeFactory();

        public CustomMembershipProvider<UsuarioBusiness> CustomMembership
        {
            get
            {
                return (CustomMembershipProvider<UsuarioBusiness>)Membership.Provider;
            }
        }


        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {

            //HttpCookie languageCookie = System.Web.HttpContext.Current.Request.Cookies["Language"];
            //if (languageCookie != null)
            //{
            //    Thread.CurrentThread.CurrentCulture = new CultureInfo(languageCookie.Value)
            //    {
            //        NumberFormat =
            //        {
            //            NumberDecimalSeparator = "."
            //        }
            //    };
            //    Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageCookie.Value)
            //    {
            //        NumberFormat =
            //        {
            //            NumberDecimalSeparator = "."
            //        }
            //    };
            //}
            //else
            //{
            //    Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR")
            //    {
            //        NumberFormat =
            //        {
            //            NumberDecimalSeparator = ","
            //        }
            //    };

            //    Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-AR")
            //    {
            //        NumberFormat =
            //        {
            //            NumberDecimalSeparator = ","
            //        }
            //    };
            //}
            
            //AppContext.Lang = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            
            base.Initialize(requestContext);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            InitContext(filterContext);

            if (SessionContext.Key == "")
            {
                SessionContext.Key = DateTime.Now.GetHashCode().ToString();
            }

            if (Request.IsAuthenticated)
            {
                var usuario = CustomMembership.GetUser(User.Identity.Name, true) as CustomMembershipUser;

                if (usuario != null)
                {
                    Controller controller = filterContext.Controller as Controller;
                    List<Pagina> list = PaginaBusiness.Instance.GetPaginasByUsuario(usuario.User as Usuario);
                    controller.ViewBag.Menu = list.Where(p => p.EsMenu);

                    if(SessionContext.Empresa == null)
                    {
                        SessionContext.Empresa = UsuarioBusiness.Instance.GetEntity<Empresa>((usuario.User as Usuario).IdEmpresa.Value);
                        SessionContext.Configuracion = SessionContext.Empresa.Configuracion.FirstOrDefault();
                    }

//#if DEBUG
//                    if (ValidarPaginas(list, controller))
//                    {
//                        filterContext.HttpContext.Response.Redirect(Url.Action("Error", "Access", new { errorType = Resource.Warning, message = Resource.HasNoPermisosForAccion }), true);
                        
//						return;
//                    }
//#endif
                }
            }
//#if DEBUG
//            else
//            {
//                CustomMembership.ValidateUser("admin", "123", 1);

//                //SessionContext.UsuarioActual = UsuarioBusiness.Instance.GetUsuario("admin","123",0);
//                SessionContext.UsuarioActual = (Usuario)CustomMembership.GetCurrentUser("admin").User;
//                SessionContext.Empresa  = UsuarioBusiness.Instance.GetEntity<Empresa>(1);
//                SessionContext.Configuracion = new Configuracion { Gastos = 0.8, ImpuestoCheque = 1.2 };
//                FormsAuthentication.RedirectFromLoginPage("admin", true);
//            }
//#endif

            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// REaliza el dispose del context cuando termina la ejecucion del request
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var ctx = filterContext.HttpContext.Items["ContextEF"];

            if (ctx != null)
            {
                (ctx as Numero3.EntityFramework.Interfaces.IDbContextScope).Dispose();
            }

            filterContext.HttpContext.Items.Remove("ContextEF");
        }

        private void InitContext(ActionExecutingContext filterContext)
        {
            var ctx = filterContext.HttpContext.Items["ContextEF"];

            if (ctx != null)
            {
                //(ctx as Numero3.EntityFramework.Interfaces.IDbContextScope).Dispose();
                //filterContext.HttpContext.Items.Remove("ContextEF");
                return;
            }

            var ctxFC = factory.Create(Numero3.EntityFramework.Interfaces.DbContextScopeOption.JoinExisting);
            filterContext.HttpContext.Items.Add("ContextEF", ctxFC);

            /// Dado que las grillas usan ajax y el context ya esta cerrado cuando quieren usar alguna property anidada lanza la exception. Para estos casos
            /// se habilita el lazy false , se debe tener cuidado con la performance , lo ideal es en las consultas traer las entidades necesarias y en un futuro deshabilitar
            /// esta opcion
            if (NeedToSerialitazion(filterContext.HttpContext.Request))
                DataAccess.DataAccessManager<DCREntities>.Instance.SetLazy(false);
        }

        /// <summary>
        /// Dado que las grillas usan ajax y el context ya esta cerrado cuando quieren usar alguna property anidada lanza la exception. Para estos casos
        /// se habilita el lazy false , se debe tener cuidado con la performance , lo ideal es en las consultas traer las entidades necesarias y en un futuro deshabilitar
        /// esta opcion
        /// </summary>
        /// <param name="httpRequest"></param>
        /// <returns></returns>
        private bool NeedToSerialitazion(HttpRequestBase httpRequest)
        {
            return httpRequest["$skip"] != null && httpRequest["$top"] != null;
        }

        private bool ValidarPaginas(List<Pagina> list, Controller controller)
        {
            return false;

            if (controller != null)
            {
                controller.ViewBag.Menu = list.Where(p => p.EsMenu);

                string path = this.Request.Url.AbsolutePath.Replace("/Index", "");
                path = path.Substring(1, path.Length - 1);
                path = path.Equals("") ? "Home" : path;

                string pathParams;
                string[] pathParts = this.Request.Url.PathAndQuery.Split('/');

                if (pathParts.Length > 3)
                {
                    pathParams = string.Format("{0}/{1}", pathParts[0], pathParts[1]);
                }
                else
                {
                    pathParams = this.Request.Url.PathAndQuery.Remove(0, 1);
                }

                if (PaginaBusiness.Instance.IsPagina(path) && !UsuarioBusiness.Instance.HasPermiso(path))
                {
                    if (PaginaBusiness.Instance.IsPagina(pathParams) && !UsuarioBusiness.Instance.HasPermiso(pathParams))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool TienePermiso(List<Pagina> paginas, string path)
        {
            if (paginas.Any(m => m.Url.Equals(path)))
            {
                return true;
            }

            return false;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                var codigo = LogManager.Log.RegisterError(filterContext.Exception);

                filterContext.Result = ExceptionManager.Instance.JsonError(string.Format("Error Inesperado", codigo));
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();
            }
            else
            {
                base.OnException(filterContext);
            }
        }
    }
}