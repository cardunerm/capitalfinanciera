﻿using CrystalDecisions.CrystalReports.Engine;
using Enumerations;
using Business;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Security;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Web.Mvc;
using Mvc.Utils;

namespace Controllers
{
    public class AuditoriaController : BaseController
    {
        //
        // GET: /Auditoria/

        public ActionResult Index()
        {
            ViewBag.Usuarios = WebHelper.GetSelectListItem(UsuarioBusiness.Instance.GetAllActives(), "IdUsuario", "NombreUsuario", true);
            ViewBag.Acciones = DCREnumerationsHelper.GetAuditActions(true);
            ViewBag.Modulos = WebHelper.GetSelectListItem(ModuloBusiness.Instance.GetAllActives(), "IdModulo", "Nombre", true);

            return View();
        }

        public JsonResult GetAuditoria(long idUsuario = 0, string ipDomicilio = "", int accionValue = 0, long idModulo = 0, string fechaDesde = "", string fechaHasta = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                DateTime tempDate;
                DateTime? dateS = null, dateE = null;

                if (!string.IsNullOrEmpty(fechaDesde) && DateTime.TryParse(fechaDesde, out tempDate))
                {
                    dateS = tempDate;
                }

                if (!string.IsNullOrEmpty(fechaHasta) && DateTime.TryParse(fechaHasta + " 23:59:59.000", out tempDate))
                {
                    dateE = tempDate;
                }

                var list = AuditoriaBusiness.Instance.GetByFilters(idUsuario, ipDomicilio, accionValue, idModulo, dateS, dateE, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AuditoriaReport(long idUsuario = 0, string direccionIP = "", int accionValue = 0, long idModulo = 0, string fechaDesde = "", string fechaHasta = "", int formatoReporte = 1)
        {
            try
            {
                Usuario usuario = UsuarioBusiness.Instance.GetEntity(idUsuario);
                Modulo modulo = ModuloBusiness.Instance.GetEntity(idModulo);

                DateTime tempDate;
                DateTime? dateS = null, dateE = null;

                if (!string.IsNullOrEmpty(fechaDesde) && DateTime.TryParse(fechaDesde, out tempDate))
                {
                    dateS = tempDate;
                }

                if (!string.IsNullOrEmpty(fechaHasta) && DateTime.TryParse(fechaHasta + " 23:59:59.000", out tempDate))
                {
                    dateE = tempDate;
                }

                var list = AuditoriaBusiness.Instance.GetByFilters(idUsuario, direccionIP, accionValue, idModulo, dateS, dateE);
                var modelList = new List<AuditoriaModel>();

                list.ForEach(p => modelList.Add(new AuditoriaModel
                {
                    IdAuditoria = p.IdAuditoria,
                    NombreUsuario = p.Usuario.NombreUsuario,
                    IPUsuario = p.IPUsuario,
                    Accion = DCREnumerationsHelper.GetAuditActionName((AuditAction)p.Accion),
                    ModuloNombre = p.Modulo.Nombre,
                    Descripcion = p.Descripcion,
                    Date = p.Fecha.ToString("dd/MM/yyyy HH:mm:ss")
                }));

                ReportClass reportClass = ReportHelper.LoadReport("AuditoriaReport");

                reportClass.SetDataSource(modelList);
                reportClass.SetParameterValue("nombreUsuario", (usuario != null ? usuario.NombreUsuario : "Indistinto"));
                reportClass.SetParameterValue("direccionIP", direccionIP);
                reportClass.SetParameterValue("accion", (accionValue > 0 ? DCREnumerationsHelper.GetAuditActionName((AuditAction)accionValue) : "Indistinto"));
                reportClass.SetParameterValue("moduloNombre", (modulo != null ? modulo.Nombre : "Indistinto"));
                reportClass.SetParameterValue("fechaDesde", (dateS.HasValue ? dateS.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty));
                reportClass.SetParameterValue("fechaHasta", (dateE.HasValue ? dateE.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty));

                string fileNombre = string.Format("Reporte de auditoría - ", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                string contentType = "";

                switch ((ReportFormat)formatoReporte)
                {
                    case ReportFormat.PDF:
                        contentType = MediaTypeNames.Application.Pdf;
                        fileNombre = string.Format("{0}.pdf", fileNombre);
                        break;
                    case ReportFormat.Excel:
                        fileNombre = string.Format("{0}.xls", fileNombre);

                        return File(ReportHelper.ShowReport(reportClass, (ReportFormat)formatoReporte),
                                    MediaTypeNames.Application.Octet,
                                    fileNombre);
                    default:
                        break;
                }

                return File(ReportHelper.ShowReport(reportClass, (ReportFormat)formatoReporte), contentType);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = "Error fatal", message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }
    }
}