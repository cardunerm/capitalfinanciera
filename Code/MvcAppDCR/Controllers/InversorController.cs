﻿using Business;
using System.Linq;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Net;
using Enumerations;
using System.Web;
using System.IO;
using System.Net.Mime;

namespace Controllers
{
    public class InversorController : BaseController
    {
        //
        // GET: /Inversores/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetInversor(string inversor = "", string cuit = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = InversorBussines.Instance.GetByFilters(inversor, cuit, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMInversor(long? id, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
            LoadCombos();
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new Inversor());
            }

            var entity = InversorBussines.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        private void LoadCombos()
        {
            ViewBag.Tasas = InversorBussines.Instance.GetByConditions<Tasa>(tasa => tasa.Activa == true)
                .OrderByDescending(order => order.Value)
                .GetSelectListItem("Id", "Value");

            ViewBag.ClasificacionInversor = DCREnumerationsHelper.GetClasificacionInversorTypes();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ABMInversor(Inversor inversor, bool? popUp, HttpPostedFileBase contrato)
        {
            ViewBag.PopUp = (popUp ?? false);           
            LoadCombos();
            try
            {
                
                if (ViewBag.PopUp)
                ModelState.Remove("Tasa");
                ModelState.Remove("TasaUSD");

                if (ModelState.IsValid)
                {
                    inversor.Contrato = (contrato != null) ? contrato.FileName : "";
                    inversor = InversorBussines.Instance.SaveOrUpdateInversores(inversor);


                    if (contrato != null)
                    {
                        var fileName = Path.GetFileName(contrato.FileName);
                        var path = Server.MapPath("/FileContrato/") + fileName;
                        contrato.SaveAs(path);                      
                    }

                    return Json(
                        new
                        {
                            id = inversor.IdInversor,
                            name = inversor.NameAndCuit,
                            msg = Resources.Resource.SaveSuccess,
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });


                    //return RedirectToAction("Index");
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                InversorBussines.Instance.DeleteLogic(id, InversorBussines.Instance.ValidateDelete, AuditoriaBusiness.Instance.AuditoriaInversores);

                CuentaCorrienteInversorBussines.Instance.DeleteLogicCuentaCorrienteInversortByInversor(id);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        public JsonResult ValidateInversores(string nombre = "", string cuit = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(nombre))
                {
                    var entity = InversorBussines.Instance.InversorExist(nombre, cuit);

                    if (entity != null)
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true, Id = entity.IdInversor });
                    }
                    else
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                    }
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Requerido");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        public ActionResult Upload(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Inversor inversor = InversorBussines.Instance.GetByConditions(i => i.IdInversor == id.Value).FirstOrDefault();

            if (inversor == null)
            {
                return HttpNotFound();
            }
            else
            {
                string url = "~/AttachmentsFiles/Inversores_Adjuntos/" + id.ToString() + "/";
                return RedirectToAction("Index", "ArchivoAdjunto", new { url = url, entityIdenum = (int)Enumerations.EntidadesAdjuntos.Inversor, returnUrl = "~/Inversor/Index", destino = "Inversor: " + inversor.NameAndCuit });
            }
        }

        public FileResult GetContrato(int idInversor)
        {
            var Inversor = InversorBussines.Instance.GetEntity(idInversor);
            var path = Server.MapPath("/FileContrato/") + Inversor.Contrato;
            return File(path, MediaTypeNames.Application.Pdf);
        }

    }
}