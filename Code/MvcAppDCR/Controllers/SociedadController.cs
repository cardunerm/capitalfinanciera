﻿using Business;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Web.Mvc;

namespace Controllers
{
    public class SociedadController : BaseController
    {
        //
        // GET: /Sociedad/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetSociedades(string nombre = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = SociedadBusiness.Instance.GetByFilters(nombre,skip,top,ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMSociedad(long? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new Sociedad());
            }

            var entity = SociedadBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMSociedad(Sociedad Sociedad)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SociedadBusiness.Instance.SaveOrUpdateSociedad(Sociedad);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }


    }
}