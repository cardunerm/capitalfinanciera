﻿using Business;
using Controllers;
using CrystalDecisions.CrystalReports.Engine;
using Domine;
using Enumerations;
using Mvc.Utils;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using MvcAppDCR.Utils;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class ComisionController : BaseController
    {

        #region property

        public List<ComisionModel> ComisionesModelList
        {
            get { return (List<ComisionModel>)Session["ComisionesModelLilst"]; }
            set { Session["ComisionesModelLilst"] = value; }
        }

        #endregion


        public ActionResult Index()
        {
            ViewBag.Vendedores = GetVendedores(true);
            return View();
        }

        public List<SelectListItem> GetVendedores(bool indistinto = false)
        {
            List<SelectListItem> estadoList = new List<SelectListItem>();
            var _listaVendedor = FacturaVendedorBusiness.Instance.GetByConditions(p => p.Activo == true);

            foreach (var item in _listaVendedor)            
                estadoList.Add(new SelectListItem { Value = item.IdVendedor.ToString(), Text = string.Format("{0} , {1}",item.Nombre,item.Apellido)});

            estadoList.Insert(0, new SelectListItem { Value = "0", Text = "Seleccionar Vendedor" });

            //if (indistinto)
            //    estadoList.Insert(0, new SelectListItem { Value = "0", Text = "Indistinto" });

            return estadoList;
        }

        public JsonResult GetComision(string nombre = "",  [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            List<ComisionModel> _comsionmodelList = new List<ComisionModel>();

            try
            {
                int rowCount = 0;
                var list = ComisionBusiness.Instance.GetByFilters(nombre, skip, top, ref rowCount);


                foreach (var item in list)
                {
                    var _vendedor = FacturaVendedorBusiness.Instance.GetEntity(long.Parse(item.IdVendedor.ToString()));
                    
                    _comsionmodelList.Add(new ComisionModel {
                        Id = item.Id.ToString(),
                        Vendedor = string.Format("{0}, {1}", _vendedor.Nombre, _vendedor.Apellido),
                        FechaDesde = item.FechaDesde.Value.ToShortDateString(),
                        FechaHasta = item.FechaHasta.Value.ToShortDateString(),
                        MontoCobrado = item.ComisionDetalle.Sum(p => p.MontoCobrado).ToString(),
                        MontoComisionado = item.ComisionDetalle.Sum(p=>p.MontoComisionado).ToString(),
                        FechaCreacion = item.Fecha.Value.ToShortDateString()
                    });
                }

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = _comsionmodelList, count = _comsionmodelList.Count() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult GetComisionesPorCobrar(string fechadesde, string fechahasta, string vendedor)
        {           
          
            try
            {
                Session.Remove("ComisionesModelList");
                var _comisiones = GetComisionesXVendedor(fechadesde,fechahasta,vendedor);


                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = _comisiones, count = _comisiones.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMComision(long? id, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
            ViewBag.Vendedores = GetVendedores(true);
            Session.Remove("ComisionesModelList");
            List<ComisionModel> comisionModelList = new List<ComisionModel>();

            if (!id.HasValue || id.Value <= 0)
            {
                Comision _comision = new Comision();
                _comision.FechaDesde = DateTime.Now.AddDays(-7);
                _comision.FechaHasta = DateTime.Now;
                return View(_comision);
            }

            var entity = ComisionBusiness.Instance.GetEntity(id.Value);

            

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        public List<ComisionModel> GetComisionesXVendedor(string fechadesde, string fechahasta, string vendedorparam)
        {
            DateTime _fechadesde = DateTime.Parse(fechadesde);
            DateTime _fechahasta = DateTime.Parse(fechahasta).AddDays(1);
            int _idvendedor = int.Parse(vendedorparam);
            string _porcentajecomision = ParametroBusiness.Instance.GetEntity(long.Parse("1")).Valor;
            List<ComisionModel> comisionModelList = new List<ComisionModel>();
            
            #region Facturas

            var resul = FacturaCobroBusiness.Instance.GetByConditions(p => p.Activo == true && p.Fecha >= _fechadesde && p.Fecha <= _fechahasta && p.FacturaOperacion.IdFacturaVendedor == _idvendedor);

            foreach (var item in resul)
            {
                //Valido que ya no se haya liquidado

                var comisiondetalle = ComisionDetalleBusiness.Instance.GetByConditions<ComisionDetalle>(p => p.IdFacturaCobro == item.IdFacturaCobro && p.Activo == true);

                if (comisiondetalle.Count() != 0)
                    continue;

                ////////////////////////////////////

                //var _facturaOperacion = FacturaOperacionBusiness.Instance.GetEntity(long.Parse(item.IdFacturaOperacion.ToString()));
                var _idFacturaOperacion = long.Parse(item.IdFacturaOperacion.ToString());

                var _facturaOperacion = FacturaOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == _idFacturaOperacion && p.IdFacturaVendedor == _idvendedor);
                var vendedor = FacturaVendedorBusiness.Instance.GetEntity(long.Parse(item.FacturaOperacion.IdFacturaVendedor.ToString()));
                var _facturaCliente = FacturaClienteBusiness.Instance.GetEntity(long.Parse(item.FacturaOperacion.IdFacturaCliente.ToString()));

                comisionModelList.Add(new ComisionModel
                {
                    Operacion = item.FacturaOperacion.FacturaNumero,
                    Cliente = _facturaCliente.Nombre,
                    FechaCobro = item.Fecha.Value.ToShortDateString(),
                    Cobrado = item.Monto.ToString(),
                    Comision = ((item.Monto * decimal.Parse(_porcentajecomision)) / 100).ToString(),
                    Vendedor = vendedor.Nombre + ", " + vendedor.Apellido,

                    IdFacturaVendedor = vendedor.IdVendedor,
                    IdFactura = _facturaOperacion[0].IdFacturaOperacion,
                    IdFacturaCobro = item.IdFacturaCobro,                 
                    TipoComision = "Factura"
                });
            }

            #endregion

            #region Operaciones
            var resulOpe = OperacionBusiness.Instance.GetByConditions(p => p.Estado == 8 && p.Fecha >= _fechadesde && p.Fecha <= _fechahasta);

            foreach (var item in resulOpe)
            {
                //Valido que ya no se haya liquidado

                var comisiondetalle = ComisionDetalleBusiness.Instance.GetByConditions<ComisionDetalle>(p => p.IdOperacion == item.Id && p.Activo == true);

                if (comisiondetalle.Count() != 0)
                    continue;

                ////////////////////////////////////

                //var _facturaOperacion = FacturaOperacionBusiness.Instance.GetEntity(long.Parse(item.IdFacturaOperacion.ToString()));
                var _idOperacion = long.Parse(item.Id.ToString());

                var _operacion = OperacionBusiness.Instance.GetByConditions(p => p.Id == _idOperacion && p.IdVendedor == _idvendedor);
                var vendedor = FacturaVendedorBusiness.Instance.GetEntity(long.Parse(item.IdVendedor.ToString()));
                var _facturaCliente = ClienteBusiness.Instance.GetEntity(long.Parse(item.IdCliente.ToString()));

                comisionModelList.Add(new ComisionModel
                {
                    Operacion = item.Id.ToString(),
                    Cliente = _facturaCliente.Nombre,
                    FechaCobro = item.Fecha.ToShortDateString(),
                    Cobrado = item.Monto.ToString(),
                    Comision = ((item.Monto * decimal.Parse(_porcentajecomision)) / 100).ToString(),
                    Vendedor = vendedor.Nombre + ", " + vendedor.Apellido,

                    IdFacturaVendedor = vendedor.IdVendedor,
                    IdOperacion = (int)_operacion[0].Id,                 
                    TipoComision = "Cheque"
                });
            }

            #endregion Operaciones

            ComisionesModelList = comisionModelList;
            return comisionModelList;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMComision(Comision comision, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);

            try
            {
                if (ViewBag.PopUp)
                    ModelState.Remove("IdTasa");

                if (ModelState.IsValid)
                {
                    comision.Activo = true;
                    comision.Fecha = DateTime.Now;

                    //Valido si es una actualización o una nuevo
                    if (comision.Id > 0)
                    {
                        Comision _entidad = ComisionBusiness.Instance.GetEntity(long.Parse(comision.Id.ToString()));
                        _entidad.Activo = true;
                        _entidad.Descripcion = (comision.Descripcion == null) ? string.Empty : comision.Descripcion.Trim();
                        _entidad.Fecha = comision.Fecha;
                    }
                    
                    
                    List<ComisionModel> comisionModelList = ComisionesModelList;


                    foreach (var item in comisionModelList)
                    {
                        comision.ComisionDetalle.Add(new ComisionDetalle {
                            IdFacturaVendedor = item.IdFacturaVendedor,
                            Comision = comision,
                            IdFactura = item.IdFactura,
                            IdFacturaCobro = item.IdFacturaCobro,
                            IdOperacion = item.IdOperacion,
                            MontoCobrado = decimal.Parse(item.Cobrado),
                            MontoComisionado = decimal.Parse(item.Comision),
                            Activo = true
                        });
                    }

                    if (comision.Descripcion == null)
                        comision.Descripcion = string.Empty;

                    ComisionBusiness.Instance.SaveOrUpdate(comision);
                   

                    return Json(
                        new
                        {
                            id = comision.Id,
                            name = comision.Descripcion,
                            msg = Resources.Resource.SaveSuccess,
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                Comision entidad = ComisionBusiness.Instance.GetEntity(id);
                entidad.Activo = false;
               
                foreach (var item in entidad.ComisionDetalle)
                {
                    item.Activo = false;
                }

                ComisionBusiness.Instance.SaveOrUpdate(entidad);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
        
        public ActionResult GetComprobante(long? id)
        {
            try
            {
                try
                {
                    List<ComisionModel> comisionModelList = new List<ComisionModel>();
                    var facturaoperacion = FacturaOperacionBusiness.Instance.GetEntity((long)id);
                    ReportClass reportClass = ReportHelper.LoadReport("ComisionReport");
                    Comision _comision = ComisionBusiness.Instance.GetEntity((long)id);
                    var vendedor = FacturaVendedorBusiness.Instance.GetEntity(long.Parse(_comision.IdVendedor.ToString()));
                    string _porcentajecomision = ParametroBusiness.Instance.GetEntity(long.Parse("1")).Valor;
                    foreach (var item in _comision.ComisionDetalle)
                    {

                        if (item.IdFactura != 0)
                        {
                            var _idFacturaOperacion = long.Parse(item.IdFactura.ToString());

                            var _facturaOperacion = FacturaOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == _idFacturaOperacion && p.IdFacturaVendedor == _comision.IdVendedor);

                            var _facturaCliente = FacturaClienteBusiness.Instance.GetEntity(long.Parse(_facturaOperacion[0].IdFacturaCliente.ToString()));

                            comisionModelList.Add(new ComisionModel
                            {
                                Operacion = _facturaOperacion[0].FacturaNumero,
                                Cliente = _facturaCliente.Nombre,
                                FechaCobro = _facturaOperacion[0].FacturaFechaCobro.Value.ToShortDateString(),
                                Cobrado = string.Format("$ {0}", Math.Round(decimal.Parse(item.MontoCobrado.ToString()), 2)),
                                Comision = string.Format("$ {0}", Math.Round(decimal.Parse(((item.MontoCobrado * decimal.Parse(_porcentajecomision)) / 100).ToString()))),
                                TipoComision = "Factura"
                            });
                        }
                        else
                        {
                            var _idOperacion = long.Parse(item.IdOperacion.ToString());

                            var _facturaOperacion = OperacionBusiness.Instance.GetByConditions(p => p.Id == _idOperacion && p.IdVendedor == _comision.IdVendedor);

                            var _facturaCliente = ClienteBusiness.Instance.GetEntity(long.Parse(_facturaOperacion[0].IdCliente.ToString()));

                            comisionModelList.Add(new ComisionModel
                            {
                                Operacion = _facturaOperacion[0].Id.ToString(),
                                Cliente = _facturaCliente.Nombre,
                                FechaCobro = _facturaOperacion[0].FechaPago.Value.ToShortDateString(),
                                Cobrado = string.Format("$ {0}", Math.Round(decimal.Parse(item.MontoCobrado.ToString()), 2)),
                                Comision = string.Format("$ {0}", Math.Round(decimal.Parse(((item.MontoCobrado * decimal.Parse("1.25")) / 100).ToString()))),
                                TipoComision = "Cheque"
                            });

                        }
                    }                    
                    reportClass.SetDataSource(comisionModelList);

                    reportClass.SetParameterValue("VendedorParam", string.Format("{0}, {1}", vendedor.Nombre, vendedor.Apellido));
                    reportClass.SetParameterValue("FechaDesdeParam", _comision.FechaDesde.Value.ToShortDateString());
                    reportClass.SetParameterValue("FechaHastaParam", _comision.FechaHasta.Value.ToShortDateString());
                    reportClass.SetParameterValue("DescripcionParam", _comision.Descripcion);
                    reportClass.SetParameterValue("TotalComisionadoParam", string.Format("$ {0}", _comision.ComisionDetalle.Sum(p => p.MontoComisionado)));

                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);

                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
                }

            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}