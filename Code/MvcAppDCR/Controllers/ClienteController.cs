﻿using Business;
using System.Linq;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Web.Mvc;
using System.Collections.Generic;

namespace Controllers
{
    public class ClienteController : BaseController
    {
        //
        // GET: /Cliente/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetClientes(string nombre = "", string cuit = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = ClienteBusiness.Instance.GetByFilters(nombre, cuit, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMCliente(long? id, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
            LoadCombos();
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new Cliente());
            }

            var entity = ClienteBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        private void LoadCombos()
        {
            ViewBag.Tasas = ClienteBusiness.Instance.GetByConditions<Tasa>(tasa => tasa.Activa == true)
                .OrderByDescending(order => order.Value)
                .GetSelectListItem("Id", "Value");


            List<SelectListItem> _condiciontributaria = new List<SelectListItem>();
            _condiciontributaria.Add(new SelectListItem { Text = "Consumidor Final", Value = "1" });
            _condiciontributaria.Add(new SelectListItem { Text = "Responsable Incripto", Value = "2" });
            ViewBag.CondicionTributaria = _condiciontributaria;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMCliente(Cliente cliente, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
            LoadCombos();
            try
            {
                if (ViewBag.PopUp)
                    ModelState.Remove("IdTasa");

                if (ModelState.IsValid)
                {
                    cliente = ClienteBusiness.Instance.SaveOrUpdateCliente(cliente);

                    return Json(
                        new
                        {
                            id = cliente.IdCliente,
                            name = cliente.NameAndCuit,
                            msg = Resources.Resource.SaveSuccess,
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                ClienteBusiness.Instance.DeleteLogic(id, ClienteBusiness.Instance.ValidateDelete, AuditoriaBusiness.Instance.AuditoriaCliente);

                CuentaCorrienteBusiness.Instance.DeleteLogicCuentaCorrienteByCliente(id);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        public JsonResult ValidateCliente(string nombre = "", string cuit = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(nombre))
                {
                    var entity = ClienteBusiness.Instance.ClienteExist(nombre, cuit);

                    if (entity != null)
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true, Id = entity.IdCliente });
                    }
                    else
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                    }
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Requerido");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }



        [HttpPost]     
        public ActionResult GuardarTasa(string tasa)
        {
            try
            {
                Tasa _tasaEnt = new Tasa();
                _tasaEnt.Activa = true;
                _tasaEnt.Value = decimal.Parse(tasa);
                TasaBusiness.Instance.SaveOrUpdate(_tasaEnt);

                return Json(new{success = true, id = _tasaEnt.Id, tasa = _tasaEnt.Value.ToString()});
            }
            catch (Exception ex)
            {
                return Json(new { success = false});
            }
                
            
        }
    }
}