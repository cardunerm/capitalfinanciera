﻿using Business;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Web.Mvc;

namespace Controllers
{
    public class EmpresaController : BaseController
    {
        [HttpPost]
        [AllowAnonymous]
        public JsonResult GetEmpresasByNombreUsuarioOrderById(string nombreUsuario = "")
        {
            try
            {
                var empresas = EmpresaBusiness.Instance.GetEmpresasByNombreUsuario(nombreUsuario);

                var options = WebHelper.GetSelectListItem(empresas, "Id", "Nombre");

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Options = options });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}