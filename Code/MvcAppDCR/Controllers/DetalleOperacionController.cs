﻿using Business;
using Controllers;
using MvcAppDCR.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class DetalleOperacionController : BaseController
    {
        // GET: DetalleOperacion
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetDetalleOperacion(int IdOperacionInversor)
        {
            try
            {

                var list = DetalleOperacionBusiness.Instance.GetDetalleOperacion(IdOperacionInversor);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list}, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}