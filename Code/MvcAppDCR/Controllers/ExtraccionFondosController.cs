﻿using Business;
using CurrentContext;
using Domine;
using Domine.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Luma.Utils.Logs;
using MvcAppDCR.Model.Entities;
using CrystalDecisions.CrystalReports.Engine;
using Mvc.Utils;
using Enumerations;
using Resources;
using Controllers;
using System.Net.Mime;
using Luma.Utils;

namespace MvcAppDCR.Controllers
{
    public class ExtraccionFondosController : BaseController
    {
        // GET: ExtraccionFondos
        public ActionResult Index(long? id, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
            DtoOperacionInversor dtoOperacion = new DtoOperacionInversor();
            return View(dtoOperacion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExtraccionFondos(DtoOperacionInversor dtoOperacion, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);

            DtoReporteEfectivo dtoReporte = new DtoReporteEfectivo();

            Inversor inversor = InversorBussines.Instance.GetEntity(dtoOperacion.IdInversor);

            if (inversor.ClasificacionInversor != (int)ClasificacionInversor.BBB)
            {
                //Valido que el monto que se quiera egresar exista saldo suficiente.
                decimal _saldo = OperacionInversoresBussines.Instance.GetSaldoActual(dtoOperacion.IdCuentaCorrienteInversor);
                decimal limiteDescubierto = (OperacionInversoresBussines.Instance.GetLimiteByIdCtaCteInversor(dtoOperacion.IdCuentaCorrienteInversor)) * -1;

                if (_saldo - dtoOperacion.Monto < limiteDescubierto)
                {
                    return Json(new
                    {
                        success = false,
                        msg = Resources.Resource.FueraLimiteDescubierto
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            var success = OperacionInversoresBussines.Instance.ExtraccionFondos(dtoOperacion, SessionContext.UsuarioActual, ref dtoReporte);


            if (success)
            {
                return Json(new
                {
                    success = success,
                    data = dtoReporte,
                    msg = Resources.Resource.OperacionCorrecta,
                    url = "BandejaOperaciones"
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = success,
                    msg = Resources.Resource.ErrorOperacion
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetCuentasCorrientesByIdInversor(int idInversor)
        {
            try
            {
                List<CuentaCorrienteInversor> cuentas = CuentaCorrienteInversorBussines.Instance.GetByConditions<CuentaCorrienteInversor>(c => c.IdInversor == idInversor, new List<string> { "Inversor" });
                List<DtoCuentaCorrienteInversor> dto = new List<DtoCuentaCorrienteInversor>();
                decimal montoMaximoDescubierto = 0;
                decimal montoOperativo = 0;
                decimal montoGarantia = 0;
                bool esInversorBBB = false;

                foreach (var cuenta in cuentas)
                {
                    //Calculo el monto máximo que puede retirar el inversor si es de clase BBB
                    if (cuenta.ClasificacionInversor == (int)ClasificacionInversor.BBB)
                    {
                        montoMaximoDescubierto = CalcularMontoDescubiertoBBB(cuenta, out montoOperativo, out montoGarantia);
                        esInversorBBB = true;
                    }
                    dto.Add(new DtoCuentaCorrienteInversor
                    {
                        Id = cuenta.IdCuentaCorrienteInversor,
                        NumeroCuenta = cuenta.IdCuentaCorrienteInversor.ToString("000-0000000"),
                        TipoMoneda = cuenta.TipoMoneda,
                        Saldo = Math.Round(OperacionInversoresBussines.Instance.GetSaldoActual(cuenta.IdCuentaCorrienteInversor)),
                        ClasificacionInversor = cuenta.ClasificacionInversor,
                        LimiteDescubierto = Math.Round(montoMaximoDescubierto,2)
                    });
                }

                return Json(new
                {
                    success = true,
                    data = dto
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);

                return Json(new
                {
                    success = false,
                    msg = Resources.Resource.ErrorOperacion
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public decimal CalcularMontoDescubiertoBBB(CuentaCorrienteInversor cuenta, out decimal mOperativo, out decimal mGarantia)
        {

            mOperativo = 0;
            mGarantia = 0;

            try
            {
                List<Cheque> listaChequesEnCartera = new List<Cheque>();
                DateTime fechaActual = DateTime.Now.Date;
                using (DCREntities context = new DCREntities())
                {
                    listaChequesEnCartera = (from ch in context.Cheque
                                             join detOp in context.DetalleOperacion on ch.IdCheque equals detOp.IdCheque
                                             join opInv in context.OperacionInversor on detOp.IdOperacionInversor equals opInv.IdOperacionInversor
                                             join ctacte in context.CuentaCorrienteInversor on opInv.IdCuentaCorrienteInversor equals ctacte.IdCuentaCorrienteInversor
                                             where
                                             ctacte.IdCuentaCorrienteInversor == cuenta.IdCuentaCorrienteInversor &&
                                             ch.Clasificacion == 2 &&
                                             ch.FechaVencimiento >= fechaActual
                                             select ch).ToList();
                }

                if (listaChequesEnCartera.Count == 0)
                    return 0;

                List<Cheque> chequesOperativos = new List<Cheque>();
                List<Cheque> chequesEnGarantia = new List<Cheque>();

                decimal montoOperativo = 0;
                decimal montoGarantia = 0;

                foreach (var chequeEnCartera in listaChequesEnCartera)
                {
                    if (chequeEnCartera.Clasificacion == (int)ClasificacionCheque.Garantia)
                    {
                        montoGarantia = montoGarantia + chequeEnCartera.Monto;
                        chequesEnGarantia.Add(chequeEnCartera);
                    }
                    else
                    {
                        montoOperativo = montoOperativo + chequeEnCartera.Monto;
                        chequesOperativos.Add(chequeEnCartera);
                    }
                }

                if (montoGarantia == 0 || montoOperativo == 0)
                    return 0;

                Cheque ultimoChequeOperativo = chequesOperativos.OrderByDescending(c => c.FechaVencimiento).First();
                decimal tasaInteres = cuenta.TipoMoneda == (int)TipoMoneda.Peso ? cuenta.Inversor.Tasa : cuenta.Inversor.TasaUSD;
                int cantDias = Convert.ToInt32((ultimoChequeOperativo.FechaVencimiento.Date - DateTime.Now.Date).TotalDays);

                //Calculo los intereses que me debería reportar el monto en garantía
                decimal montoInteres = Convert.ToDecimal(UtilHelper.CalcularInteres(montoOperativo, tasaInteres, cantDias));

                //Resto el monto de las operaciones que se han realizado en el día y no se han procesado aún
                List<OperacionInversor> operaciones = OperacionInversoresBussines.Instance.GetByConditions(o => o.IdCuentaCorrienteInversor == cuenta.IdCuentaCorrienteInversor && o.Fecha == fechaActual);
                decimal montoExtraidoHoy = 0;

                foreach (var op in operaciones)
                {
                    montoExtraidoHoy = montoExtraidoHoy + op.Monto;
                }

                mOperativo = montoOperativo;
                mGarantia = montoGarantia;

                return montoOperativo - montoInteres - montoExtraidoHoy;
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw;
            }
        }

        public ActionResult GenerarReporteExtraccion(string paramDtoReporteDeposito)
        {
            DtoReporteEfectivo dtoReporteDeposito = Newtonsoft.Json.JsonConvert.DeserializeObject<DtoReporteEfectivo>(paramDtoReporteDeposito);
            try
            {
                if (dtoReporteDeposito != null)
                {
                    var depositoModel = new List<DepositoModel>();


                    ReportClass reportClass = ReportHelper.LoadReport("ExtraccionFondosReport");

                    reportClass.SetParameterValue("nombreUsuario", dtoReporteDeposito.NombreUsuario);
                    reportClass.SetParameterValue("fecha", dtoReporteDeposito.Fecha);
                    reportClass.SetParameterValue("nombreInversor", dtoReporteDeposito.NombreInversor);
                    reportClass.SetParameterValue("numeroCUIT", dtoReporteDeposito.CuitInversor);
                    reportClass.SetParameterValue("monto", dtoReporteDeposito.Monto);
                    reportClass.SetParameterValue("montoStr", "(" + new UtilConverNumber().Convertir(dtoReporteDeposito.Monto.ToString(),true,null) + ")");
                    reportClass.SetParameterValue("numeroCuentaCorriente", dtoReporteDeposito.CuentaCorrienteStr);
                    reportClass.SetParameterValue("numeroOperacion", dtoReporteDeposito.NumeroOperacion);
                    reportClass.SetParameterValue("tipoMoneda", dtoReporteDeposito.TipoMoneda);

           
                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                }
                else
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = "Operacion no encontrada", btnGoToLabel = "", redirectBtn = false });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }
    }
}