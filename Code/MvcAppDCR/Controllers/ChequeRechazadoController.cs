﻿using Business;
using Domine;
using Domine.Dto;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Controllers
{
    public class ChequeRechazadoController : BaseController
    {
        //
        // GET: /ChequeRechazado/

        public ActionResult Cheque()
        {
            DtoRechazoCheque rechazo = new DtoRechazoCheque();
            rechazo.IdCheque = 0;
            rechazo.Impuesto = 0;
            rechazo.Multa = 0;
            rechazo.Gasto = 0;
            rechazo.Motivo = string.Empty;
            ViewBag.Banco = BancoBusiness.Instance.GetAll().OrderBy(banco => banco.Nombre).GetSelectListItem("IdBanco", "Nombre", true);
            ViewBag.Estado = ChequeEstadoBusiness.Instance.GetAll().OrderBy(p => p.Descripcion).GetSelectListItem("Id", "Descripcion", true);
            return View(rechazo);
        }


        public ActionResult Index()
        {
            ViewBag.Bancos = BancoBusiness.Instance.GetAll().OrderBy(banco => banco.Nombre).GetSelectListItem("IdBanco", "Nombre");

            return View();
        }

        public JsonResult GetChequesRechazados(string numero = "", int banco = 0, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = ChequeRechazadoBusiness.Instance.GetByFilters(numero, banco, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list.Result, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetCheques(string numero = "", int banco = -1, int estado = -1, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {

            List<ChequeModel> resul = new List<ChequeModel>();

            try
            {
                int rowCount = 0;

                var list = ChequeBusiness.Instance.GetByFilters(numero, banco, estado, skip, top, ref rowCount);


                foreach (var item in list)
                    resul.Add(new ChequeModel { IdCheque = item.IdCheque, Numero = item.Numero, BancoStr = item.BancoStr, FechaVencimiento = item.FechaVencimiento, Monto = item.Monto, EstadoStr = item.EstadoStr });


                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = resul, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMChequeRechazado(int? id)
        {
            ViewBag.Bancos = BancoBusiness.Instance.GetAll().OrderBy(banco => banco.Nombre).GetSelectListItem("IdBanco", "Nombre");

            if (!id.HasValue || id.Value <= 0)
            {
                return View(new ChequeRechazado());
            }

            var entity = ChequeRechazadoBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMChequeRechazado(ChequeRechazado Cheque)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ChequeRechazadoBusiness.Instance.SaveOrUpdateCheque(Cheque);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(DtoRechazoCheque rechazo)
        {
            try
            {
                ChequeBusiness.Instance.ChangeStatus(rechazo.IdCheque, Utils.Enumeraciones.Estados.eEstadosCheque.Rechazado, rechazo,1);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateChequeRechazado(string cheques, ChequeRechazado chequeRechazadoModel)
        {
            try
            {
                Cheque chequeBD = new Cheque() { Activo = true };

                var listCheques = new JavaScriptSerializer().Deserialize<List<Cheque>>(cheques);

                if (listCheques.Count < 1)
                {
                    throw new Exception("Se debe seleccionar un cheque");
                }

                int a = ChequeRechazadoBusiness.Instance.CreateChequeRechazado(listCheques, chequeRechazadoModel);

                return Json(new { success = true, url = Url.Action("Index") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, url = Url.Action("Index"), count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetChequesByFilters(string numero = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = ChequeBusiness.Instance.GetChequesByFilters(numero, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list.Result, count = list.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
