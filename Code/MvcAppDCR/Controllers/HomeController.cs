﻿using Business;
using Common.Logging;
using CurrentContext;
using Domine;
using Domine.Dto;
using MvcAppDCR.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace Controllers
{
    public class HomeController : BaseController
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            if (CurrentContext.SessionContext.Perfil == Luma.Utils.Enumeraciones.Perfil.TipoPerfil.Inversor)
            {
                ViewBag.CuentaCorriente = GetCuentaCorriente();
            }


            return View();
        }


        public List<SelectListItem> GetCuentaCorriente()
        {

            List<SelectListItem> _cuentaCorrienteListItem = new List<SelectListItem>();
            var idInversor = InversorBussines.Instance.GetByConditions(p => p.IdUsuario == SessionContext.UsuarioActual.IdUsuario).First().IdInversor;
            List<CuentaCorrienteInversor> cuentas = CuentaCorrienteInversorBussines.Instance.GetByConditions(c => c.IdInversor == idInversor);
            List<DtoCuentaCorrienteInversor> dto = new List<DtoCuentaCorrienteInversor>();
            foreach (var cuenta in cuentas)
            {
                decimal saldo = ResumenDiarioOperacionInversorBusiness.Instance.GetSaldoByIdCuenta(cuenta.IdCuentaCorrienteInversor, cuenta.TipoMoneda);                

                _cuentaCorrienteListItem.Add(new SelectListItem {
                    Value = cuenta.IdCuentaCorrienteInversor.ToString(),
                    Text = string.Format("{0} - {1}", (cuenta.TipoMoneda == 1)? "Pesos $" : "Dolares USD", saldo)
                    });
            }


            return _cuentaCorrienteListItem;
        }

        public JsonResult GetProyeccion(int idCuenta = -1, [Bind(Prefix = "$top")] int top = 10, [Bind(Prefix = "$skip")] int skip = 0)
        {
            try
            {
                var idInversor = InversorBussines.Instance.GetByConditions(p => p.IdUsuario == SessionContext.UsuarioActual.IdUsuario).First().IdInversor;
                List<DtoProyeccionFinanciera> list = new List<DtoProyeccionFinanciera>();
                list = ProyeccionFinancieraBusiness.Instance.GetProyeccion(1, idInversor, idCuenta);
                int rowCount = list.Count();
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //LogManager.Log.RegisterError(ex);
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public FileResult GetContrato()
        {
            var idInversor = InversorBussines.Instance.GetByConditions(p => p.IdUsuario == SessionContext.UsuarioActual.IdUsuario).First().IdInversor;
            var Inversor = InversorBussines.Instance.GetEntity(idInversor);
            var path = Server.MapPath("/FileContrato/") + Inversor.Contrato;            
            return File(path, MediaTypeNames.Application.Pdf);
        }

    }
}
