﻿using Business;
using Controllers;
using Domine.Dto;
using Luma.Utils.Logs;
using MvcAppDCR.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class ProyeccionFinancieraController : BaseController
    {
        // GET: ProyeccionFinanciera
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetProyeccion(int idInversor = -1, int idCuenta = -1, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                List<DtoProyeccionFinanciera> list = new List<DtoProyeccionFinanciera>();             
                list = ProyeccionFinancieraBusiness.Instance.GetProyeccion(1,idInversor, idCuenta);

                int rowCount = list.Count();

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public FileResult GetContrato(int idInversor)
        {           
            var Inversor = InversorBussines.Instance.GetEntity(idInversor);
            var path = Server.MapPath("/FileContrato/") + Inversor.Contrato;
            return File(path, MediaTypeNames.Application.Pdf);
        }
    }
}