﻿using Business;
using Common.Logging;
using Controllers;
using CrystalDecisions.CrystalReports.Engine;
using CurrentContext;
using DataAccess;
using Domine;
using Domine.Dto;
using Enumerations;
using Luma.Utils.Security;
using Mvc.Utils;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using MvcAppDCR.SignalRhubs;
using MvcAppDCR.Utils;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Utils.Enumeraciones;

namespace MvcAppDCR.Controllers
{
    public class DepositoChequeController : BaseController
    {
        // GET: DepositoCheque
        public ActionResult NuevoDepositoCheque()
        {
            OperacionInversoresBussines.Instance.CalcularChequeGarantia(3090);


            LoadCombo(false);
            return View(new DtoOperacionInversor());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendCreacion(DtoOperacionInversor operacion)
        {
            if (IsValidCabecera(operacion))
            {

                int id = OperacionInversoresBussines.Instance.CreateOperacion(operacion, Session["itemDeposito_session"] as List<DtoDetalleOperacion>, CurrentContext.SessionContext.UsuarioActual, SessionContext.Configuracion);
                Session["itemDeposito_session"] = new List<DtoDetalleOperacion>();
                SendNotificationAprobadores(id, Estados.eEstadosOp.Pendiente);

                return Json(new
                {
                    success = true,
                    data = id,
                    msg = Resources.Resource.OperacionCorrecta,
                    url = "NuevoDepositoCheque"
                }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { success = false, msg = Resources.Resource.DatosObligatorios }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReporteDepositoCheque(int IdOperacionInversor = 0)
        {
            try
            {
                var include = new List<string>();
                include.Add("CuentaCorrienteInversor");
                include.Add("CuentaCorrienteInversor.Inversor");
                var deposito = OperacionInversoresBussines.Instance.GetByConditions<OperacionInversor>(o => o.IdOperacionInversor == IdOperacionInversor, include).FirstOrDefault();

                var listaCheques = new List<ChequeModel>();

                List<string> includes = new List<string>();
                includes.Add("Cheque");
                List<DetalleOperacion> detOperacion = DetalleOperacionBusiness.Instance.GetByConditions<DetalleOperacion>(o => o.IdOperacionInversor == IdOperacionInversor, includes).ToList();

                foreach (var detalle in detOperacion)
                {
                    listaCheques.Add((ChequeModel)WebHelper.GenericLoad(detalle.Cheque, new ChequeModel()));
                }

                if (deposito != null)
                {
                    var depositoModel = new List<DepositoModel>();


                    ReportClass reportClass = ReportHelper.LoadReport("DepositoChequeReport");

                    reportClass.SetDataSource(listaCheques);
                    reportClass.SetParameterValue("fecha", deposito.Fecha);
                    reportClass.SetParameterValue("inversor", deposito.CuentaCorrienteInversor.Inversor.Nombre);
                    reportClass.SetParameterValue("cuit", deposito.CuentaCorrienteInversor.Inversor.CUIT);
                    reportClass.SetParameterValue("monto", deposito.Monto);
                    reportClass.SetParameterValue("numeroCtaCte", deposito.CuentaCorrienteInversor.NumeroCtaCteStr);
                    reportClass.SetParameterValue("numeroOperacion", deposito.IdOperacionInversor);
                    reportClass.SetParameterValue("tipoMoneda", ((TipoMoneda)deposito.CuentaCorrienteInversor.TipoMoneda).ToString());


                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                }
                else
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = "Error en la operacion", btnGoToLabel = "", redirectBtn = false });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }

        private bool IsValidCabecera(DtoOperacionInversor operacion)
        {
            if (operacion.IdInversor == 0)
            {
                return false;
            }

            if (Session["itemDeposito_session"] == null)
            {
                return false;
            }
            return true;
        }

        private void SendNotificationAprobadores(long id, Estados.eEstadosOp estado)
        {
            string ret = "La operación {1} ha cambiado de estado a {0}.";
            var msg = string.Format(ret, estado.GetEnumeracionName(), id);
            NotificationHub.SendToGroup(msg);
        }

        public RedirectResult Volver(string bandeja)
        {
            return Redirect("~/Home/");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectResult ClearCreacion()
        {
            return Redirect("~/DepositoCheque/NuevoDepositoCheque");
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult EditDetalleCreacion(string id)
        {
            if (Session["itemDeposito_session"] != null)
            {
                var edit = (Session["itemDeposito_session"] as List<DtoDetalleOperacion>).FirstOrDefault(det => det.UniqueId.ToString() == id);
                return Json(new { success = true, result = edit }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult DeleteDetalleCreacion(string id)
        {
            if (Session["itemDeposito_session"] != null)
            {
                var detalle = Session["itemDeposito_session"] as List<DtoDetalleOperacion>;
                detalle.Remove(detalle.FirstOrDefault(det => det.UniqueId.ToString() == id));
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddDetalleOperacion(DtoOperacionInversor operacion)
        {
            try
            {
                if (operacion.DetalleOperacion.Monto<=0)
                {
                    throw new Exception("El Importe no puede ser 0 o menor");
                }
                RemoveModelStateOperacion(ModelState);
                if (ModelState.IsValid)
                {
                    if (Session["itemDeposito_session"] == null)
                    {
                        Session["itemDeposito_session"] = new List<DtoDetalleOperacion>();
                    }
                    var detalle = Session["itemDeposito_session"] as List<DtoDetalleOperacion>;

                    if (ChequeBusiness.Instance.GetByConditions(
                        c => c.IdBanco == operacion.DetalleOperacion.IdBanco
                        && c.Numero == operacion.DetalleOperacion.Numero
                        && c.NroCuenta == operacion.DetalleOperacion.NumeroCuenta
                        && c.Activo == true
                        ).Count > 0
                        || detalle.Count(p => p.IdBanco == operacion.DetalleOperacion.IdBanco && p.Numero == operacion.DetalleOperacion.Numero && operacion.DetalleOperacion.UniqueId != p.UniqueId) > 0)
                    {
                        throw new Exception("El cheque ya ha sido ingresado en el sistema");
                    }

                    if (!string.IsNullOrEmpty(operacion.DetalleOperacion.UniqueId))
                    {
                        var detRemove = detalle.FirstOrDefault(det => det.UniqueId == operacion.DetalleOperacion.UniqueId);
                        detalle.Remove(detRemove);
                    }
                    else
                    {
                        operacion.DetalleOperacion.UniqueId = Guid.NewGuid().ToString();
                    }

                    operacion.DetalleOperacion.Estado = GetEnumeracionName(Estados.eEstadosOpDetalle.Pendiente);
                    var tasa = OperacionInversoresBussines.Instance.GetTasa(operacion.IdCuentaCorrienteInversorHidden);
                    operacion.DetalleOperacion.MontoCalculado = CalcularMonto(operacion.DetalleOperacion.FechaVencimiento, DateTime.Now.Date, tasa, operacion.DetalleOperacion.Monto, SessionContext.Configuracion);
                    detalle.Add(operacion.DetalleOperacion);

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private void RemoveModelStateOperacion(ModelStateDictionary modelState)
        {
            modelState.Remove("IdInversor");
            modelState.Remove("Fecha");
            modelState.Remove("Monto");
        }

        private string GetEnumeracionName(Enum value)
        {
            return (value).GetType()
                          .GetMember(value.ToString())
                          .First()
                          .GetCustomAttribute<DisplayAttribute>().GetName();
        }

        private decimal CalcularMonto(DateTime fechaPago, DateTime fechaLiquidacion, decimal tasa, decimal monto, Configuracion configuracion)
        {
            try
            {
                //Calculos
                var diasClering = OperacionInversoresBussines.CalcularDiasClering(fechaLiquidacion, fechaPago);
                var impuestoDiario = OperacionInversoresBussines.CalcularImpuestoDiario(diasClering, monto, tasa);
                var impuestoAlCheque = OperacionInversoresBussines.CalcularImpustoAlCheque(monto, configuracion);
                var impuestoPorGastos = OperacionInversoresBussines.CalcularImpustoPorGastos(monto, configuracion);

                return Math.Round(monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2);
            }
            catch (Exception ex) { throw ex; }

        }

        private void LoadCombo(bool aprobador = false, bool bandeja = false, Estados.eEstadosOp estado = Estados.eEstadosOp.Pendiente)
        {
            ViewBag.Bancos = BancoBusiness.Instance.GetAll().OrderBy(banco => banco.Nombre).ToList();
            ViewBag.Estados = OperacionInversoresBussines.Instance.GetEstados(bandeja, aprobador, estado);
            ViewBag.ClasificacionInversor = DCREnumerationsHelper.GetClasificacionInversorTypes();
        }

        public ActionResult ViewOp(string IDKey, string Url)
        {
            //if (IDKey == null)
            //    return Redirect(Request.UrlReferrer.AbsoluteUri);

            //var idOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
            //var operacion = OperacionInversoresBussines.Instance.GetByConditions<OperacionInversor>(op => op.IdOperacionInversor == idOp, new List<string> { { "Inversor" } }).FirstOrDefault();
            //if (operacion != null)
            //{
            //    ViewBag.Url = Url;
            //    LoadCombo(false);
            //    var dto = operacion.GetDtoOperacionInv();
            //    var resumen = OperacionInversoresBussines.Instance.GetSaldoInversor(operacion.CuentaCorrienteInversor.Inversor.IdInversor);
            //    dto.Saldo = resumen.Saldo;
            //    dto.CantidadRechazos = resumen.CantidadRechazos;
            //    dto.MontoRechazos = resumen.MontoRechazos;
            //    return View(dto);
            //}
            //else
            //{
            //    return Redirect(Url);
            //}
            return null;
        }

        public JsonResult GetMotivosRechazo(string IDKey)
        {
            if (IDKey != null)
            {
                try
                {
                    var idDetOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
                    var detalles = OperacionInversoresBussines.Instance.GetMotivosRechazo(idDetOp);
                    return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = detalles }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { result = new { }, count = 0 }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult GetCuentasCorrientesByIdInversor(int idInversor)
        {
            try
            {

                List<CuentaCorrienteInversor> cuentas = CuentaCorrienteInversorBussines.Instance.GetByConditions(c => c.IdInversor == idInversor);
                int clasificacionInversor = cuentas.First().ClasificacionInversor;
                List<DtoCuentaCorrienteInversor> dto = new List<DtoCuentaCorrienteInversor>();
                foreach (var cuenta in cuentas)
                {
                    decimal saldo = ResumenDiarioOperacionInversorBusiness.Instance.GetSaldoByIdCuenta(cuenta.IdCuentaCorrienteInversor, cuenta.TipoMoneda);
                    dto.Add(new DtoCuentaCorrienteInversor
                    {
                        Id = cuenta.IdCuentaCorrienteInversor,
                        NumeroCuenta = cuenta.IdCuentaCorrienteInversor.ToString("000-0000000"),
                        TipoMoneda = cuenta.TipoMoneda,
                        Saldo = saldo
                    });
                }

                return Json(new
                {
                    success = true,
                    data = dto,
                    clasificacionInversor = clasificacionInversor
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Luma.Utils.Logs.LogManager.Log.RegisterError(ex);

                return Json(new
                {
                    success = false,
                    msg = Resources.Resource.ErrorOperacion
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetLabelByCtaCte(int idCuentaCorrienteInversor)
        {
            try
            {


                var ctacteinversor = CuentaCorrienteInversorBussines.Instance.GetByConditions(c => c.IdCuentaCorrienteInversor == idCuentaCorrienteInversor).FirstOrDefault();

                int cantCheques;

                List<DetalleOperacion> detalle = DetalleOperacionBusiness.Instance.GetByConditions(d => d.OperacionInversor.IdCuentaCorrienteInversor == idCuentaCorrienteInversor);

                List<string> includes = new List<string>
                {
                    "DetalleOperacion", "RecepcionCheque", "DetalleOperacion.OperacionInversor"
                };

                var context = ChequeBusiness.Instance.GetContext();

                var query = from opinv in context.OperacionInversor
                            join detop in context.DetalleOperacion on opinv.IdOperacionInversor equals detop.IdOperacionInversor
                            join cheque in context.Cheque on detop.IdCheque equals cheque.IdCheque
                            where opinv.IdCuentaCorrienteInversor == idCuentaCorrienteInversor && cheque.IdEstado == (int)EstadoCheque.Rechazado
                            select cheque;

                List<Cheque> chequesRechazados = query.ToList();
                
                decimal montoRechazado = chequesRechazados.Sum(m => m.Monto);
                
                decimal saldo = ResumenDiarioOperacionInversorBusiness.Instance.GetSaldoByIdCuenta(idCuentaCorrienteInversor, ctacteinversor.TipoMoneda);
                
                cantCheques = detalle.Count();

                var tasa = OperacionInversoresBussines.Instance.GetTasa(idCuentaCorrienteInversor);

                return Json(new
                {
                    success = true,
                    cant = cantCheques,
                    tasa = tasa,
                    saldo = saldo,
                    cantrechazo = chequesRechazados.Count(),
                    montorechazado = montoRechazado
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Luma.Utils.Logs.LogManager.Log.RegisterError(ex);

                return Json(new
                {
                    success = false,
                    msg = Resources.Resource.ErrorOperacion
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private void RecalcularMontos(decimal tasa)
        {
            if (Session["itemDeposito_session"] != null)
                foreach (var item in (Session["itemDeposito_session"] as List<DtoDetalleOperacion>))
                {
                    item.MontoCalculado = CalcularMonto(item.FechaVencimiento, DateTime.Now.Date, tasa, item.Monto, SessionContext.Configuracion);
                }
        }

        [HttpPost]
        public JsonResult CreateImagesScan(string front, string back)
        {
            try
            {
                var frontName = CreateImage(front, "front");
                var backName = CreateImage(back, "back");
                return
                  Json(new
                  {
                      success = true,
                      frontName = frontName,
                      frontSrc = Url.Content("~/UploadImages/Cheques/") + frontName,
                      backName = backName,
                      backSrc = Url.Content("~/UploadImages/Cheques/") + backName,
                  });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private string CreateImage(string imageBase64, string prefix)
        {
            try
            {
                if (!string.IsNullOrEmpty(imageBase64))
                {
                    var fileName = prefix + DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ".jpg";
                    byte[] imageBytes = Convert.FromBase64String(imageBase64);
                    var path = Path.Combine(Server.MapPath("~/UploadImages/Cheques/"), fileName);
                    System.IO.File.WriteAllBytes(path, imageBytes);
                    return fileName;
                }
            }
            catch
            { }
            return "no-Imagen.png";

        }

        public JsonResult GetChequesCreacion()
        {
            try
            {
                var list = new List<dynamic>();
                if (Session["itemDeposito_session"] != null)
                {
                    var detalles = Session["itemDeposito_session"] as List<DtoDetalleOperacion>;
                    list.AddRange(detalles.Select(det =>
                        new
                        {
                            Id = det.UniqueId.ToString(),
                            Nro = det.Numero,
                            Banco = det.Banco,
                            Importe = det.Monto,
                            FechaVencimiento = det.FechaVencimiento,
                            ImporteApr = det.MontoCalculado,
                            Tasas = "",
                            Estado = det.Estado,
                            Cp = det.Cp,
                            CMC7 = det.CMC7
                        }));
                }

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = list.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UploadBack(HttpPostedFileBase fileBack)
        {
            var ext = Path.GetExtension(fileBack.FileName);
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
            {
                var fileName = DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ext;

                if (fileBack != null && fileBack.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/UploadImages/Cheques/"), fileName);
                    fileBack.SaveAs(path);
                }
                return Json(new { src = Url.Content("~/UploadImages/Cheques/") + fileName, success = true, image = fileName });
            }

            return Json(new { success = false });
        }

        [HttpPost]
        public JsonResult UploadFront(HttpPostedFileBase file)
        {
            var ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
            {
                var fileName = DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ext;

                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/UploadImages/Cheques/"), fileName);
                    file.SaveAs(path);
                }
                return Json(new { src = Url.Content("~/UploadImages/Cheques/") + fileName, success = true, image = fileName });
            }

            return Json(new { success = false });
        }

    }
}