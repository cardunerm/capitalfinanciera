﻿using Business;
using Controllers;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class FacturaVendedorController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetFacturaVendedor(string nombre = "", string cuit = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = FacturaVendedorBusiness.Instance.GetByFilters(nombre, cuit, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMFacturaVendedor(long? id, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
            ;
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new FacturaVendedor());
            }

            var entity = FacturaVendedorBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

      

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMFacturaVendedor(FacturaVendedor vendedor, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
          
            try
            {
                if (ViewBag.PopUp)
                    ModelState.Remove("IdTasa");

                if (ModelState.IsValid)
                {
                    vendedor.Activo = true;

                    //Valido si es una actualización o una nuevo
                    if (vendedor.IdVendedor > 0)
                    {
                        FacturaVendedor _entidad = FacturaVendedorBusiness.Instance.GetEntity(long.Parse(vendedor.IdVendedor.ToString()));
                        _entidad.Nombre = vendedor.Nombre;
                        _entidad.Apellido = vendedor.Apellido;
                        _entidad.Activo = true;

                    }

                    FacturaVendedorBusiness.Instance.SaveOrUpdate(vendedor);                   

                    return Json(
                        new
                        {
                            id = vendedor.IdVendedor,
                            name = vendedor.Nombre,
                            msg = Resources.Resource.SaveSuccess,
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                FacturaCliente entidad = FacturaClienteBusiness.Instance.GetEntity(id);
                entidad.Activo = false;
                FacturaClienteBusiness.Instance.SaveOrUpdate(entidad);
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        public JsonResult ValidateCliente(string nombre = "", string cuit = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(nombre))
                {
                    var entity = ClienteBusiness.Instance.ClienteExist(nombre, cuit);

                    if (entity != null)
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true, Id = entity.IdCliente });
                    }
                    else
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                    }
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Requerido");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}