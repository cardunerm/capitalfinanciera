﻿using Business;
using Controllers;
using CurrentContext;
using Luma.Utils.Security;
using Domine;
using Domine.Dto;
using MvcAppDCR.Code;
using MvcAppDCR.SignalRhubs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Utils.Enumeraciones;
using System.IO;
using Luma.Utils;
using System.Globalization;

namespace MvcAppDCR.Controllers
{
    public class OperacionController : BaseController
    {
        #region Aprobacion
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SaveDetalleOperacionAprobacion(DtoOperacionAprobacion operacion, short? EstadoOperacion)
        {
            try
            {
                OperacionBusiness.Instance.UpdateDetalleOperacion(operacion.DetalleOperacion, EstadoOperacion, SessionContext.UsuarioActual, SessionContext.Configuracion);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult ActivarEdicion()
        {
            try
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult Pagar(string IDKey)
        {

            if (IDKey == null)
                return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
            try
            {
                var idOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
                OperacionBusiness.Instance.Pagar(idOp, CurrentContext.SessionContext.UsuarioActual);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception)
            {
                return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult FinalizarOperacion(DtoOperacionAprobacion operacion)
        {
            try
            {
                string msg = Resources.Resource.ErrorOperacion;

                var result = OperacionBusiness.Instance.Finalizar(operacion, SessionContext.UsuarioActual, ref msg);
                SendNotification(operacion);
                if (result)
                    return Json(new
                    {
                        success = true,
                        url = Url.Content("bandejaaprobacion")
                    });
                else
                {
                    return Json(new { success = false, msg = msg }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
            }
        }

        private void SendNotification(DtoOperacionAprobacion operacion)
        {
            var msg = CreateMessage(operacion);
            if (msg != null)
                NotificationHub.SendToEvaluador(operacion.IdUser.ToString(), msg);
        }

        private string CreateMessage(DtoOperacionAprobacion operacion)
        {
            string ret = "La operación {0} ha cambio el estado a : {1}";
            if ((Estados.eEstadosOp)operacion.IdEstado != Estados.eEstadosOp.Pendiente)
            {
                return string.Format(ret, operacion.Id, ((Estados.eEstadosOp)operacion.IdEstado).GetEnumeracionName());
            }
            return null;
        }

        public ActionResult PrintOperacion(string idOperacion, string idgarantia)
        {
            var idOp = Convert.ToInt64(StringCipher.Decrypt(idOperacion, CurrentContext.SessionContext.Key));
            if (idOperacion != null)
            {
                var operacion = OperacionBusiness.Instance.GetEntity(idOp);
                //var montoLetra = new UtilConverNumber().Convertir(operacion.MontoAprobado.ToString(), false, null);


                var _impuestocheque = operacion.DetalleOperacion.Sum(p => p.ImpuestosCheque);
                var _gastos = operacion.DetalleOperacion.Sum(p => p.Gastos);
                var _tasas = operacion.DetalleOperacion.Sum(p => p.Impuestos);
                var _gastototal = Math.Round(_impuestocheque + _gastos + _tasas,2);
                var _iva = Math.Round((_gastototal * 21) / 100,2);
                var _sesion = Math.Round((decimal)operacion.Sesion,2);               
                var _total = Math.Round((decimal)operacion.Monto - _gastototal - _iva - (decimal)_sesion,2);

                var montoLetra = (idgarantia == "1")? new UtilConverNumber().Convertir(_total.ToString(), false, null) : new UtilConverNumber().Convertir(operacion.MontoAprobado.ToString(), false, null);

                var model = new DtoPrintOperacion
                {
                    Empresa = SessionContext.Empresa,
                    Operacion = operacion,
                    MontoLetra = montoLetra,
                    ImprimeDetalle = int.Parse(idgarantia),
                    TipoContribuyente = (int)operacion.Cliente.IdCondicionTributaria,
                    Gastos = _gastototal.ToString(),
                    Iva = _iva.ToString(),
                    Sesion =  _sesion.ToString(),
                    Total = (idgarantia == "1")? _total.ToString() : operacion.MontoAprobado.ToString()
                };
                return View(model);
            }
            return HttpNotFound();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult EditDetalleAprobacion(int Id)
        {
            try
            {
                var edit = OperacionBusiness.Instance.GetByConditions<DetalleOperacion>(model => model.Id == Id, model => model.Cheque, model => model.Cheque.Banco, model => model.Operacion, model => model.Operacion.Cliente).FirstOrDefault();
                if (edit.TasaInteres == null)
                {
                    var tasa = OperacionBusiness.Instance.GetByConditions<EmisorValor>(emisor => emisor.NumeroEmisor == edit.Cheque.NroCuenta).FirstOrDefault();
                    edit.TasaInteres = tasa != null ? tasa.TasaAsignada : 0.0M;
                }

                if (edit.Gastos > 0)
                {
                    edit.Gastos = ((decimal)edit.Gastos * 100) / (decimal)edit.Operacion.Monto;
                }


                return Json(new
                {
                    success = true,
                    result = new DtoDetalleOperacionAprobacion
                    {
                        Numero = edit.Cheque.Numero,
                        TitularCuenta = edit.Cheque.TitularCuenta,
                        NumeroCuenta = edit.Cheque.NroCuenta,
                        IdBanco = edit.Cheque.IdBanco,
                        SucursalEmision = edit.Cheque.SucursalEmision,
                        FechaVencimiento = edit.Cheque.FechaVencimiento,
                        Monto = edit.Cheque.Monto,
                        DiasClearing = OperacionBusiness.CalcularDiasClering(edit.Operacion.Fecha, edit.Cheque.FechaVencimiento),
                        //DiasClearingHidden = OperacionBusiness.CalcularDiasClering(edit.Operacion.Fecha, edit.Cheque.FechaVencimiento),
                        Gastos = edit.Gastos,
                        //GastoStr = edit.Gastos.ToString("#,00"),
                        GastoStr = edit.Gastos.ToString(),
                        Cp = edit.Cheque.CP == null ? String.Empty : edit.Cheque.CP.ToString(),
                        CMC7 = edit.Cheque.CMC7,
                        Tasa = edit.TasaInteres,
                        //TasaStr = ((double)edit.TasaInteres).ToString("#,00"),
                        TasaStr = ((double)edit.TasaInteres).ToString(),
                        MontoAprobado = (double?)edit.MontoAprobado,
                        Observacion = edit.Observacion,
                        IdEstado = edit.Estado,
                        Id = edit.Id,
                        DetalleCalculo = OperacionBusiness.DescripcionCalculo(edit),
                        ImagenFront = edit.Cheque.ImagenFront ?? "no-Imagen.png",
                        ImagenBack = edit.Cheque.ImagenBack ?? "no-Imagen.png",


                    }
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
            }
        }

        private decimal CalcularMonto(DetalleOperacion edit, Configuracion configuracion)
        {
            if (edit.Estado == (int)Estados.eEstadosOpDetalle.Pendiente)
            {
                var fechaLiquidacion = edit.Operacion.Fecha;
                var tasa = edit.Operacion.Cliente.IdTasa != null ? edit.Operacion.Cliente.IdTasa.Value : 0;
                var fechaPago = edit.Cheque.FechaVencimiento;

                //Calculos
                var diasClering = OperacionBusiness.CalcularDiasClering(fechaLiquidacion, fechaPago);
                var impuestoDiario = OperacionBusiness.CalcularImpuestoDiario(diasClering, edit.Cheque.Monto, tasa);
                var impuestoAlCheque = OperacionBusiness.CalcularImpustoAlCheque(edit.Cheque.Monto, configuracion);
                var impuestoPorGastos = OperacionBusiness.CalcularImpustoPorGastos(edit.Cheque.Monto, configuracion);
                edit.Gastos = impuestoPorGastos;
                edit.Impuestos = impuestoDiario;
                edit.ImpuestosCheque = impuestoAlCheque;
                edit.MontoAprobado = (decimal)Math.Round(edit.Cheque.Monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2);

                return Math.Round(edit.Cheque.Monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2);
            }
            return (decimal)(edit.MontoAprobado ?? 0);
        }

        private JsonResult CalcularMontoConGasto(DateTime fechaPago, DateTime fechaLiquidacion, decimal tasa, decimal monto, decimal gasto, int estado, int diasC, bool? calculateCl, Configuracion configuracion)
        {
            try
            {
                //Calculos
                var diasClering = 0;

                if (calculateCl.HasValue && calculateCl.Value)
                    diasClering = OperacionBusiness.CalcularDiasClering(fechaLiquidacion, fechaPago);
                else
                    diasClering = diasC;

                var impuestoDiario = OperacionBusiness.CalcularImpuestoDiario(diasClering, monto, tasa);
                var impuestoAlCheque = OperacionBusiness.CalcularImpustoAlCheque(monto, configuracion);
                var impuestoPorGastos = OperacionBusiness.CalcularImpustoPorGastosByGasto(monto, gasto);

                return Json(new { amount = Math.Round(monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2), dc = diasClering });
            }
            catch (Exception ex) { throw ex; }

        }

        private decimal CalcularMonto(DateTime fechaPago, DateTime fechaLiquidacion, decimal tasa, decimal monto, Configuracion configuracion)
        {
            try
            {
                //Calculos
                var diasClering = OperacionBusiness.CalcularDiasClering(fechaLiquidacion, fechaPago);
                var impuestoDiario = OperacionBusiness.CalcularImpuestoDiario(diasClering, monto, tasa);
                var impuestoAlCheque = OperacionBusiness.CalcularImpustoAlCheque(monto, configuracion);
                var impuestoPorGastos = OperacionBusiness.CalcularImpustoPorGastos(monto, configuracion);

                return Math.Round(monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2);
            }
            catch (Exception ex) { throw ex; }

        }

        public JsonResult GetBandejaOperacionAprobacion(DateTime? fecha, int? cliente, int? estado, int? op, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                var result = OperacionBusiness.Instance.GetBandejaAprobacionByFilters(fecha, cliente, estado, op, skip, top);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = result.Result, count = result.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Aprobacion(string IDKey)
        {
            if (IDKey == null)
                return Redirect("BandejaAprobacion");
            try
            {
                var idOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
                var operacion = OperacionBusiness.Instance.GetByConditions(op => op.Id == idOp, op => op.Cliente).FirstOrDefault();
                if (operacion == null)
                    return Redirect("BandejaAprobacion");

                LoadCombo(true, false, (Estados.eEstadosOp)operacion.Estado);

                var resumen = OperacionBusiness.Instance.GetSaldoCliente(operacion.IdCliente);

                var dto = new DtoOperacionAprobacion
                {
                    Id = operacion.Id,
                    Fecha = operacion.Fecha,
                    Cliente = operacion.Cliente.NameAndCuit,
                    Observacion = operacion.Observacion,
                    ObservacionEvaluacion = operacion.ObservacionEvaluacion,
                    DetalleOperacion = null,
                    IdEstado = operacion.Estado,
                    Saldo = resumen.Saldo,
                    MontoRechazos = resumen.MontoRechazos,
                    CantidadRechazos = resumen.CantidadRechazos,
                    Sesion = (operacion.Sesion != null) ? operacion.Sesion.ToString() : ""
                };
                ViewBag.IdOperacion = StringCipher.Encrypt(operacion.Id.ToString(), CurrentContext.SessionContext.Key);
                return View(dto);
            }
            catch
            {
                return Redirect("BandejaAprobacion");
            }
        }

        public JsonResult GetCheques(string IDKey)
        {
            int id = 0;

            id = Convert.ToInt32(StringCipher.Decrypt(HttpUtility.UrlDecode(IDKey), CurrentContext.SessionContext.Key));

            try
            {
                var detalles = OperacionBusiness.Instance.GetChequesByOperacion(id);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = detalles, count = detalles.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Operacion

        public ActionResult BandejaAprobacion()
        {
            //AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
            LoadCombo(false, true);
            return View(new DtoOperacion());

        }

        #endregion
        #region Creacion
        public JsonResult GetBandejaOperacionOperador(DateTime? fecha, int? cliente, int? estado, int? op, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                var result = OperacionBusiness.Instance.GetBandejaOperadorByFilters(fecha, cliente, estado, op, SessionContext.UsuarioActual, skip, top);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = result.Result, count = result.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Operacion
        public ActionResult BandejaOperador()
        {
            LoadCombo(false, true);
            return View(new DtoOperacionInversor());

        }

        [HttpPost]
        public JsonResult UploadFront(HttpPostedFileBase file)
        {
            var ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
            {
                var fileName = DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ext;

                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/UploadImages/Cheques/"), fileName);
                    file.SaveAs(path);
                }
                return Json(new { src = Url.Content("~/UploadImages/Cheques/") + fileName, success = true, image = fileName });
            }

            return Json(new { success = false });
        }

        [HttpPost]
        public JsonResult UploadBack(HttpPostedFileBase fileBack)
        {
            var ext = Path.GetExtension(fileBack.FileName);
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
            {
                var fileName = DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ext;

                if (fileBack != null && fileBack.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/UploadImages/Cheques/"), fileName);
                    fileBack.SaveAs(path);
                }
                return Json(new { src = Url.Content("~/UploadImages/Cheques/") + fileName, success = true, image = fileName });
            }

            return Json(new { success = false });
        }


        // GET: Operacion
        public ActionResult Crear()
        {

            ViewBag.Vendedores = GetVendedores();
            LoadCombo(false);
            return View(new DtoOperacion());

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult DeleteDetalleCreacion(string id)
        {
            if (Session["itemOperacion_session"] != null)
            {
                var detalle = Session["itemOperacion_session"] as List<DtoDetalleOperacion>;
                detalle.Remove(detalle.FirstOrDefault(det => det.UniqueId.ToString() == id));
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult EditDetalleCreacion(string id)
        {
            if (Session["itemOperacion_session"] != null)
            {
                var edit = (Session["itemOperacion_session"] as List<DtoDetalleOperacion>).FirstOrDefault(det => det.UniqueId.ToString() == id);
                return Json(new { success = true, result = edit }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetChequesCreacion()
        {
            try
            {
                var list = new List<dynamic>();
                if (Session["itemOperacion_session"] != null)
                {
                    var detalles = Session["itemOperacion_session"] as List<DtoDetalleOperacion>;
                    list.AddRange(detalles.Select(det =>
                        new
                        {
                            Id = det.UniqueId.ToString(),
                            Nro = det.Numero,
                            Banco = det.Banco,
                            Importe = det.Monto,
                            FechaVencimiento = det.FechaVencimiento,
                            ImporteApr = det.MontoCalculado,
                            Tasas = "",
                            Estado = det.Estado,
                            Cp = det.Cp,
                            CMC7 = det.CMC7
                        }));
                }

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = list.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private void LoadCombo(bool aprobador = false, bool bandeja = false, Estados.eEstadosOp estado = Estados.eEstadosOp.Pendiente)
        {
            ViewBag.Bancos = BancoBusiness.Instance.GetAll().OrderBy(banco => banco.Nombre).ToList();
            ViewBag.Estados = OperacionBusiness.Instance.GetEstados(bandeja, aprobador, estado);

            ViewBag.Tasas = ClienteBusiness.Instance.GetByConditions<Tasa>(tasa => tasa.Activa == true)
                .OrderBy(order => order.Value)
                .GetSelectListItem("Id", "Value");

            List<SelectListItem> _condiciontributaria = new List<SelectListItem>();
            _condiciontributaria.Add(new SelectListItem { Text = "Consumidor Final", Value = "1" });
            _condiciontributaria.Add(new SelectListItem { Text = "Responsable Incripto", Value = "2" });
            ViewBag.CondicionTributaria = _condiciontributaria;

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendCreacion(DtoOperacion operacion)
        {
            if (IsValidCabecera(operacion))
            {
                long id = OperacionBusiness.Instance.CreateOperacion(operacion, Session["itemOperacion_session"] as List<DtoDetalleOperacion>, CurrentContext.SessionContext.UsuarioActual, SessionContext.Configuracion);
                Session["itemOperacion_session"] = new List<DtoDetalleOperacion>();
                SendNotificationAprobadores(id, Estados.eEstadosOp.Pendiente);
                return Json(new { success = true, url = "bandejaoperador" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, msg = Resources.Resource.DatosObligatorios }, JsonRequestBehavior.AllowGet);
        }

        private void SendNotificationAprobadores(long id, Estados.eEstadosOp estado)
        {
            string ret = "La operación {1} ha cambiado de estado a {0}.";
            var msg = string.Format(ret, estado.GetEnumeracionName(), id);
            NotificationHub.SendToGroup(msg);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectResult Volver(string bandeja)
        {
            Session["itemOperacion_session"] = new List<DtoDetalleOperacion>();
            return Redirect("~/Operacion/" + bandeja);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelOperacion(string IDKey)
        {
            if (IDKey == null)
                return Redirect("BandejaAprobacion");

            var idOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
            OperacionBusiness.Instance.CancelOperacion(idOp);
            SendNotificationAprobadores(idOp, Estados.eEstadosOp.Cancelada);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectResult ClearCreacion()
        {
            Session["itemOperacion_session"] = new List<DtoDetalleOperacion>();
            return Redirect("~/Operacion/Crear");
        }

        private bool IsValidCabecera(DtoOperacion operacion)
        {
            if (operacion.IdCliente == 0)
            {
                return false;
            }

            if (Session["itemOperacion_session"] == null)
            {
                return false;
            }
            return true;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CalcularMontoCheque(DateTime? vencimiento, DateTime? fecha, string monto, string tasa, string gasto, int? estado, int? diasClearing, bool? calculateCl)
        {

            decimal? _monto = 0, _tasa = 0, _gasto = 0;
            monto = (monto ?? "").Replace('.', ',');
            tasa = (tasa ?? "").Replace('.', ',');
            gasto = (gasto ?? "").Replace('.', ',');


            decimal montotemp = 0;
            if (decimal.TryParse(monto, NumberStyles.Float, new CultureInfo("es-AR"), out montotemp))
            {
                _monto = montotemp;
            }

            if (tasa != "")
            {
                decimal tasatemp = 0;
                if (decimal.TryParse(tasa, NumberStyles.Float, new CultureInfo("es-AR"), out tasatemp))
                {
                    _tasa = tasatemp;
                }
            }

            if (gasto != "")
            {
                decimal gastotemp = 0;
                if (decimal.TryParse(gasto, NumberStyles.Float, new CultureInfo("es-AR"), out gastotemp))
                {
                    _gasto = gastotemp;
                }
            }

            JsonResult calculo = null;

            if (vencimiento.HasValue && fecha.HasValue && _monto.HasValue && _tasa.HasValue && _gasto.HasValue)
                calculo = CalcularMontoConGasto(vencimiento.Value, fecha.Value, _tasa.Value, _monto.Value, _gasto.Value, estado.Value, diasClearing.Value, calculateCl, SessionContext.Configuracion);


            return calculo;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddDetalleOperacion(DtoOperacion operacion)
        {
            try
            {
                RemoveModelStateOperacion(ModelState);
                if (ModelState.IsValid)
                {
                    if (Session["itemOperacion_session"] == null)
                    {
                        Session["itemOperacion_session"] = new List<DtoDetalleOperacion>();
                    }
                    var detalle = Session["itemOperacion_session"] as List<DtoDetalleOperacion>;

                    if (ChequeBusiness.Instance.GetByConditions(
                        c => c.IdBanco == operacion.DetalleOperacion.IdBanco
                        && c.Numero == operacion.DetalleOperacion.Numero
                        && c.NroCuenta == operacion.DetalleOperacion.NumeroCuenta
                        && c.Activo == true
                        ).Count > 0
                        || detalle.Count(p => p.IdBanco == operacion.DetalleOperacion.IdBanco && p.Numero == operacion.DetalleOperacion.Numero && operacion.DetalleOperacion.UniqueId != p.UniqueId) > 0)
                    {
                        throw new Exception("El cheque ya ha sido ingresado en el sistema");
                    }

                    if (!string.IsNullOrEmpty(operacion.DetalleOperacion.UniqueId))
                    {
                        var detRemove = detalle.FirstOrDefault(det => det.UniqueId == operacion.DetalleOperacion.UniqueId);
                        detalle.Remove(detRemove);
                    }
                    else
                    {
                        operacion.DetalleOperacion.UniqueId = Guid.NewGuid().ToString();
                    }

                    operacion.DetalleOperacion.Estado = GetEnumeracionName(Estados.eEstadosOpDetalle.Pendiente);
                    //var tasa = OperacionBusiness.Instance.GetMaxTasa();
                    var tasa = ClienteBusiness.Instance.GetTasaCliente(operacion.DetalleOperacion.IdCliente);                    
                    operacion.DetalleOperacion.MontoCalculado = CalcularMonto(operacion.DetalleOperacion.FechaVencimiento, DateTime.Now.Date, tasa, operacion.DetalleOperacion.Monto, SessionContext.Configuracion);
                    detalle.Add(operacion.DetalleOperacion);

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GetEnumeracionName(Enum value)
        {
            return (value).GetType()
                          .GetMember(value.ToString())
                          .First()
                          .GetCustomAttribute<DisplayAttribute>().GetName();
        }

        private void RemoveModelStateOperacion(ModelStateDictionary modelState)
        {
            modelState.Remove("IdCliente");
            modelState.Remove("Fecha");
            modelState.Remove("Monto");
        }
        #endregion
        #region Comunes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ViewOp(string IDKey, string Url)
        {
            if (IDKey == null)
                return Redirect(Request.UrlReferrer.AbsoluteUri);

            var idOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
            var operacion = OperacionBusiness.Instance.GetByConditions<Operacion>(op => op.Id == idOp, new List<string> { { "Cliente" } }).FirstOrDefault();
            if (operacion != null)
            {
                ViewBag.Url = Url;
                LoadCombo(false);
                var dto = operacion.GetDtoOperacion();
                var resumen = OperacionBusiness.Instance.GetSaldoCliente(operacion.IdCliente);
                dto.Saldo = resumen.Saldo;
                dto.CantidadRechazos = resumen.CantidadRechazos;
                dto.MontoRechazos = resumen.MontoRechazos;
                dto.Sesion = (operacion.Sesion != null) ? operacion.Sesion.ToString() : string.Empty;
                return View(dto);
            }
            else
            {
                return Redirect(Url);
            }

        }

        public JsonResult GetMotivosRechazo(string IDKey)
        {
            if (IDKey != null)
            {
                try
                {
                    var idDetOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
                    var detalles = OperacionBusiness.Instance.GetMotivosRechazo(idDetOp);
                    return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = detalles }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { result = new { }, count = 0 }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetResumenCuenta(string key)
        {

            if (key != null)
            {
                var tasa = OperacionBusiness.Instance.GetTasaCliente(key);
                RecalcularMontos(tasa);

                return Json(
                    new
                    {
                        success = true,
                        tasa = tasa,
                        datos = OperacionBusiness.Instance.GetSaldoCliente(Convert.ToInt32(key))
                    });
            }
            return Json(new { success = false, saldo = 0 });
        }

        private void RecalcularMontos(decimal tasa)
        {
            if (Session["itemOperacion_session"] != null)
                foreach (var item in (Session["itemOperacion_session"] as List<DtoDetalleOperacion>))
                {
                    item.MontoCalculado = CalcularMonto(item.FechaVencimiento, DateTime.Now.Date, tasa, item.Monto, SessionContext.Configuracion);
                }
        }

        [HttpPost]
        public JsonResult CreateImagesScan(string front, string back)
        {
            try
            {
                var frontName = CreateImage(front, "front");
                var backName = CreateImage(back, "back");
                return
                  Json(new
                  {
                      success = true,
                      frontName = frontName,
                      frontSrc = Url.Content("~/UploadImages/Cheques/") + frontName,
                      backName = backName,
                      backSrc = Url.Content("~/UploadImages/Cheques/") + backName,
                  });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private string CreateImage(string imageBase64, string prefix)
        {
            try
            {
                if (!string.IsNullOrEmpty(imageBase64))
                {
                    var fileName = prefix + DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ".jpg";
                    byte[] imageBytes = Convert.FromBase64String(imageBase64);
                    var path = Path.Combine(Server.MapPath("~/UploadImages/Cheques/"), fileName);
                    System.IO.File.WriteAllBytes(path, imageBytes);
                    return fileName;
                }
            }
            catch
            { }
            return "no-Imagen.png";

        }


        public List<SelectListItem> GetVendedores(bool indistinto = false)
        {
            List<SelectListItem> vendedorList = new List<SelectListItem>();
            var vendedores = FacturaVendedorBusiness.Instance.GetClientes();

            foreach (var item in vendedores)
                vendedorList.Add(new SelectListItem { Value = item.IdVendedor.ToString(), Text = item.Nombre });


            if (indistinto)
                vendedorList.Insert(0, new SelectListItem { Value = "0", Text = "Indistinto" });

            return vendedorList;
        }
        #endregion
    }
}