﻿using Business;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controllers
{
    public class RecepcionChequesController : BaseController
    {
        //
        // GET: /RecepcionCheques/

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetRecepcionCheque(DateTime fechaDesde, DateTime fechaHasta, string cliente = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = RecepcionChequeBusiness.Instance.GetRecepcionCheques(fechaDesde, fechaHasta, cliente, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMRecepcionCheque(int? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new RecepcionCheque());
            }

            var entity = RecepcionChequeBusiness.Instance.GetRecepcionCheque(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMRecepcionCheque(RecepcionCheque recepcionCheque, Cheque[] cheques)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (recepcionCheque.IdRecepcionCheque == 0)
                    {
                        RecepcionChequeBusiness.Instance.CreateRecepcionCheques(recepcionCheque, cheques);
                    }
                    else
                    {
                        RecepcionChequeBusiness.Instance.UpdateRecepcionCheques(recepcionCheque);
                    }

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                RecepcionChequeBusiness.Instance.DeleteLogic(id, RecepcionChequeBusiness.Instance.ValidateDelete, AuditoriaBusiness.Instance.AuditoriaRecepcionCheque);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}
