﻿using Business;
using Controllers;
using Domine;
using Domine.Dto;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using MvcAppDCR.Utils;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class CajaController : BaseController
    {
        public ActionResult Caja()
        {
            LoadCombo();
            return View(new DtoCaja());

        }

        public ActionResult CerrarCaja()
        {
            Caja modelCaja = CajaBusiness.Instance.GetUltimaCaja();

            return View(modelCaja);
        }
        public ActionResult Movimientos(int idCaja)
        {
            Caja modelCaja = CajaBusiness.Instance.GetEntity<Caja>(idCaja);

            return View(modelCaja);
        }

        public ActionResult CreateOperacionCaja(int idCaja, int tipoMovimiento)
        {
            OperacionCaja modelOperacion = new OperacionCaja();

            modelOperacion.IdCaja = idCaja;
            modelOperacion.TipoMovimiento = tipoMovimiento;

            return View(modelOperacion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateOperacionCaja(OperacionCaja operacion)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    operacion = OperacionCajaBusiness.Instance.SaveOrUpdateOperacionCaja(operacion);

                    return Json(
                        new
                        {
                            id = operacion.IdOperacionCaja,
                            msg = Resources.Resource.SaveSuccess,
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        public JsonResult GetCajas(string fecha, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                DateTime? _fecha = null;

                if (fecha != null && fecha != "")
                {
                   _fecha = Convert.ToDateTime(fecha);
                }

                var result = CajaBusiness.Instance.GetCajasByFilters(_fecha, CurrentContext.SessionContext.UsuarioActual, skip, top);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = result.Result, count = result.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetMovimientos(int idCaja, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                var result = OperacionCajaBusiness.Instance.GetMovimientos(idCaja, skip, top);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = result, count = result.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult CrearCaja()
        {
            try
            {
                CajaBusiness.Instance.CrearCaja(CurrentContext.SessionContext.UsuarioActual);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CerraUltimaCaja()
        {
            try
            {
                CajaBusiness.Instance.CerrarCaja(CurrentContext.SessionContext.UsuarioActual);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
   
        private void LoadCombo()
        {
             ViewBag.Tipo = new List<SelectListItem>
            {
                { new SelectListItem { Value = "1", Text="Egreso" } },
                { new SelectListItem { Value = "2", Text="Ingreso" } }
            };
        }

      

    }
}