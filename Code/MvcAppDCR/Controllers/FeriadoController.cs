﻿using Business;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Web.Mvc;

namespace Controllers
{
    public class FeriadoController : BaseController
    {
        //
        // GET: /Feriado/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetFeriados(string fechaStr = "" ,string descripcion = "",  [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;
                DateTime fecha;

                if (!DateTime.TryParse(fechaStr, out fecha)) 
                {
                    fecha = DateTime.Now;
                }
    
                var list = FeriadoBusiness.Instance.GetByFilters(descripcion,fecha, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMFeriado(long? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new Feriado());
            }

            var entity = FeriadoBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMFeriado(Feriado Feriado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    FeriadoBusiness.Instance.SaveOrUpdateFeriado(Feriado);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                FeriadoBusiness.Instance.DeleteLogic(id, FeriadoBusiness.Instance.ValidateDelete, AuditoriaBusiness.Instance.AuditoriaFeriado);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        public JsonResult ValidateFeriado(DateTime fecha)
        {
            try
            {
                if (fecha != null)
                {
                    var entity = FeriadoBusiness.Instance.FeriadoExist(fecha);

                    if (entity != null)
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true, Id = entity.IdFeriado });
                    }
                    else
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                    }
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Requerido");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}