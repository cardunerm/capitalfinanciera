﻿using Business;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controllers
{
    public class LiquidacionController : BaseController
    {
        //
        // GET: /Liquidacion/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetLiquidacion(string numero = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = LiquidacionBusiness.Instance.GetLiquidaciones(numero, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMLiquidacion(int? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new Liquidacion());
            }

            var entity = LiquidacionBusiness.Instance.GetLiquidacion(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMLiquidacion(Liquidacion liquidacion, LiquidacionDetalle[] detalles)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (liquidacion.IdLiquidacion == 0)
                    {
                        LiquidacionBusiness.Instance.CreateLiquidacion(liquidacion, detalles);
                    }
                    else
                    {
                        LiquidacionBusiness.Instance.UpdateLiquidacion(liquidacion);
                    }

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                LiquidacionBusiness.Instance.DeleteLogic(id, LiquidacionBusiness.Instance.ValidateDelete, AuditoriaBusiness.Instance.AuditoriaLiquidacion);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}
