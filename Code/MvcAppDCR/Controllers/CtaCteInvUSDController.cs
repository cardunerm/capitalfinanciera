﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Business;
using MvcAppDCR.Code;
using Enumerations;
using Domine;
using MvcAppDCR.Utils;
using MvcAppDCR.Model.Entities;
using CrystalDecisions.CrystalReports.Engine;
using Mvc.Utils;
using Resources;
using Domine.Dto;
using CurrentContext;
using Luma.Utils;
using System.Net.Mime;

namespace Controllers
{
    public class CtaCteInvUSDController : BaseController
    {

        public ActionResult Index()
        {

            return View();

        }

        public JsonResult GetCtaCteInvUSD(string inversor = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = CtaCteInvUSDBussines.Instance.GetCuentaCorrienteList(inversor);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMCtaCteInvUSD(long? id)
        {
            ViewBag.EstadoCtaCte = DCREnumerationsHelper.GetEstadoCtaCte(insertIndistinct: false);

            if (!id.HasValue || id.Value <= 0)
            {
                return View(new CtaCteInvUSDBussines());
            }

            var entity = CtaCteInvUSDBussines.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMCtaCteInvUSD(CuentaCorrienteInversor cuentaCorriente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CtaCteInvUSDBussines.Instance.SaveOrUpdateCuentaCorriente(cuentaCorriente);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        public ActionResult ReporteCuentaCorriente(string fechaDesde, string fechaHasta, int idCuentaCorrienteInversor = 0)
        {
            //try
            //{
            //    DateTime? fecha_Desde = null;
            //    DateTime? fecha_Hasta = null;

            //    if (fechaDesde == null)
            //    {
            //        fecha_Desde = DateTime.Now.AddMonths(-6);
            //    }
            //    else
            //    {
            //        fecha_Desde = Convert.ToDateTime(fechaDesde);
            //    }

            //    if (fechaHasta == null)
            //    {
            //        fecha_Hasta = DateTime.Now.AddDays(1);
            //    }
            //    else
            //    {
            //        fecha_Hasta = Convert.ToDateTime(fechaHasta).AddDays(1);
            //    }

            //    decimal? debito = 0.0M;
            //    decimal? credito = 0.0M;
            //    decimal? saldo = 0.0M;
            //    decimal? saldoAnterior = 0.0M;

            //    OperacionCtaCteInvUSD opcuentaCorrienteBD = new OperacionCtaCteInvUSD();

            //    var cuentaCorriente = CtaCteInvUSDBussines.Instance.GetEntity(idCuentaCorrienteInversor);

            //    List<OperacionCtaCteInvUSD> opBD = OperacionBusiness.Instance.GetByConditions<OperacionCtaCteInvUSD>(op => op.Fecha >= fecha_Desde && op.Fecha <= fecha_Hasta && op.IdCtaCteInvUSD == cuentaCorriente.IdCtaCteInvUSD);

            //    debito = CtaCteInvUSDBussines.Instance.GetCtaCteSaldoDebito(fecha_Desde, fecha_Hasta, idCuentaCorrienteInversor);

            //    credito = CtaCteInvUSDBussines.Instance.GetCtaCteSaldoCredito(fecha_Desde, fecha_Hasta, idCuentaCorrienteInversor);

            //    saldo = credito - debito;

            //    saldoAnterior = CtaCteInvUSDBussines.Instance.GetSaldoByFecha(fecha_Desde, idCuentaCorrienteInversor);

            //    if (cuentaCorriente != null)
            //    {
            //        var opcuentaCorrienteModel = new List<OperacionCtaCteInversorUSDModel>();

            //        opcuentaCorrienteModel.Add
            //                                   (
            //                                        new OperacionCtaCteInversorUSDModel
            //                                        {
            //                                            IdOperacionCtaCteInversorUSD = 0,
            //                                            IdCtaCteInversorUSD = idCuentaCorrienteInversor,
            //                                            TipoMovimiento = saldoAnterior < 0 ? 1 : 2,
            //                                            Descripcion = "Saldo al " + fecha_Desde.Value.AddDays(-1).ToShortDateString(),
            //                                            Fecha = fecha_Desde.Value.AddDays(-1),
            //                                            Monto = saldoAnterior,
            //                                            Estado = true,
            //                                            Debito = saldoAnterior < 0 ? saldoAnterior : 0,
            //                                            Credito = saldoAnterior >= 0 ? saldoAnterior : 0,
            //                                            Saldo = saldoAnterior
            //                                        }
            //                                   );

            //        foreach (var item in opBD)
            //        {
            //            opcuentaCorrienteBD = OperacionCtaCteInvUSDBussines.Instance.GetEntity(item.IdOperacionCtaCteInvUSD);

            //            if (opcuentaCorrienteBD.TipoMovimiento == 1)
            //            {
            //                saldoAnterior = saldoAnterior - opcuentaCorrienteBD.Monto;
            //            }
            //            else
            //            {
            //                saldoAnterior = saldoAnterior + opcuentaCorrienteBD.Monto;
            //            }

            //            opcuentaCorrienteModel.Add
            //                                    (
            //                                         new OperacionCtaCteInversorUSDModel
            //                                         {
            //                                             IdOperacionCtaCteInversorUSD = opcuentaCorrienteBD.IdOperacionCtaCteInvUSD,
            //                                             IdCtaCteInversorUSD = opcuentaCorrienteBD.IdCtaCteInvUSD,
            //                                             TipoMovimiento = opcuentaCorrienteBD.TipoMovimiento,
            //                                             Descripcion = opcuentaCorrienteBD.Descripcion,
            //                                             Fecha = opcuentaCorrienteBD.Fecha,
            //                                             Monto = opcuentaCorrienteBD.Monto,
            //                                             Estado = opcuentaCorrienteBD.Estado,
            //                                             Debito = opcuentaCorrienteBD.TipoMovimiento == 1 ? opcuentaCorrienteBD.Monto : 0,
            //                                             Credito = opcuentaCorrienteBD.TipoMovimiento == 2 ? opcuentaCorrienteBD.Monto : 0,
            //                                             Saldo = saldoAnterior
            //                                         }
            //                                    );
            //            //opcuentaCorrienteModel.Add((OperacionCuentaCorrienteModel)WebHelper.GenericLoad(opcuentaCorrienteBD, new OperacionCuentaCorrienteModel()));
            //        }

            //        ReportClass reportClass = ReportHelper.LoadReport("CuentaCorrienteReport");

            //        reportClass.SetDataSource(opcuentaCorrienteModel.OrderByDescending(op => op.Fecha));
            //        reportClass.SetParameterValue("nombreInversor", cuentaCorriente.Inversores.Nombre);
            //        reportClass.SetParameterValue("numeroCtaCte", cuentaCorriente.IdCtaCteInvUSD.ToString());
            //        reportClass.SetParameterValue("fechaDesde", fechaDesde);
            //        reportClass.SetParameterValue("fechaHasta", fechaHasta);
            //        reportClass.SetParameterValue("total", saldo);
            //        reportClass.SetParameterValue("debito", debito);
            //        reportClass.SetParameterValue("credito", credito);

            //        return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
            //    }
            //    else
            //    {
            //        return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = Resource.CuentaCorrienteNoEncontrada, btnGoToLabel = "", redirectBtn = false });
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            //}
            return null;
        }


        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult PagoCliente(PagoClienteModel model)
        {
            if (ModelState.IsValid)
            {
                string msg = "";
                if (ValidateInfo(model, out msg))
                {

                    try
                    {
                        int id = CtaCteInvUSDBussines.Instance.OperationInversor(model.IdInversor, model.Importe, model.Tipo);

                        return Json
                          (
                            new
                            {
                                success = true,
                                msg = "Complete los datos obligatorios",
                                id = id
                            }
                          );
                    }
                    catch
                    {
                        return Json
                          (
                            new
                            {
                                success = false,
                                msg = Resource.ErrorOperacion
                            }
                          );
                    }
                }
                else
                {
                    return Json
                          (
                            new
                            {
                                success = false,
                                msg = msg
                            }
                          );
                }
            }

            return Json
                (
                  new
                  {
                      success = false,
                      msg = "Complete los datos obligatorios"
                  }
                );
        }

        //public ActionResult PrintComprobante(string idOperacion)
        //{
        //    var idOp = Convert.ToInt64(idOperacion);
        //    if (idOperacion != null)
        //    {
        //        var operacion = OperacionCtaCteInvUSDBussines.Instance.GetEntity(idOp);

        //        var model = new DtoPrintOperacionCtaCteInvUSD
        //        {
        //            Empresa = SessionContext.Empresa,
        //            OperacionCtaCte = operacion,
        //            MontoLetra = new UtilConverNumber().Convertir(operacion.Monto.ToString(), false)
        //        };
        //        return View(model);
        //    }
        //    return HttpNotFound();
        //}

        private bool ValidateInfo(PagoClienteModel model, out string msg)
        {
            if (model.Tipo == 1)
            {
                var resumen = OperacionBusiness.Instance.GetSaldoCliente(model.IdCliente);
                if (resumen == null)
                {
                    msg = "El cliente no tiene cuenta corriente";
                    return false;
                }

                if (resumen.Saldo < model.Importe)
                {
                    msg = "El importe es mayor al saldo del cliente";
                    return false;
                }

                Caja caja = CajaBusiness.Instance.GetUltimaCaja();
                if (caja.Total < model.Importe)
                {
                    msg = "El monto del egreso es superior al saldo de caja";
                    return false;
                }
            }

            msg = "";
            return true;
        }

        [HttpPost]
        public JsonResult Saldo(int IdInversor)
        {
            try
            {
                var resumen = OperacionBusiness.Instance.GetSaldoCliente(IdInversor);

                return Json
                  (
                    new
                    {
                        success = true,
                        importe = resumen == null ? 0 : resumen.Saldo
                    }
                  );
            }
            catch
            {
                return Json
                  (
                    new
                    {
                        success = false,
                        msg = Resource.ErrorOperacion
                    }
                  );
            }
        }


    }
}