﻿using Business;
using Controllers;
using CrystalDecisions.CrystalReports.Engine;
using CurrentContext;
using Domine;
using Domine.Dto;
using Enumerations;
using Mvc.Utils;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using MvcAppDCR.Utils;
using Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class FacturaOperacionController : BaseController
    {     


        public ActionResult Index()
        {
            ViewBag.Clientes = GetClientes(true);
            ViewBag.Deudos = GetDeudos(true);
            ViewBag.Estados = GetEstados(true);
            return View();
        }

        public JsonResult GetFacturaOperaciones(string numero = "",string cliente = "", string deudor = "",string estado = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-EN");

            try
            {
                int rowCount = 0;

                var list = FacturaOperacionBusiness.Instance.GetByFilters(numero,cliente,deudor,estado, skip, top, ref rowCount);
                var listModel = new List<FacturaOperacionModel>();
                foreach (var item in list)
                {
                    listModel.Add(new FacturaOperacionModel
                    {
                        Cliente = item.FacturaCliente.Nombre,
                        CobroSaldo = ((decimal)item.FacturaCobroTotal - item.FacturaCobro.Sum(p => p.Monto)).ToString(),
                        CobroTotal = item.FacturaCobroTotal.ToString(),
                        DesembolsoSaldo = ((decimal)item.FacturaDesembolsoTotal + item.FacturaDesembolso.Where(p=>p.Activo == true).Sum(p => p.Monto)).ToString(),
                        DesembolsoTotal = item.FacturaDesembolsoTotal.ToString(),
                        Deudo = item.FacturaDeudo.Nombre.ToString(),
                        FechaFactura = item.FacturaFecha.Value.ToShortDateString(),
                        FechaCobro = item.FacturaFechaCobro.Value.ToShortDateString(),
                        IdFacturaOperacion = item.IdFacturaOperacion,
                        Monto = item.FacturaMonto.ToString(),
                        NumeroFactura = item.FacturaNumero,
                        IdFacturaEstado = (int)item.FacturaIdEstado

                    });
                }                

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = listModel, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
        public JsonResult GetFacturaOperacionesDesembolso(int idFacturaOperacion = 0)
        {
            List<FacturaDesembolso> facturadesembolsoList = FacturaDesembolsoBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == idFacturaOperacion && p.Activo == true);
            return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = facturadesembolsoList}, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetFacturaOperacionesCobro(int idFacturaOperacion = 0)
        {
            List<FacturaCobro> facturacobroList = FacturaCobroBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == idFacturaOperacion && p.Activo == true);
            return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = facturacobroList }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetFacturaOperacionesTasas(int idFacturaOperacion = 0)
        {
            List<FacturaTasa> facturacobroList = FacturaTasaBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == idFacturaOperacion && p.Activo == true);
            return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = facturacobroList }, JsonRequestBehavior.AllowGet);

        }
        
        public JsonResult GetFacturaMovimientoSaldo(int idFacturaOperacion = 0)
        {
            List<FacturaSaldoModel> _resul = new List<FacturaSaldoModel>();

            List<FacturaMovimientoSaldo> facturacobroList = FacturaMovimientoSaldoBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == idFacturaOperacion && p.Activo == true);


            foreach (var item in facturacobroList)
            {
                _resul.Add(new FacturaSaldoModel {
                        Fecha = item.Fecha.Value.ToShortDateString(),
                        Saldo = item.SaldoActualizado.ToString(),
                        Tasa = string.Format("% {0}", item.TasaActualizada.ToString()),
                        TipoMovimiento = (item.IdTipoMovimiento == 1)? "Desembolso" : (item.IdTipoMovimiento == 2)? "Cobro" : "Tasa",
                });
            }


            return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = _resul }, JsonRequestBehavior.AllowGet);

        }
        
        [HttpGet]
        public ActionResult ABMFacturaOperacion(long? id, bool? popUp)
        {
            //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-EN");

            ViewBag.PopUp = (popUp ?? false);   
            ViewBag.Clientes = GetClientes();
            ViewBag.Deudos = GetDeudos();
            ViewBag.Estados = GetEstados();
            ViewBag.Vendedores = GetVendedores(); 


            if (!id.HasValue || id.Value <= 0)
            {
                var entidad = new DtoFacturaOperacion();               
                entidad.FacturaFecha = DateTime.Now;
                entidad.FacturaFechaCobro = DateTime.Now;
                entidad.FacturaMonto = decimal.Parse("0");
                entidad.FacturaRetencion = decimal.Parse("0");
                entidad.GastoFijo = "2.00";
                entidad.DiasDemora = "2";
                entidad.ImpCheque = "1.20";
                entidad.GastoOtorgamiento = "0.80";
                entidad.TasaDiaria = "0.19";
                entidad.Escribania = "0";
                entidad.Transcripcion = "0";
                entidad.Protocolizacion = "0";
                entidad.Sello = "0";
                entidad.FacturaRetencion = decimal.Parse("13.00");
                entidad.TotalGasto = "0";
                entidad.DemoraPosible = "0";
                entidad.IncidenciaGasto = "0";
                entidad.IncidenciaInteres = "0";
                entidad.TotalAforo = "0";
                entidad.SaldoInicial = "0";
                entidad.Intereses = "0";
                entidad.Ivainteres = "0";
                entidad.Impuestos = "0";
                entidad.GastoCesion = "0";
                entidad.GastoOtorga = "0";
                entidad.Ivagasto = "0";
                entidad.InteresAdicional1pago = "0";
                entidad.IvainteresAdicional1pago = "0";
                entidad.InteresAdicional2pago = "0";
                entidad.IvainteresAdicional2pago = "0";
                entidad.Saldocuenta = "0";
                entidad.ImporteFactura = "0";
                entidad.Iva = "0";
                entidad.Garantia1 = "0";
                entidad.Garantia2 = "0";
                entidad.Garantia3 = "0";

                return View(entidad);
            }           

            var entity = FacturaOperacionBusiness.Instance.GetEntity(id.Value);
            var parametroList = FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id.Value);
            var entidadUp = new DtoFacturaOperacion();

            entidadUp.IdFacturaOperacion = entity.IdFacturaOperacion;
            entidadUp.FacturaOperacionEstado = (int)entity.FacturaIdEstado;
            entidadUp.IdFacturaDeudo = entity.IdFacturaDeudo;
            entidadUp.IdFacturaCliente = entity.IdFacturaCliente;
            entidadUp.IdFacturaVendedor = (entity.IdFacturaVendedor == null)? 0 : entity.IdFacturaVendedor;
            entidadUp.FacturaFecha = entity.FacturaFecha;
            entidadUp.FacturaFechaCobro = entity.FacturaFechaCobro;
            entidadUp.FacturaMonto = Math.Round((decimal)entity.FacturaMonto,2);
            entidadUp.FacturaRetencion = Math.Round((decimal)entity.FacturaRetencion,2);
            entidadUp.FacturaNumero = entity.FacturaNumero;
            entidadUp.Escribania = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Escribania).Valor,2).ToString();
            entidadUp.Transcripcion = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Transcripcion).Valor,2).ToString(); 
            entidadUp.Protocolizacion = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Protocolizacion).Valor,2).ToString(); 
            entidadUp.Sello = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Sello).Valor,2).ToString(); 
            entidadUp.TotalGasto = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.TotalGasto).Valor,2).ToString();
            entidadUp.DiasDemora = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.DiasDemora).Valor,2).ToString();
            entidadUp.GastoFijo = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.GastoFija).Valor,2).ToString();
            entidadUp.ImpCheque = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.ImpCheque).Valor,2).ToString();
            entidadUp.GastoOtorgamiento = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.GastoOtorgamiento).Valor,2).ToString();
            entidadUp.TasaDiaria = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.TasaDiaria).Valor,2).ToString();
            entidadUp.DemoraPosible = parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Demora).Valor.ToString();
            entidadUp.IncidenciaGasto = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.IncidenciaGasto).Valor,2).ToString();
            entidadUp.IncidenciaInteres = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.IncidenciaInteres).Valor,2).ToString();
            entidadUp.TotalAforo = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.TotalAforo).Valor,2).ToString();
            entidadUp.SaldoInicial = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.SaldoInicial).Valor,2).ToString();
            entidadUp.Intereses = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.InteresFecha).Valor,2).ToString();
            entidadUp.Ivainteres = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.IvaInteres).Valor,2).ToString();
            entidadUp.Impuestos = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Impuesto).Valor,2).ToString();
            entidadUp.GastoCesion = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.GastoCesion).Valor,2).ToString();
            entidadUp.GastoOtorga = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.GastoOtorgamientoFicha).Valor,2).ToString();
            entidadUp.Ivagasto = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.IvaGasto).Valor,2).ToString();
            entidadUp.InteresAdicional1pago = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.InteresAdicional1Pago).Valor,2).ToString();
            entidadUp.IvainteresAdicional1pago = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.IvaInteresAdicional1Pago).Valor,2).ToString();
            entidadUp.InteresAdicional2pago = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.InteresAdicional2Pago).Valor,2).ToString();
            entidadUp.IvainteresAdicional2pago = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.IvaInteresAdicional2Pago).Valor,2).ToString();            
            entidadUp.Saldocuenta = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.SalgoCuenta).Valor,2).ToString();
            entidadUp.ImporteFactura = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.importeFactura).Valor,2).ToString();
            entidadUp.Iva = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Iva).Valor,2).ToString();

            entidadUp.Garantia1 = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Garantia1).Valor, 2).ToString();
            entidadUp.Garantia2 = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Garantia2).Valor, 2).ToString();
            entidadUp.Garantia3 = Math.Round((decimal)parametroList.Single(p => p.IdFacturaParametro == (int)Enumerations.FacturaParametro.Garantia3).Valor, 2).ToString();


            if (entity == null)
            {
                return HttpNotFound();
            }
           
           

            return View(entidadUp);
        }
                

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ABMFacturaOperacion(DtoFacturaOperacion operacion)
        {
           // Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-EN");
            FacturaOperacion _entidad = new FacturaOperacion();
            List<FacturaParametroOperacion> _parametroList = new List<FacturaParametroOperacion>();
            var path = System.Configuration.ConfigurationManager.AppSettings["FilePath"];
            try
            {

                

                if (ModelState.IsValid)
                {
                    operacion.FacturaActivo = true;

                    //Valido si es una actualización o una nuevo
                    if (operacion.IdFacturaOperacion > 0)                   
                         _entidad = FacturaOperacionBusiness.Instance.GetEntity(long.Parse(operacion.IdFacturaOperacion.ToString())); 
                   



                        _entidad.IdFacturaCliente = operacion.IdFacturaCliente;
                        _entidad.IdFacturaVendedor = operacion.IdFacturaVendedor;
                        _entidad.IdFacturaDeudo = operacion.IdFacturaDeudo;
                        _entidad.FacturaNumero = operacion.FacturaNumero;
                        _entidad.FacturaFecha = operacion.FacturaFecha;
                        _entidad.FacturaMonto = operacion.FacturaMonto;
                        _entidad.FacturaDesembolsoTotal = 0;
                        _entidad.FacturaCobroTotal = 0;
                        _entidad.FacturaRetencion = operacion.FacturaRetencion;
                        _entidad.FacturaFechaCobro = operacion.FacturaFechaCobro;
                        _entidad.FacturaImagen = (operacion.FacturaImagen == null) ? string.Empty : operacion.FacturaImagen.FileName;
                        _entidad.FacturaActivo = true;
                        _entidad.FacturaFechaAlta = DateTime.Now;
                        _entidad.FacturaIdUsuarioAlta = (int)SessionContext.UsuarioActual.IdUsuario;
                        _entidad.FacturaFechaUltimaActualizacion = DateTime.Now;
                        _entidad.FacturaIdUsuarioUltimaActualizacion = (int)SessionContext.UsuarioActual.IdUsuario;
                        _entidad.FacturaDesembolsoTotal = decimal.Parse(operacion.SaldoInicial);
                        _entidad.FacturaCobroTotal = (operacion.FacturaMonto * (1 - (operacion.FacturaRetencion/100)));
                        _entidad.FacturaIdEstado = operacion.FacturaOperacionEstado;

                    //Cargo las parametros  

                    FacturaOperacionBusiness.Instance.SaveOrUpdate(_entidad);

                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Escribania, Valor = decimal.Parse(operacion.Escribania) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Transcripcion, Valor = decimal.Parse(operacion.Transcripcion) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Protocolizacion, Valor = decimal.Parse(operacion.Protocolizacion) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Sello, Valor = decimal.Parse(operacion.Sello) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.TotalGasto, Valor = decimal.Parse(operacion.TotalGasto) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.DiasDemora, Valor = decimal.Parse(operacion.DiasDemora) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.GastoFija, Valor = decimal.Parse(operacion.GastoFijo) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.ImpCheque, Valor = decimal.Parse(operacion.ImpCheque) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.GastoOtorgamiento, Valor = decimal.Parse(operacion.GastoOtorgamiento) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.TadaDemora, Valor = decimal.Parse(operacion.TotalAforo) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Demora, Valor = decimal.Parse(operacion.DemoraPosible) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.IncidenciaGasto, Valor = decimal.Parse(operacion.IncidenciaGasto) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.IncidenciaInteres, Valor = decimal.Parse(operacion.IncidenciaInteres) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.TotalAforo, Valor = decimal.Parse(operacion.TotalAforo) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.SaldoInicial, Valor = decimal.Parse(operacion.SaldoInicial) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.InteresFecha, Valor = decimal.Parse(operacion.Intereses) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.IvaInteres, Valor = (operacion.Ivainteres == null)? 0 : decimal.Parse(operacion.Ivainteres) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Impuesto, Valor = (operacion.Impuestos == null) ? 0 : decimal.Parse(operacion.Impuestos) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.GastoCesion, Valor = (operacion.GastoCesion == null) ? 0 : decimal.Parse(operacion.GastoCesion) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.GastoOtorgamientoFicha, Valor = (operacion.GastoOtorga == null) ? 0 : decimal.Parse(operacion.GastoOtorga) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.IvaGasto, Valor = (operacion.Ivagasto == null) ? 0 : decimal.Parse(operacion.Ivagasto) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.InteresAdicional1Pago, Valor = (operacion.InteresAdicional1pago == null) ? 0 : decimal.Parse(operacion.InteresAdicional1pago) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.IvaInteresAdicional1Pago, Valor = (operacion.IvainteresAdicional1pago == null) ? 0 : decimal.Parse(operacion.IvainteresAdicional1pago) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.InteresAdicional2Pago, Valor = (operacion.InteresAdicional2pago == null) ? 0 : decimal.Parse(operacion.InteresAdicional2pago) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.IvaInteresAdicional2Pago, Valor = (operacion.IvainteresAdicional2pago == null) ? 0 : decimal.Parse(operacion.IvainteresAdicional2pago) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.SalgoCuenta, Valor = (operacion.Saldocuenta == null) ? 0 : decimal.Parse(operacion.Saldocuenta) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.importeFactura, Valor = (operacion.ImporteFactura == null) ? 0 : decimal.Parse(operacion.ImporteFactura) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Iva, Valor = (operacion.Iva == null) ? 0 : decimal.Parse(operacion.Iva) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.TasaDiaria, Valor = (operacion.TasaDiaria == null) ? 0 : decimal.Parse(operacion.TasaDiaria) });                    
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Garantia1, Valor = (operacion.Garantia1 == null) ? 0 : decimal.Parse(operacion.Garantia1) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Garantia2, Valor = (operacion.Garantia2 == null) ? 0 : decimal.Parse(operacion.Garantia2) });
                    _parametroList.Add(new FacturaParametroOperacion { IdFacturaOperacion = _entidad.IdFacturaOperacion, IdFacturaParametro = (int)Enumerations.FacturaParametro.Garantia3, Valor = (operacion.Garantia3 == null) ? 0 : decimal.Parse(operacion.Garantia3) });


                    FacturaParametroOperacionBusiness.Instance.SaveOperacionParametro(_parametroList);

                    if(operacion.FacturaImagen != null)
                        operacion.FacturaImagen.SaveAs(Path.Combine(path, operacion.FacturaImagen.FileName));

                    return RedirectToAction("Index");
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                FacturaOperacion entidad = FacturaOperacionBusiness.Instance.GetEntity(id);
                entidad.FacturaActivo = false;
                FacturaOperacionBusiness.Instance.SaveOrUpdate(entidad);
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        public JsonResult ValidateCliente(string nombre = "", string cuit = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(nombre))
                {
                    var entity = ClienteBusiness.Instance.ClienteExist(nombre, cuit);

                    if (entity != null)
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true, Id = entity.IdCliente });
                    }
                    else
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                    }
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Requerido");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        public List<SelectListItem> GetClientes(bool indistinto = false)
        {
            List<SelectListItem> clienteList = new List<SelectListItem>();
            var clientes =  FacturaClienteBusiness.Instance.GetClientes();

            foreach (var item in clientes)           
                clienteList.Add(new SelectListItem { Value = item.IdFacturaCliente.ToString(), Text = item.Nombre });


            if (indistinto)
                clienteList.Insert(0, new SelectListItem { Value = "0", Text = "Indistinto" });

            return clienteList;
        }

        public List<SelectListItem> GetVendedores(bool indistinto = false)
        {
            List<SelectListItem> vendedorList = new List<SelectListItem>();
            var vendedores = FacturaVendedorBusiness.Instance.GetClientes();

            foreach (var item in vendedores)
                vendedorList.Add(new SelectListItem { Value = item.IdVendedor.ToString(), Text = item.Nombre });


            if (indistinto)
                vendedorList.Insert(0, new SelectListItem { Value = "0", Text = "Indistinto" });

            return vendedorList;
        }

        public List<SelectListItem> GetDeudos(bool indistinto = false)
        {
            List<SelectListItem> deudoList = new List<SelectListItem>();
            var clientes = FacturaDeudoBusiness.Instance.GetDeudos();

            foreach (var item in clientes)
                deudoList.Add(new SelectListItem { Value = item.IdFacturaDeudo.ToString(), Text = item.Nombre });

            if (indistinto)
                deudoList.Insert(0, new SelectListItem { Value = "0", Text = "Indistinto" });

            return deudoList;
        }

        public List<SelectListItem> GetEstados(bool indistinto = false)
        {
            List<SelectListItem> estadoList = new List<SelectListItem>();

            estadoList.Add(new SelectListItem { Value = "1", Text = "Pendiente" });
            estadoList.Add(new SelectListItem { Value = "2", Text = "Aprobada" });
            estadoList.Add(new SelectListItem { Value = "3", Text = "Terminada" });


            if (indistinto)
                estadoList.Insert(0, new SelectListItem { Value = "0", Text = "Indistinto" });

            return estadoList;
        }


        [HttpGet]
        public JsonResult GenerarDesembolso(string idfacturaoperacion,string fecha, string monto)
        {
            try
            {
                FacturaDesembolso entidad = new FacturaDesembolso();
                entidad.Activo = true;
                entidad.Fecha = DateTime.Parse(fecha);
                entidad.FechaAlta = DateTime.Now;
                entidad.IdFacturaOperacion = int.Parse(idfacturaoperacion);
                entidad.IdUsuarioAlta = (int)SessionContext.IdUsuarioActual;
                //entidad.Monto = decimal.Parse(monto.Replace('.', ','));
                entidad.Monto = -1 * decimal.Parse(monto);
                FacturaDesembolsoBusiness.Instance.SaveOrUpdate(entidad);

                //Registrar el MovimientoSaldo
                FacturaMovimientoSaldoBusiness.Instance.CalcularSaloActualizado((int)entidad.IdFacturaOperacion, 1, null, entidad.IdFacturaDesembolso, null, (DateTime)entidad.Fecha, (decimal)entidad.Monto);


                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true});
                   
                    
            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }


        [HttpGet]
        public JsonResult GenerarCobro(string idfacturaoperacion, string fecha, string monto)
        {
            try
            {
                FacturaCobro entidad = new FacturaCobro();
                entidad.Activo = true;
                entidad.Fecha = DateTime.Parse(fecha);
                entidad.FechaAlta = DateTime.Now;
                entidad.IdFacturaOperacion = int.Parse(idfacturaoperacion);
                entidad.IdUsuarioAlta = (int)SessionContext.IdUsuarioActual;
                //entidad.Monto = decimal.Parse(monto.Replace('.', ','));
                entidad.Monto = decimal.Parse(monto);
                FacturaCobroBusiness.Instance.SaveOrUpdate(entidad);

                //Registrar el MovimientoSaldo
                FacturaMovimientoSaldoBusiness.Instance.CalcularSaloActualizado((int)entidad.IdFacturaOperacion, 2, entidad.IdFacturaCobro, null, null, (DateTime)entidad.Fecha,decimal.Parse(monto));


                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true });


               

            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }


        [HttpGet]
        public JsonResult GenerarTasa(string idfacturaoperacion, string fecha, string monto)
        {
            try
            {
                FacturaTasa entidad = new FacturaTasa();
                entidad.Activo = true;
                entidad.Fecha = DateTime.Parse(fecha);
                entidad.FechaAlta = DateTime.Now;
                entidad.IdFacturaOperacion = int.Parse(idfacturaoperacion);
                entidad.IdUsuarioAlta = (int)SessionContext.IdUsuarioActual;
                //entidad.Monto = decimal.Parse(monto.Replace('.', ','));
                entidad.Tasa = decimal.Parse(monto);
                FacturaTasaBusiness.Instance.SaveOrUpdate(entidad);


                //Registrar el MovimientoSaldo
                FacturaMovimientoSaldoBusiness.Instance.CalcularSaloActualizado((int)entidad.IdFacturaOperacion, 3, null, null, entidad.IdFacturaTasa, (DateTime)entidad.Fecha,decimal.Parse(monto));


                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true });


            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }

        public ActionResult GetFactura(long? id)
        {
            try
            {

                
                var path = System.Configuration.ConfigurationManager.AppSettings["FilePath"];
                FacturaOperacion entidad = FacturaOperacionBusiness.Instance.GetEntity((long)id);

                if (entidad.FacturaImagen != "")
                {
                    path = Path.Combine(path, entidad.FacturaImagen);
                    return File(path, MediaTypeNames.Application.Octet, entidad.FacturaImagen);
                }

                return HttpNotFound();

            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }


        public ActionResult GetComprobante(long? id, string garantia = "")
        {
            try
            {                
                        
                try
                {

                        

                        var facturaoperacion = FacturaOperacionBusiness.Instance.GetEntity((long)id); 
                        
                        ReportClass reportClass = ReportHelper.LoadReport("FacturaOperacionSimulacion");

                        reportClass.SetParameterValue("numerooperacion ", facturaoperacion.IdFacturaOperacion.ToString() );
                        reportClass.SetParameterValue("cliente ", facturaoperacion.FacturaCliente.Nombre);
                        reportClass.SetParameterValue("fechafactura ", facturaoperacion.FacturaFecha.Value.ToShortDateString());
                        reportClass.SetParameterValue("fechacobro ", facturaoperacion.FacturaFechaCobro.Value.ToShortDateString());
                        reportClass.SetParameterValue("deudor ", facturaoperacion.FacturaDeudo.Nombre);
                        reportClass.SetParameterValue("numerofactura ", facturaoperacion.FacturaNumero);
                        reportClass.SetParameterValue("montofactura ", string.Format("$ {0}", Math.Round((decimal)facturaoperacion.FacturaMonto,2).ToString()));
                        reportClass.SetParameterValue("retencion ", Math.Round((decimal)facturaoperacion.FacturaRetencion,2).ToString());                    

                        
                        reportClass.SetParameterValue("interesygasto ", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && (p.IdFacturaParametro == 11 || p.IdFacturaParametro == 12 || p.IdFacturaParametro == 13)).Sum(p=>p.Valor),2));
                        reportClass.SetParameterValue("totalaforo ", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 10 ).Sum(p => p.Valor),2));
                        reportClass.SetParameterValue("montoadesembolsar ",string.Format("$ {0}", Math.Round((decimal)facturaoperacion.FacturaDesembolsoTotal, 2)));




                        //reportClass.SetParameterValue("intereses ", string.Format("$ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 16).Sum(p => p.Valor),2)));
                        //reportClass.SetParameterValue("gastos ", string.Format("$ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 28).Sum(p => p.Valor),2)));
                        //reportClass.SetParameterValue("iva ", string.Format("$ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id &&  p.IdFacturaParametro == 17 || p.IdFacturaParametro == 21).Sum(p => p.Valor),2)));

                        double _tasadiaria = (double)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 29)[0].Valor;
                        TimeSpan _demora = facturaoperacion.FacturaFechaCobro.Value - facturaoperacion.FacturaFecha.Value;
                        var _interes = facturaoperacion.FacturaDesembolsoTotal * ((decimal)Math.Pow((1 + (_tasadiaria / 100)), _demora.Days)) - facturaoperacion.FacturaDesembolsoTotal;
                        //reportClass.SetParameterValue("intereses ", string.Format("$ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 16).Sum(p => p.Valor),2)));
                        reportClass.SetParameterValue("intereses ", string.Format("$ {0}", Math.Round((decimal)_interes, 2)));
                        reportClass.SetParameterValue("gastos ", string.Format("$ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 28).Sum(p => p.Valor), 2)));
                        var _gasto = Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 28).Sum(p => p.Valor), 2);
                        //reportClass.SetParameterValue("iva ", string.Format("$ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id &&  p.IdFacturaParametro == 17 || p.IdFacturaParametro == 21).Sum(p => p.Valor),2)));
                        var _iva = ((decimal)_interes + (decimal)_gasto) * decimal.Parse("0.21");
                        reportClass.SetParameterValue("iva ", string.Format("$ {0}", Math.Round(_iva, 2)));




                        reportClass.SetParameterValue("montodesembolsado",string.Format("$ {0}", Math.Round((decimal)facturaoperacion.FacturaDesembolso.Sum(p => p.Monto),2)));
                        reportClass.SetParameterValue("saldo", string.Format("$ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 15).Sum(p => p.Valor) - (decimal)facturaoperacion.FacturaDesembolso.Sum(p => p.Monto),2)));

                    if (garantia == "1")
                    {
                        reportClass.SetParameterValue("garantiatitulo", "Listado de Garantias");
                        reportClass.SetParameterValue("garantia1", string.Format("Garantia Uno $ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 30).Sum(p => p.Valor), 2)));
                        reportClass.SetParameterValue("garantia2", string.Format("Garantia Dos $ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 31).Sum(p => p.Valor), 2)));
                        reportClass.SetParameterValue("garantia3", string.Format("Garantia Tres $ {0}", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 32).Sum(p => p.Valor), 2)));
                    }
                    else
                    {
                        reportClass.SetParameterValue("garantiatitulo", string.Empty);
                        reportClass.SetParameterValue("garantia1", string.Empty);
                        reportClass.SetParameterValue("garantia2", string.Empty);
                        reportClass.SetParameterValue("garantia3", string.Empty);
                    }


                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                  
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
                }

            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }

        public ActionResult GetComprobanteAprobado(long? id)
        {

            List<FacturaOperacionAceptadaModel> _facturaOperacion = new List<FacturaOperacionAceptadaModel>();

            try
            {
                try
                {
                    var facturaoperacion = FacturaOperacionBusiness.Instance.GetEntity((long)id);
                    ReportClass reportClass = ReportHelper.LoadReport("FacturaOperacionAceptada");
                         
                    foreach (var item in facturaoperacion.FacturaMovimientoSaldo)
                    {
                        string _monto = "0";

                        if (item.IdFacturaCobro != null)
                        {
                            var facturacobro = FacturaCobroBusiness.Instance.GetEntity(long.Parse(item.IdFacturaCobro.ToString()));
                            _monto = facturacobro.Monto.ToString();
                        }

                        if(item.IdFacturaDesembolso != null)
                        {
                            var facturadesembolso = FacturaDesembolsoBusiness.Instance.GetEntity(long.Parse(item.IdFacturaDesembolso.ToString()));
                            _monto = facturadesembolso.Monto.ToString();
                        }

                        _facturaOperacion.Add(new FacturaOperacionAceptadaModel {
                            Fecha = item.Fecha.Value.ToShortDateString(),
                            Movimiento = (item.IdTipoMovimiento == 1) ? "Desembolso" : (item.IdTipoMovimiento == 2) ? "Cobro" : "Tasa",
                            Monto = string.Format("$ {0}", _monto),
                            Saldo = string.Format("$ {0}", item.SaldoActualizado.ToString()),
                            Tasa = string.Format("% {0}", item.TasaActualizada.ToString())
                        });
                    }

                    reportClass.SetDataSource(_facturaOperacion);
                    reportClass.SetParameterValue("numerooperacion ", facturaoperacion.IdFacturaOperacion.ToString());
                    reportClass.SetParameterValue("cliente ", facturaoperacion.FacturaCliente.Nombre);
                    reportClass.SetParameterValue("fechafactura ", facturaoperacion.FacturaFecha.Value.ToShortDateString());
                    reportClass.SetParameterValue("fechacobro ", facturaoperacion.FacturaFechaCobro.Value.ToShortDateString());
                    reportClass.SetParameterValue("deudor ", facturaoperacion.FacturaDeudo.Nombre);
                    reportClass.SetParameterValue("numerofactura ", facturaoperacion.FacturaNumero);
                    reportClass.SetParameterValue("montofactura ", string.Format("$ {0}", Math.Round((decimal)facturaoperacion.FacturaMonto, 2).ToString()));
                    reportClass.SetParameterValue("retencion ", Math.Round((decimal)facturaoperacion.FacturaRetencion, 2).ToString());
                    reportClass.SetParameterValue("interesygasto ", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && (p.IdFacturaParametro == 11 || p.IdFacturaParametro == 12 || p.IdFacturaParametro == 13)).Sum(p => p.Valor), 2));
                    reportClass.SetParameterValue("totalaforo ", Math.Round((decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == id && p.IdFacturaParametro == 10).Sum(p => p.Valor), 2));
                    
                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);

                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
                }

            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }

        public ActionResult GetComprobanteTerminado(long? id, string garantia = "")
        {
            try
            {
                try
                {
                    var facturaoperacion = FacturaOperacionBusiness.Instance.GetEntity((long)id);
                    ReportClass reportClass = ReportHelper.LoadReport("FacturaOperacionTerminada");
                    reportClass.SetParameterValue("numerooperacion ", facturaoperacion.IdFacturaOperacion.ToString());
                    reportClass.SetParameterValue("cliente ", facturaoperacion.FacturaCliente.Nombre);
                    reportClass.SetParameterValue("numerofactura ", facturaoperacion.FacturaNumero);
                    reportClass.SetParameterValue("FechaLiquidacion", facturaoperacion.FacturaFechaAlta.Value.ToShortDateString());
                    reportClass.SetParameterValue("MontoFactura", string.Format("$ {0}",Math.Round((decimal)facturaoperacion.FacturaMonto,2)));
                    reportClass.SetParameterValue("Desembolso", string.Format("$ {0}", (-1) * Math.Round((decimal)facturaoperacion.FacturaDesembolso.Where(p => p.Activo == true).Sum(p => p.Monto), 2)));
                    reportClass.SetParameterValue("InteresesFechaCobro", string.Format("$ {0}", (-1) * Math.Round((decimal)facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.Interes), 2)));
                    reportClass.SetParameterValue("Ivainteres", string.Format("$ {0}", (-1) * Math.Round((decimal)facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.Iva), 2)));
                    reportClass.SetParameterValue("ImpuestoDebCre" , string.Format("$ {0}", (-1) * Math.Round((decimal)facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.ImpuestoDebCred), 2)));

                    var facturaparametro = FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == facturaoperacion.IdFacturaOperacion && p.IdFacturaParametro == 5);
                    reportClass.SetParameterValue("RetencionGasto", string.Format("$ {0}", Math.Round((decimal)facturaparametro[0].Valor, 2)));

                    reportClass.SetParameterValue("GastoOtorgamiento", string.Format("$ {0}", (-1) * Math.Round((decimal)facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.GastoOtorgamiento), 2)));
                    reportClass.SetParameterValue("IvaGasto", string.Format("$ {0}", (-1) * Math.Round((decimal)facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.IvaGasto), 2)));


                    var interesfechacobro = (-1) * facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.Interes);
                    var impuestodebcred = (-1) * facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.ImpuestoDebCred);
                    var gastootorgamiento = (-1) * facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.GastoOtorgamiento);

                    reportClass.SetParameterValue("ImporteFactura", string.Format("$ {0}", Math.Round((decimal)interesfechacobro + (decimal)impuestodebcred + (decimal)gastootorgamiento, 2)));
                    reportClass.SetParameterValue("IVaFactura", string.Format("$ {0}",  Math.Round(((decimal)interesfechacobro + (decimal)impuestodebcred + (decimal)gastootorgamiento) * (decimal)0.21, 2)));

                    reportClass.SetParameterValue("MontoAcreditado", string.Format("$ {0}",  Math.Round((decimal)facturaoperacion.FacturaCobro.Where(p => p.Activo == true).Sum(p => p.Monto), 2)));


                    //Calcular Saldo
                    var _desembolso = -1 * facturaoperacion.FacturaDesembolso.Where(p => p.Activo == true).Sum(p => p.Monto);
                    var _interesfechacobro = -1 * facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.Interes);
                    var _ivainteres = -1 * facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.Iva);
                    var _impuestodebcred = -1 * facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.ImpuestoDebCred);
                    var _retencion = facturaparametro[0].Valor;
                    var _gastootorgamiento = -1 * facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.GastoOtorgamiento);
                    var _ivagasto = -1 * facturaoperacion.FacturaMovimientoSaldo.Sum(p => p.IvaGasto);
                    var _montoacreditado = facturaoperacion.FacturaCobro.Where(p => p.Activo == true).Sum(p => p.Monto);
                    _montoacreditado = _montoacreditado - _desembolso - _interesfechacobro - _ivainteres - _impuestodebcred - _retencion - _gastootorgamiento - _ivagasto;
                    reportClass.SetParameterValue("Saldo", string.Format("$ {0}", Math.Round((decimal)_montoacreditado, 2)));

                                       

                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);

                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
                }

            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpGet]
        public JsonResult EliminarDesembolso(string idfacturadesembolso)
        {
            try
            {
                FacturaDesembolso _facturaDesembolso = FacturaDesembolsoBusiness.Instance.GetEntity(long.Parse(idfacturadesembolso));
                _facturaDesembolso.Activo = false;
                FacturaDesembolsoBusiness.Instance.SaveOrUpdate(_facturaDesembolso);

                //Registrar el MovimientoSaldo
                FacturaMovimientoSaldoBusiness.Instance.EliminarSaldoMovimiento(idfacturadesembolso);


                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true });


            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpGet]
        public JsonResult EliminarCobro(string idfacturacobro)
        {
            try
            {
                FacturaCobro _facturaCobro = FacturaCobroBusiness.Instance.GetEntity(long.Parse(idfacturacobro));
                _facturaCobro.Activo = false;
                FacturaCobroBusiness.Instance.SaveOrUpdate(_facturaCobro);

                //Registrar el MovimientoSaldo
                FacturaMovimientoSaldoBusiness.Instance.EliminarSaldoMovimiento(idfacturacobro);


                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true });


            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }


        [HttpGet]
        public JsonResult EliminarTasa(string idfacturatasa)
        {
            try
            {
                //Registrar el MovimientoSaldo
                FacturaMovimientoSaldoBusiness.Instance.EliminarSaldoMovimiento(idfacturatasa);


                FacturaTasa _facturaTasa = FacturaTasaBusiness.Instance.GetEntity(long.Parse(idfacturatasa));
                _facturaTasa.Activo = false;
                FacturaTasaBusiness.Instance.SaveOrUpdate(_facturaTasa);

            


                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true });


            }
            catch (Exception ex)
            {
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                //return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}