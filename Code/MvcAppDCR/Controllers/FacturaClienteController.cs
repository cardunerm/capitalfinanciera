﻿using Business;
using Controllers;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class FacturaClienteController : BaseController
    {
        //
        // GET: /Cliente/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetFacturaClientes(string nombre = "", string cuit = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = FacturaClienteBusiness.Instance.GetByFilters(nombre, cuit, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMFacturaCliente(long? id, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
            ;
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new FacturaCliente());
            }

            var entity = FacturaClienteBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        private void LoadCombos()
        {
            ViewBag.Tasas = ClienteBusiness.Instance.GetByConditions<Tasa>(tasa => tasa.Activa == true)
                .OrderByDescending(order => order.Value)
                .GetSelectListItem("Id", "Value");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMFacturaCliente(FacturaCliente cliente, bool? popUp)
        {
            ViewBag.PopUp = (popUp ?? false);
            LoadCombos();
            try
            {
                if (ViewBag.PopUp)
                    ModelState.Remove("IdTasa");

                if (ModelState.IsValid)
                {
                    cliente.Activo = true;

                    //Valido si es una actualización o una nuevo
                    if(cliente.IdFacturaCliente > 0)
                    {
                        FacturaCliente _entidad = FacturaClienteBusiness.Instance.GetEntity(long.Parse(cliente.IdFacturaCliente.ToString()));
                        _entidad.Nombre = cliente.Nombre;
                        _entidad.CUIT = cliente.CUIT;
                        _entidad.Domicilio = cliente.Domicilio;
                        _entidad.Email = cliente.Email;
                        _entidad.TelefonoFijo = cliente.TelefonoFijo;
                        _entidad.TelefonoMovil = cliente.TelefonoMovil;                       

                    }


                    FacturaClienteBusiness.Instance.SaveOrUpdate(cliente);

                    return Json(
                        new
                        {
                            id = cliente.IdFacturaCliente,
                            name = cliente.Nombre,
                            msg = Resources.Resource.SaveSuccess,
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                FacturaCliente entidad = FacturaClienteBusiness.Instance.GetEntity(id);
                entidad.Activo = false;
                FacturaClienteBusiness.Instance.SaveOrUpdate(entidad);
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        public JsonResult ValidateCliente(string nombre = "", string cuit = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(nombre))
                {
                    var entity = ClienteBusiness.Instance.ClienteExist(nombre, cuit);

                    if (entity != null)
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true, Id = entity.IdCliente });
                    }
                    else
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                    }
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Requerido");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}