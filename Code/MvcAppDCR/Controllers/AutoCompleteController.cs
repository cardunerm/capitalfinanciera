﻿using Business;
using Controllers;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class AutoCompleteController : BaseController
    {
        public JsonResult AutocompleteClientes([Bind(Prefix = "_filter")]string filter = "", [Bind(Prefix = "$filter")]string filter2 = "", int take = 15, bool insertarIndistinto = false)
        {
            try
            {
                if (String.IsNullOrEmpty(filter) && !String.IsNullOrEmpty(filter2))
                    filter = filter2;
                try
                {
                    filter = filter.Substring(filter.IndexOf("'") + 1, filter.LastIndexOf("'") - 1 - filter.IndexOf("'"));
                }
                catch
                {
                    filter = string.Empty;
                }

                var count = 0;
                var list = ClienteBusiness.Instance.GetByFilters(filter, string.Empty, 0, take, ref count).GetSelectListItem("IdCliente", "NameAndCuit");

                return Json(new { d = list }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        public JsonResult AutocompleteInversor([Bind(Prefix = "_filter")]string filter = "", [Bind(Prefix = "$filter")]string filter2 = "", int take = 15, bool insertarIndistinto = false, int clasificacionInversor = -1)
        {
            try
            {
                if (String.IsNullOrEmpty(filter) && !String.IsNullOrEmpty(filter2))
                    filter = filter2;
                try
                {
                    filter = filter.Substring(filter.IndexOf("'") + 1, filter.LastIndexOf("'") - 1 - filter.IndexOf("'"));
                }
                catch
                {
                    filter = string.Empty;
                }

                var count = 0;
                var list = InversorBussines.Instance.GetByFilters(filter, string.Empty, 0, take, ref count, clasificacionInversor).GetSelectListItem("IdInversor", "NameAndCuit");

                return Json(new { d = list }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

    }
}