﻿using Business;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Web.Mvc;

namespace Controllers
{
    public class EmisorValorController : BaseController
    {
        //
        // GET: /EmisorValor/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetEmisoresValor(string numero = "", string cuit = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = EmisorValorBusiness.Instance.GetByFilters(numero, cuit, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMEmisorValor(long? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new EmisorValor());
            }

            var entity = EmisorValorBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMEmisorValor(EmisorValor emisorValor)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EmisorValorBusiness.Instance.SaveOrUpdateEmisorValor(emisorValor);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                EmisorValorBusiness.Instance.DeleteLogic(id, EmisorValorBusiness.Instance.ValidateDelete, AuditoriaBusiness.Instance.AuditoriaEmisorValor);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

       
    }
}