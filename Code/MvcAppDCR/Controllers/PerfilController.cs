﻿using Business;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Code.Business.Security;
using MvcAppDCR.Model.Security;
using MvcAppDCR.Utils;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Controllers
{
    public class PerfilController : BaseController
    {
        //
        // GET: /Perfil/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetPerfiles(string nombre = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = PerfilBusiness.Instance.GetByFilters(nombre, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMPerfil(long? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                ViewBag.Modulos = ModuloBusiness.Instance.GetByConditions(m => m.IdModulo != 100 && m.IdModulo != 200 && m.IdModulo != 300 && m.IdModulo != 400 && m.IdModulo != 500 && m.IdModulo != 600 && m.IdModulo != 700).Select(p => new ModuloModel
                {
                    IdModulo = p.IdModulo,
                    Nombre = p.Nombre
                }).ToList();

                return View(new Perfil());
            }

            var entity = PerfilBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            ViewBag.Modulos = new BSecurity().GetModuloModels(entity);

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMPerfil(Perfil perfil, string modulos)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var permisos = new JavaScriptSerializer().Deserialize<List<PerfilModulo>>(modulos);

                    PerfilBusiness.Instance.SaveOrUpdatePerfil(perfil, permisos);

                    return Json(
                        new
                        {
                            msg = Resource.SaveSuccess,
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (ValidationException vex)
            {
                return ExceptionManager.Instance.JsonError(vex.Message);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                PerfilBusiness.Instance.DeleteLogic(id, PerfilBusiness.Instance.ValidateDelete, AuditoriaBusiness.Instance.AuditoriaPerfil);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}