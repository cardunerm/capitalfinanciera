﻿using Business;
using Controllers;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Code.Business.Security;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Controllers
{
    public class SucursalUsuarioController : BaseController
    {
        public ActionResult AssignSucursal(int idUsuario)
        {
            Usuario user = UsuarioBusiness.Instance.GetEntity(idUsuario);

            ViewBag.SucursalOperaciones = new BSecurity().GetSucursalOperacionModels(user);

            return View(user);
        }
                
            [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AssignSucursal(string sucursales, Usuario user)
        {
            try
            {
                var listSucursales = new JavaScriptSerializer().Deserialize<List<Sucursal>>(sucursales);
                
                SucursalBusiness.Instance.CreateSucursalUsuario(user, listSucursales);

                return Json(new { success = true, url = "Usuario/Index" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

            [HttpPost]
            public JsonResult GetUsuarioSucursal()
            {
                try
                {
                    var list = SucursalUsuarioBusiness.Instance.GetUsuarioSucursal()
                                        .Select(p => p.Sucursal);

                    var options = WebHelper.GetSelectListItem(list, "IdSucursal", "Descripcion");

                    return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Options = options });
                }
                catch (Exception ex)
                {
                    return ExceptionManager.Instance.JsonError(ex);
                }
            }
    }
}