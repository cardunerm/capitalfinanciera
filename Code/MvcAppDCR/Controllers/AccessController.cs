﻿using Business;
using System.Linq;
using CurrentContext;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Security;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using Resources;

namespace Controllers
{
    public class AccessController : BaseController
    {
        [HttpGet]
        public ActionResult Error(string errorType, string message, string url = "", string btnGoToLabel = "", bool redirectBtn = true)
        {
            ViewData["ErrorType"] = errorType;
            ViewData["Message"] = message;
            ViewData["redirectBtn"] = redirectBtn;

            if (redirectBtn)
            {
                if (!string.IsNullOrEmpty(url) && !string.IsNullOrEmpty(btnGoToLabel))
                {
                    ViewData["UrlRedirect"] = url;
                    ViewData["BtnGoToLabel"] = btnGoToLabel;
                }
                else
                {
                    ViewData["UrlRedirect"] = Url.Action("Login", "Access");
                    ViewData["BtnGoToLabel"] = "Login";
                }
            }

            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl = "")
        {
           
               
              if (User.Identity.IsAuthenticated)
            {
                return Logout();
            }

            ViewBag.ReturnUrl = returnUrl;
            ViewBag.Sucursales = new List<SelectListItem>();

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UsuarioLogin model, string returnUrl = "")
        {

            ModelState.Remove("IdEmpresa");

            if (ModelState.IsValid)
            {
                if (CustomMembership.ValidateUser(model.NombreUsuario, model.Contrasenia))
                {
                    FormsAuthentication.SetAuthCookie(model.NombreUsuario, model.RememberMe);

                    var currentUsuario = CustomMembership.GetCurrentUser(model.NombreUsuario);

                    if (currentUsuario != null)
                    {
                        SessionContext.UsuarioActual = (Usuario)currentUsuario.User;
                        //SessionContext.Empresa = SessionContext.UsuarioActual.Empresa;

                        var usuarioSucursal = SucursalUsuarioBusiness.Instance.GetCurrentUsuarioDefaultSucursal();

                        SessionContext.IdSucursalOperacionActual = model.IdSucursal;
                        SessionContext.IdEmpresaActual = model.IdEmpresa;
                        SessionContext.Configuracion = ChequeBusiness.Instance.GetByConditions<Configuracion>(config => config.IdEmpresa == SessionContext.IdEmpresaActual).FirstOrDefault();



                        var perfiles = UsuarioBusiness.Instance.GetUsuarioById(SessionContext.UsuarioActual.IdUsuario);
                        SessionContext.Perfil = (Luma.Utils.Enumeraciones.Perfil.TipoPerfil)perfiles.IdPerfil;
                        //SessionContext.Perfil = (Luma.Utils.Enumeraciones.Perfil.TipoPerfil)perfiles.ToList()[0].IdPerfil;





                        AuditoriaBusiness.Instance.AuditoriaLogin();
                    }

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index", "Home")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Nombre de usuario o contraseña incorrectas");
                }
            }

            return ExceptionManager.Instance.JsonError(ModelState);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult DirectLogin(long idUsuario, long idEmpresa, string returnUrl = "")
        {
            var usuario = UsuarioBusiness.Instance.GetEntity(idUsuario);

            if (usuario != null)
            {
                FormsAuthentication.SetAuthCookie(usuario.NombreUsuario, false);

                var currentUsuario = CustomMembership.GetCurrentUser(usuario.NombreUsuario);

                if (currentUsuario != null)
                {
                    SessionContext.UsuarioActual = (Usuario)currentUsuario.User;
                    
                    AuditoriaBusiness.Instance.AuditoriaLogin();
                }

                return Redirect(returnUrl);
            }

            return HttpNotFound();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AllowAnonymous]
        public ActionResult Logout()
        {
            if (CustomMembership.GetCurrentUser() != null)
            {
                AuditoriaBusiness.Instance.AuditoriaLogout();

                SessionContext.UsuarioActual = null;
            }

            FormsAuthentication.SignOut();

            return Redirect(FormsAuthentication.LoginUrl);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public JsonResult ResetPassword(Usuario model)
        {
            try
            {
                ModelState.Remove("Nombre");
                ModelState.Remove("Apellido");
                ModelState.Remove("NombreUsuario");
                ModelState.Remove("Contrasenia");
                ModelState.Remove("IdEmpresa");

                if (ModelState.IsValid)
                {
                    UsuarioBusiness.Instance.RestablecerContrasenia(model.Email);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Debe ingresar un e-mail válido", Constant.SuccessMessages.ERROR_VAR_MODAL);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex, Constant.SuccessMessages.ERROR_VAR_MODAL);
            }
        }
              
          [HttpPost]
        public JsonResult ChangeCurrentSucursal(long? idSucursal)
		{
			try
			{
				if (idSucursal.HasValue && idSucursal.Value > 0)
				{
                    if (SucursalUsuarioBusiness.Instance.HasUsuarioSucursalAssigned(idSucursal.Value))
					{						
                        SessionContext.IdSucursalOperacionActual = idSucursal;

						return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
					}
					else
					{
						return ExceptionManager.Instance.JsonError(Resource.CannotChangeToSucursalOperacion);
					}
				}
				else
				{
					return ExceptionManager.Instance.JsonError(Resource.ServerConnectionError);
				}
			}
			catch (System.Exception ex)
			{
				return ExceptionManager.Instance.JsonError(ex);
			}
		}
    }
}