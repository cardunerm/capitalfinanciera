﻿using Business;
using CurrentContext;
using Enumerations;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Code.Business.Security;
using MvcAppDCR.Model;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Controllers
{
    public class UsuarioController : BaseController
    {
        private readonly BSecurity Business = new BSecurity();

        public ActionResult Index()
        {
            ViewBag.Perfiles = WebHelper.GetSelectListItem(PerfilBusiness.Instance.GetAllActives(), "IdPerfil", "Nombre", true);

            return View();
        }

        public JsonResult GetUsuarios(string nombre = "", string apellido = "", string nombreUsuario = "", long idPerfil = 0, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = UsuarioBusiness.Instance.GetByFilters(nombre, apellido, nombreUsuario, idPerfil, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMUsuario(long? id)
        {
            var empresas = EmpresaBusiness.Instance.GetAllEmpresas();

            ViewBag.Perfiles = new BSecurity().GetPerfilModel(id);

            ViewBag.Empresas = WebHelper.GetSelectListItem(empresas, "Id", "Nombre");

            if (!id.HasValue)
            {
                return View(Business.GetUsuarioModel(id));
            }

            UsuarioModel usuario = Business.GetUsuarioModel(id);

            if (usuario == null)
            {
                return HttpNotFound();
            }

            return View(usuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMUsuario(UsuarioModel usuario, string perfiles, string vendedoresEmpresas, bool changecontrasenia = false)
        {
            try
            {
                if (usuario.UsuarioMod.IdUsuario > 0 && !changecontrasenia)
                {
                    ModelState.Remove("UsuarioMod.Contrasenia");
                }

                if (usuario.UsuarioMod.IdUsuario > 0 && changecontrasenia && usuario.UsuarioMod.Contrasenia == null)
                {
                    return ExceptionManager.Instance.JsonError("Debe ingresar una contraseña");
                }

                if (ModelState.IsValid)
                {
                    var selectedPerfiles = new JavaScriptSerializer().Deserialize<List<Perfil>>(perfiles);

                    Business.SaveOrUpdate(usuario, selectedPerfiles, changecontrasenia);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (ValidationException vex)
            {
                return ExceptionManager.Instance.JsonError(vex.Message);
            }
        }

        [HttpPost]
        public JsonResult ChangeContrasenia(string actual = "", string contrasenia = "", string confirmacion = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(contrasenia) || !string.IsNullOrEmpty(actual) || !string.IsNullOrEmpty(confirmacion))
                {
                    if (contrasenia == confirmacion)
                    {
                        if (CustomMembership.ValidateUser(SessionContext.UsuarioActual.NombreUsuario, actual))
                        {
                            UsuarioBusiness.Instance.ChangeContrasenia(contrasenia);

                            return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
                        }
                        else
                        {
                            return ExceptionManager.Instance.JsonError("La contraseña actual ingresada es incorrecta.");
                        }
                        
                    }
                    else
                    {
                        return ExceptionManager.Instance.JsonError("La contraseña nueva no coincide con la confirmacion.");
                    }                   
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Debe completar los campos requeridos (*)");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Delete(long id)
        {
            try
            {
                if (SessionContext.UsuarioActual.IdUsuario != id)
                {
                    UsuarioBusiness.Instance.DeleteLogic(id, null, AuditoriaBusiness.Instance.AuditoriaUsuario);

                    return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("No puede borrar el usuario con el que ha iniciado sesión actualmente");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}