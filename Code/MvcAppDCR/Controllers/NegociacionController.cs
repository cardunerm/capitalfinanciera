﻿using Business;
using Controllers;
using CurrentContext;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Utils.Enumeraciones;

namespace MvcAppDCR.Controllers
{
    public class NegociacionController : BaseController
    {

        #region property

        public int IdNegociacion
        {
            get { return int.Parse(Session["IdNegociacion"].ToString()); }
            set { Session["IdNegociacion"] = value; }
        }

        #endregion 


        // GET: Negociacion
        public ActionResult Index()
        {
            
            ViewBag.TipoOperacion = Enumerations.DCREnumerationsHelper.GetTipoNegociacion(true);
            ViewBag.Bancos = BancoBusiness.Instance.GetAll().OrderBy(banco => banco.Nombre).GetSelectListItem("IdBanco", "Nombre",true);
          //  ViewBag.Mayorista = MayoristaBusiness.Instance.GetAll().OrderBy(mayo => mayo.Nombre).GetSelectListItem("Id", "Nombre",true);
            ViewBag.Proveedor = ProveedorBusiness.Instance.GetAll().OrderBy(prove => prove.Nombre).GetSelectListItem("Id", "Nombre",true);
            return View();
        }


        public ActionResult ABMNegociacion(int? id)
        {
            ViewBag.Title = (id != null)? "Editar Negociación" : "Crear Negociación";
            ViewBag.TipoOperacion = Enumerations.DCREnumerationsHelper.GetTipoNegociacion(false);
            ViewBag.Bancos = BancoBusiness.Instance.GetAll().OrderBy(banco => banco.Nombre).GetSelectListItem("IdBanco", "Nombre", true);
            ViewBag.Mayorista = MayoristaBusiness.Instance.GetAll().OrderBy(mayo => mayo.Nombre).GetSelectListItem("Id", "Nombre", true);
            ViewBag.Proveedor = ProveedorBusiness.Instance.GetAll().OrderBy(prove => prove.Nombre).GetSelectListItem("Id", "Nombre", true);

            if (!id.HasValue || id.Value <= 0)
            {
                /*creo una nueva negociación*/
                Negociacion entidad = new Negociacion();
                entidad.IdTipoOperacion = (int)Enumerations.TipoNegociacion.Bancaria;
                entidad.IdBanco = 1;
                entidad.IdProveedor = null;
                entidad.IdMayorista = null;
                entidad.Fecha = DateTime.Now;
                entidad.Observacion = string.Empty;
                entidad.IdUsuario = SessionContext.UsuarioActual.IdUsuario;
                entidad.IdEstado = 0;

                NegociacionBusiness.Instance.SaveOrUpdate(entidad);
                IdNegociacion = entidad.Id;
                return View(entidad);
            }
            else
            {
                var entity = NegociacionBusiness.Instance.GetEntity(id.Value);
                IdNegociacion = entity.Id;

                if (entity == null)
                {
                    return HttpNotFound();
                }

                return View(entity);
            }
            
        

           

            
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateNegociacion(Negociacion negociacion)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    NegociacionBusiness.Instance.Save(IdNegociacion,negociacion);                   

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }


        public JsonResult GetNegociaciones(string tiponegociacion = "", string banco = "",string mayorista = "",string proveedor = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;
                var list = NegociacionBusiness.Instance.GetByFilters(tiponegociacion,banco,mayorista,proveedor, skip, top, ref rowCount);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list.Result, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult AgregarCheque(string IdsCheque)
        {
            try
            {
                string[] ids = IdsCheque.Split(',');

                foreach (var item in ids.ToList())
                {

                    if (item == "")
                        continue;

                    NegociacionDetalle entidad = new NegociacionDetalle();
                    entidad.IdCheque = int.Parse(item);
                    entidad.Fecha = DateTime.Now;
                    entidad.IdNegociacion = IdNegociacion;
                    entidad.IdEstado = (int)Enumerations.EstadoCheque.Negociado;
                    NegociacionDetalleBusiness.Instance.SaveOrUpdate(entidad);


                    Negociacion ent = NegociacionBusiness.Instance.GetEntity(IdNegociacion);
                    //Cambio es estado del cheque
                    if(ent.IdBanco != null)
                        ChequeBusiness.Instance.ChangeStatus((long)entidad.IdCheque, Estados.eEstadosCheque.Depositado);
                    else
                        ChequeBusiness.Instance.ChangeStatus((long)entidad.IdCheque, Estados.eEstadosCheque.Negociado);

                }
                         

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }


        public JsonResult GetChequesByNegociacion()
        {

            List<NegociacionDetalleModel> negList = new List<NegociacionDetalleModel>();

            try
            {
                int rowCount = 0;
                var list = NegociacionDetalleBusiness.Instance.GetByIdNegociacion(IdNegociacion);

                foreach (var item in list)
                {
                    negList.Add(new NegociacionDetalleModel
                    {
                        Id = item.Id,
                        NombreBanco = item.NombreBanco,
                        Numero = item.Numero,
                        Monto = item.Monto,
                        FechaVencimiento = item.FechaVencimiento,
                        Estado = item.Estado
                    });
                }


                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = negList, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }


        public JsonResult CerrarNegociacion(string IdNegociacion, string Monto)
        {            
            try
            {
                NegociacionBusiness.Instance.Cerrar(IdNegociacion, Monto);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }


        public JsonResult CancelarNegociacion(int IdNegociacion)
        {

            try
            {
                NegociacionBusiness.Instance.Cancelar(IdNegociacion);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }

        public JsonResult DeleteChequeSeleccionado(int IdNegociacionDetalle)
        {

            try
            {
                NegociacionBusiness.Instance.EliminarChequeSeleccionado(IdNegociacionDetalle);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }

        

    }
}