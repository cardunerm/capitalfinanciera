﻿using Business;
using Controllers;
using CrystalDecisions.CrystalReports.Engine;
using CurrentContext;
using Domine;
using Domine.Dto;
using Enumerations;
using Luma.Utils;
using Luma.Utils.Logs;
using Luma.Utils.Security;
using Mvc.Utils;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using MvcAppDCR.Utils;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using Utils.Enumeraciones;

namespace MvcAppDCR.Controllers
{
    public class BandejaOperacionController : BaseController
    {
        // GET: BandejaOperacion
        public ActionResult Index()
        {
            LoadCombo(false, true);
            return View();
        }

        private void LoadCombo(bool aprobador = false, bool bandeja = false, Estados.eEstadosOp estado = Estados.eEstadosOp.Pendiente)
        {
            ViewBag.Estados = OperacionInversoresBussines.Instance.GetEstados(bandeja, aprobador, estado);
            
        }


        //public JsonResult GetBandejaOperacion(int? idEstado, DateTime? fechaDesde, DateTime? fechaHasta, int? tipoMoneda, int? condicion, string inversor = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        public JsonResult GetBandejaOperacion(int? idEstado, string fechaDesde, string fechaHasta, int? tipoMoneda, int? condicion, string inversor = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            DateTime _fechadesde;
            DateTime _fechahasta;

            //Variables de validacion
            DateTime fecha_desde;
            DateTime fecha_hasta;
              
            try
            {
                if ((fechaDesde!= null) && (fechaHasta!=null))
                {
                    fecha_desde = DateTime.Parse(fechaDesde);
                    fecha_hasta = DateTime.Parse(fechaHasta);

                    if (fecha_hasta < fecha_desde)
                    {
                        return Json(new
                        {
                            msg = Resources.Resource.FechaHastaMenorFechaDesde
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                

                int rowCount = 0;
                           

                if (fechaDesde == "" || fechaDesde == null)
                    _fechadesde = DateTime.Now.AddDays(-7);
                else
                    _fechadesde = DateTime.Parse(fechaDesde);

                if (fechaHasta == "" || fechaHasta == null)
                    _fechahasta = DateTime.Now.AddDays(1);
                else
                    _fechahasta = DateTime.Parse(fechaHasta).AddDays(1);                              

                var list = BandejaOperacionBusiness.Instance.GetBandejaOperacionByFilters(idEstado, _fechadesde, _fechahasta, inversor, tipoMoneda, condicion, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {

                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }

        public JsonResult GetChequesOp(int IdOperacionInversor)
        {
            try
            {
                
                List<DtoCheque> list = new List<DtoCheque>();

                list = BandejaOperacionBusiness.Instance.GetCheques(IdOperacionInversor);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet); ;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RechazarCheque(int IdCheque)
        {
            try
            {
                BandejaOperacionBusiness.Instance.RechazoCheque(IdCheque);
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {

                return ExceptionManager.Instance.JsonError(ex);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelarOperacion(int IdOperacionInversor)
        {
            try
            {
                BandejaOperacionBusiness.Instance.CancelarOperacion(IdOperacionInversor);
                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {

                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetDataInversor(int idInversor, int tipoMoneda)
        {

            try
            {
                var dtoData = BandejaOperacionBusiness.Instance.GetDataInversor(idInversor, tipoMoneda);

                return Json(
                    new
                    {
                        success = true,
                        nombre = dtoData.Nombre,
                        cuit = dtoData.Cuit,
                        numero = dtoData.Numero.ToString("000-0000000"),
                        saldo = dtoData.Saldo
                    });
            }

            catch
            {
                return Json(new { success = false, saldo = 0 });
            }
        }

        

        public ActionResult ReporteDeposito(int IdOperacionInversor = 0)
        {
            try
            {
                View_OperacionesInversor operacion = new View_OperacionesInversor();
                var inlcude = new List<string>();
                inlcude.Add("CuentaCorrienteInversor");
                inlcude.Add("CuentaCorrienteInversor.Inversor");
                var deposito = OperacionInversoresBussines.Instance.GetByConditions<OperacionInversor>(o => o.IdOperacionInversor == IdOperacionInversor, inlcude).FirstOrDefault();

                var getTipo = OperacionInversoresBussines.Instance.GetTipoCuentaByIdCtaCte(deposito.IdCuentaCorrienteInversor);

                UtilConverNumber util = new UtilConverNumber();
                deposito.MontoLetter = util.Convertir(deposito.Monto.ToString(), true, getTipo );                          
                

                if (deposito != null)
                {
                    var depositoModel = new List<DepositoModel>();


                    ReportClass reportClass = ReportHelper.LoadReport("DepositoEfectivoReport");

                    //reportClass.SetDataSource(chequeModel);
                    reportClass.SetParameterValue("nombreUsuario", SessionContext.UsuarioActual.Nombre);
                    reportClass.SetParameterValue("fecha", deposito.Fecha);
                    reportClass.SetParameterValue("nombreInversor", deposito.CuentaCorrienteInversor.Inversor.Nombre);
                    reportClass.SetParameterValue("numeroCUIT", deposito.CuentaCorrienteInversor.Inversor.CUIT);                    
                    reportClass.SetParameterValue("monto", deposito.Monto);
                    reportClass.SetParameterValue("montoStr", deposito.MontoLetter);
                    reportClass.SetParameterValue("numeroCuentaCorriente", deposito.CuentaCorrienteInversor.NumeroCtaCteStr);
                    reportClass.SetParameterValue("numeroOperacion", deposito.IdOperacionInversor);
                    reportClass.SetParameterValue("tipoMoneda", ((TipoMoneda)deposito.CuentaCorrienteInversor.TipoMoneda).ToString());


                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                }
                else
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = "Error en la operacion", btnGoToLabel = "", redirectBtn = false });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }

        public ActionResult ReporteDepositoCheque(int IdOperacionInversor = 0)
        {
            try
            {
                //View_OperacionesInversor operacion = new View_OperacionesInversor();
                var include = new List<string>();
                include.Add("CuentaCorrienteInversor");
                include.Add("CuentaCorrienteInversor.Inversor");
                var deposito = OperacionInversoresBussines.Instance.GetByConditions<OperacionInversor>(o => o.IdOperacionInversor == IdOperacionInversor, include).FirstOrDefault();
                           
                var listaCheques = new List<ChequeModel>();
                
                List<string> includes = new List<string>();
                includes.Add("Cheque");
                List<DetalleOperacion> detOperacion = DetalleOperacionBusiness.Instance.GetByConditions<DetalleOperacion>(o => o.IdOperacionInversor == IdOperacionInversor, includes).ToList();

                var getTipo = OperacionInversoresBussines.Instance.GetTipoCuentaByIdCtaCte(deposito.IdCuentaCorrienteInversor);

                UtilConverNumber util = new UtilConverNumber();
                deposito.MontoLetter = util.Convertir(deposito.Monto.ToString(), true, getTipo);

                //var cheques = ChequeBusiness.Instance.GetByConditions<Cheque>(c => c.IdCheque == detcheques.IdCheque);
                foreach (var detalle in detOperacion)
                {
                    listaCheques.Add((ChequeModel)WebHelper.GenericLoad(detalle.Cheque, new ChequeModel()));
                }
                
                if (deposito != null)
                {
                    var depositoModel = new List<DepositoModel>();


                    ReportClass reportClass = ReportHelper.LoadReport("DepositoChequeReport");

                    reportClass.SetDataSource(listaCheques);
                    reportClass.SetParameterValue("fecha", deposito.Fecha);                   
                    reportClass.SetParameterValue("inversor", deposito.CuentaCorrienteInversor.Inversor.Nombre);
                    reportClass.SetParameterValue("cuit", deposito.CuentaCorrienteInversor.Inversor.CUIT);
                    reportClass.SetParameterValue("monto", deposito.Monto);
                    reportClass.SetParameterValue("montoStr", deposito.MontoLetter);
                    reportClass.SetParameterValue("numeroCtaCte", deposito.CuentaCorrienteInversor.NumeroCtaCteStr);
                    reportClass.SetParameterValue("numeroOperacion", deposito.IdOperacionInversor);
                    reportClass.SetParameterValue("tipoMoneda", ((TipoMoneda)deposito.CuentaCorrienteInversor.TipoMoneda).ToString());
                    

                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                }
                else
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = "Error en la operacion", btnGoToLabel = "", redirectBtn = false });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }

        public ActionResult ExtraccionReporte(int IdOperacionInversor = 0)
        {
            try
            {
                View_OperacionesInversor operacion = new View_OperacionesInversor();
                var inlcude = new List<string>();
                inlcude.Add("CuentaCorrienteInversor");
                inlcude.Add("CuentaCorrienteInversor.Inversor");
                var deposito = OperacionInversoresBussines.Instance.GetByConditions<OperacionInversor>(o => o.IdOperacionInversor == IdOperacionInversor, inlcude).FirstOrDefault();

                var getTipo = OperacionInversoresBussines.Instance.GetTipoCuentaByIdCtaCte(deposito.IdCuentaCorrienteInversor);

                UtilConverNumber util = new UtilConverNumber();
                deposito.MontoLetter = util.Convertir(deposito.Monto.ToString(), true, getTipo);

                if (deposito != null)
                {
                    var depositoModel = new List<DepositoModel>();


                    ReportClass reportClass = ReportHelper.LoadReport("ExtraccionFondosReport");

                    //reportClass.SetDataSource(chequeModel);
                    reportClass.SetParameterValue("nombreUsuario", SessionContext.UsuarioActual.Nombre);
                    reportClass.SetParameterValue("fecha", deposito.Fecha);
                    reportClass.SetParameterValue("nombreInversor", deposito.CuentaCorrienteInversor.Inversor.Nombre);
                    reportClass.SetParameterValue("numeroCUIT", deposito.CuentaCorrienteInversor.Inversor.CUIT);
                    reportClass.SetParameterValue("monto", deposito.Monto);
                    reportClass.SetParameterValue("montoStr", deposito.MontoLetter);
                    reportClass.SetParameterValue("numeroCuentaCorriente", deposito.CuentaCorrienteInversor.NumeroCtaCteStr);
                    reportClass.SetParameterValue("numeroOperacion", deposito.IdOperacionInversor);
                    reportClass.SetParameterValue("tipoMoneda", ((TipoMoneda)deposito.CuentaCorrienteInversor.TipoMoneda).ToString());


                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                }
                else
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = "Error en la Operacion", btnGoToLabel = "", redirectBtn = false });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }

    }
}