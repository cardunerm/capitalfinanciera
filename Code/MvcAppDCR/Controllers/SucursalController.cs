﻿using Business;
using Controllers;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Controllers
{
    public class SucursalController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetSucursales(string numero = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = SucursalBusiness.Instance.GetByFilters(numero, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMSucursal(long? id)
        {
            var empresas = EmpresaBusiness.Instance.GetAllEmpresas();

            ViewBag.Empresas = WebHelper.GetSelectListItem(empresas, "Id", "Nombre");

            if (!id.HasValue || id.Value <= 0)
            {
                return View(new Sucursal());
            }

            var entity = SucursalBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMSucursal(Sucursal sucursal)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SucursalBusiness.Instance.SaveOrUpdateSucursal(sucursal);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                SucursalBusiness.Instance.DeleteLogic(id, SucursalBusiness.Instance.ValidateDelete, AuditoriaBusiness.Instance.AuditoriaSucursal);

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        public JsonResult ValidateSucursal(string numero = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(numero))
                {
                    var entity = SucursalBusiness.Instance.SucursalExist(numero);

                    if (entity != null)
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true, Id = entity.IdSucursal });
                    }
                    else
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                    }
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Requerido");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult GetSucursalesByNombreUsuarioOrderById(string nombreUsuario = "")
      {
            try
            {
                var sucursales = SucursalBusiness.Instance.GetSucursalByNombreUsuario(nombreUsuario);

                var empresa = EmpresaBusiness.Instance.GetEmpresasByNombreUsuario(nombreUsuario).FirstOrDefault();

                var options = WebHelper.GetSelectListItem(sucursales, "IdSucursal", "Descripcion");

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Options = options, Empresa = empresa.Id});
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
        
    }
}