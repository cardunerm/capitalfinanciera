﻿using Business;
using Controllers;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcAppDCR.Controllers
{
    public class ParametroController : BaseController
    {
        //
        // GET: /Banco/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetParametros(string nombre = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = ParametroBusiness.Instance.GetByFilters(nombre, skip, top, ref rowCount);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMParametro(long? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                return View(new Parametro());
            }

            var entity = ParametroBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMParametro(Parametro parametro)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (parametro.Id != 0)
                    {
                        Parametro _parametro = ParametroBusiness.Instance.GetEntity(long.Parse(parametro.Id.ToString()));
                        _parametro.Valor = parametro.Valor;
                        ParametroBusiness.Instance.SaveOrUpdate(parametro);
                    }
                    else
                    {
                        ParametroBusiness.Instance.SaveOrUpdate(parametro);
                    }

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long id)
        {
            try
            {
                

                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK });
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        [HttpPost]
        public JsonResult ValidateBanco(string nombre = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(nombre))
                {
                    var entity = BancoBusiness.Instance.BancoExist(nombre);

                    if (entity != null)
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true, Id = entity.IdBanco });
                    }
                    else
                    {
                        return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = false });
                    }
                }
                else
                {
                    return ExceptionManager.Instance.JsonError("Requerido");
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }
    }
}