﻿using AutoMapper;

using Business;
using Controllers;
using CrystalDecisions.CrystalReports.Engine;
using CurrentContext;
using Domine;
using Domine.Dto;
using Enumerations;
using Luma.Utils;
using Luma.Utils.Logs;
using Mvc.Utils;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web.Mvc;
using Utils.Enumeraciones;

namespace MvcAppDCR.Controllers
{
    public class ProyeccionFinancieraBBBController : BaseController
    {
        // GET: ProyeccionFinancieraBBB
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetProyeccion(int idInversor = -1, int idCuenta = -1, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                List<DtoProyeccionFinanciera> list = new List<DtoProyeccionFinanciera>();
                
                list = ProyeccionFinancieraBusiness.Instance.GetProyeccionBBB(idInversor, idCuenta);
                
                int rowCount = list.Count();

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetCheques(int idCuenta = -1)
        {
            try
            {
                List<DtoCheque> chequesEnCartera = new List<DtoCheque>();

                string montoStr = string.Empty;
                string montoOperativoStr = string.Empty;
                string montoGarantiaStr = string.Empty;

                if (idCuenta != -1 && idCuenta != 0)
                {
                    List<Cheque> listaChequesEncontrados = ProyeccionFinancieraBusiness.Instance.GetChequesEnCartera(idCuenta);
                    foreach (var cheque in listaChequesEncontrados)
                    {
                        chequesEnCartera.Add(new DtoCheque
                        {
                            IdCheque = cheque.IdCheque,
                            Numero = cheque.Numero,
                            FechaVencimiento = cheque.FechaVencimiento,
                            Monto = cheque.Monto,
                            EstadoStr = cheque.EstadoStr,
                            ClasificacionStr = cheque.ClasificacionStr
                        });

                    }
                    //chequesEnCartera = Mapper.Map<List<Cheque>, List<DtoCheque>>(ProyeccionFinancieraBusiness.Instance.GetChequesEnCartera(idCuenta));
                    CuentaCorrienteInversor cuenta = CuentaCorrienteInversorBussines.Instance.GetByConditions(c => c.IdCuentaCorrienteInversor == idCuenta).FirstOrDefault();

                    ExtraccionFondosController exCont = new ExtraccionFondosController();

                    decimal montoOperativo = 0;
                    decimal montoGarantia = 0;
                    decimal monto = exCont.CalcularMontoDescubiertoBBB(cuenta, out montoOperativo, out montoGarantia);
                    montoStr = cuenta.TipoMoneda == (int)TipoMoneda.Peso ? "ARS " + monto.ToString("0.##") : "USD " + monto.ToString("0.##");
                    montoOperativoStr = cuenta.TipoMoneda == (int)TipoMoneda.Peso ? "ARS " + montoOperativo.ToString("0.##") : "USD " + montoOperativo.ToString("0.##");
                    montoGarantiaStr = cuenta.TipoMoneda == (int)TipoMoneda.Peso ? "ARS " + montoGarantia.ToString("0.##") : "USD " + montoGarantia.ToString("0.##");
                }

                return Json(
                    new
                    {
                        cheques = chequesEnCartera,
                        montoMaxRetiro = montoStr,
                        montoOperativo = montoOperativoStr,
                        montoGarantia = montoGarantiaStr
                    }
                    , JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult CalcularTotal(int idCuenta = -1)
        {
            try
            {
                List<DtoCheque> chequesEnCartera = new List<DtoCheque>();
                List<Cheque> listaChequesEncontrados = new List<Cheque>();
                DtoCheque ultimoChequeACobrar = new DtoCheque();

                string montoOperacionStr = string.Empty;
                string montoPorcentajeGarantiaStr = string.Empty;
                string montoCobrarStr = string.Empty;

                //Obtener cheques ordenados (descendiente) por fecha de vencimiento segun idCuenta.
                if (idCuenta != -1 && idCuenta != 0)
                {
                    listaChequesEncontrados = ProyeccionFinancieraBusiness.Instance.GetChequesEnCartera(idCuenta, null, null, 2);
                    foreach (var cheque in listaChequesEncontrados)
                    {
                        chequesEnCartera.Add(new DtoCheque
                        {
                            IdCheque = cheque.IdCheque,
                            Numero = cheque.Numero,
                            FechaVencimiento = cheque.FechaVencimiento,
                            Monto = cheque.Monto,
                            EstadoStr = cheque.EstadoStr,
                            ClasificacionStr = cheque.ClasificacionStr
                        });
                    }

                    if (chequesEnCartera.Count == 0)
                    {
                        return Json(
                                        new
                                        {
                                            montochequeOperacion = "$ 0",
                                            montochequeGarantia = "0 %",
                                            montototalRetirar = "$ 0"
                                        }, JsonRequestBehavior.AllowGet
                                    );
                    }

                    chequesEnCartera = chequesEnCartera.OrderByDescending(c => c.FechaVencimiento).ToList();
                    ultimoChequeACobrar = chequesEnCartera.First();
                }
                else
                {
                    return Json(new { result = new { }, count = 0, msg = "El Id de Cuenta es inválido." }, JsonRequestBehavior.AllowGet);
                }

                //Obtener cuenta segun idCuenta y obtener porcentaje de interés diario (Tasa Diaria: R)
                CuentaCorrienteInversor cuenta = CuentaCorrienteInversorBussines.Instance.GetByConditions<CuentaCorrienteInversor>(c => c.IdCuentaCorrienteInversor == idCuenta).FirstOrDefault();
                HistorialTasaInteresInversor tasa = HistorialTasaInteresBusiness.Instance.GetByConditions<HistorialTasaInteresInversor>(t => t.IdInversor == cuenta.IdInversor && t.TipoMonedaTasa == cuenta.TipoMoneda && t.FechaHasta == null).FirstOrDefault();
                decimal interesDiario = tasa.ValorTasa.Value / 100;

                //Calcular cantidad de días hasta la fecha de cobro del último cheque (periodo: t)
                int periodo = (ultimoChequeACobrar.FechaVencimiento - DateTime.Now.Date).Days;

                //Obtener capital inicial de acuerdo al saldo en cuenta
                decimal saldo = ResumenDiarioOperacionInversorBusiness.Instance.GetSaldoByIdCuenta(cuenta.IdCuentaCorrienteInversor, cuenta.TipoMoneda);

                //Obtener el saldo proyectado hacia la fecha de vencimiento del último cheque, utilizando la fórmula de interés compuesto con IVA (Cap Final: Cf)
                int counter = 1;
                double capFinal = 0;
                double saldoAnterior = (double)saldo;

                DateTime dateCounter = DateTime.Now.AddDays(1).Date;
                do
                {
                    double gtosCheque = 0;
                    double porcIvaGtosCheque = (double)21 / 100;
                    double porcGtosCheque = 2.5 / 100;
                    double montoCheque = 0;

                    foreach (var cheque in chequesEnCartera)
                    {
                        if (cheque.FechaVencimiento.Date == dateCounter)
                        {
                            gtosCheque = gtosCheque + (((double)cheque.Monto * porcGtosCheque) + (((double)cheque.Monto * porcGtosCheque) * porcIvaGtosCheque));
                            montoCheque = montoCheque + (double)cheque.Monto;
                        }
                    }

                    capFinal = (saldoAnterior * (1 + ((double)interesDiario * 1.21))) + montoCheque - gtosCheque;
                    saldoAnterior = capFinal;

                    dateCounter = dateCounter.AddDays(1);
                    ++counter;
                }
                while (counter <= periodo);

                //Calculo de valores adicionales de monto operativo actual y porcentaje en garantía actual
                decimal montoOperativo = listaChequesEncontrados.Sum(p => p.Monto);
                montoOperacionStr = montoOperativo.ToString("0.##");

                decimal porcentajeGarantia = cuenta.Inversor.PorcentajeBalanceGarantiaOperativo;
                montoPorcentajeGarantiaStr = porcentajeGarantia.ToString();

                //Valido si a la fecha del último cobro de cheque en cartera el saldo aún sigue siendo negativo, no le puedo prestar mas plata, devuelvo $0
                if (capFinal <= 0)
                {
                    return Json(
                    new
                    {
                        montochequeOperacion = "$ " + montoOperacionStr,
                        montochequeGarantia = montoPorcentajeGarantiaStr + " %",
                        montototalRetirar = "$ 0.00"
                    }
                    , JsonRequestBehavior.AllowGet);
                }

                //Obtener el monto tope de prestamo utilizndo la fórmula ideada
                double capAPrestar = (capFinal) / (Math.Pow(1 + ((double)interesDiario * 1.21), periodo));

                return Json(
                    new
                    {
                        montochequeOperacion = "$ " + montoOperacionStr,
                        montochequeGarantia = montoPorcentajeGarantiaStr + " %",
                        montototalRetirar = "$ " + capAPrestar.ToString("0.##")
                    }
                    , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpGet]
        public JsonResult ExtraerFondo(int idCuenta = -1,string monto = "")
        {
            try
            {
                DtoOperacionInversor dtoOperacion = new DtoOperacionInversor();
                DtoReporteEfectivo dtoReporte = new DtoReporteEfectivo();
                dtoOperacion.IdCuentaCorrienteInversor = idCuenta;
                dtoOperacion.Monto = decimal.Parse(monto.Replace("$",""));
                dtoOperacion.Observacion = "Egreso por Operación";


                var success = OperacionInversoresBussines.Instance.ExtraccionFondos(dtoOperacion, SessionContext.UsuarioActual, ref dtoReporte);


                //Actualizo el estado de las operacion para que no se consideren mas.
                OperacionInversoresBussines.Instance.MarcarOperacionLiquidada();

                ////////////////////////////////////////////////////////////////////
                if (success)
                {
                    return Json(new
                    {
                        success = success,
                        data = dtoReporte,
                        msg = Resources.Resource.OperacionCorrecta,
                        url = "BandejaOperaciones"
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        success = success,
                        msg = Resources.Resource.ErrorOperacion
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GenerarReporteExtraccion(string paramDtoReporteDeposito)
        {
            DtoReporteEfectivo dtoReporteDeposito = Newtonsoft.Json.JsonConvert.DeserializeObject<DtoReporteEfectivo>(paramDtoReporteDeposito);
            try
            {
                if (dtoReporteDeposito != null)
                {
                    var depositoModel = new List<DepositoModel>();


                    ReportClass reportClass = ReportHelper.LoadReport("ExtraccionFondosReport");

                    reportClass.SetParameterValue("nombreUsuario", dtoReporteDeposito.NombreUsuario);
                    reportClass.SetParameterValue("fecha", dtoReporteDeposito.Fecha);
                    reportClass.SetParameterValue("nombreInversor", dtoReporteDeposito.NombreInversor);
                    reportClass.SetParameterValue("numeroCUIT", dtoReporteDeposito.CuitInversor);
                    reportClass.SetParameterValue("monto", dtoReporteDeposito.Monto);
                    reportClass.SetParameterValue("montoStr", "(" + new UtilConverNumber().Convertir(dtoReporteDeposito.Monto.ToString(), true, null) + ")");
                    reportClass.SetParameterValue("numeroCuentaCorriente", dtoReporteDeposito.CuentaCorrienteStr);
                    reportClass.SetParameterValue("numeroOperacion", dtoReporteDeposito.NumeroOperacion);
                    reportClass.SetParameterValue("tipoMoneda", dtoReporteDeposito.TipoMoneda);


                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                }
                else
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = "Operacion no encontrada", btnGoToLabel = "", redirectBtn = false });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }
    }
}