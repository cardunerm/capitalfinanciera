﻿using Business;
using Controllers;
using Domine;
using Luma.Utils.ActualizarYDescargarArchivos;
using Luma.Utils.Logs;
using Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MvcAppDCR.Controllers
{
    public class ArchivoAdjuntoController : BaseController
    {
        private string urlAttachment;
        private JavaScriptSerializer js = new JavaScriptSerializer();
        private string StorageRoot
        {
            get { return Path.Combine(System.Web.HttpContext.Current.Server.MapPath(urlAttachment)); } //Path should! always end with '/'
        }

        public ActionResult Index(string url = null, int entityIdenum = 0, string returnUrl = null, string destino = null)
        {
            SetUrlsAndId(url, entityIdenum);

            ViewBag.ReturnUrl = returnUrl;

            ViewBag.Destino = destino;

            return PartialView();
        }

        /// <summary>
        /// Carga los archivos adjuntados anteriormente para el registro en cuestión (idRecord, idEntityEnum) .
        /// </summary>
        /// <returns></returns>
        public PartialViewResult LoadAttachedFiles()
        {
            int idRecord = int.Parse(Session["IdRegistro"].ToString());
            int idEntityEnum = int.Parse(Session["IdEntityEnum"].ToString());
            List<DetalleArchivoAdjunto> attachedFilesList = GetAttachedFiles(idRecord, idEntityEnum);
            var statuses = new List<FilesStatus>();
            if (attachedFilesList.Count > 0)
            {
                foreach (DetalleArchivoAdjunto item in attachedFilesList)
                {
                    statuses.Add(new FilesStatus(item.Nombre, item.Tamaño, item.FullPathArchivoAdjunto));
                }

            }
            return PartialView("~/Views/Shared/ArchivosAdjuntos/_ListDownloadFiles.cshtml", statuses);
        }

        private List<DetalleArchivoAdjunto> GetAttachedFiles(int idRecord, int entityIdEnum)
        {
            List<DetalleArchivoAdjunto> detailsList = ArchivoAdjuntoBusiness.Instance.GetAttachedfiles(idRecord, entityIdEnum);
            return detailsList;
        }

        /// <summary>
        /// Extrae del parámetro la url raíz, la url full (donde se guardará el adjunto) y el id del registro (objeto) al que se le relacionará el adjunto.
        /// </summary>
        /// <param name="url">contiene el path completo (incluye la carpeta con el id) donde se almacenará el adjunto</param>
        private void SetUrlsAndId(string url, int idEntityEnum)
        {
            if (url != null && idEntityEnum != 0)
            {
                string[] urlAndId = url.Split('/');
                var id = urlAndId[urlAndId.Length - 2];
                var urlCabecera = url.Substring(0, url.IndexOf(id));

                Session["UrlCabecera"] = urlCabecera; //Path raíz.
                Session["Url"] = url;
                Session["IdRegistro"] = id; // id del registro al que se le adjuntarán archivos.
                Session["IdEntityEnum"] = idEntityEnum;
            }

        }

        [HttpPost]
        public void UploadHandle()
        {
            js.MaxJsonLength = 41943040;
            urlAttachment = Session["Url"].ToString();
            UploadFile(System.Web.HttpContext.Current);
        }

        [HttpPost]
        public JsonResult DeleteAttachedFiles(string[] files)
        {
            foreach (string file in files)
            {
                DeleteAttachedFile(file);
            }

            return SendJson(true, "info", Resource.EliminacionCorrecta);

        }

        [HttpPost]
        public JsonResult DeleteAttachedFile(string fileName)
        {
            try
            {
                urlAttachment = Session["Url"].ToString();
                string filePath = StorageRoot + fileName;

                string[] url = filePath.Split('\\');
                var idRecord = int.Parse(url[url.Length - 2]);

                DetalleArchivoAdjunto detail = ArchivoAdjuntoBusiness.Instance.GetByConditions(a => a.IdEntidadRegistro == idRecord && a.Nombre == fileName).FirstOrDefault();

                if (detail != null)
                {
                    //Se elimina de la base.
                    ArchivoAdjuntoBusiness.Instance.Remove(detail);

                    if (System.IO.File.Exists(filePath))
                    {
                        //Se elimina lógicamente el archivo.
                        System.IO.File.Delete(filePath);
                    }
                    return SendJson(true, "info", Resource.EliminacionCorrecta);
                }
                else
                {
                    // mostrar mensaje "No se pudo encontra o eliminar el archivo".
                    return SendJson(false, "error", Resource.EliminacionIncorrecta);
                }

            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                return SendJson(false, "error", Resource.EliminacionIncorrecta);
            }

        }

        private JsonResult SendJson(bool result, string typeMessage, string message)
        {
            return Json(new
            {
                status = result,
                type = typeMessage,
                message = message
            });
        }

        public void DownLoadFile(string fileName)
        {
            urlAttachment = Session["Url"].ToString();
            DeliverFile(System.Web.HttpContext.Current); // fileName);
        }

        // Descarga de archivos.
        public JsonResult DeliverFile(System.Web.HttpContext context) // string fileName)
        {
            var filename = context.Request["f"];
            var filePath = StorageRoot + filename;

            // var cont = System.Web.HttpContext.Current;
            if (System.IO.File.Exists(filePath))
            {
                System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
                System.Web.HttpContext.Current.Response.ClearContent();
                System.Web.HttpContext.Current.Response.WriteFile(filePath);

                return SendJson(true, "info", Resource.DescargaExitosa);
            }
            else
            {
                return SendJson(false, "error", Resource.ArchivoInexistente);
            }
        }

        protected void SaveEntitiesAttachment(ref string fileName, int size, ref string fullPath)
        {
            // buscar la cabecera, si no existe crearla. 
            int idEntityEnum = int.Parse(Session["IdEntityEnum"].ToString());
            CabeceraAdjuntos header = CabeceraAdjuntosBusiness.Instance.GetByConditions(c => c.IdEntidadEnum == idEntityEnum).FirstOrDefault();
            
            if (header == null)
            {
                header = new CabeceraAdjuntos();

                header.IdEntidadEnum = int.Parse(Session["IdEntityEnum"].ToString());
                header.PathRaiz = Session["UrlCabecera"].ToString();

                CabeceraAdjuntosBusiness.Instance.SaveOrUpdate(header);
            }

            int idRecord = int.Parse(Session["IdRegistro"].ToString());
            if (!Exists_validate(idRecord, fileName, header.Id))
            {
                SaveDetail(fileName, size, fullPath, header, idRecord);
            }
            else
            {
                //Cambio el nombre al archivo, ya que el que tiene existe actualmente.
                ChangeNameAndPath(idRecord, ref fileName, header.Id, ref fullPath);

                SaveDetail(fileName, size, fullPath, header, idRecord);
            }

        }

        private void SaveDetail(string fileName, int size, string fullPath, CabeceraAdjuntos header, int idRecord)
        {
            // crea el archivo adjunto y lo asociar a la cabecera.
            DetalleArchivoAdjunto detail = new DetalleArchivoAdjunto();
            detail.IdCabeceraAdjuntos = header.Id;
            detail.FullPathArchivoAdjunto = fullPath;
            detail.IdEntidadRegistro = idRecord;
            detail.Nombre = fileName;
            detail.Tamaño = size;

            ArchivoAdjuntoBusiness.Instance.SaveOrUpdate(detail);
        }

        private void ChangeNameAndPath(int idRecord, ref string fileName, int idHeader, ref string fullPath)
        {
            var ext = Path.GetExtension(fullPath);
            var fName = fileName.Substring(0, (fileName.Length - ext.Length)); // le quito la extencion
            var files = ArchivoAdjuntoBusiness.Instance.GetByConditions<DetalleArchivoAdjunto>(f => f.IdEntidadRegistro == idRecord
            && f.IdCabeceraAdjuntos == idHeader
            && f.Nombre.Trim().ToUpper().Contains(fName + "("));

            if (files.Count == 0)
            {
                // es el primero repetido. Se le asigna el "(1)"
                fName = fName + "({0})" + ext;
                fileName = string.Format(fName, 1);
                fullPath = StorageRoot + Path.GetFileName(fileName);
            }
            else
            {
                //Se le asigna el siguiente número del último guardado.
                int index = files.Count - 1;
                string lastNameFile = files[index].Nombre;
                var start = lastNameFile.IndexOf('(') + 1;
                var length = lastNameFile.Length - start - ext.Length - 1;
                string number = lastNameFile.Substring(start, length);
                int nexNumber = int.Parse(number) + 1;

                fName = fName + "({0})" + ext;
                fileName = string.Format(fName, nexNumber);
                fullPath = StorageRoot + Path.GetFileName(fileName);

            }
        }

        private bool Exists_validate(int idRecord, string fileName, int idHeader)
        {
            return ArchivoAdjuntoBusiness.Instance.Any<DetalleArchivoAdjunto>(p => p.IdEntidadRegistro == idRecord && p.IdCabeceraAdjuntos == idHeader && p.Nombre.Trim().ToUpper() == fileName.Trim().ToUpper());
        }

        // Upload file to the server
        private void UploadFile(System.Web.HttpContext context)
        {
            var statuses = new List<FilesStatus>();
            var headers = context.Request.Headers;

            try
            {
                if (string.IsNullOrEmpty(headers["X-File-Name"]))
                {
                    UploadWholeFile(context, statuses);
                }
                else
                {
                    UploadPartialFile(headers["X-File-Name"], context, statuses);
                }

                WriteJsonIframeSafe(context, statuses);

            }

            catch (Exception ex)
            {

                LogManager.Log.RegisterError(ex);
            }
        }

        // Upload entire file
        private void UploadWholeFile(System.Web.HttpContext context, List<FilesStatus> statuses)
        {
            for (int i = 0; i < context.Request.Files.Count; i++)
            {
                var file = context.Request.Files[i];

                var fullPath = StorageRoot + Path.GetFileName(file.FileName);
                string fullName = Path.GetFileName(file.FileName);
                SaveEntitiesAttachment(ref fullName, file.ContentLength, ref fullPath);

                if (!Directory.Exists(StorageRoot))
                {
                    Directory.CreateDirectory(StorageRoot);
                }

                file.SaveAs(fullPath);



                statuses.Add(new FilesStatus(fullName, file.ContentLength, fullPath));
            }
        }

        private void WriteJsonIframeSafe(System.Web.HttpContext context, List<FilesStatus> statuses)
        {
            context.Response.AddHeader("Vary", "Accept");
            try
            {
                if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
                    context.Response.ContentType = "application/json";
                else
                    context.Response.ContentType = "text/plain";
            }
            catch
            {
                context.Response.ContentType = "text/plain";
            }

            var jsonObj = js.Serialize(statuses.ToArray());
            context.Response.Write(jsonObj);
        }

        private void UploadPartialFile(string fileName, System.Web.HttpContext context, List<FilesStatus> statuses)
        {
            if (context.Request.Files.Count != 1) throw new HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request");
            var inputStream = context.Request.Files[0].InputStream;
            var fullName = StorageRoot + Path.GetFileName(fileName);

            using (var fs = new FileStream(fullName, FileMode.Append, FileAccess.Write))
            {
                var buffer = new byte[1024];

                var l = inputStream.Read(buffer, 0, 1024);
                while (l > 0)
                {
                    fs.Write(buffer, 0, l);
                    l = inputStream.Read(buffer, 0, 1024);
                }
                fs.Flush();
                fs.Close();
            }
            statuses.Add(new FilesStatus(new FileInfo(fullName)));
        }



        // GET: File
        // Usado para exportación - No modificar.
        public FileResult Download(string name)
        {
            return File(name, "application/vnd.ms-excel", "exp.xls");
        }
    }
}