﻿using Business;
using Controllers;
using CurrentContext;
using Luma.Utils.Security;
using Domine;
using Domine.Dto;
using MvcAppDCR.Code;
using MvcAppDCR.SignalRhubs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Utils.Enumeraciones;
using System.IO;
using Luma.Utils;

namespace MvcAppDCR.Controllers
{
    public class OperacionInversorController : BaseController
    {
        #region Aprobacion
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SaveDetalleOperacionAprobacion(DtoOperacionAprobacion operacion, short? EstadoOperacion)
        {
            try
            {
                OperacionBusiness.Instance.UpdateDetalleOperacion(operacion.DetalleOperacion, EstadoOperacion, SessionContext.UsuarioActual, SessionContext.Configuracion);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult ActivarEdicion()
        {
            try
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
            }
        }

        //[ValidateAntiForgeryToken]
        //[HttpPost]
        //public JsonResult Pagar(string IDKey)
        //{

        //    if (IDKey == null)
        //        return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
        //    try
        //    {
        //        var idOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
        //        OperacionInversoresBussines.Instance.Pagar(idOp, CurrentContext.SessionContext.UsuarioActual);
        //        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        //    }

        //    catch (Exception)
        //    {
        //        return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
        //    }

        //}

        //[ValidateAntiForgeryToken]
        //[HttpPost]
        //public JsonResult FinalizarOperacion(DtoOperacionAprobacion operacion)
        //{
        //    try
        //    {
        //        string msg = Resources.Resource.ErrorOperacion;

        //        var result = OperacionInversoresBussines.Instance.Finalizar(operacion, SessionContext.UsuarioActual, ref msg);
        //        SendNotification(operacion);
        //        if (result)
        //            return Json(new
        //            {
        //                success = true,
        //                url = Url.Content("bandejaaprobacion")
        //            });
        //        else
        //        {
        //            return Json(new { success = false, msg = msg }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //private void SendNotification(DtoOperacionAprobacion operacion)
        //{
        //    var msg = CreateMessage(operacion);
        //    if (msg != null)
        //        NotificationHub.SendToEvaluador(operacion.IdUser.ToString(), msg);
        //}

        //private string CreateMessage(DtoOperacionAprobacion operacion)
        //{
        //    string ret = "La operación {0} ha cambio el estado a : {1}";
        //    if ((Estados.eEstadosOp)operacion.IdEstado != Estados.eEstadosOp.Pendiente)
        //    {
        //        return string.Format(ret, operacion.Id, ((Estados.eEstadosOp)operacion.IdEstado).GetEnumeracionName());
        //    }
        //    return null;
        //}

        public ActionResult PrintOperacion(string idOperacion)
        {
            //var idOp = Convert.ToInt64(StringCipher.Decrypt(idOperacion, CurrentContext.SessionContext.Key));
            //if (idOperacion != null)
            //{
            //    var operacion = OperacionInversoresBussines.Instance.GetEntity(idOp);
            //    var montoLetra = new UtilConverNumber().Convertir(operacion.MontoAprobado.ToString(), false);

            //    var model = new DtoPrintOperacionInversor
            //    {
            //        Empresa = SessionContext.Empresa,
            //        Operacion = operacion,
            //        MontoLetra = montoLetra
            //    };
            //    return View(model);
            //}
            //return HttpNotFound();
            return null;
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult EditDetalleAprobacion(int Id)
        {
            try
            {
                var edit = OperacionInversoresBussines.Instance.GetByConditions<DetalleOperacion>(model => model.Id == Id, model => model.Cheque, model => model.Cheque.Banco, model => model.OperacionInversor, model => model.OperacionInversor.CuentaCorrienteInversor, model => model.OperacionInversor.CuentaCorrienteInversor.Inversor).FirstOrDefault();
                if (edit.TasaInteres == null)
                {
                    var tasa = OperacionInversoresBussines.Instance.GetByConditions<EmisorValor>(emisor => emisor.NumeroEmisor == edit.Cheque.NroCuenta).FirstOrDefault();
                    edit.TasaInteres = tasa != null ? tasa.TasaAsignada : 0.0M;
                }

                if (edit.Gastos > 0)
                {
                    edit.Gastos = ((decimal)edit.Gastos * 100) / (decimal)edit.Operacion.Monto;
                }


                return Json(new
                {
                    success = true,
                    result = new DtoDetalleOperacionAprobacion
                    {
                        Numero = edit.Cheque.Numero,
                        TitularCuenta = edit.Cheque.TitularCuenta,
                        NumeroCuenta = edit.Cheque.NroCuenta,
                        IdBanco = edit.Cheque.IdBanco,
                        SucursalEmision = edit.Cheque.SucursalEmision,
                        FechaVencimiento = edit.Cheque.FechaVencimiento,
                        Monto = edit.Cheque.Monto,
                        DiasClearing = OperacionInversoresBussines.CalcularDiasClering(edit.Operacion.Fecha, edit.Cheque.FechaVencimiento),
                        Gastos = edit.Gastos,
                        GastoStr = edit.Gastos.ToString("#.00"),
                        Cp = edit.Cheque.CP == null ? String.Empty : edit.Cheque.CP.ToString(),
                        CMC7 = edit.Cheque.CMC7,
                        Tasa = edit.TasaInteres,
                        TasaStr = ((double)edit.TasaInteres).ToString("#.00"),
                        MontoAprobado = (double?)edit.MontoAprobado,
                        Observacion = edit.Observacion,
                        IdEstado = edit.Estado,
                        Id = edit.Id,
                        DetalleCalculo = OperacionInversoresBussines.DescripcionCalculo(edit),
                        ImagenFront = edit.Cheque.ImagenFront ?? "no-Imagen.png",
                        ImagenBack = edit.Cheque.ImagenBack ?? "no-Imagen.png",


                    }
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, msg = Resources.Resource.ErrorOperacion }, JsonRequestBehavior.AllowGet);
            }
        }

        private decimal CalcularMonto(DetalleOperacion edit, Configuracion configuracion)
        {
            if (edit.Estado == (int)Estados.eEstadosOpDetalle.Pendiente)
            {
                var fechaLiquidacion = edit.Operacion.Fecha;
                var tasa = edit.Operacion.Cliente.IdTasa != null ? edit.Operacion.Cliente.IdTasa.Value : 0;
                var fechaPago = edit.Cheque.FechaVencimiento;

                //Calculos
                var diasClering = OperacionInversoresBussines.CalcularDiasClering(fechaLiquidacion, fechaPago);
                var impuestoDiario = OperacionInversoresBussines.CalcularImpuestoDiario(diasClering, edit.Cheque.Monto, tasa);
                var impuestoAlCheque = OperacionInversoresBussines.CalcularImpustoAlCheque(edit.Cheque.Monto, configuracion);
                var impuestoPorGastos = OperacionInversoresBussines.CalcularImpustoPorGastos(edit.Cheque.Monto, configuracion);
                edit.Gastos = impuestoPorGastos;
                edit.Impuestos = impuestoDiario;
                edit.ImpuestosCheque = impuestoAlCheque;
                edit.MontoAprobado = (decimal)Math.Round(edit.Cheque.Monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2);

                return Math.Round(edit.Cheque.Monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2);
            }
            return (decimal)(edit.MontoAprobado ?? 0);
        }

        private JsonResult CalcularMontoConGasto(DateTime fechaPago, DateTime fechaLiquidacion, decimal tasa, decimal monto, decimal gasto, int estado, int diasC, bool? calculateCl, Configuracion configuracion)
        {
            try
            {
                //Calculos
                var diasClering = 0;

                if (calculateCl.HasValue && calculateCl.Value)
                    diasClering = OperacionInversoresBussines.CalcularDiasClering(fechaLiquidacion, fechaPago);
                else
                    diasClering = diasC;

                var impuestoDiario = OperacionInversoresBussines.CalcularImpuestoDiario(diasClering, monto, tasa);
                var impuestoAlCheque = OperacionInversoresBussines.CalcularImpustoAlCheque(monto, configuracion);
                var impuestoPorGastos = OperacionInversoresBussines.CalcularImpustoPorGastosByGasto(monto, gasto);

                return Json(new { amount = Math.Round(monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2), dc = diasClering });
            }
            catch (Exception ex) { throw ex; }

        }

        private decimal CalcularMonto(DateTime fechaPago, DateTime fechaLiquidacion, decimal tasa, decimal monto, Configuracion configuracion)
        {
            try
            {
                //Calculos
                var diasClering = OperacionInversoresBussines.CalcularDiasClering(fechaLiquidacion, fechaPago);
                var impuestoDiario = OperacionInversoresBussines.CalcularImpuestoDiario(diasClering, monto, tasa);
                var impuestoAlCheque = OperacionInversoresBussines.CalcularImpustoAlCheque(monto, configuracion);
                var impuestoPorGastos = OperacionInversoresBussines.CalcularImpustoPorGastos(monto, configuracion);

                return Math.Round(monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2);
            }
            catch (Exception ex) { throw ex; }

        }

        public JsonResult GetBandejaOperacionAprobacion(DateTime? fecha, int? cliente, int? estado, int? op, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                var result = OperacionInversoresBussines.Instance.GetBandejaAprobacionByFilters(fecha, cliente, estado, op, skip, top);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = result.Result, count = result.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Aprobacion(string IDKey)
        {
            if (IDKey == null)
                return Redirect("BandejaAprobacion");
            try
            {
                var idOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
                var operacion = OperacionInversoresBussines.Instance.GetByConditions(op => op.IdOperacionInversor == idOp, op => op.CuentaCorrienteInversor, op => op.CuentaCorrienteInversor.Inversor).FirstOrDefault();
                if (operacion == null)
                    return Redirect("BandejaAprobacion");

                LoadCombo(true, false, (Estados.eEstadosOp)operacion.Estado);

                var resumen = OperacionInversoresBussines.Instance.GetSaldoInversor(operacion.CuentaCorrienteInversor.Inversor.IdInversor);

                var dto = new DtoOperacionAprobacion
                {
                    Id = operacion.IdOperacionInversor,
                    Fecha = operacion.Fecha,
                    Cliente = operacion.CuentaCorrienteInversor.Inversor.NameAndCuit,
                    Observacion = operacion.Observacion,
                    DetalleOperacion = null,
                    IdEstado = operacion.Estado,
                    Saldo = resumen.Saldo,
                    MontoRechazos = resumen.MontoRechazos,
                    CantidadRechazos = resumen.CantidadRechazos
                };
                ViewBag.IdOperacion = StringCipher.Encrypt(operacion.IdOperacionInversor.ToString(), CurrentContext.SessionContext.Key);
                return View(dto);
            }
            catch
            {
                return Redirect("BandejaAprobacion");
            }
        }

        public JsonResult GetCheques(string IDKey)
        {
            int id = 0;

            id = Convert.ToInt32(StringCipher.Decrypt(HttpUtility.UrlDecode(IDKey), CurrentContext.SessionContext.Key));

            try
            {
                var detalles = OperacionInversoresBussines.Instance.GetChequesByOperacion(id);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = detalles, count = detalles.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Operacion

        public ActionResult BandejaAprobacion()
        {
            //AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
            LoadCombo(false, true);
            return View(new DtoOperacionInversor());

        }

        #endregion
        #region Creacion
        public JsonResult GetBandejaOperacionOperador(DateTime? fecha, int? inversor, int? estado, int? op, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                var result = OperacionInversoresBussines.Instance.GetBandejaOperadorByFilters(fecha, inversor, estado, op, SessionContext.UsuarioActual, skip, top);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = result.Result, count = result.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Operacion
        public ActionResult BandejaOperador()
        {
            LoadCombo(false, true);
            return View(new DtoOperacionInversor());

        }

        [HttpPost]
        public JsonResult UploadFront(HttpPostedFileBase file)
        {
            var ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
            {
                var fileName = DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ext;

                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/UploadImages/Cheques/"), fileName);
                    file.SaveAs(path);
                }
                return Json(new { src = Url.Content("~/UploadImages/Cheques/") + fileName, success = true, image = fileName });
            }

            return Json(new { success = false });
        }

        [HttpPost]
        public JsonResult UploadBack(HttpPostedFileBase fileBack)
        {
            var ext = Path.GetExtension(fileBack.FileName);
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
            {
                var fileName = DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ext;

                if (fileBack != null && fileBack.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/UploadImages/Cheques/"), fileName);
                    fileBack.SaveAs(path);
                }
                return Json(new { src = Url.Content("~/UploadImages/Cheques/") + fileName, success = true, image = fileName });
            }

            return Json(new { success = false });
        }


        // GET: Operacion
        public ActionResult Crear()
        {
            LoadCombo(false);
            return View(new DtoOperacionInversor());

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult DeleteDetalleCreacion(string id)
        {
            if (Session["itemOperacion_session"] != null)
            {
                var detalle = Session["itemOperacion_session"] as List<DtoDetalleOperacion>;
                detalle.Remove(detalle.FirstOrDefault(det => det.UniqueId.ToString() == id));
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult EditDetalleCreacion(string id)
        {
            if (Session["itemOperacion_session"] != null)
            {
                var edit = (Session["itemOperacion_session"] as List<DtoDetalleOperacion>).FirstOrDefault(det => det.UniqueId.ToString() == id);
                return Json(new { success = true, result = edit }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetChequesCreacion()
        {
            try
            {
                var list = new List<dynamic>();
                if (Session["itemOperacion_session"] != null)
                {
                    var detalles = Session["itemOperacion_session"] as List<DtoDetalleOperacion>;
                    list.AddRange(detalles.Select(det =>
                        new
                        {
                            Id = det.UniqueId.ToString(),
                            Nro = det.Numero,
                            Banco = det.Banco,
                            Importe = det.Monto,
                            FechaVencimiento = det.FechaVencimiento,
                            ImporteApr = det.MontoCalculado,
                            Tasas = "",
                            Estado = det.Estado,
                            Cp = det.Cp,
                            CMC7 = det.CMC7
                        }));
                }

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = list.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private void LoadCombo(bool aprobador = false, bool bandeja = false, Estados.eEstadosOp estado = Estados.eEstadosOp.Pendiente)
        {
            ViewBag.Bancos = BancoBusiness.Instance.GetAll().OrderBy(banco => banco.Nombre).ToList();
            ViewBag.Estados = OperacionInversoresBussines.Instance.GetEstados(bandeja, aprobador, estado);

            ViewBag.Tasas = InversorBussines.Instance.GetByConditions<Tasa>(tasa => tasa.Activa == true)
                .OrderBy(order => order.Value)
                .GetSelectListItem("Id", "Value");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendCreacion(DtoOperacionInversor operacion)
        {
            if (IsValidCabecera(operacion))
            {
                long id = OperacionInversoresBussines.Instance.CreateOperacion(operacion, Session["itemOperacion_session"] as List<DtoDetalleOperacion>, CurrentContext.SessionContext.UsuarioActual, SessionContext.Configuracion);
                Session["itemOperacion_session"] = new List<DtoDetalleOperacion>();
                SendNotificationAprobadores(id, Estados.eEstadosOp.Pendiente);
                return Json(new { success = true, url = "bandejaoperador" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, msg = Resources.Resource.DatosObligatorios }, JsonRequestBehavior.AllowGet);
        }

        private void SendNotificationAprobadores(long id, Estados.eEstadosOp estado)
        {
            string ret = "La operación {1} ha cambiado de estado a {0}.";
            var msg = string.Format(ret, estado.GetEnumeracionName(), id);
            NotificationHub.SendToGroup(msg);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectResult Volver(string bandeja)
        {
            Session["itemOperacion_session"] = new List<DtoDetalleOperacion>();
            return Redirect("~/OperacionInversores/" + bandeja);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelOperacion(string IDKey)
        {
            if (IDKey == null)
                return Redirect("BandejaAprobacion");

            var idOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
            OperacionInversoresBussines.Instance.CancelOperacion(idOp);
            SendNotificationAprobadores(idOp, Estados.eEstadosOp.Cancelada);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectResult ClearCreacion()
        {
            Session["itemOperacion_session"] = new List<DtoDetalleOperacion>();
            return Redirect("~/OperacionInversores/Crear");
        }

        private bool IsValidCabecera(DtoOperacionInversor operacion)
        {
            if (operacion.IdInversor == 0)
            {
                return false;
            }

            if (Session["itemOperacion_session"] == null)
            {
                return false;
            }
            return true;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CalcularMontoCheque(DateTime? vencimiento, DateTime? fecha, string monto, string tasa, string gasto, int? estado, int? diasClearing, bool? calculateCl)
        {

            decimal? _monto = 0, _tasa = 0, _gasto = 0;

            if (monto != "")
                _monto = decimal.Parse(monto.Replace('.', ','));

            if (tasa != "")
                _tasa = decimal.Parse(tasa.Replace('.', ','));

            if (gasto != "")
                _gasto = decimal.Parse(gasto.Replace('.', ','));

            JsonResult calculo = null;

            if (vencimiento.HasValue && fecha.HasValue && _monto.HasValue && _tasa.HasValue && _gasto.HasValue)
                calculo = CalcularMontoConGasto(vencimiento.Value, fecha.Value, _tasa.Value, _monto.Value, _gasto.Value, estado.Value, diasClearing.Value, calculateCl, SessionContext.Configuracion);


            return calculo;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddDetalleOperacion(DtoOperacionInversor operacion)
        {
            try
            {
                RemoveModelStateOperacion(ModelState);
                if (ModelState.IsValid)
                {
                    if (Session["itemOperacion_session"] == null)
                    {
                        Session["itemOperacion_session"] = new List<DtoDetalleOperacion>();
                    }
                    var detalle = Session["itemOperacion_session"] as List<DtoDetalleOperacion>;

                    if (ChequeBusiness.Instance.GetByConditions(
                        c => c.IdBanco == operacion.DetalleOperacion.IdBanco
                        && c.Numero == operacion.DetalleOperacion.Numero
                        && c.NroCuenta == operacion.DetalleOperacion.NumeroCuenta
                        && c.Activo == true
                        ).Count > 0
                        || detalle.Count(p => p.IdBanco == operacion.DetalleOperacion.IdBanco && p.Numero == operacion.DetalleOperacion.Numero && operacion.DetalleOperacion.UniqueId != p.UniqueId) > 0)
                    {
                        throw new Exception("El cheque ya ha sido ingresado en el sistema");
                    }

                    if (!string.IsNullOrEmpty(operacion.DetalleOperacion.UniqueId))
                    {
                        var detRemove = detalle.FirstOrDefault(det => det.UniqueId == operacion.DetalleOperacion.UniqueId);
                        detalle.Remove(detRemove);
                    }
                    else
                    {
                        operacion.DetalleOperacion.UniqueId = Guid.NewGuid().ToString();
                    }

                    operacion.DetalleOperacion.Estado = GetEnumeracionName(Estados.eEstadosOpDetalle.Pendiente);
                    var tasa = OperacionBusiness.Instance.GetMaxTasa();
                    operacion.DetalleOperacion.MontoCalculado = CalcularMonto(operacion.DetalleOperacion.FechaVencimiento, DateTime.Now.Date, tasa, operacion.DetalleOperacion.Monto, SessionContext.Configuracion);
                    detalle.Add(operacion.DetalleOperacion);

                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GetEnumeracionName(Enum value)
        {
            return (value).GetType()
                          .GetMember(value.ToString())
                          .First()
                          .GetCustomAttribute<DisplayAttribute>().GetName();
        }

        private void RemoveModelStateOperacion(ModelStateDictionary modelState)
        {
            modelState.Remove("IdInversor");
            modelState.Remove("Fecha");
            modelState.Remove("Monto");
        }
        #endregion
        #region Comunes
        public ActionResult ViewOp(string IDKey, string Url)
        {
            //if (IDKey == null)
            //    return Redirect(Request.UrlReferrer.AbsoluteUri);

            //var idOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
            //var operacion = OperacionInversoresBussines.Instance.GetByConditions<OperacionInversor>(op => op.IdOperacionInversor == idOp, new List<string> { { "Inversor" } }).FirstOrDefault();
            //if (operacion != null)
            //{
            //    ViewBag.Url = Url;
            //    LoadCombo(false);
            //    var dto = operacion.GetDtoOperacionInv();
            //    var resumen = OperacionInversoresBussines.Instance.GetSaldoInversor(operacion.CuentaCorrienteInversor.Inversor.IdInversor);
            //    dto.Saldo = resumen.Saldo;
            //    dto.CantidadRechazos = resumen.CantidadRechazos;
            //    dto.MontoRechazos = resumen.MontoRechazos;
            //    return View(dto);
            //}
            //else
            //{
            //    return Redirect(Url);
            //}
            return null;
        }

        public JsonResult GetMotivosRechazo(string IDKey)
        {
            if (IDKey != null)
            {
                try
                {
                    var idDetOp = Convert.ToInt64(StringCipher.Decrypt(IDKey, CurrentContext.SessionContext.Key));
                    var detalles = OperacionInversoresBussines.Instance.GetMotivosRechazo(idDetOp);
                    return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = detalles }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { result = new { }, count = 0 }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult GetResumenCuenta(string key)
        //{

        //    if (key != null)
        //    {
        //        var tasa = OperacionInversoresBussines.Instance.GetTasaInversor(key);
        //        RecalcularMontos(tasa);

        //        return Json(
        //            new
        //            {
        //                success = true,
        //                tasa = tasa,
        //                datos = OperacionInversoresBussines.Instance.GetSaldoInversor(Convert.ToInt32(key))
        //            });
        //    }
        //    return Json(new { success = false, saldo = 0 });
        //}

        private void RecalcularMontos(decimal tasa)
        {
            if (Session["itemOperacion_session"] != null)
                foreach (var item in (Session["itemOperacion_session"] as List<DtoDetalleOperacion>))
                {
                    item.MontoCalculado = CalcularMonto(item.FechaVencimiento, DateTime.Now.Date, tasa, item.Monto, SessionContext.Configuracion);
                }
        }

        [HttpPost]
        public JsonResult CreateImagesScan(string front, string back)
        {
            try
            {
                var frontName = CreateImage(front, "front");
                var backName = CreateImage(back, "back");
                return
                  Json(new
                  {
                      success = true,
                      frontName = frontName,
                      frontSrc = Url.Content("~/UploadImages/Cheques/") + frontName,
                      backName = backName,
                      backSrc = Url.Content("~/UploadImages/Cheques/") + backName,
                  });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private string CreateImage(string imageBase64, string prefix)
        {
            try
            {
                if (!string.IsNullOrEmpty(imageBase64))
                {
                    var fileName = prefix + DateTime.Now.ToString("yyyy_MM_dd_HH_MM_ss") + ".jpg";
                    byte[] imageBytes = Convert.FromBase64String(imageBase64);
                    var path = Path.Combine(Server.MapPath("~/UploadImages/Cheques/"), fileName);
                    System.IO.File.WriteAllBytes(path, imageBytes);
                    return fileName;
                }
            }
            catch
            { }
            return "no-Imagen.png";

        }
        #endregion
    }
}