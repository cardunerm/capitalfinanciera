﻿using Common.Logging;
using Controllers;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Luma.Utils.Logs;
using MvcAppDCR.Utils;
using Business;

namespace MvcAppDCR.Controllers
{
    public class ResumenInversionController : BaseController
    {

        #region property

        public List<ResumenInversionModel> DetalleInversorList
        {
            get {
                if (Session["DetalleInversorList"] == null)
                    Session["DetalleInversorList"] = new List<ResumenInversionModel>();


                return (List<ResumenInversionModel>)Session["DetalleInversorList"];
            }
            set { Session["DetalleInversorList"] = value; }
        }

        #endregion property

        // GET: ResumenInversion
        public ActionResult Index()
        {           
            ViewBag.Inversores = GetInversores();
            ViewBag.TipoMoneda = WebHelper.GetTipoMoneda();
            return View();
        }

        public JsonResult GetResumenInversion(int IdTipoMoneda = 1, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                List<ResumenInversionModel> list = new List<ResumenInversionModel>();
                var resul = ProyeccionFinancieraBusiness.Instance.GetProyeccionResumen(IdTipoMoneda);
                var resulAgrupado = resul.ToList().GroupBy(p => p.Fecha);



                List<ResumenInversionModel> resultFinal = resul
                                                          .GroupBy(l => l.Fecha)
                                                          .Select(cl => new ResumenInversionModel
                                                          {
                                                              Fecha = cl.First().Fecha.ToShortDateString(),
                                                              Aporte = cl.Sum(p=>p.Ingreso).ToString(), 
                                                              Amortizacion = cl.Sum(p=>p.Egreso).ToString(),
                                                              Interes = cl.Sum(p=>p.Intereses).ToString(),
                                                              Tasa = (cl.Sum(p => p.Intereses) / ((cl.Sum(p=>p.Saldo) == 0)? 1 : cl.Sum(p=>p.Saldo))).ToString(),
                                                              Saldo = cl.Sum(p => p.Saldo).ToString()
                                                          }).ToList();

                int rowCount = resultFinal.Count();

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = resultFinal, count = rowCount }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {               
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
               
        public JsonResult GetDetalleInversor()
        {
            return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = DetalleInversorList}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDetalleInversorCalcular(int idTipomoneda, int IdInversor, string Fecha)
        {
            DetalleInversorList = new List<ResumenInversionModel>();

            try
            {
                var inversor = InversorBussines.Instance.GetEntity(long.Parse(IdInversor.ToString()));
                var cuentacorriente = inversor.CuentaCorrienteInversor.ToList().Single(p => p.TipoMoneda == idTipomoneda);

                var resul = ProyeccionFinancieraBusiness.Instance.GetProyeccion(idTipomoneda, IdInversor, cuentacorriente.IdCuentaCorrienteInversor);


                List<ResumenInversionModel> resultFinal = resul
                                                        .GroupBy(l => l.Fecha)
                                                        .Select(cl => new ResumenInversionModel
                                                        {
                                                            Fecha = cl.First().Fecha.ToShortDateString(),
                                                            Aporte = cl.Sum(p => p.Ingreso).ToString(),
                                                            Amortizacion = cl.Sum(p => p.Egreso).ToString(),
                                                            Interes = cl.Sum(p => p.Intereses).ToString(),
                                                            Tasa = (cl.Sum(p => p.Intereses) / ((cl.Sum(p => p.Saldo) == 0) ? 1 : cl.Sum(p => p.Saldo))).ToString(),
                                                            Saldo = cl.Sum(p => p.Saldo).ToString()
                                                        }).ToList();


                DetalleInversorList = resultFinal;


                return Json(new { status = Constant.SuccessMessages.MESSAGE_OK, Exist = true });
            }
            catch (Exception)
            {

                throw;
            }

           


        }


        public List<SelectListItem> GetInversores()
        {
            List<SelectListItem> _inversoresList = new List<SelectListItem>();            
            _inversoresList.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
            var result = InversorBussines.Instance.GetInversoresList().Where(p=>p.ClasificacionInversor == 1).ToList().OrderBy(p=>p.Nombre);


            foreach (var item in result)
            {
                _inversoresList.Add(new SelectListItem { Value = item.IdInversor.ToString(), Text = item.Nombre });
            }

            return _inversoresList;

        }
    }
}