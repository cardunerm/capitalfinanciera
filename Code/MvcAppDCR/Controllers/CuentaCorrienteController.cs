﻿using Business;
using CrystalDecisions.CrystalReports.Engine;
using CurrentContext;
using Domine;
using Enumerations;
using Mvc.Utils;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using MvcAppDCR.Utils;
using Resources;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Web.Mvc;
using System.Linq;
using Luma.Utils.Security;
using Domine.Dto;
using Luma.Utils;

namespace Controllers
{
    public class CuentaCorrienteController : BaseController
    {
        //
        // GET: /CuentaCorriente/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetCuentasCorrientes(string cliente = "", [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                int rowCount = 0;

                var list = CuentaCorrienteBusiness.Instance.GetCuentaCorrienteList(cliente);

                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = list, count = rowCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ABMCuentaCorriente(long? id)
        {
            ViewBag.EstadoCtaCte = DCREnumerationsHelper.GetEstadoCtaCte(insertIndistinct: false);

            if (!id.HasValue || id.Value <= 0)
            {
                return View(new CuentaCorriente());
            }

            var entity = CuentaCorrienteBusiness.Instance.GetEntity(id.Value);

            if (entity == null)
            {
                return HttpNotFound();
            }

            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ABMCuentaCorriente(CuentaCorriente cuentaCorriente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CuentaCorrienteBusiness.Instance.SaveOrUpdateCuentaCorriente(cuentaCorriente);

                    return Json(
                        new
                        {
                            msg = "Registro guardado correctamente",
                            status = Constant.SuccessMessages.MESSAGE_OK,
                            url = Url.Action("Index")
                        });
                }
                else
                {
                    return ExceptionManager.Instance.JsonError(ModelState);
                }
            }
            catch (Exception ex)
            {
                return ExceptionManager.Instance.JsonError(ex);
            }
        }

        public ActionResult ReporteCuentaCorriente(string fechaDesde, string fechaHasta, int idCuentaCorriente = 0)
        {
            try
            {
                DateTime? fecha_Desde = null;
                DateTime? fecha_Hasta = null;

                if (fechaDesde == null)
                {
                    fecha_Desde = DateTime.Now.AddMonths(-6);
                }
                else
                {
                    fecha_Desde = Convert.ToDateTime(fechaDesde);
                }

                if (fechaHasta == null)
                {
                    fecha_Hasta = DateTime.Now.AddDays(1);
                }
                else
                {
                    fecha_Hasta = Convert.ToDateTime(fechaHasta).AddDays(1);
                }

                decimal debito = 0.0M;
                decimal credito = 0.0M;
                decimal saldo = 0.0M;
                decimal saldoAnterior = 0.0M;

                OperacionCuentaCorriente opcuentaCorrienteBD = new OperacionCuentaCorriente();

                var cuentaCorriente = CuentaCorrienteBusiness.Instance.GetEntity(idCuentaCorriente);

                List<OperacionCuentaCorriente> opBD = OperacionBusiness.Instance.GetByConditions<OperacionCuentaCorriente>(op => op.Fecha >= fecha_Desde && op.Fecha <= fecha_Hasta && op.IdCuentaCorriente == cuentaCorriente.IdCuentaCorriente);

                debito = CuentaCorrienteBusiness.Instance.GetCtaCteSaldoDebito(fecha_Desde, fecha_Hasta, idCuentaCorriente);

                credito = CuentaCorrienteBusiness.Instance.GetCtaCteSaldoCredito(fecha_Desde, fecha_Hasta, idCuentaCorriente);

                saldo = credito - debito;

                saldoAnterior = CuentaCorrienteBusiness.Instance.GetSaldoByFecha(fecha_Desde, idCuentaCorriente);

                if (cuentaCorriente != null)
                {
                    var opcuentaCorrienteModel = new List<OperacionCuentaCorrienteModel>();

                    opcuentaCorrienteModel.Add
                                               (
                                                    new OperacionCuentaCorrienteModel
                                                    {
                                                        IdOperacionCuentaCorriente = 0,
                                                        IdCuentaCorriente = idCuentaCorriente,
                                                        TipoMovimiento = saldoAnterior < 0 ? 1 : 2,
                                                        Descripcion = "Saldo al " + fecha_Desde.Value.AddDays(-1).ToShortDateString(),
                                                        Fecha = fecha_Desde.Value.AddDays(-1),
                                                        Monto = saldoAnterior,
                                                        Estado = 1,
                                                        Debito = saldoAnterior < 0 ? saldoAnterior : 0,
                                                        Credito = saldoAnterior >= 0 ? saldoAnterior : 0,
                                                        Saldo = saldoAnterior
                                                    }
                                               );

                    foreach (var item in opBD)
                    {
                        opcuentaCorrienteBD = OperacionCuentaCorrienteBusiness.Instance.GetEntity(item.IdOperacionCuentaCorriente);

                          if (opcuentaCorrienteBD.TipoMovimiento == 1)
	                      {
	                      	 saldoAnterior = saldoAnterior - opcuentaCorrienteBD.Monto;
	                      }
                          else
	                      {
                             saldoAnterior = saldoAnterior + opcuentaCorrienteBD.Monto;
	                      }

                        opcuentaCorrienteModel.Add
                                                (                                              
                                                     new OperacionCuentaCorrienteModel
                                                     {
                                                        IdOperacionCuentaCorriente = opcuentaCorrienteBD.IdOperacionCuentaCorriente,
                                                        IdCuentaCorriente = opcuentaCorrienteBD.IdCuentaCorriente,
                                                        TipoMovimiento = opcuentaCorrienteBD.TipoMovimiento,
                                                        Descripcion = opcuentaCorrienteBD.Descripcion,
                                                        Fecha = opcuentaCorrienteBD.Fecha,
                                                        Monto = opcuentaCorrienteBD.Monto,
                                                        Estado = opcuentaCorrienteBD.Estado,
                                                        Debito = opcuentaCorrienteBD.TipoMovimiento == 1 ? opcuentaCorrienteBD.Monto : 0,
                                                        Credito = opcuentaCorrienteBD.TipoMovimiento == 2 ? opcuentaCorrienteBD.Monto : 0,
                                                        Saldo = saldoAnterior
                                                     }
                                                );
                        //opcuentaCorrienteModel.Add((OperacionCuentaCorrienteModel)WebHelper.GenericLoad(opcuentaCorrienteBD, new OperacionCuentaCorrienteModel()));
                    }

                    ReportClass reportClass = ReportHelper.LoadReport("CuentaCorrienteReport");

                    //reportClass.SetDataSource(opcuentaCorrienteModel.OrderByDescending(op => op.Fecha));
                    reportClass.SetDataSource(opcuentaCorrienteModel);
                    reportClass.SetParameterValue("nombreCliente", cuentaCorriente.Cliente.Nombre);
                    reportClass.SetParameterValue("numeroCtaCte", cuentaCorriente.IdCuentaCorriente.ToString());
                    reportClass.SetParameterValue("fechaDesde", fechaDesde);
                    reportClass.SetParameterValue("fechaHasta", fechaHasta);
                    reportClass.SetParameterValue("total", saldo);
                    reportClass.SetParameterValue("debito", debito);
                    reportClass.SetParameterValue("credito", credito);

                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                }
                else
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = Resource.CuentaCorrienteNoEncontrada, btnGoToLabel = "", redirectBtn = false });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult PagoCliente(PagoClienteModel model)
        {
            if (ModelState.IsValid)
            {
                string msg = "";
                if (ValidateInfo(model, out msg))
                {

                    try
                    {
                        int id = CuentaCorrienteBusiness.Instance.OperationClient(model.IdCliente, model.Importe, model.Tipo);

                        return Json
                          (
                            new
                            {
                                success = true,
                                msg = "Complete los datos obligatorios",
                                id = id
                            }
                          );
                    }
                    catch
                    {
                        return Json
                          (
                            new
                            {
                                success = false,
                                msg = Resource.ErrorOperacion
                            }
                          );
                    }
                }
                else
                {
                    return Json
                          (
                            new
                            {
                                success = false,
                                msg = msg
                            }
                          );
                }
            }

            return Json
                (
                  new
                  {
                      success = false,
                      msg = "Complete los datos obligatorios"
                  }
                );
        }

        public ActionResult PrintComprobante(string idOperacion)
        {
            var idOp = Convert.ToInt64(idOperacion);
            if (idOperacion != null)
            {
                var operacion = OperacionCuentaCorrienteBusiness.Instance.GetEntity(idOp);

                var model = new DtoPrintOperacionCtaCte
                {
                    Empresa = SessionContext.Empresa,
                    OperacionCtaCte = operacion,
                    MontoLetra = new UtilConverNumber().Convertir(operacion.Monto.ToString(), false, null)
                };
                return View(model);
            }
            return HttpNotFound();
        }

        private bool ValidateInfo(PagoClienteModel model, out string msg)
        {
            if (model.Tipo == 1)
            {
                var resumen = OperacionBusiness.Instance.GetSaldoCliente(model.IdCliente);
                if (resumen == null)
                {
                    msg = "El cliente no tiene cuenta corriente";
                    return false;
                }

                if (resumen.Saldo < model.Importe)
                {
                    msg = "El importe es mayor al saldo del cliente";
                    return false;
                }

                Caja caja = CajaBusiness.Instance.GetUltimaCaja();
                if (caja.Total < model.Importe)
                {
                    msg = "El monto del egreso es superior al saldo de caja";
                    return false;
                }
            }

            msg = "";
            return true;
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult Saldo(int idCliente)
        {
            try
            {
                var resumen = OperacionBusiness.Instance.GetSaldoCliente(idCliente);

                return Json
                  (
                    new
                    {
                        success = true,
                        importe = resumen == null ? 0 : resumen.Saldo
                    }
                  );
            }
            catch
            {
                return Json
                  (
                    new
                    {
                        success = false,
                        msg = Resource.ErrorOperacion
                    }
                  );
            }
        }
    }
}