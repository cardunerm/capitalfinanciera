﻿using Business;
using Controllers;
using Domine;
using CrystalDecisions.CrystalReports.Engine;
using CurrentContext;
using Enumerations;
using Mvc.Utils;
using MvcAppDCR.Code;
using MvcAppDCR.Model.Entities;
using MvcAppDCR.Utils;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MvcAppDCR.Controllers
{
    public class ArqueoController : BaseController
    {
        public ActionResult Arqueo()
        {
            return View();
        }

        public ActionResult CreateArqueo()
        {
            return View(new Arqueo());

        }

        public ActionResult EditArqueo(int idArqueo)
        {
            Arqueo arqueoModel = ArqueoBusiness.Instance.GetByConditions(a => a.IdArqueo == idArqueo).FirstOrDefault();
            ViewBag.EstadoCheques = DCREnumerationsHelper.GetEstadoCheques(insertIndistinct: false);

            return View(arqueoModel);
        }

        public JsonResult GetArqueos(string fecha, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                DateTime? _fecha = null;

                if (fecha != null)
                {
                    _fecha = Convert.ToDateTime(fecha);
                }

                var result = ArqueoBusiness.Instance.GetArqueosByFilters(_fecha, CurrentContext.SessionContext.UsuarioActual, skip, top);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = result, count = result.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateArqueo(string cheques, Arqueo arqueoModel)
        {
            try
            {
                Cheque chequeBD = new Cheque() {Activo = true };

                var listCheques = new JavaScriptSerializer().Deserialize<List<Cheque>>(cheques);

                if (listCheques.Count < 1)
                {
                    throw new Exception("Se debe seleccionar al menos un cheque");
                }

                foreach (var item in listCheques)
                {
                    chequeBD = ChequeBusiness.Instance.GetCheque(item.IdCheque);

                    item.Monto = chequeBD.Monto;
                }

                int a = ArqueoBusiness.Instance.CreateArqueo(listCheques, arqueoModel, CurrentContext.SessionContext.UsuarioActual);

                return Json(new { success = true, url = "Arqueo/Arqueo" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditArqueo(string cheques, string ddlEstados, Arqueo arqueoModel)
        {
            try
            {
                Cheque chequeBD = new Cheque() { Activo = true };

                var listCheques = new JavaScriptSerializer().Deserialize<List<Cheque>>(cheques);
                if (listCheques == null)
                {
                    throw new Exception("Se debe seleccionar al menos un cheque");
                }

                if (listCheques.Count < 1 )
                {
                    throw new Exception("Se debe seleccionar al menos un cheque");
                }

                foreach (var item in listCheques)
                {
                    chequeBD = ChequeBusiness.Instance.GetCheque(item.IdCheque);
                    chequeBD.IdEstado = Convert.ToInt32(ddlEstados);

                    ChequeBusiness.Instance.SaveOrUpdate(chequeBD);
                }

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetChequesArqueo(int idArqueo, [Bind(Prefix = "$top")]int top = 10, [Bind(Prefix = "$skip")]int skip = 0)
        {
            try
            {
                var result = ArqueoBusiness.Instance.GetChequesArqueo(idArqueo, skip, top);
                return this.JsonSerialize(new { status = Constant.SuccessMessages.MESSAGE_OK, result = result, count = result.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { result = new { }, count = 0, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReporteArqueo(int idArqueo = 0)
        {
            try
            {
                Cheque cheque = new Cheque() { Activo = true };

                var arqueo = ArqueoBusiness.Instance.GetEntity(idArqueo);

                if (arqueo != null)
                {
                    var chequeModel = new List<ChequeModel>();

                    foreach (var item in arqueo.ArqueoDetalle)
                    {
                        cheque = ChequeBusiness.Instance.GetCheque(item.IdCheque);

                        chequeModel.Add((ChequeModel)WebHelper.GenericLoad(cheque, new ChequeModel()));
        }

                    ReportClass reportClass = ReportHelper.LoadReport("ArqueoReport");

                    reportClass.SetDataSource(chequeModel);
                    reportClass.SetParameterValue("nombreUsuario", SessionContext.UsuarioActual.Nombre);
                    reportClass.SetParameterValue("nombreEmpresa", "Empresa");
                    reportClass.SetParameterValue("nombreSucursal", "Sucursal");
                    reportClass.SetParameterValue("numeroArqueo", arqueo.IdArqueo.ToString());
                    reportClass.SetParameterValue("fechaArqueo", arqueo.Fecha);
                    reportClass.SetParameterValue("montoArqueo", arqueo.Monto);
                    reportClass.SetParameterValue("estadoArqueo", arqueo.Estado);
                    reportClass.SetParameterValue("descripcionArqueo", arqueo.Descripcion);

                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                }
                else
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = Resource.ArqueoNoEncontrado, btnGoToLabel = "", redirectBtn = false });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }
    }
}