﻿using Business;
using System.Linq;
using Domine;
using MvcAppDCR.Code;
using MvcAppDCR.Utils;
using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data.Entity;
using Luma.Utils.Logs;
using Domine.Dto;
using Enumerations;
using CurrentContext;
using CrystalDecisions.CrystalReports.Engine;
using MvcAppDCR.Model.Entities;
using Mvc.Utils;
using System.Net.Mime;
using Resources;
using Luma.Utils;

namespace Controllers
{
    public class DepositoEfectivoController : BaseController
    {
        public ActionResult NuevoDepositoEfectivo(long? id, bool? popUp)
        {
            LoadCombo();
            ViewBag.PopUp = (popUp ?? false);
            DtoOperacionInversor dtoOperacion = new DtoOperacionInversor();
            dtoOperacion.Fecha = DateTime.Now;
            return View(dtoOperacion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NuevoDepositoEfectivo(DtoOperacionInversor dtoOperacion, bool? popUp)
        {
            LoadCombo();
            ViewBag.PopUp = (popUp ?? false);

            DtoReporteEfectivo dtoReporte = new DtoReporteEfectivo();

            if (dtoOperacion.Monto<=0)
            {
                return Json(new
                {
                    msg = "El monto no puede ser 0 o menor"
                }, JsonRequestBehavior.AllowGet);
            }

            var success = OperacionInversoresBussines.Instance.NuevoDepositoEfectivo(dtoOperacion, SessionContext.UsuarioActual, ref dtoReporte);

            if (success)
            {
                return Json(new
                {
                    success = success,
                    data = dtoReporte,
                    msg = Resources.Resource.OperacionCorrecta,
                    url = "BandejaOperaciones"
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = success,
                    msg = Resources.Resource.ErrorOperacion
                }, JsonRequestBehavior.AllowGet);
            }
                       
        }

        [HttpGet]
        public JsonResult GetCuentasCorrientesByIdInversor(int idInversor)
        {
            try
            {

                List<CuentaCorrienteInversor> cuentas = CuentaCorrienteInversorBussines.Instance.GetByConditions(c => c.IdInversor == idInversor);
                List<DtoCuentaCorrienteInversor> dto = new List<DtoCuentaCorrienteInversor>();
                foreach (var cuenta in cuentas)
                {
                    decimal saldo = ResumenDiarioOperacionInversorBusiness.Instance.GetSaldoByIdCuenta(cuenta.IdCuentaCorrienteInversor, cuenta.TipoMoneda);
                    dto.Add(new DtoCuentaCorrienteInversor
                    {
                        Id = cuenta.IdCuentaCorrienteInversor,
                        NumeroCuenta = ((cuenta.TipoMoneda == 1) ? ("001") : ("002")) + cuenta.IdCuentaCorrienteInversor.ToString("-0000000"),//  cuenta.IdCuentaCorrienteInversor.ToString("000-0000000"),
                        TipoMoneda = cuenta.TipoMoneda,
                        Saldo = saldo
                    }); ;
                }

                return Json(new
                {
                    success = true,
                    data = dto
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);

                return Json(new
                {
                    success = false,
                    msg = Resources.Resource.ErrorOperacion
                }, JsonRequestBehavior.AllowGet);
            }
        }
        
        public ActionResult GenerarReporte(string paramDtoReporteDeposito)//DtoReporteDepositoEfectivo dtoReporteDeposito)
        {
            DtoReporteEfectivo dtoReporteDeposito = Newtonsoft.Json.JsonConvert.DeserializeObject<DtoReporteEfectivo>(paramDtoReporteDeposito);
            try
            {
                UtilConverNumber util = new UtilConverNumber();
                string _montoLetter = util.Convertir(dtoReporteDeposito.Monto.ToString(), true,1);

                if (dtoReporteDeposito != null)
                {         

                    ReportClass reportClass = ReportHelper.LoadReport("DepositoEfectivoReport");
                    
                    reportClass.SetParameterValue("nombreUsuario", dtoReporteDeposito.NombreUsuario);
                    reportClass.SetParameterValue("fecha", dtoReporteDeposito.Fecha);
                    reportClass.SetParameterValue("nombreInversor", dtoReporteDeposito.NombreInversor);
                    reportClass.SetParameterValue("numeroCUIT", dtoReporteDeposito.CuitInversor);
                    reportClass.SetParameterValue("monto", dtoReporteDeposito.Monto);
                    reportClass.SetParameterValue("montoStr", _montoLetter);
                    reportClass.SetParameterValue("numeroCuentaCorriente", dtoReporteDeposito.CuentaCorrienteStr);
                    reportClass.SetParameterValue("numeroOperacion", dtoReporteDeposito.NumeroOperacion);
                    reportClass.SetParameterValue("tipoMoneda", dtoReporteDeposito.TipoMoneda);


                    return File(ReportHelper.ShowReport(reportClass, ReportFormat.PDF), MediaTypeNames.Application.Pdf);
                }
                else
                {
                    return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = "Operacion no encontrada", btnGoToLabel = "", redirectBtn = false });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Access", new { errorType = Resource.Error, message = ex.Message, btnGoToLabel = "", redirectBtn = false });
            }
        }

        private void LoadCombo()
        {
            ViewBag.ClasificacionInversor = DCREnumerationsHelper.GetClasificacionInversorTypes();
        }

    }
}