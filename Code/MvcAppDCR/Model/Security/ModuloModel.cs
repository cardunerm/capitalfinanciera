﻿
namespace MvcAppDCR.Model.Security
{
    public class ModuloModel
    {
        public long IdModulo { get; set; }
        public bool Selected { get; set; }
        public string Nombre { get; set; }
        public bool ViewSelected { get; set; }
        public bool CreateSelected { get; set; }
        public bool EditSelected { get; set; }
        public bool DeleteSelected { get; set; }
    }
}