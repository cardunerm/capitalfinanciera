﻿
namespace MvcAppDCR.Model.Security
{
    public class AuditoriaModel
    {
        public long IdAuditoria { get; set; }
        public string NombreUsuario { get; set; }
        public string IPUsuario { get; set; }
        public string Accion { get; set; }
        public string ModuloNombre { get; set; }
        public string Descripcion { get; set; }
        public string Date { get; set; }
    }
}