﻿
namespace MvcAppDCR.Model
{
    public class PerfilModel
    {
        public long IdPerfil { get; set; }
        public bool Selected { get; set; }
        public string Nombre { get; set; }
    }
}