﻿using Resources;
using System.ComponentModel.DataAnnotations;

namespace MvcAppDCR.Model.Security
{
    public class UsuarioLogin
    {
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "NombreUsuario")]
        public string NombreUsuario { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Contrasenia")]
        public string Contrasenia { get; set; }

        public bool RememberMe { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public int IdEmpresa { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public int IdSucursal { get; set; }

    }
}