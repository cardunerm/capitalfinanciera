﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppDCR.Model.Security
{
    public class SucursalModel
    {
        public long IdSucursal{ get; set; }
		public bool Selected { get; set; }
		public string EmpresaNombre { get; set; }
		public string Nombre { get; set; }
    }
}