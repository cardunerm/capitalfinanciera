﻿
namespace MvcAppDCR.Model.Security
{
    public class ValidacionModel
    {
        public int Validacion { get; set; }
        public bool Selected { get; set; }
        public string Nombre { get; set; }
    }
}