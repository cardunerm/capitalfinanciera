﻿using System.Collections.Generic;
using Domine;

namespace MvcAppDCR.Model
{
    public class UsuarioModel
    {
        public UsuarioModel()
        {
            PerfilMod = new List<PerfilModel>();
            UsuarioMod = new Usuario();
        }

        public List<PerfilModel> PerfilMod { get; set; }
        public Usuario UsuarioMod { get; set; }
    }
}