﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppDCR.Model.Entities
{
    public class FacturaOperacionModel
    {
        public int IdFacturaOperacion { get; set; }

        public string NumeroFactura { get; set; }

        public string Cliente { get; set; }

        public string Deudo { get; set; }

        public string FechaFactura { get; set; }

        public string FechaCobro { get; set; }

        public string Monto { get; set; }

        public string DesembolsoTotal { get; set; }

        public string DesembolsoSaldo { get; set; }

        public string CobroTotal { get; set; }

        public string CobroSaldo { get; set; }

        public int IdFacturaEstado { get; set; }
    }
}