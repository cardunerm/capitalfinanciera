﻿using Resources;
using System.ComponentModel.DataAnnotations;

namespace MvcAppDCR.Model.Entities
{
    public class PagoClienteModel
    {
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Cliente")]
        public int IdCliente { get; set; }

        public int IdInversor { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Monto")]        
        public decimal Importe { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(Description ="Tipo")]
        public int Tipo
        {
            get; set;
        }
    }
}