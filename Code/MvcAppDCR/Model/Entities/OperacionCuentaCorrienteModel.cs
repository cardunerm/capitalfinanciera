﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utils.Enumeraciones;

namespace MvcAppDCR.Model.Entities
{
    public class OperacionCuentaCorrienteModel
    {
        public int IdOperacionCuentaCorriente { get; set; }
        public int IdCuentaCorriente { get; set; }
        public int TipoMovimiento { get; set; }
        public string TipoMovimientoStr { set { } get { return ((Estados.mTipoMovimientoCuentaCorriente)TipoMovimiento).GetEnumeracionName(); } }
        public string Descripcion { get; set; }
        public System.DateTime Fecha { get; set; }
        public decimal Monto { get; set; }
        public int Estado { get; set; }
        public decimal Debito { get; set; }
        public decimal Credito { get; set; }
        public decimal Saldo { get; set; }
    }
}