﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppDCR.Model.Entities
{
    public class NegociacionDetalleModel
    {

        public int Id { get; set; }
        public string NombreBanco { get; set; }
        public string Numero { get; set; }
        public decimal Monto { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string Estado { get; set; }

    }
}