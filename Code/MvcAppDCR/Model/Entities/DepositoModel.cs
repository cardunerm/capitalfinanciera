﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppDCR.Model.Entities
{
    public class DepositoModel
    {
        public int IdOperacionInversor { get; set; }
        public Nullable<long> IdUsuario { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Observacion { get; set; }
        public int Estado { get; set; }
        public Nullable<int> IdEmpresa { get; set; }
        public decimal Monto { get; set; }
        public Nullable<int> IdSucursal { get; set; }
        public Nullable<int> IdCuentaCorrienteInversor { get; set; }
        public Nullable<int> TipoMovimiento { get; set; }
        public Nullable<int> IdHistorialTasaInteresInversorAplicado { get; set; }
        public int Condicion { get; set; }
        public int ClasificacionMovimiento { get; set; }
        public Nullable<int> IdResumen { get; set; }

    }
}