﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppDCR.Model.Entities
{
    public class FacturaOperacionAceptadaModel
    {
        public string Fecha { get; set; }

        public string Movimiento { get; set; }

        public string Monto { get; set; }

        public string Saldo { get; set; }

        public string Tasa { get; set; }
    }
}