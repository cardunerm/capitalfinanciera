﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppDCR.Model.Entities
{
    public class ChequeModel
    {
        public int IdCheque { get; set; }
        public int IdBanco { get; set; }
        public int IdRecepcionCheque { get; set; }        
        public string Numero { get; set; }
        public string TitularCuenta { get; set; }
        public string NroCuenta { get; set; }
        public string SucursalEmision { get; set; }
        public System.DateTime FechaVencimiento { get; set; }
        public decimal Monto { get; set; }
        public int DiasClearing { get; set; }
        public int Estado { get; set; }
        public int Cp { get; set; }
        public string BancoStr { get; set; }
        public string EstadoStr { get; set; }
        
    }
}