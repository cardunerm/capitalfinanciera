﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppDCR.Model.Entities
{
    public class FacturaOperacionComprobanteModel
    {

        public string numerooperacion { get; set; }
        public string cliente { get; set; }
        public string fechafactura { get; set; }
        public string fechacobro { get; set; }
        public string deudor { get; set; }
        public string numerofactura { get; set; }
        public string montofactura { get; set; }
        public string retencion { get; set; }
        public string interesygasto { get; set; }
        public string totalaforo { get; set; }
        public string montoadesembolsar { get; set; }
        public string intereses { get; set; }
        public string gastos { get; set; }
        public string iva { get; set; }

    }


}