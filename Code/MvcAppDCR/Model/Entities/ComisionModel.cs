﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppDCR.Model.Entities
{
    public class ComisionModel
    {

        public string Id { get; set; }

        public string Vendedor { get; set; }
        public string Operacion { get; set; }
        public string Cliente { get; set; }
        public string FechaCobro { get; set; }

        public string FechaCreacion { get; set; }
        public string FechaDesde { get; set; }
        public string FechaHasta { get; set; }
        public string MontoCobrado { get; set; }
        public string MontoComisionado { get; set; }

        public string Cobrado { get; set; }
        public string Comision { get; set; }

        public int IdFacturaVendedor { get; set; }
        public int IdFactura { get; set; }
        public int IdFacturaCobro { get; set; }
        public int IdOperacion { get; set; }

        public string TipoComision { get; set; }



    }
}