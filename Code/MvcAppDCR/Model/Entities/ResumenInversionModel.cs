﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAppDCR.Model.Entities
{
    public class ResumenInversionModel
    {
        public string Fecha { get; set; }
        public string Aporte { get; set; }
        public string Amortizacion { get; set; }
        public string Interes { get; set; }
        public string Tasa { get; set; }        
        public string Saldo { get; set; }

    }
}