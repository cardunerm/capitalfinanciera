﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utils.Enumeraciones;

namespace MvcAppDCR.Model.Entities
{
    public class OperacionCtaCteInversorModel
    {
        public int IdOperacionCtaCteInversor { get; set; }
        public int IdCuentaCorrienteInversor { get; set; }
        public int TipoMovimiento { get; set; }
        public string TipoMovimientoStr { set { } get { return ((Estados.mTipoMovimientoCuentaCorriente)TipoMovimiento).GetEnumeracionName(); } }
        public string Descripcion { get; set; }
        public System.DateTime Fecha { get; set; }
        public decimal? Monto { get; set; }
        public int Estado { get; set; }
        public decimal? Debito { get; set; }
        public decimal? Credito { get; set; }
        public decimal? Saldo { get; set; }
    }
}