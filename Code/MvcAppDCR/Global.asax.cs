﻿using Business;
using MvcAppDCR.App_Start;
using Quartz;
using Quartz.Impl;
using System.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MvcAppDCR
{
    // Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
    // visite http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutomapperConfiguration.StartConfiguration();

            //InitQuartzScheduler();

            Luma.Utils.Logs.LogManager.Log.RegisterInfo("Loge iniciado");
        }

        private void InitQuartzScheduler()
        {
            // construct a scheduler factory
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // get a scheduler
            IScheduler sched = schedFact.GetScheduler();
            sched.Start();

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<JobProcesoResumenDiario>()
                .WithIdentity("JobProcesoResumenDiario", "FinancieraGroup")
                .Build();

            // Trigger the job to run now, then every cron scheduler indicate
            ITrigger trigger = TriggerBuilder.Create()
              .WithIdentity("TriggerResumenDiario", "FinancieraGroup")
              .StartNow()
              .WithCronSchedule(ConfigurationManager.AppSettings["CronSchedulerResumenDiario"])
              .Build();

            sched.ScheduleJob(job, trigger);
        }
    }
}