﻿using log4net;
using Enumerations;
using System;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using MvcAppDCR.Code;

namespace MvcAppDCR.Utils
{
    public class ExceptionManager : Controller
    {
        #region Singleton

        private static readonly ExceptionManager instance = new ExceptionManager();

        public static ExceptionManager Instance { get { return instance; } }

        private ExceptionManager() { }

        static ExceptionManager() { }

        #endregion

        protected static readonly ILog log = LogManager.GetLogger("LogFileAppender");

        public void RegisterLog(string className, string method, string logMessage, Exception ex, string displayMessage, LogSeverity logSeverity)
        {
            string enabled = ConfigurationManager.AppSettings["LogEnabled"];

            if (bool.Parse(enabled))
            {
                string logInnerException = (ex != null && ex.InnerException != null) ? ex.InnerException.ToString() : "";

                switch (logSeverity)
                {
                    case LogSeverity.Info:
                        log.Info("Clase: " + className + ". Método: " + method + "." + System.Environment.NewLine + logMessage + "\n" + logInnerException);
                        break;
                    case LogSeverity.Warning:
                        log.Warn("Clase: " + className + ". Método: " + method + "." + System.Environment.NewLine + logMessage + "\n" + logInnerException);
                        break;
                    case LogSeverity.Error:
                        log.Error("Clase: " + className + ". Método: " + method + "." + System.Environment.NewLine + logMessage + "\n" + logInnerException);
                        //Response.Redirect(Url.Action("Error", "Access", new { errorType = Resource.ErrorMessage, message = displayMessage, url = Url.Action("Index", "Home"), Resource.MainLower }), true);
                        return;
                    case LogSeverity.Fatal:
                        log.Fatal("Clase: " + className + ". Método: " + method + "." + System.Environment.NewLine + logMessage + "\n" + logInnerException);
                        //Response.Redirect(Url.Action("Error", "Access", new { errorType = Resource.FatalError, message = displayMessage, url = Url.Action("Index", "Home"), Resource.MainLower }), true);
                        return;
                    default:
                        break;
                }
            }
        }

        public JsonResult ReturnError(string controllerName, string methodName, Exception ex)
        {
            LogError(controllerName, methodName, ex);

            return Json(new { status = Constant.SuccessMessages.MESSAGE_ERROR, msg = (ex.InnerException != null ? ex.InnerException.Message : ex.Message) }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReturnError(string controllerName, string methodName, string message)
        {
            RegisterLog(controllerName, methodName, message, null, message, LogSeverity.Warning);

            return Json(new { status = Constant.SuccessMessages.MESSAGE_ERROR, msg = message }, JsonRequestBehavior.AllowGet);
        }

        public void LogError(string controllerName, string methodName, Exception ex)
        {
            if (ex != null)
            {
                RegisterLog(controllerName, methodName, ex.Message, ex, "", LogSeverity.Warning);
            }
        }

        internal JsonResult JsonError(string msg, string error_var = null)
        {
            return Json(
                new
                {
                    msg = msg,
                    status = Constant.SuccessMessages.MESSAGE_ERROR,
                    error = "#" + (error_var == null ? Constant.SuccessMessages.ERROR_VAR : error_var)
                }, JsonRequestBehavior.AllowGet);
        }

        internal JsonResult JsonError(Exception ex, string error_var = null)
        {
            if (ex.GetType().Name.Equals("DbEntityValidationException"))
            {
                StringBuilder strBuilder = new StringBuilder();

                foreach (var item in ((DbEntityValidationException)ex).EntityValidationErrors)
                {
                    foreach (var item2 in item.ValidationErrors)
                    {
                        strBuilder.Append(item2.ErrorMessage + "<br />");
                    }
                }

                return JsonError(strBuilder.ToString(), error_var);
            }
            else
            {
                return JsonError(ex.Message, error_var);
            }
        }

        internal JsonResult JsonError(ModelStateDictionary modelState, string error_var = null)
        {
            StringBuilder strBuilder = new StringBuilder();

            var errors = modelState.Where(p => p.Value.Errors.Any());

            foreach (var item in errors)
            {
                strBuilder.Append(item.Value.Errors[0].ErrorMessage + "<br />");
            }

            return JsonError(strBuilder.ToString(), error_var);
        }
    }
}