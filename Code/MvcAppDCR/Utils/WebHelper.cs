﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;

namespace MvcAppDCR.Utils
{
    public class WebHelper
    {
        public static object GenericLoad(object original, object filled)
        {
            // get all the fields in the class
            PropertyInfo[] fieldsOfClass = original.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            PropertyInfo[] fieldsOfToClass = filled.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            // copy each value over to 'this'
            foreach (PropertyInfo fi in fieldsOfClass)
            {
                PropertyInfo destination = fieldsOfToClass.SingleOrDefault(f => f.Name == fi.Name);

                try
                {
                    if (destination != null)
                    {
                        object a = fi.GetValue(original, null);
                        destination.SetValue(filled, a, null);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return filled;
        }

        public static List<SelectListItem> GetSelectListItem<T>(IEnumerable<T> list, string valueProperty, string textProperty, bool insertIndistinct = false, bool insertNone = false, string selected = null)
        {
            try
            {
                var selectList = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    selectList.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }
                else if (insertNone)
                {
                    selectList.Add(new SelectListItem { Value = "-1", Text = "Ninguno" });
                }

                Type type = typeof(T);
                PropertyInfo propertyValue = type.GetProperty(valueProperty);
                PropertyInfo propertyText = type.GetProperty(textProperty);

                foreach (var item in list)
                {
                    selectList.Add(new SelectListItem
                    {
                        Value = propertyValue.GetValue(item, null).ToString(),
                        Text = propertyText.GetValue(item, null).ToString(),
                        Selected = (!string.IsNullOrEmpty(selected) ? (propertyValue.GetValue(item, null).ToString().Equals(selected)) : false)
                    });
                }

                return selectList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetSelectListItem(object entity, string valueProperty, string textProperty)
        {
            try
            {
                var selectList = new List<SelectListItem>();

                Type type = entity.GetType();
                PropertyInfo propertyValue = type.GetProperty(valueProperty);
                PropertyInfo propertyText = type.GetProperty(textProperty);

                selectList.Add(new SelectListItem
                {
                    Value = propertyValue.GetValue(entity, null).ToString(),
                    Text = propertyText.GetValue(entity, null).ToString(),
                });

                return selectList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetSelectListItem(IEnumerable<string[]> list, bool insertIndistinct = false, bool insertNone = false, string selected = null)
        {
            try
            {
                var selectList = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    selectList.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }
                else if (insertNone)
                {
                    selectList.Add(new SelectListItem { Value = "-1", Text = "Ninguno" });
                }

                foreach (var item in list)
                {
                    selectList.Add(new SelectListItem
                    {
                        Value = item[0],
                        Text = item[1],
                        Selected = (!string.IsNullOrEmpty(selected) ? (item[0].Equals(selected)) : false)
                    });
                }

                return selectList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetIndistinctItemList()
        {
            try
            {
                return new List<SelectListItem>() { new SelectListItem { Value = "-1", Text = "Indistinto" } };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetEmptyList()
        {
            try
            {
                return new List<SelectListItem>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetNoneList()
        {
            try
            {
                return new List<SelectListItem>() { new SelectListItem { Value = "-1", Text = "Ninguno" } };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string RenderViewToString(ControllerContext context, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = context.RouteData.GetRequiredString("action");

            ViewDataDictionary viewData = new ViewDataDictionary(model);

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                ViewContext viewContext = new ViewContext(context, viewResult.View, viewData, new TempDataDictionary(), sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public static List<SelectListItem> GetTipoMoneda()
        {
            List<SelectListItem> _tipoMoneda = new List<SelectListItem>();
            _tipoMoneda.Add(new SelectListItem { Value = "1", Text = "Pesos" });
            _tipoMoneda.Add(new SelectListItem { Value = "0", Text = "Dolares" });

            return _tipoMoneda;

        }
    }
}