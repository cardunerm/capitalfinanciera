﻿using AutoMapper;
using Domine;
using Domine.Dto;
using Enumerations;

namespace MvcAppDCR.App_Start
{
    public class AutomapperConfiguration
    {
        public static void StartConfiguration()
        {
            Mapper.Initialize(
                cfg =>
                    {
                        cfg.CreateMap<ResumenDiarioOperacionInversor, DtoProyeccionFinanciera>()
                        .ForMember(destination => destination.Ingreso, op => op.MapFrom(source => source.TotalIngresos))
                        .ForMember(destination => destination.Egreso, op => op.MapFrom(source => source.TotalEgresos)).ReverseMap();

                        cfg.CreateMap<Cheque, DtoCheque>().ReverseMap();

                    }
                );
        }
    }
}