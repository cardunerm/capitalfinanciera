﻿using AutoMapper;
using Domine;
using MvcAppDCR.Model;


namespace MvcAppDCR.App_Start
{
    public class AutomapperConfig
    {

        public static void RegisterMapper()
        {

            Mapper.Initialize(
               cfg =>
               {
                #regionInversores
                cfg.CreateMap<Domine.Dto.DtoInversores, Inversores>().ReverseMap();
                #endregion



               }
                );

        }

    }
}
