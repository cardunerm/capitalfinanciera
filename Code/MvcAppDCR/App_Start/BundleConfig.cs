﻿using MvcAppDCR.Code;
using System.Web;
using System.Web.Optimization;

namespace MvcAppDCR
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Scripts
            //All jquery
            bundles.Add(
                new ScriptBundle(Constant.Bundle.Script_JQueryAll)
                .Include("~/Scripts/JQuery/jquery-{version}.js")
                .Include("~/Scripts/JQuery/jquery.unobtrusive-ajax.min.js")
                .Include("~/Scripts/JQuery/jquery.validate.min.js")
                .Include("~/Scripts/JQuery/jquery.validate.unobtrusive.bootstrap.min.js")
                .Include("~/Scripts/JQuery/jquery.validate.unobtrusive.min.js")
                .Include("~/Scripts/JQuery/jquery.form.min.js")
                .Include("~/Scripts/JQuery/jquery.maskMoney.min.js")
                .Include("~/Scripts/JQuery/jquery-confirm.js")
//       .Include("~/Scripts/jquery.uploadify.min.js")

//  .IncludeDirectory("~/Scripts/JQuery/", "*.js")
#if DEBUG
                //Solo lo agregamos en modo debug
                .IncludeDirectory("~/Scripts/Intellisense/", "*.js")
#endif
);

            bundles.Add(
              new ScriptBundle(Constant.Bundle.Script_DateTimeFormat)
              .Include("~/Scripts/JQuery/jquery.date.format.js"));

            //Only jquery
            bundles.Add(
                  new ScriptBundle(Constant.Bundle.Script_JQuery)
                  .Include("~/Scripts/JQuery/jquery-{version}.js")
                 );

            //Only jquery
            bundles.Add(
                new ScriptBundle(Constant.Bundle.Script_Bootstrap)
                .Include("~/Scripts/Bootstrap/bootstrap.js")
                .Include("~/Scripts/Bootstrap/moment.js")
                .Include("~/Scripts/Bootstrap/bootstrap-datepicker.js")
                .Include("~/Scripts/Bootstrap/bootstrap-timepicker.min.js")
               );

            //All template
            bundles.Add(
                new ScriptBundle(Constant.Bundle.Script_TemplateAll)
                .Include("~/Scripts/Template/*.js")
               );

            //All template

            bundles.Add(
              new ScriptBundle(Constant.Bundle.Script_UploadFile)
              .Include("~/Scripts/Site/Adjuntos/uploadFile.js"));

            bundles.Add(
                new ScriptBundle(Constant.Bundle.Script_Site)
                .Include("~/Scripts/Site/*.js"));

            bundles.Add(
               new ScriptBundle(Constant.Bundle.Script_Banco)
               .Include("~/Scripts/Site/Banco/*.js"));


            bundles.Add(
               new ScriptBundle(Constant.Bundle.Script_Autocomplete)
               .Include("~/Scripts/Site/utils-autocomplete.js"));
               

            bundles.Add(
              new ScriptBundle(Constant.Bundle.Script_Operacion)
              .Include("~/Scripts/Site/Operacion/operacion.js"));

            bundles.Add(
              new ScriptBundle(Constant.Bundle.Script_OperacionInversor)
              .Include("~/Scripts/Site/OperacionInversor/operacioninversor.js"));

            bundles.Add(
             new ScriptBundle(Constant.Bundle.Script_OperacionAprobacion)
             .Include("~/Scripts/Site/Operacion/operacion.aprobacion.js"));

            bundles.Add(
            new ScriptBundle(Constant.Bundle.Script_OperacionBandeja)
            .Include("~/Scripts/Site/Operacion/Bandeja/badeja.entrada.js"));

            bundles.Add(
            new ScriptBundle(Constant.Bundle.Script_OperacionBandejaInversor)
            .Include("~/Scripts/Site/OperacionInversor/Bandeja/badeja.entrada.js"));


            bundles.Add(
          new ScriptBundle(Constant.Bundle.Script_OperacionBandejaAprobacion)
          .Include("~/Scripts/Site/Operacion/Bandeja/bandeja.entrada.aprobacion.js"));
            

            bundles.Add(
          new ScriptBundle(Constant.Bundle.Script_BandejaOperacion)
          .Include("~/Scripts/Site/BandejaOperacion/bandejaOperacion.js"));


            //Globalize
            bundles.Add(
                new ScriptBundle(Constant.Bundle.Script_Globalize)
                .Include("~/Scripts/JQuery/globalize.min.js")
                .Include("~/Scripts/JQuery/globalize.culture.es-AR.js"));

            ////Syncfusion
            bundles.Add(
                new ScriptBundle(Constant.Bundle.Script_SyncfusionAll)
                .Include("~/Scripts/ej/jsrender.min.js")
                .Include("~/Scripts/ej/jquery.easing-1.3.min.js")
                .Include("~/Scripts/ej/ej.web.all.min.js")
                .Include("~/Scripts/ej/ej.unobtrusive.min.js")
               );


            bundles.Add(
                new ScriptBundle(Constant.Bundle.Script_DepositoEfectivo)
                .Include("~/Scripts/Site/DepositoEfectivo/depositoEfectivo.js"));

            bundles.Add(
                new ScriptBundle(Constant.Bundle.Script_ExtraccionFondos)
                .Include("~/Scripts/Site/ExtraccionFondos/extraccionFondos.js"));

            bundles.Add(
                new ScriptBundle(Constant.Bundle.Script_DepositoCheque)
                .Include("~/Scripts/Site/DepositoCheque/depositoCheque.js"));

            bundles.Add(
           new ScriptBundle(Constant.Bundle.Script_FacturaOperacion)
           .Include("~/Scripts/Site/FacturaOperacion/*.js"));

            #endregion

            #region CSS

            ////Syncfusion
            bundles.Add(new StyleBundle(Constant.Bundle.CSS_SyncfusionAll)
                  .Include("~/Content/ej/ej.widgets.core.min.css")
                  .Include("~/Content/ej/default-theme/ej.theme.min.css")
               );

            //Only jquery
            bundles.Add(
                new StyleBundle(Constant.Bundle.CSS_BootstrapAll)
                .IncludeDirectoryCustom("~/Content/Bootstrap/css", "*.min.css")
                .Include("~/Content/Bootstrap/css/jquery-confirm.css")
               );

            //Only jquery
            bundles.Add(
                new StyleBundle(Constant.Bundle.CSS_Template)
                .IncludeDirectoryCustom("~/Content/Template", "*.min.css")
                .Include("~/Content/Template/font-awesome/css/font-awesome.min.css")
               );

            //Only jquery
            bundles.Add(
                new StyleBundle(Constant.Bundle.CSS_Site)
                .Include("~/Content/Site.css")
//                .Include("~/Content/uploadify.css")
               );

            //Only jquery
            bundles.Add(
                new StyleBundle(Constant.Bundle.CSS_Operaciones)
                .Include("~/Content/operaciones.css")
               );

            #endregion
        }
    }
}