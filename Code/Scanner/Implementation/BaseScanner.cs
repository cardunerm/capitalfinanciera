﻿using Scanner.Interface;
using System;
using Scanner.Model;
using Scanner.Utils;
using Scanner.Implementation.Epson.TMS1000;

namespace Scanner.Implementation
{
    public abstract class BaseScanner : IScanner
    {
        public ConfigurationScanner Config = new ConfigurationScanner();
        public static IScanner GetInstance(ConfigurationScanner configuration)
        {
            var type = configuration == null ? "" : configuration.ScannerType;
            return GetAssembly(type.ToString());
        }

        private static IScanner GetAssembly(string type)
        {
            switch (type)
            {
                case "TMS1000":
                    return new ScannerEpsonTMS1000();
                default:
                    return new ScannerEpsonTMS1000();
            }
        }

        public virtual void Dispose()
        {
            throw new NotImplementedException();
        }

        public virtual object GetDefaultValue(string property)
        {
            throw new NotImplementedException();
        }

        public virtual Cheque Scan()
        {
            throw new NotImplementedException();
        }

        public virtual Cheque Scan(ConfigurationScanner configuration)
        {
            throw new NotImplementedException();
        }
    }
}
