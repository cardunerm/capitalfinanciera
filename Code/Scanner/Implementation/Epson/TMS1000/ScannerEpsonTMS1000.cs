﻿using Scanner.Model;
using Scanner.Utils;
using com.epson.bank.driver;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;

namespace Scanner.Implementation.Epson.TMS1000
{
    public class ScannerEpsonTMS1000 : BaseScanner
    {
        private static object _obj = new object();
        private MFDevice _mfDevice = null;
        private MFBase _mfBase = null;
        private MFScan _mfScanFront = null;
        private MFScan _mfScanBack = null;
        private MFMicr _mfMicr = null;
        private ErrorCode _errorCode = ErrorCode.SUCCESS;
        private ConfigurationScanner _configuration = null;
        private Cheque _curretCheque = new Cheque();
        private bool _finish = false;
#if !DEBUG
        public ScannerEpsonTMS1000()
        {
            MFDevice.ESCNEnable(Storage.CROP_STORE_MEMORY);
            _mfDevice = new MFDevice(OpenType.TYPE_PRINTER, "TM-S1000U");
            _mfScanBack = new MFScan();
            _mfScanFront = new MFScan();
            _mfBase = new MFBase();
            _mfMicr = new MFMicr();
            _mfDevice.SCNMICRStatusCallback += new MFDevice.SCNMICRStatusCallbackHandler(SCNMICRSetStatusBack);
            _mfDevice.SCNMICRSetStatusBack();
        }
#endif
        private void SCNMICRSetStatusBack(int transactionNumber, MainStatus mainStatus, ErrorCode subStatus, string portName)
        {
            if (mainStatus == MainStatus.MF_CHECKPAPER_PROCESS_DONE)
            {
                var cmc7 = string.Empty;

                if (_configuration == null)
                    _configuration = new ConfigurationScanner(this);

                SetConfiguration(_configuration);

                _errorCode |= GetImage(_mfScanFront, transactionNumber, true, _configuration);
                _errorCode |= GetImage(_mfScanBack, transactionNumber, false, _configuration);
                _errorCode |= GetCMC7(_mfMicr, transactionNumber, out cmc7);

                if (_errorCode == ErrorCode.SUCCESS)
                {
                    _curretCheque.CMC7 = cmc7;
                    _curretCheque.Base64Front = GetBase64(_mfScanFront.Image, System.Drawing.Imaging.ImageFormat.Jpeg);
                    _curretCheque.Base64Back = GetBase64(_mfScanBack.Image, System.Drawing.Imaging.ImageFormat.Jpeg);

                }
                lock (_obj)
                {
                    _finish = true;
                }
            }

        }

        public override void Dispose()
        {
            if (_mfDevice != null)
            {
                _mfDevice.SCNMICRCancelStatusBack();
                _mfDevice.CancelStatusBack();
                _mfDevice.SCNMICRStatusCallback -= new MFDevice.SCNMICRStatusCallbackHandler(SCNMICRSetStatusBack);
                _mfDevice.SCNMICRCancelFunction(MfEjectType.MF_EJECT_DISCHARGE);
                _mfDevice.CloseMonPrinter();
            }
        }

        private string GetBase64(Bitmap image, System.Drawing.Imaging.ImageFormat format)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
            finally
            {
                image.Dispose();
            }
        }

        private ErrorCode GetCMC7(MFMicr mfMicr, int transactionNumber, out string cmc7)
        {
            _mfMicr.MicrOcrSelect = MfMicrType.MF_MICR_USE_MICR;
            _mfMicr.Parsing = false;
            var errResultCode = _mfDevice.GetMicrText(transactionNumber, mfMicr);
            cmc7 = mfMicr.MicrStr.Substring(0, mfMicr.MicrStr.IndexOf("\0"));
            return errResultCode;
        }

        private ErrorCode GetImage(MFScan imageScan, int transactionNumber, bool front, ConfigurationScanner configuration)
        {
            var image = front ? ScanSide.MF_SCAN_FACE_FRONT : ScanSide.MF_SCAN_FACE_BACK;
            _mfDevice.SCNSetImageFormat(Format.EPS_BI_SCN_JPEGNORMAL);
            var errResultCode = _mfDevice.SCNSelectScanFace(image);

            var bitColor = (int)configuration.BitColor;
            _mfDevice.SCNSetImageQuality((ColorDepth)bitColor, 0, com.epson.bank.driver.Color.EPS_BI_SCN_MONOCHROME, ExOption.EPS_BI_SCN_SHARP);

            if (ErrorCode.SUCCESS.Equals(errResultCode))
            {
                errResultCode = _mfDevice.GetScanImage(transactionNumber, imageScan);
            }
            return errResultCode;
        }

        public override object GetDefaultValue(string property)
        {
            switch (property)
            {
                case Constant.Dpi:
                    return MfScanDpi.MF_SCAN_DPI_DEFAULT;
                case Constant.BitColor:
                    return ColorDepth.EPS_BI_SCN_8BIT;
                default:
                    return null;
            }
        }

        public override Cheque Scan()
        {
#if !DEBUG
            return Scan(null);
#else
            return new Cheque
            {
                CMC7 = "c07244155210000054100000002747e"
            };
#endif
        }

        public override Cheque Scan(ConfigurationScanner configuration)
        {
            Task.Run(() => Run(configuration));
            int count = 1;
            while (count != 10)
            {
                ++count;
                Thread.Sleep(1500);

                lock (_obj)
                {
                    if (_finish)
                        break;
                }
            }

            return _curretCheque;
        }

        public void Run(ConfigurationScanner configuration)
        {
            _configuration = configuration;
            SetScannerConfigurations();
            ErrorCode bRet = _mfDevice.SCNMICRFunctionPostPrint(FunctionType.MF_EXEC);
            if ((bRet | _errorCode) != ErrorCode.SUCCESS)
            {
                _curretCheque = null;

                lock (_obj)
                {
                    _finish = true;
                }
            }
        }

        private void SetScannerConfigurations()
        {
            // Base
            var errorCode = _mfDevice.SCNMICRFunctionPostPrint(_mfBase, FunctionType.MF_GET_BASE_DEFAULT);
            errorCode = _mfDevice.SCNMICRFunctionPostPrint(_mfBase, FunctionType.MF_SET_BASE_PARAM);

            // ScanFront           
            errorCode = _mfDevice.SCNMICRFunctionPostPrint(_mfScanFront, FunctionType.MF_GET_SCAN_FRONT_DEFAULT);
            errorCode = _mfDevice.SCNMICRFunctionPostPrint(_mfScanFront, FunctionType.MF_SET_SCAN_FRONT_PARAM);

            // ScanBack
            errorCode = _mfDevice.SCNMICRFunctionPostPrint(_mfScanBack, FunctionType.MF_GET_SCAN_BACK_DEFAULT);
            errorCode = _mfDevice.SCNMICRFunctionPostPrint(_mfScanBack, FunctionType.MF_SET_SCAN_BACK_PARAM);

            // Micr
            errorCode = _mfDevice.SCNMICRFunctionPostPrint(_mfMicr, FunctionType.MF_GET_MICR_DEFAULT);
            _mfMicr.Font = MfMicrFont.MF_MICR_FONT_CMC7;
            errorCode = _mfDevice.SCNMICRFunctionPostPrint(_mfMicr, FunctionType.MF_SET_MICR_PARAM);

        }

        private void SetConfiguration(ConfigurationScanner configuration)
        {
            var dpi = (int)configuration.Dpi;
            _mfScanFront.Resolution = (MfScanDpi)dpi;
            _mfScanBack.Resolution = (MfScanDpi)dpi;
        }
    }
}
