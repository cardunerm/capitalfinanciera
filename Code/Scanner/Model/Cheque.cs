﻿using System;
using System.Text.RegularExpressions;

namespace Scanner.Model
{
    public class Cheque
    {
       private readonly Regex _cmc7RegexParser =
          new Regex(@"^c((?<banco>\d{3})|.{3})((?<sucursal>\d{3})|.{3})((?<codigopostal>\d{4})|.{4})((?<cheque>\d{8})|.{8})((?<cuenta>\d{11})|.{11})e$");

        Match _match;

        private Match Match
        {
            get
            {
                if (CMC7 != null && _match == default(Match))
                    _match = _cmc7RegexParser.Match(CMC7);

                return _match;
            }
        }
        string _cmc7 = "";
        public string CMC7
        {
            get
            {
                return _cmc7;
            }
            set
            {
                _cmc7 = value.Replace("=", "c").Replace("^", "e");
            }
        }
        public string Base64Front { get; set; }
        public string Base64Back { get; set; }
        public string Banck
        {
            get
            {
                return GetValue(Match, "banco");
            }
            set { }
        }
        public string Suc
        {
            get
            {
                return GetValue(Match, "sucursal");
            }
            set { }
        }
        public string Cp
        {
            get
            {
                return GetValue(Match, "codigopostal");
            }
            set { }
        }
        public string Number
        {
            get
            {
                return GetValue(Match, "cheque");
            }
            set { }
        }
        public string Account
        {
            get
            {
                return GetValue(Match, "cuenta");
            }
            set { }
        }

        private string GetValue(Match match, string groupName)
        {
            if (match == default(Match))
                return null;

            var group = match.Groups[groupName];
            if (group == null) return null;

            try
            {
                return group.Value;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
