﻿using Scanner.Model;
using Scanner.Utils;
using System;

namespace Scanner.Interface
{
    public interface IScanner :IDisposable
    {
        Cheque Scan();
        Cheque Scan(ConfigurationScanner configuration);
        object GetDefaultValue(string property);
    }
}
