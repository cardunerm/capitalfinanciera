﻿using Scanner.Interface;

namespace Scanner.Utils
{
    public class ConfigurationScanner
    {
        IScanner _scanner = null;

        public ConfigurationScanner(IScanner scanner)
        {
            _scanner = scanner;
        }

        public ConfigurationScanner()
        {
        }

        private object _dpi = null;

        public object Dpi
        {
            get
            {
                return _dpi ?? _scanner.GetDefaultValue(Constant.Dpi);
            }
            set
            {
                _dpi = value;
            }
        }

        private object _bitColor = null;

        public object BitColor
        {
            get
            {
                return _bitColor ?? _scanner.GetDefaultValue(Constant.BitColor);
            }
            set
            {
                _bitColor = value;
            }          
        }

        private object _scannerType = null;

        internal object ScannerType
        {            
            get
            {
                return _scannerType ?? "TMS1000";
            }
            set
            {
                _scannerType = value;
            }

        }
    }
}