﻿namespace Scanner.Utils
{
    public class Constant
    {
        public const string Dpi = "Dpi";
        public const string BitColor = "BitColor";
        public const string ScannerType = "ScannerType";
    }
}
