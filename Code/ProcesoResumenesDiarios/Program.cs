﻿using Business;
using Luma.Utils.Logs;
using MvcAppDCR.App_Start;
using System;

namespace ProcesoResumenesDiarios
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                LogManager.Log.RegisterInfo("Inicio del Proceso: " + DateTime.Now );

                AutomapperConfiguration.StartConfiguration();
                JobProcesoResumenDiario job = new JobProcesoResumenDiario();
                job.RunProcess();

                LogManager.Log.RegisterInfo("Fin del Proceso" + DateTime.Now);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
            }
            
        }
    }
}
