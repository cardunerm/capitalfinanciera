﻿using Castle.Core.Resource;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;

namespace  Luma.Utils
{
    public class AOPCastle
    {
        private static WindsorContainer Container { get; set; }
        
        static AOPCastle()
        {
            IResource resource = new AssemblyResource("assembly://Utils/Config/Xml/castlewindsor.xml");
            Container = new WindsorContainer(new XmlInterpreter(resource));
        }

        public static T CreateObject<T>()
        {
            return Container.Resolve<T>();
        }

        internal static T CreateObject<T>(string key)
        {
            return Container.Resolve<T>(key);
        }
    }
}