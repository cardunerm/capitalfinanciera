﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace  Luma.Utils
{
    public static class UtilHelper
	{
		public static bool AreConflictsInPeriods(DateTime oS, DateTime oE, DateTime nS, DateTime nE)
		{
			try
			{
				if ((oS >= nS && oS <= nE) ||
					(oE >= nS && oE <= nE))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static bool IsInPeriod(int month, int year, DateTime creationDate, DateTime? deleteDate)
		{
			try
			{
				string period = string.Format("{0}-{1}", month, year);

				List<string> periods = new List<string>();

				DateTime endDate = (deleteDate.HasValue ? deleteDate.Value : DateTime.Now);
				endDate = endDate.AddDays((DateTime.DaysInMonth(endDate.Year, endDate.Month) - endDate.Day));
				endDate = endDate.AddHours(23 - endDate.Hour);
				endDate = endDate.AddMinutes(59 - endDate.Minute);
				endDate = endDate.AddSeconds(59 - endDate.Second);

				do
				{
					periods.Add(string.Format("{0}-{1}", creationDate.Month, creationDate.Year));

					creationDate = creationDate.AddMonths(1);
				} while (creationDate <= endDate);

				return periods.Contains(period);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static bool IsInPeriod(DateTime? oS, DateTime? oE, DateTime pS)
		{
			try
			{
				if (oS.HasValue && oE.HasValue)
				{
					return (oS <= pS && pS <= oE);
				}
				else if (oS.HasValue)
				{
					return (oS <= pS);
				}
				else if (oE.HasValue)
				{
					return (pS <= oE);
				}
				else
				{
					return true;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static bool AreInPeriod(DateTime oS, DateTime oE, DateTime pS, DateTime pE)
		{
			try
			{
				if ((pS <= oS && oS <= pE) ||
					(pS <= oE && oE <= pE) ||
					(oS <= pS && pS <= oE) ||
					(oS <= pE && pE <= oE))
				{
					return true;
				}

				return false;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public static long ConvertToUnixTime(DateTime dateTime)
        {
            try
            {
                return ((dateTime.ToUniversalTime() - DateTime.Parse("1970/01/01")).Ticks / TimeSpan.TicksPerMillisecond);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		public static string GetFileExtension(string fileName)
		{
			try
			{
				return fileName.Split('.').Last().ToLower();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public static string GetMD5String(string input)
        {
            try
            {
                MD5 md5 = MD5.Create();
                byte[] buffer = Encoding.ASCII.GetBytes(input);
                byte[] hash = md5.ComputeHash(buffer);

                StringBuilder output = new StringBuilder();

                foreach (var item in hash)
                {
                    output.Append(item.ToString("X2"));
                }

                return output.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		public static bool IsValidEmail(string email)
		{
			try
			{
				string matchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
										 + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
													[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
										 + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
													[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
										 + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

				if (!string.IsNullOrEmpty(email))
				{
					return Regex.IsMatch(email, matchEmailPattern);
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static List<SelectListItem> GetWeekDays(string selected = null)
		{
			try
			{
				return new List<SelectListItem>()
				{
					new SelectListItem { Text = "Domingo", Value = "1", Selected = (!string.IsNullOrEmpty(selected) ? selected.Equals("1") : false) },
					new SelectListItem { Text = "Lunes", Value = "2", Selected = (!string.IsNullOrEmpty(selected) ? selected.Equals("2") : false) },
					new SelectListItem { Text = "Martes", Value = "3", Selected = (!string.IsNullOrEmpty(selected) ? selected.Equals("3") : false) },
					new SelectListItem { Text = "Miercoles", Value = "4", Selected = (!string.IsNullOrEmpty(selected) ? selected.Equals("4") : false) },
					new SelectListItem { Text = "Jueves", Value = "5", Selected = (!string.IsNullOrEmpty(selected) ? selected.Equals("5") : false) },
					new SelectListItem { Text = "Viernes", Value = "6", Selected = (!string.IsNullOrEmpty(selected) ? selected.Equals("6") : false) },
					new SelectListItem { Text = "Sabado", Value = "7", Selected = (!string.IsNullOrEmpty(selected) ? selected.Equals("7") : false) }
				};
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static string GetWeekDayName(int dayNumber)
		{
			switch (dayNumber)
			{
				case 1:
					return "Domingo";
				case 2:
					return "Lunes";
				case 3:
					return "Martes";
				case 4:
					return "Miercoles";
				case 5:
					return "Jueves";
				case 6:
					return "Viernes";
				case 7:
					return "Sabado";
				default:
					return string.Empty;
			}
		}

		public static string GetWeekDayName(string dayNumber)
		{
			return GetWeekDayName(int.Parse(dayNumber));
		}

		public static string GetMonthName(int monthNumber)
		{
			switch (monthNumber)
			{
				case 1:
					return "Enero";
				case 2:
					return "Febrero";
				case 3:
					return "Marzo";
				case 4:
					return "Abril";
				case 5:
					return "Mayo";
				case 6:
					return "Junio";
				case 7:
					return "Julio";
				case 8:
					return "Agosto";
				case 9:
					return "Setiembre";
				case 10:
					return "Octubre";
				case 11:
					return "Noviembre";
				case 12:
					return "Diciembre";
				default:
					return string.Empty;
			}
		}

		public static int DaysInMonthOfPeriod(DateTime dateS, DateTime dateE, int year, int month)
		{
			try
			{
				DateTime currentMontStart = DateTime.Parse(string.Format("{0}-{1}-01 00:00:00.000", year.ToString().PadLeft(2, '0'), month.ToString().PadLeft(2, '0')));
				DateTime currentMontEnd = DateTime.Parse(string.Format("{0}-{1}-{2} 23:59:59.000", year.ToString().PadLeft(2, '0'), month.ToString().PadLeft(2, '0'), DateTime.DaysInMonth(year, month).ToString().PadLeft(2, '0')));

				if (dateS < currentMontStart)
				{
					dateS = currentMontStart;
				}

				if (dateE > currentMontEnd)
				{
					dateE = currentMontEnd;
				}

				if (dateS <= currentMontStart)
				{
					return dateE.Day;
				}
				else
				{
					return ((dateE - dateS).Days + 1);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public static void SaveFile(Stream stream, string virtualPath, string fileName, string fileExt)
		{
			try
			{
				string fullVirtualPath = string.Format(@"{0}/{1}.{2}", virtualPath, fileName, fileExt);

				string fullPhysicalPath = HttpContext.Current.Server.MapPath(fullVirtualPath);

				FileStream file = File.Create(fullPhysicalPath);
				stream.Seek(0, SeekOrigin.Begin);
				stream.CopyTo(file);

				file.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public static double CalcularInteres(decimal capital, decimal interes, int cantDias)
        {
            //Formula del calculo de interés
            return (Convert.ToDouble(capital) * Math.Pow(Convert.ToDouble(1 + interes), cantDias)) - Convert.ToDouble(capital);
        }

        public static double CalcularDescuento(decimal capital, decimal interes, int cantDias)
        {
            //Formula del calculo de descuento
            return Convert.ToDouble(capital) - (Convert.ToDouble(capital) * Math.Pow(Convert.ToDouble(1 + interes), (-1)*cantDias));
        }
    }
}