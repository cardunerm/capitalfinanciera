﻿using System;
using System.IO;

namespace Luma.Utils.ActualizarYDescargarArchivos
{
    public class FilesStatus
    {
        public const string HandlerPath = "/ArchivoAdjunto/";

        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }

        public FilesStatus() { }

        public FilesStatus(FileInfo fileInfo) { SetValues(fileInfo.Name, (int)fileInfo.Length, fileInfo.FullName); }

        public FilesStatus(string fileName, int fileLength, string fullPath) { SetValues(fileName, fileLength, fullPath); }

        private void SetValues(string fileName, int fileLength, string fullPath)
        {
            try
            {                
                name = fileName;
                type = "image/png";
                size = fileLength;
                progress = "1.0";
                url = HandlerPath + "DownLoadFile?f=" + fileName;
                delete_url = "DeleteHandle?f=" + fileName; //HandlerPath + "DeleteHandle?f=" + fileName;
                delete_type = "DELETE";

                var ext = Path.GetExtension(fullPath);

                var fileSize = ConvertBytesToMegabytes(fileLength); // new FileInfo(fullPath).Length);
                if (fileSize > 3 || !IsImage(ext)) thumbnail_url = "../Content/Images/AttachmentsImg/generalFile.png";
                else 
                {
                    if (File.Exists(fullPath))
                    {
                        thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath);
                    }
                    else
                        thumbnail_url = "../Content/Images/AttachmentsImg/imageNotFound.png";
                }
                    

            }
            catch (FileNotFoundException exfile)
            {
                //no se encontró uno de los archivos.
                throw exfile;
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool IsImage(string ext)
        {
            return ext == ".gif" || ext == ".jpg" || ext == ".png";
        }

        private string EncodeFile(string fileName)
        {
            return Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName));
        }

        static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
    }
}