﻿
using Resources;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace System.Collections.Generic
{
    public static class ListHTML
    {
        public static List<SelectListItem> GetSelectListItem<T>(this List<T> list, string valueProperty, string textProperty, bool insertIndistinct = false, bool insertNone = false, string selected = null)
        {
            try
            {
                var selectList = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    selectList.Add(new SelectListItem { Value = "-1", Text = Resource.Indistinct });
                }
                else if (insertNone)
                {
                    selectList.Add(new SelectListItem { Value = "-1", Text = Resource.None });
                }

                Type type = typeof(T);
                PropertyInfo propertyValue = type.GetProperty(valueProperty);
                PropertyInfo propertyText = type.GetProperty(textProperty);

                foreach (var item in list)
                {
                    selectList.Add(new SelectListItem
                    {
                        Value = propertyValue.GetValue(item, null).ToString(),
                        Text = propertyText.GetValue(item, null).ToString(),
                        Selected = (!string.IsNullOrEmpty(selected) ? (propertyValue.GetValue(item, null).ToString().Equals(selected)) : false)
                    });
                }

                return selectList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetSelectListItem<T>(this IOrderedEnumerable<T> list, string valueProperty, string textProperty, bool insertIndistinct = false, bool insertNone = false, string selected = null)
        {
            try
            {
                var selectList = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    selectList.Add(new SelectListItem { Value = "-1", Text = Resource.Indistinct });
                }
                else if (insertNone)
                {
                    selectList.Add(new SelectListItem { Value = "-1", Text = Resource.None });
                }

                Type type = typeof(T);
                PropertyInfo propertyValue = type.GetProperty(valueProperty);
                PropertyInfo propertyText = type.GetProperty(textProperty);

                foreach (var item in list)
                {
                    selectList.Add(new SelectListItem
                    {
                        Value = propertyValue.GetValue(item, null).ToString(),
                        Text = propertyText.GetValue(item, null).ToString(),
                        Selected = (!string.IsNullOrEmpty(selected) ? (propertyValue.GetValue(item, null).ToString().Equals(selected)) : false)
                    });
                }

                return selectList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
