﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace System
{
    public static class EnumExtension
    {
        public static string GetEnumeracionName(this Enum value)
        {
            return (value).GetType()
                          .GetMember(value.ToString())
                          .First()
                          .GetCustomAttribute<DisplayAttribute>().GetName();
        }

    }
}
