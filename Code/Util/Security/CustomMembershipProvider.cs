﻿using Luma.Utils.Security;
using System;
using System.Web;
using System.Web.Security;

namespace  Luma.Utils.Security
{
    public class CustomMembershipProvider<TMembershipValidator> : MembershipProvider
        where TMembershipValidator : IMembershipValidator, new()
    {
        private IMembershipValidator Validator = new TMembershipValidator();

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object proveedorUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
			return HttpContext.Current.Session[string.Format("login_{0}", username)] as CustomMembershipUser;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Clear; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public bool ValidateUser(string username, string password, params object[] metadata)
        {
            var user = Validator.IsValid(username, password, metadata);

            if (user != null)
            {
				HttpContext.Current.Session[string.Format("login_{0}", username)] = new CustomMembershipUser() { User = user };

                return true;
            }

            return false;
        }

		public bool ValidateUserDirect(string username, params object[] metadata)
		{
			var user = Validator.IsValidDirect(username, metadata);

			if (user != null)
			{
				HttpContext.Current.Session[string.Format("login_{0}", username)] = new CustomMembershipUser() { User = user };

				return true;
			}

			return false;
		}

        public bool HasPermission(string url, int permission)
        {
			return Validator.HasPermission(Membership.GetUser(HttpContext.Current.User.Identity.Name) as CustomMembershipUser, url, permission);
        }

        public override bool ValidateUser(string username, string password)
        {
            throw new NotImplementedException();
        }

		public CustomMembershipUser GetCurrentUser(string username = "")
		{
			if (string.IsNullOrEmpty(username))
			{
				return Membership.GetUser(HttpContext.Current.User.Identity.Name) as CustomMembershipUser;
			}
			else
			{
				return Membership.GetUser(username) as CustomMembershipUser;
			}
		}
    }
}