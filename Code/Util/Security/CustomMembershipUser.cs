﻿using System.Web.Security;

namespace  Luma.Utils.Security
{
    public class CustomMembershipUser : MembershipUser
    {
        public dynamic User { get; set; }
    }
}