﻿using System.Web;

namespace  Luma.Utils.Security
{
    public interface IMembershipValidator
    {
		dynamic IsValid(string name, string password, params object[] metadata);
		dynamic IsValidDirect(string name, params object[] metadata);
        bool HasPermission(HttpContextBase context, CustomMembershipUser user, params object[] metadata);
        bool HasPermission(params object[] metadata);
    }
}