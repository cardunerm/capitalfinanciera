﻿using System;
using System.Runtime.CompilerServices;

namespace Domine.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IDPropertyAttribute : BaseModelNamePropertyAttribute
    {
        public IDPropertyAttribute([CallerMemberName] string propertyName = null)
            : base(propertyName) { }
    }
}