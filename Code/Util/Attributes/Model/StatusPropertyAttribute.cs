﻿using System;
using System.Runtime.CompilerServices;

namespace Domine.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class StatusPropertyAttribute : BaseModelNamePropertyAttribute
    {
        public StatusPropertyAttribute([CallerMemberName] string propertyName = null)
            : base(propertyName) { }
    }
}