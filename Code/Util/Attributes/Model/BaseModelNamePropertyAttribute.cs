﻿using System;

namespace Domine.Attributes
{
    public class BaseModelNamePropertyAttribute : Attribute
    {
        public string Property { get; set; }
        public BaseModelNamePropertyAttribute(string propertyName = null)
        {
            Property = propertyName;
        }
    }
}