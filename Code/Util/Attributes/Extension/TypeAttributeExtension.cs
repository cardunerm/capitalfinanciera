﻿using Domine.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace System
{
    public static class TypeAttributeExtension
    {
        public static String GetStatusPropertyOfMetaType(this Type type)
        {
            return GetNameOfPropertyMetaType<StatusPropertyAttribute>(type, "Property");
        }

        public static String GetIDPropertyOfMetaType(this Type type)
        {
            return GetNameOfPropertyMetaType<IDPropertyAttribute>(type, "Property");
        }

        public static String GetNameOfPropertyMetaType<TAttribute>(Type type, String property)
        {
            var att = type.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();

            if (att != null)
            {
                var prop = (att as MetadataTypeAttribute)
                    .MetadataClassType
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .FirstOrDefault(p => p.GetCustomAttributes(typeof(TAttribute), false).Count() == 1);

                //Retorna el primero que encontro
                //TODO: Implementar para soporta multiples propiedades de estado
                if (prop != null)
                {
                    var temp = ((TAttribute)prop.GetCustomAttributes(typeof(TAttribute), false)[0]);

                    return temp.GetType().InvokeMember(property, BindingFlags.GetProperty, null, temp, null).ToString();
                }
            }

            return null;
        }
    }
}