﻿using System.IO;

namespace System.Web.Optimization
{
    public static class BundleExtentions
    {
        public static Bundle IncludeDirectoryWithExclusion(this StyleBundle bundle, string directoryVirtualPath, string searchPattern, params string[] toExclude)
        {
            var folderPath = HttpContext.Current.Server.MapPath(directoryVirtualPath);

            foreach (var file in Directory.GetFiles(folderPath, searchPattern))
            {
                if (!String.IsNullOrEmpty(Array.Find(toExclude, s => s.ToLower() == file.ToLower())))
                {
                    continue;
                }

                bundle.Include(directoryVirtualPath + "/" + file);
            }

            return bundle;
        }

        public static Bundle IncludeDirectoryCustom(this StyleBundle bundle, string directoryVirtualPath, string searchPattern)
        {
            var folderPath = HttpContext.Current.Server.MapPath(directoryVirtualPath);

            foreach (var file in Directory.GetFiles(folderPath, searchPattern))
            {
                bundle.Include(directoryVirtualPath + "/" + new FileInfo(file).Name);
            }

            return bundle;
        }
    }
}