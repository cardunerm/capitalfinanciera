﻿using Luma.Utils.Security;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace  Luma.Utils.Attributes.Security
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private IMembershipValidator Validator;

        public CustomAuthorizeAttribute(Type typeValidator)
        {
			if (typeValidator == null)
			{
				throw new NotImplementedException("Debe especificar un validador");
			}

            Validator = Activator.CreateInstance(typeValidator) as IMembershipValidator;
        }

        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            //TODO: Verificar si quieren que el usuario sea recordado mas alla de la session
			if (!httpContext.User.Identity.IsAuthenticated || Membership.GetUser(httpContext.User.Identity.Name, true) == null)
			{
				return false;
			}

            return true;
            //return Validator.HasPermission(httpContext, Membership.GetUser(httpContext.User.Identity.Name) as CustomMembershipUser);
        }
    }
}