﻿using System.ComponentModel.DataAnnotations;

namespace Utils.Enumeraciones
{
    public class Estados
    {

        public enum eEstadosOp
        {
            [Display(Name = "Pendiente")]
            Pendiente = 1,

            [Display(Name = "Aprobada")]
            Aprobado = 3,

            [Display(Name = "Rechazada")]
            Rechazado = 4,

            [Display(Name = "Pendiente Aprobación en 24hs")]
            Pendiente24 = 5,

            [Display(Name = "Pendiente Aprobación en 48hs")]
            Pendiente48 = 6,

            [Display(Name = "Cancelada")]
            Cancelada = 7,

            [Display(Name = "Pagada")]
            Pagada = 8,

            [Display(Name = "Liquidada")]
            Liquidada = 9
        }

        public enum eEstadosOpDetalle
        {
            [Display(Name = "Pendiente")]
            Pendiente = 1,

            [Display(Name = "Aprobado Parcial")]
            AprobadoParcial = 2,

            [Display(Name = "Aprobado")]
            Aprobado = 3,

            [Display(Name = "Rechazado")]
            Rechazado = 4,

            [Display(Name = "Cancelado")]
            Cancelado = 5,

            [Display(Name = "Pagada")]
            Pagada = 6
        }

        public enum eEstadosArqueo
        {
            [Display(Name = "Pendiente")]
            Pendiente = 1,

            [Display(Name = "Parcial")]
            Aprobado = 2,

            [Display(Name = "Auditado")]
            Rechazado = 3,


        }

        public enum eEstadosCheque
        {
            

            [Display(Name = "En Operación")]
            EnOperacion = 1,
                      

            [Display(Name = "No Aceptado")]
            NoAceptado = 5,

            [Display(Name = "Cartera")]
            Cartera = 6,

            [Display(Name = "Depositado")]
            Depositado = 7,

            [Display(Name = "Negociado")]
            Negociado = 8,

            [Display(Name = "Cobrado")]
            Cobrado = 9,

            [Display(Name = "Rechazado")]
            Rechazado = 10,

            [Display(Name = "Cancelado")]
            Cancelado = 11,


        }

        public enum mTipoMovimientoCuentaCorriente
        {
            [Display(Name = "Debito")]
            Debito = 1,

            [Display(Name = "Credito")]
            Credito = 2

        }



        


    }
}
