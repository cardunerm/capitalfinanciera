﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Luma.Utils.Enumeraciones
{
    public class Perfil
    {

        public enum TipoPerfil
        {
            Administrador = 1,
            Operador = 2,
            Testing = 3,
            Demos = 4,
            Aprobador = 5,
            PerfilTest = 6,
            Inversor = 7
        }
    }
}
