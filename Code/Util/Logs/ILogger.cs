﻿using System;

namespace Luma.Utils.Logs
{
    public interface ILogger
    {
        string RegisterError(Exception ex);
        void RegisterInfo(string info);
    }
}