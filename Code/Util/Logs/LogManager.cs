﻿
using Luma.Utils.Logs;
namespace  Luma.Utils.Logs
{
    public class LogManager
    {
        static LogManager()
        {
            Log = new Log4net.Log4Net();
        }

        public static ILogger Log { get; private set; }
    }
}