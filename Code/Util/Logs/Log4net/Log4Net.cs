﻿[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Luma.Utils.Logs.Log4net
{
    public class Log4Net : ILogger
    {

        static log4net.ILog _log;

        public string RegisterError(System.Exception ex)
        {
            var code = ex.GetHashCode().ToString();
            _log.Error(code, ex);
            return code;
        }

        public void RegisterInfo(string info)
        {
            _log.Info(info);
        }

        static Log4Net()
        {
            _log = log4net.LogManager.GetLogger(typeof(Log4Net));
        }
    }
}