//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domine
{
    using System;
    using System.Collections.Generic;
    
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ArqueoMetadata))]
    public partial class Arqueo
    {
        public Arqueo()
        {
            this.ArqueoDetalle = new HashSet<ArqueoDetalle>();
        }
    
        public int IdArqueo { get; set; }
        public int IdEmpresa { get; set; }
        public int IdSucursal { get; set; }
        public long IdUsuario { get; set; }
        public int Estado { get; set; }
        public System.DateTime Fecha { get; set; }
        public int CantidadCheques { get; set; }
        public decimal Monto { get; set; }
        public string Descripcion { get; set; }
    
        public virtual ICollection<ArqueoDetalle> ArqueoDetalle { get; set; }
    }
}
