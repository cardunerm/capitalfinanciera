//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domine
{
    using System;
    using System.Collections.Generic;
    
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(SociedadMetadata))]
    public partial class Sociedad
    {
        public int IdSociedad { get; set; }
        public string Nombre { get; set; }
        public string CUIT { get; set; }
        public int IdCliente { get; set; }
        public int Estado { get; set; }
    
        public virtual Cliente Cliente { get; set; }
    }
}
