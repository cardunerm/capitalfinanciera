//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domine
{
    using System;
    using System.Collections.Generic;
    
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ViewCajaMetadata))]
    public partial class ViewCaja
    {
        public System.DateTime FechaApertura { get; set; }
        public Nullable<System.DateTime> FechaCierre { get; set; }
        public decimal MontoInicial { get; set; }
        public Nullable<decimal> MontoFinal { get; set; }
        public long IdUsuario { get; set; }
        public int IdCaja { get; set; }
        public int IdEmpresa { get; set; }
        public int IdSucursal { get; set; }
    }
}
