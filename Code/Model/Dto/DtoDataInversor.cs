﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.Dto
{
    public class DtoDataInversor
    {
        
        public string Nombre { get; set; }

        public string Cuit { get; set; }
       
        public int Numero { get; set; }

        public decimal? Saldo { get; set; }

    }
}
