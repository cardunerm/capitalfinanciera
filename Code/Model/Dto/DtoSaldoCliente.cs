﻿using System;

namespace Domine.Dto
{
    public class DtoSaldoCliente
    {
        decimal _saldo;
        decimal _montoRechazo;

        public decimal Saldo
        {
            get
            {
                return Math.Round(_saldo, 2);
            }
            set
            {
                _saldo = value;
            }
        }
        public int CantidadRechazos { get; set; }
        public decimal MontoRechazos
        {
            get
            {
                return Math.Round(_montoRechazo, 2);
            }
            set
            {
                _montoRechazo = value;
            }
        }
    }
}
