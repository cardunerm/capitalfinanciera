﻿using Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace Domine.Dto
{
    public class DtoOperacionAprobacion
    {
        public long? IdUser { get; set; }
        public long Id { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Cliente")]
        public string Cliente { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Fecha")]
        public System.DateTime Fecha { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Observaciones")]
        public string Observacion { get; set; }

        [MaxLength(5000)]
        [StringLength(500, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "ObsEvaluacion")]
        public string ObservacionEvaluacion { get; set; }

        decimal _saldo;
        decimal _montoRechazo;

        public decimal Saldo
        {
            get
            {
                return Math.Round(_saldo, 2);
            }
            set
            {
                _saldo = value;
            }
        }
        public int CantidadRechazos { get; set; }
        public decimal MontoRechazos
        {
            get
            {
                return Math.Round(_montoRechazo, 2);
            }
            set
            {
                _montoRechazo = value;
            }
        }

        [Display(ResourceType = typeof(Resource), Name = "Estado")]
        public int IdEstado
        {
            get; set;
        }

        public string Sesion { get; set; }

        public DtoDetalleOperacionAprobacion DetalleOperacion { get; set; }
    }
}
