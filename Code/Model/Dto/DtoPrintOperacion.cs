﻿
namespace Domine.Dto
{
    public class DtoPrintOperacion
    {

        public Configuracion Configuracion { get; set; }
        public Operacion Operacion { get; set; }
        public Empresa Empresa { get; set; }
        public string MontoLetra { get; set; }
        public int ImprimeDetalle { get; set; }
        public int TipoContribuyente { get; set; }
        public string Gastos { get; set; }
        public string Iva { get; set; }
        public string Sesion { get; set; }
        public string Total { get; set; }
    }
}
