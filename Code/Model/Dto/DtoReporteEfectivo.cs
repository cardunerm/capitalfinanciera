﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.Dto
{
    public class DtoReporteEfectivo
    {
        public string NombreUsuario { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreInversor { get; set; }
        public string CuitInversor { get; set; }
        public decimal Monto { get; set; }
        public string CuentaCorrienteStr { get; set; }
        public int NumeroOperacion { get; set; }
        public string TipoMoneda { get; set; }
        public string MontoLetter { get; set; }

    }
}
