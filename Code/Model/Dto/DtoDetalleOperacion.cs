﻿using Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Domine.Dto
{
    public class DtoDetalleOperacion
    {

        public int IdCliente { get; set; }
        public string UniqueId { get; set; }
        public string Banco { get; set; }
        public string Estado { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Banco")]
        public int IdBanco { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Numero")]
        public string Numero { get; set; }

        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "CMC7")]
        public string CMC7 { get; set; }

        [MaxLength(4)]
        [StringLength(4, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "CP")]
        public string Cp { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Titular")]
        public string TitularCuenta { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Cuenta")]
        public string NumeroCuenta { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Sucursal")]
        public string SucursalEmision { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Vencimiento")]
        public System.DateTime FechaVencimiento { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Importe")]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public decimal Monto { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Importe")]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public string MontoStr {
                get { return Monto.ToString(); }
                set {
                    Monto = Convert.ToDecimal(value.Replace(',','.'));
            }
        }


        public string ImagenFront
        {
            get;
            set;
        }

        public string ImagenBack
        {
            get;
            set;
        }

        public decimal MontoCalculado { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Garantia")]
        public bool EsChequeGarantia { get; set; }

        public Cheque Cheque { get; set; }
    }
}
