﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.Dto
{
    public class DtoArqueo
    {
        [IDProperty]
        public int IdArqueo { get; set; }

        public int IdEmpresa { get; set; }

        public int IdSucursal { get; set; }

        public int IdUsuario { get; set; }

        [StatusProperty]
        public int Estado { get; set; }

        public DtoArqueoDetalle DetalleArqueo { get; set; }
    }
}
