﻿using Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace Domine.Dto
{
    public class DtoOperacion
    {
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Cliente")]
        public int IdCliente { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Fecha")]
        public System.DateTime Fecha { get; set; }

        [MaxLength(5000)]
        [StringLength(500, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Observaciones")]
        public string Observacion { get; set; }

        [MaxLength(5000)]
        [StringLength(500, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "ObsEvaluacion")]
        public string ObservacionEvaluacion { get; set; }
        public string EstadoStr { get; set; }
        public string Cliente { get; set; }

        public DtoDetalleOperacion DetalleOperacion { get; set; }
        

        decimal _saldo;
        decimal _montoRechazo;

        public decimal Saldo
        {
            get
            {
                return Math.Round(_saldo, 2);
            }
            set
            {
                _saldo = value;
            }
        }
        public int CantidadRechazos { get; set; }
        public decimal MontoRechazos
        {
            get
            {
                return Math.Round(_montoRechazo, 2);
            }
            set
            {
                _montoRechazo = value;
            }
        }
        public int IdSucursal { get; set; }
        public string Sesion { get; set; }
        public int IdVendedor { get; set; }

    }
}
