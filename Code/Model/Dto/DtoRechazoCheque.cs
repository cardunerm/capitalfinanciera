﻿namespace Domine.Dto
{
    public class DtoRechazoCheque
    {
        public int IdCheque { get; set; }
        public double Gasto { get; set; }
        public double Impuesto { get; set; }
        public double Multa { get; set; }
        public string Motivo { get; set; }
    }
}
