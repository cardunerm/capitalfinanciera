﻿namespace Domine.Dto
{
    public class DtoPrintOperacionCtaCteInvUSD
    {
        public Configuracion Configuracion { get; set; }
        public OperacionInversor OperacionCtaCte { get; set; }
        public Empresa Empresa { get; set; }
        public string MontoLetra { get; set; }

    }
}
