﻿
namespace Domine.Dto
{
    public class DtoPrintOperacionInversor
    {

        public Configuracion Configuracion { get; set; }
        public OperacionInversor Operacion { get; set; }
        public Empresa Empresa { get; set; }
        public string MontoLetra { get; set; }

    }
}
