﻿namespace Domine.Dto
{
    public class DtoPrintOperacionCtaCte
    {

        public Configuracion Configuracion { get; set; }
        public OperacionCuentaCorriente OperacionCtaCte { get; set; }
        public Empresa Empresa { get; set; }
        public string MontoLetra { get; set; }
    }
}
