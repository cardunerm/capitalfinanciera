﻿using Enumerations;

namespace Domine.Dto
{
    public class DtoCuentaCorrienteInversor
    {
        public int Id { get; set; }
        public string NumeroCuenta { get; set; }
        public int TipoMoneda { get; set; }
        public decimal Saldo { get; set; }
        public string NumeroTipo
        {
            get
            {
                switch ((TipoMoneda)TipoMoneda)
                {
                    case Enumerations.TipoMoneda.Peso:
                        return "(ARS) " + NumeroCuenta + " - Saldo:" + Saldo.ToString();
                    default:
                        return "(USD) " + NumeroCuenta + " - Saldo:" + Saldo.ToString(); 

                }
            }
        }
        public decimal LimiteDescubierto { get; set; }
        public int ClasificacionInversor { get; set; }
    }
}
