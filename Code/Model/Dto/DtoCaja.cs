﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.Dto
{
    public class DtoCaja
    {
        [IDProperty]
        public int IdCaja { get; set; }
        
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "FechaApertura")]
        public System.DateTime FechaApertura { get; set; }

        public Nullable<System.DateTime> FechaCierre { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "MontoIncial")]
        public string MontoInicial { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "MontoFinal")]
        public string MontoFinal { get; set; }
    }
}
