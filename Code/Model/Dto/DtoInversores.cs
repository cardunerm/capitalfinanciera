﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.Dto
{
    public  class DtoInversores
        
    {
        [IDProperty]
        public int IdInversores { get; set; }

        [Display(Name = "InversoresNombre", ResourceType = typeof(Resource))]
        [MaxLength(50, ErrorMessageResourceName = "LargoSuperado", ErrorMessageResourceType = typeof(Resource))]
        public string Nombre { get; set; }

        [Display(Name = "InversoresCUIT", ResourceType = typeof(Resource))]
        [MaxLength(11, ErrorMessageResourceName = "LargoSuperado", ErrorMessageResourceType = typeof(Resource))]
        public string CUIT { get; set; }

        [Display(Name = "InversoresDomicilio", ResourceType = typeof(Resource))]
        [MaxLength(150, ErrorMessageResourceName = "LargoSuperado", ErrorMessageResourceType = typeof(Resource))]
        public string Domicilio { get; set; }

        [Display(Name = "InversoresTelefonoFijo", ResourceType = typeof(Resource))]
        [MaxLength(50, ErrorMessageResourceName = "LargoSuperado", ErrorMessageResourceType = typeof(Resource))]
        public string TelefonoFijo { get; set; }

        [Display(Name = "InversoresTelefonoMovil", ResourceType = typeof(Resource))]
        [MaxLength(50, ErrorMessageResourceName = "LargoSuperado", ErrorMessageResourceType = typeof(Resource))]
        public string TelefonoMovil { get; set; }

        [Display(Name = "InversoresEmail", ResourceType = typeof(Resource))]
        [MaxLength(150, ErrorMessageResourceName = "LargoSuperado", ErrorMessageResourceType = typeof(Resource))]
        public string Email { get; set; }

        [Display(Name = "InversoresClasificacion", ResourceType = typeof(Resource))]        
        public int Clasificacion { get; set; }

        
        public int Estado { get; set; }

        public int IdTasa { get; set; }

        public bool Activo { get; set; }




    }
}
