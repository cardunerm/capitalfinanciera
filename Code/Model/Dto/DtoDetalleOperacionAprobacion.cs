﻿using Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace Domine.Dto
{
    public partial class DtoDetalleOperacionAprobacion
    {
        public int Id { get; set; }
        public string Banco { get; set; }
        public string Estado { get; set; }

        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "CMC7")]
        public string CMC7 { get; set; }

        [MaxLength(4)]
        [StringLength(4, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "CP")]
        public string Cp { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Banco")]
        public int IdBanco { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Numero")]
        public string Numero { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Titular")]
        public string TitularCuenta { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Cuenta")]
        public string NumeroCuenta { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Sucursal")]
        public string SucursalEmision { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Vencimiento")]
        public System.DateTime FechaVencimiento { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Importe")]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Range(0, int.MaxValue)]
        public decimal Monto { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Importe")]
        //[Range(0,int.MaxValue)]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public string MontoStr
        {
            //get { return Monto.ToString(); }
            //set
            //{
            //    Monto = Convert.ToDecimal(value.Replace('.', ','));
            //}
             get; set; 
        }

        [Display(ResourceType = typeof(Resource), Name = "TasaAsignada")]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public decimal? Tasa { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "TasaAsignada")]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public string TasaStr { get; set; }




        [Display(ResourceType = typeof(Resource), Name = "MontoAprobado")]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Range(0, int.MaxValue)]
        public double? MontoAprobado { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Gasto")]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Range(0, int.MaxValue)]
        public decimal? Gastos { get; set; }


        [Display(ResourceType = typeof(Resource), Name = "Gasto")]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]       
        public string GastoStr { get; set; }


        [Display(ResourceType = typeof(Resource), Name = "DiasClearing")]
        public int? DiasClearing { get; set; }

        //[Display(ResourceType = typeof(Resource), Name = "DiasClearing")]
        //public int? DiasClearingHidden { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Observacion")]
        public string Observacion { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Estado")]
        public int IdEstado
        {
            get; set;
        }

        public string DetalleCalculo { get; set; }

        public string ImagenFront
        {
            get;
            set;
        }

        public string ImagenBack
        {
            get;
            set;
        }
    }
}
