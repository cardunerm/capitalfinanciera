﻿using System;

namespace Domine.Dto
{
    public class DtoCheque
    {
        public int IdCheque { get; set; }

        public string Numero { get; set; }

        public string BancoStr { get; set; }

        public DateTime FechaVencimiento { get; set; }

        public string FechaVencimientoStr
        {
            get
            {
                return FechaVencimiento.ToShortDateString();
            }
        }

        public decimal Monto { get; set; }

        public string EstadoStr { get; set; }

        public string ClasificacionStr { get; set; }
    }
}
