﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.Dto
{
    public class DtoOperacionCaja
    {
        [IDProperty]
        public int IdOperacionCaja { get; set; }

        public int IdCaja { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Monto")] 
        public double Monto { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "TipoMovimiento")] 
        public int TipoMovimiento { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(250)]
        [StringLength(250, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Descripcion")]
        public string Descripcion { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Fecha")]
        public DateTime Fecha { get; set; }
    }
}
