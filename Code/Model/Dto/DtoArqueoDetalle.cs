﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.Dto
{
    public class DtoArqueoDetalle
    {
        [IDProperty]
        public int IdArqueoDetalle { get; set; }

        public int IdArqueo { get; set; }

        public int IdCheque { get; set; }
    }
}
