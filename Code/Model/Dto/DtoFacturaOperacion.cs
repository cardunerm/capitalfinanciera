﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Domine.Dto
{
    public class DtoFacturaOperacion
    {

        public int IdFacturaOperacion { get; set; }

        [Display(Name = "Cliente")]
        public Nullable<int> IdFacturaCliente { get; set; }


        [Display(Name = "Vendedor")]
        public Nullable<int> IdFacturaVendedor { get; set; }

        [Display(Name = "Deudo")]
        public Nullable<int> IdFacturaDeudo { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(Name = "Número")]
        public string FacturaNumero { get; set; }

        [Display(Name = "Fecha")]
        public Nullable<System.DateTime> FacturaFecha { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(Name = "Monto")]
        public Nullable<decimal> FacturaMonto { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(Name = "Retención")]
        public Nullable<decimal> FacturaRetencion { get; set; }

        [Display(Name = "F. Cobro")]
        public Nullable<System.DateTime> FacturaFechaCobro { get; set; }

        [Display(Name = "Factura")]
        public HttpPostedFileBase FacturaImagen { get; set; }

        [Display(Name = "Estado")]
        public int FacturaOperacionEstado { get; set; }




        public Nullable<bool> FacturaActivo { get; set; }
        public Nullable<System.DateTime> FacturaFechaAlta { get; set; }
        public Nullable<int> FacturaIdUsuarioAlta { get; set; }
        public Nullable<System.DateTime> FacturaFechaUltimaActualizacion { get; set; }
        public Nullable<int> FacturaIdUsuarioUltimaActualizacion { get; set; }

        #region Gastos

        [Display(Name = "Escribania")]
        public string Escribania { get; set; }

        [Display(Name = "Transcipción")]
        public string Transcripcion { get; set; }

        [Display(Name = "Protocolización")]
        public string Protocolizacion { get; set; }

        [Display(Name = "Sello")]
        public string Sello { get; set; }

        [Display(Name = "Total Gasto")]
        public string TotalGasto { get; set; }

        [Display(Name = "Gasto Fijo")]
        public string GastoFijo { get; set; }

        [Display(Name = "Imp. Cheque")]
        public string ImpCheque { get; set; }

        [Display(Name = "G.Otorgamiento")]
        public string GastoOtorgamiento { get; set; }

        [Display(Name = "Tasa D.")]
        public string TasaDiaria { get; set; }

        [Display(Name = "Demora")]
        public string DemoraPosible { get; set; }

        //[Display(Name = "Días Demora")]
        //public int DiasDemora { get; set; }

        [Display(Name = "Inc. Gasto")]
        public string IncidenciaGasto { get; set; }

        [Display(Name = "Inc. Interes")]
        public string IncidenciaInteres { get; set; }

        [Display(Name = "Dias Demora")]
        public string DiasDemora { get; set; }

        [Display(Name = "Total Aforo")]
        public string TotalAforo { get; set; }

        #endregion

        #region Ficha

        [Display(Name = "Monto a Desembolsar")]
        public string SaldoInicial { get; set; }

        [Display(Name = "Intereses de la Operación")]
        public string Intereses { get; set; }








        [Display(Name = "Iva Interes")]
        public string Ivainteres { get; set; }

        [Display(Name = "G. Impuestos")]
        public string Impuestos { get; set; }

        [Display(Name = "G. Cesión")]
        public string GastoCesion { get; set; }

        [Display(Name = "G. Otorgamiento")]
        public string GastoOtorga { get; set; }

        [Display(Name = "Iva Gasto")]
        public string Ivagasto { get; set; }

        [Display(Name = "Int. Adic. 1er pago")]
        public string InteresAdicional1pago { get; set; }

        [Display(Name = "IVA Int. Adic. 1er pago")]
        public string IvainteresAdicional1pago { get; set; }

        [Display(Name = "Int. Adic. 2er pago")]
        public string InteresAdicional2pago { get; set; }

        [Display(Name = "IVA Int. Adic. 2er pago")]
        public string IvainteresAdicional2pago { get; set; }

        [Display(Name = "Saldo Cuenta")]
        public string Saldocuenta { get; set; }

        [Display(Name = "Importe Factura")]
        public string ImporteFactura { get; set; }

        [Display(Name = "Total Gastos")]
        public string Iva { get; set; }

        [Display(Name = "Garantía 1")]
        public string Garantia1 { get; set; }

        [Display(Name = "Garantía 2")]
        public string Garantia2 { get; set; }


        [Display(Name = "Garantía 3")]
        public string Garantia3 { get; set; }

        #endregion 
    }
}
