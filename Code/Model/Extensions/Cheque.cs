﻿using Enumerations;
using System.Text;

namespace Domine
{
    public partial class Cheque
    {
        // Métodos de extensión
        public string ClasificacionStr
        {
            get
            {
                switch (Clasificacion)
                {
                    case (int)ClasificacionCheque.Garantia:
                        return "Garantía";
                    case (int)ClasificacionCheque.Activo:
                        return "Operativo";
                    default:
                        return string.Empty;
                }
            }
        }

        public string EstadoStr
        {
            get
            {
                return ((Utils.Enumeraciones.Estados.eEstadosCheque)IdEstado).ToString();
            }
        }

        public string BancoStr
        {
            get
            {
                return this.Banco.Nombre;
            }
        }

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdCheque: " + IdCheque);
            fields.AppendLine("Numero: " + Numero);
            fields.AppendLine("Titular Cuenta: " + TitularCuenta);
            fields.AppendLine("Nro Cuenta: " + NroCuenta);
            fields.AppendLine("Sucursal Emision: " + SucursalEmision);
            fields.AppendLine("Fecha Vencimiento: " + FechaVencimiento.ToString("dd/MM/yyyy HH:mm:ss"));
            fields.AppendLine("Monto: " + Monto);
            fields.AppendLine("Dias Clearing: " + DiasClearing);
            fields.AppendLine("Estado: " + IdEstado);

            return fields.ToString();
        }

        public override bool Equals(object obj)
        {
            return true;
            //return (obj as Cheque).IdCheque == IdCheque;
        }

        public string MontoStr
        {
            get
            {
                return "$" + Monto;
            }
        }
    }
}