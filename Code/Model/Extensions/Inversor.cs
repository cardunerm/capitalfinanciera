﻿using System.Text;


namespace Domine
{
    public partial class Inversor
    {
        // Métodos de extensión

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdInversores: " + IdInversor);
            fields.AppendLine("Nombre: " + Nombre);
            fields.AppendLine("CUIT: " + CUIT);
            fields.AppendLine("Domicilio: " + Domicilio);
            fields.AppendLine("Telefono Fijo: " + TelefonoFijo);
            fields.AppendLine("Telefono Movil: " + TelefonoMovil);
            fields.AppendLine("Email: " + Email);            
            fields.AppendLine("Clasificacion: " + Clasificacion);
            fields.AppendLine("Estado: " + Estado);
            

            return fields.ToString();
        }

        public string NameAndCuit { get { return "( " + CUIT + ") - " + Nombre; } }

       
    }
}