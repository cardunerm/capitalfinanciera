﻿using Domine.Dto;
using Utils.Enumeraciones;
using System;
namespace Domine
{
    public partial class Operacion
    {
        public DtoOperacion GetDtoOperacion()
        {
            DtoOperacion ret = new DtoOperacion();
            ret.Cliente = Cliente.NameAndCuit;
            ret.Fecha = Fecha;
            ret.Observacion = Observacion;
            ret.ObservacionEvaluacion = ObservacionEvaluacion;
            ret.EstadoStr = ((Estados.eEstadosOp)Estado).GetEnumeracionName();
            return ret;
        }
    }
}
