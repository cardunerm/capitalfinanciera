﻿using System.Text;

namespace Domine
{
    public partial class Feriado
    {
        // Métodos de extensión
        public string FechaStr
        {
            get
            {
                return Fecha.ToString("dd/MM/yyyy");
            }
        }
        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdFeriado: " + IdFeriado);
            fields.AppendLine("Fecha: " + Fecha.ToString("dd/MM/yyyy HH:mm:ss"));
            fields.AppendLine("Descripcion: " + Descripcion);
            fields.AppendLine("Estado: " + Estado);

            return fields.ToString();
        }
    }
}