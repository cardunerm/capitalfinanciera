﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.Extensions
{
    public class ViewBandejaOperacion
    {

        public int  IdBandejaOperacion { get; set; }
        public DateTime Fecha { get; set; }
        public string NombreInversor { get; set; }
        public decimal Tasa { get; set; }
        public int TipoMoneda { get; set; }
        public int TipoMovimiento { get; set; }
        public int  FormaPago { get; set; }
        public decimal Monto { get; set; }
        public string Observacion { get; set; }




    }
}
