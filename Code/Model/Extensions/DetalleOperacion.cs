﻿using Utils.Enumeraciones;
using System;
namespace Domine
{
    public partial class DetalleOperacion
    {
        public string EstadoStr
        {
            get
            {
                return ((Estados.eEstadosOp)Estado).GetEnumeracionName(); 
            }
        }
    }
}
