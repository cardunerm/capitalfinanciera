﻿using Domine.Dto;
using System.Linq;
using Utils.Enumeraciones;
using System;
namespace Domine
{
    public partial class Caja
    {
        public DtoCaja GetDtoOperacion()
        {
            DtoCaja ret = new DtoCaja();
            ret.FechaApertura = FechaApertura;
            ret.FechaCierre = FechaCierre;
            ret.MontoInicial = MontoInicial.ToString();
            ret.MontoFinal = MontoFinal.ToString();
            return ret;
        }

        public decimal TotalEgresos
        {
            get
            {
                return OperacionCaja.Where(p => p.TipoMovimiento == 2).Sum(p => p.Monto);
            }
        }

        public decimal TotalIngresos
        {
            get
            {
                return OperacionCaja.Where(p => p.TipoMovimiento == 1).Sum(p => p.Monto);
            }
        }

        public decimal Total
        {
            get
            {
                return MontoInicial - OperacionCaja.Where(p => p.TipoMovimiento == 2).Sum(p => p.Monto) + OperacionCaja.Where(p => p.TipoMovimiento == 1).Sum(p => p.Monto);
            }
        }
    }
}