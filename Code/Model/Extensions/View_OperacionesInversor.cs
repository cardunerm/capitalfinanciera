﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Utils.Enumeraciones.Estados;

namespace Domine
{ 
    public partial class View_OperacionesInversor
    {
        public string TipoMonedaStr
        {
            get
            {
                return ((TipoMoneda)TipoMoneda).ToString();
            }
        }

        public string CuentaCorrienteStr {
            get
            {
                string _tipo = TipoMoneda == 0 ? "(USD) " : "(ARS) ";
                return _tipo + IdCuentaCorrienteInversor.ToString("000-0000000");
            }
        }

        public string TipoMovimientoStr
        {
            get
            {
                return ((mTipoMovimientoCuentaCorriente)TipoMovimiento).ToString();
            }
        }

        public string CondicionStr
        {
            get
            {
                return ((Condicion)Condicion).ToString();
            }
        }

        public string EstadoStr
        {
            get
            {
                return ((eEstadosOp)Estado).ToString();
            }
        }

        public string ClasificacionMovimientoStr
        {
            get
            {
                return ((ClasificacionMovimiento)ClasificacionMovimiento).ToString();
            }
        }

        public string TasaAplicadaStr
        {
            get
            {
                return TasaAplicada + "%"; 
            }
        }

        public string MontoStr
        {
            get
            {
                return "$" + Monto;
            }
        }
    }
}
