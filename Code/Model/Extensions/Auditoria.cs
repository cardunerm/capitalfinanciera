﻿using Enumerations;

namespace Domine
{
    public partial class Auditoria
    {
        public string UsuarioNombreUsuario
        {
            get
            {
                if (Usuario != null)
                {
                    return Usuario.NombreUsuario;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string AccionNombre
        {
            get
            {
                return DCREnumerationsHelper.GetAuditActionName((AuditAction)Accion);
            }
        }

        public string ModuloNombre
        {
            get
            {
                if (Modulo != null)
                {
                    return Modulo.Nombre;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
}