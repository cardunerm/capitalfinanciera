﻿using System.Text;

namespace Domine
{
    public partial class Banco
    {
        // Métodos de extensión

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdBanco: " + IdBanco);
            fields.AppendLine("Nombre: " + Nombre);
            fields.AppendLine("Contacto: " + Contacto);
            fields.AppendLine("Telefono: " + Telefono);
            fields.AppendLine("Activo: " + Activo);
            fields.AppendLine("Direccion: " + Direccion);
            fields.AppendLine("Observaciones: " + Observaciones);
            fields.AppendLine("Estado: " + Estado);

            return fields.ToString();
        }
    }
}