﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
    public partial class NegociacionDetalle
    {

        public string NombreBanco
        {
            get { return this.Cheque.Banco.Nombre; }       

        }


        public string Numero
        {
            get { return this.Cheque.Numero; }
        }

        public decimal Monto
        {
            get { return this.Cheque.Monto; }
        
        }

        public DateTime FechaVencimiento
        {
            get { return this.Cheque.FechaVencimiento; }

        }

        public string Estado
        {
            get { return this.Cheque.EstadoStr; }

        }


    }
}
