﻿using Luma.Utils.Security;
using System;
using System.Web;
using Utils.Enumeraciones;

namespace Domine
{
    public partial class ViewChequeRechazado
    {
        public string EstadoStr
        {
            get
            {
                return ((Estados.eEstadosCheque)Estado).GetEnumeracionName();
            }
        }
    }
}
