﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Enumeraciones;

namespace Domine
{
    public partial class Arqueo
    {
        public string EstadoStr
        {
            get
            {
                return ((Estados.eEstadosArqueo)Estado).GetEnumeracionName();
            }
        }
    }
}
