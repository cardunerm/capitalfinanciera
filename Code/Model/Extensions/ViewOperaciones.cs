﻿using Luma.Utils.Security;
using System;
using System.Web;
using Utils.Enumeraciones;

namespace Domine
{
    public partial class ViewOperaciones
    {
        public string EstadoStr
        {
            get
            {
                return ((Estados.eEstadosOp)Estado).GetEnumeracionName();
            }
        }

        public string IDKey
        {
            get
            {
                return StringCipher.Encrypt(Id.ToString(), HttpContext.Current.Session["Key"].ToString());
            }
        }
    }
}
