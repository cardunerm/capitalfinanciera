﻿using System.Text;

namespace Domine
{
    public partial class Sociedad
    {
        // Métodos de extensión

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdSociedad: " + IdSociedad);
            fields.AppendLine("Nombre: " + Nombre);
            fields.AppendLine("CUIT: " + CUIT);
            fields.AppendLine("Estado: " + Estado);

            return fields.ToString();
        }
    }
}