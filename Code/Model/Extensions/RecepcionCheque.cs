﻿using System.Text;

namespace Domine
{
    public partial class RecepcionCheque
    {
        // Métodos de extensión

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdRecepcionCheque: " + IdRecepcionCheque);
            fields.AppendLine("Cliente: " + IdCliente);
            fields.AppendLine("Fecha: " + Fecha.ToString("dd/MM/yyyy HH:mm:ss"));
            fields.AppendLine("Estado: " + Estado);

            return fields.ToString();
        }
    }
}