﻿using System.Text;

namespace Domine
{
    public partial class CuentaCorriente
    {       
        // Métodos de extensión
        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdCuentaCorriente: " + IdCuentaCorriente);
            fields.AppendLine("IdCliente: " + IdCliente);
            fields.AppendLine("Estado: " + Estado);

            return fields.ToString();
        }
    }
}