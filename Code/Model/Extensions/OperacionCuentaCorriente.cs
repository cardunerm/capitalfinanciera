﻿using System.Text;
using Utils.Enumeraciones;
using System;
namespace Domine
{
    public partial class OperacionCuentaCorriente
    {
        // Métodos de extensión

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdOperacionCuentaCorriente: " + IdOperacionCuentaCorriente);
            fields.AppendLine("Monto: " + Monto);
            fields.AppendLine("TipoMoviemiento: " + ((Estados.mTipoMovimientoCuentaCorriente)TipoMovimiento).GetEnumeracionName());
            fields.AppendLine("Fecha: " + Fecha.ToString("dd/MM/yyyy HH:mm:ss"));
            fields.AppendLine("Descripcion: " + Descripcion);
            fields.AppendLine("Estado: " + Estado);

            return fields.ToString();
        }
    }
}