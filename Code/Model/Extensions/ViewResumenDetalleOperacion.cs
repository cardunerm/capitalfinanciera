﻿using Luma.Utils.Security;
using System;
using System.Web;
using Utils.Enumeraciones;

namespace Domine
{
    public partial class ViewResumenDetalleOperacion
    {
        public string EstadoStr
        {
            get
            {
                return ((Estados.eEstadosOpDetalle)Estado).GetEnumeracionName();
            }
        }

        public string IDKey
        {
            get
            {
                return StringCipher.Encrypt(Id.ToString(), HttpContext.Current.Session["Key"].ToString());
            }
        }

        public string ImagenFront
        {
            get
            {
                return imgFron != null && imgFron != "no-Imagen.png" ? "/UploadImages/Cheques/" + imgFron : null;
            }
        }

        public string ImagenBack
        {
            get
            {
                return imgBack != null && imgBack != "no-Imagen.png" ? "/UploadImages/Cheques/" + imgBack : null;
            }
        }
    }
}