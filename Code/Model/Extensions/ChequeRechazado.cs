﻿using System.Text;

namespace Domine
{
    public partial class ChequeRechazado
    {
        // Métodos de extensión

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdChequeRechazado: " + IdChequeRechazado);
            fields.AppendLine("Motivo: " + Motivo);
            fields.AppendLine("Gasto: " + Gasto);
            fields.AppendLine("Fecha: " + Fecha.ToString("dd/MM/yyyy HH:mm:ss"));
            fields.AppendLine("Estado: " + Estado);

            return fields.ToString();
        }
    }
}