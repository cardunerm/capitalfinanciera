﻿using Luma.Utils.Security;
using System;
using System.Web;
using Utils.Enumeraciones;

namespace Domine
{
    public partial class ViewCheque
    {
        public string EstadoStr
        {
            get
            {
                return ((Estados.eEstadosCheque)IdEstado).GetEnumeracionName();
            }
        }
    }
}
