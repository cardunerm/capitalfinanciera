﻿using System.Text;

namespace Domine
{
    public partial class EmisorValor
    {
        // Métodos de extensión

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdEmisorValor: " + IdEmisorValor);
            fields.AppendLine("Numero de Emisor: " + NumeroEmisor);
            fields.AppendLine("CUIT: " + CUIT);
            fields.AppendLine("Clasificacion: " + Clasificacion);
            fields.AppendLine("Tasa Asignada: " + TasaAsignada);
            fields.AppendLine("Observaciones: " + Observaciones);
            fields.AppendLine("Estado: " + Estado);

            return fields.ToString();
        }
    }
}