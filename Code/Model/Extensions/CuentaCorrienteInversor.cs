﻿using System.Text;

namespace Domine
{
    public partial class CuentaCorrienteInversor
    {
        // Métodos de extensión
        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdCuentaCorriente: " + IdCuentaCorrienteInversor);
            fields.AppendLine("IdCliente: " + IdInversor);
            fields.AppendLine("Estado: " + Estado);

            return fields.ToString();
        }

        public string NumeroCtaCteStr
        {
            get
            {
                return ((TipoMoneda == 0)? "002-" : "001-") + IdCuentaCorrienteInversor.ToString("0000000");
            }
        }
    }
}