﻿

using System;
using System.Collections.Generic;

namespace Domine
{
    public partial class ReporteTest
    {

        public DateTime Fecha { get; set; }

        public decimal Saldo_Inicial { get; set; }

        public decimal Interes
        {
            get
            {

                return 5;

            }

        }

        public decimal Credito
        {
            get
            {

                return 1000;

            }

        }

        public decimal InteresDiario
        {
            get
            {
                return Interes / 100 * Credito;
            }

        }

        public decimal Debito
        {
            get
            {
                return 0;
            }
        }

        public decimal GananciaDiaria
        {
            get
            {
                return InteresDiario + Credito;
            }
        }
        

        public decimal SaldoFinal
        {

            get
            {

                DateTime inicio = new DateTime(1, 4, 2017);
                DateTime final = new DateTime(30, 4, 2017);


                
                                    
                    for (DateTime i =inicio; i<final;i=i.AddDays(1))
                    {
                    i = Fecha;
                    decimal saldofinal = GananciaDiaria+Credito;

                    }

                return SaldoFinal;

            }
            }
        
        public string Inversor { get; set; }

    }
}
