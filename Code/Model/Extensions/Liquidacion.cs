﻿using System.Text;

namespace Domine
{
    public partial class Liquidacion
    {
        // Métodos de extensión

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdLiquidacion: " + IdLiquidacion);
            fields.AppendLine("Numero: " + Numero);
            fields.AppendLine("Dia Liquidacion: " + DiaLiquidacion);
            fields.AppendLine("Estado: " + Estado);
            fields.AppendLine("Facturacion: " + Facturacion);

            return fields.ToString();
        }
    }
}