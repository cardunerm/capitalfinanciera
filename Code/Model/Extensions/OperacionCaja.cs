﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
    public partial class OperacionCaja
    {
        public string TipoMovimientoStr
        {
            get
            {
                if (TipoMovimiento == 1)
                {
                    return "Ingreso";
                }
                else
                {
                    return "Egreso";
                }
            }
        }
    }
}
