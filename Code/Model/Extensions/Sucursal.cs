﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
    public partial class Sucursal
    {
        //public string EmpresaStr
        //{
        //    get
        //    {
        //        return Empresa.Nombre;
        //    }
        //}

        public string GetFieldsAsText()
        {
            StringBuilder fields = new StringBuilder();

            fields.AppendLine("IdSucursal: " + IdSucursal);
            fields.AppendLine("Descripcion: " + Descripcion);
            fields.AppendLine("Numero: " + Numero);
            fields.AppendLine("IdEmpresa: " + IdEmpresa);

            return fields.ToString();
        }
    }
}
