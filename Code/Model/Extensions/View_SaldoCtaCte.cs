﻿namespace Domine
{
    public partial class View_SaldoCtaCte
    {
        public decimal Saldo
        {
            get
            {
                return Credito.Value - Debito.Value;
            }
        }
    }
}
