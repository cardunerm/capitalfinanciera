//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domine
{
    using Domine.PartialModel;
    using System;
    using System.Collections.Generic;
    
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(FacturaClienteMetadata))]
    public partial class FacturaCliente
    {
        public FacturaCliente()
        {
            this.FacturaOperacion = new HashSet<FacturaOperacion>();
        }
    
        public int IdFacturaCliente { get; set; }
        public string Nombre { get; set; }
        public string CUIT { get; set; }
        public string Domicilio { get; set; }
        public string TelefonoFijo { get; set; }
        public string TelefonoMovil { get; set; }
        public string Email { get; set; }
        public decimal Tasa { get; set; }
        public Nullable<bool> Activo { get; set; }
    
        public virtual ICollection<FacturaOperacion> FacturaOperacion { get; set; }
    }
}
