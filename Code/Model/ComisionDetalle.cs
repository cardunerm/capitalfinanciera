//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domine
{
    using Domine.PartialModel;
    using System;
    using System.Collections.Generic;
    
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ComisionDetalleMetadata))]
    public partial class ComisionDetalle
    {
        public int IdComisionDetalle { get; set; }
        public Nullable<int> IdFacturaVendedor { get; set; }
        public Nullable<int> IdComision { get; set; }
        public Nullable<int> IdFactura { get; set; }
        public Nullable<int> IdFacturaCobro { get; set; }
        public Nullable<int> IdOperacion { get; set; }
        public Nullable<decimal> MontoCobrado { get; set; }
        public Nullable<decimal> MontoComisionado { get; set; }
        public Nullable<bool> Activo { get; set; }
    
        public virtual Comision Comision { get; set; }
        public virtual FacturaVendedor FacturaVendedor { get; set; }
    }
}
