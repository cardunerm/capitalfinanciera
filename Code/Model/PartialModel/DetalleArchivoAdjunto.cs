﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
    public class DetalleArchivoAdjuntoMetadata
    {
        [IDProperty]
        public int Id { get; set; }
        public string FullPathArchivoAdjunto { get; set; }
        public int IdEntidadRegistro { get; set; }
        public int IdCabeceraAdjuntos { get; set; }
        public string Nombre { get; set; }
        public int Tamaño { get; set; }

        public virtual CabeceraAdjuntos CabeceraAdjuntos { get; set; }
    }
}
