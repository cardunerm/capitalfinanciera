﻿
namespace Domine
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Domine.Attributes;
    using Resources;
    using Newtonsoft.Json;

    public partial class SociedadMetadata
    {
        [IDProperty]
        public int IdSociedad { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(11)]
        [StringLength(11, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "CUIT")]
        public string CUIT { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Cliente")]
        public int IdCliente { get; set; }

        [StatusProperty]
        public int Estado { get; set; }

    }
}