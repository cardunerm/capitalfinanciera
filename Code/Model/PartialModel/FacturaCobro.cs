﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.PartialModel
{
    public class FacturaCobroMetadata
    {
        [IDProperty]
        public int IdFacturaCobro { get; set; }
        public Nullable<int> IdFacturaOperacion { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public Nullable<decimal> Monto { get; set; }
        public Nullable<bool> Activo { get; set; }
        public Nullable<int> IdUsuarioAlta { get; set; }
        public Nullable<System.DateTime> FechaAlta { get; set; }

        public virtual FacturaOperacion FacturaOperacion { get; set; }
    }
}
