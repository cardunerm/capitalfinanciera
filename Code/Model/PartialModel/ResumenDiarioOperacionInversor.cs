﻿using System;
using System.Collections.Generic;

namespace Domine
{
    public partial class ResumenDiarioOperacionInversorMetadata
    {
        public int IdResumen { get; set; }
        public System.DateTime Fecha { get; set; }
        public Nullable<decimal> TotalIngresos { get; set; }
        public Nullable<decimal> TotalEgresos { get; set; }
        public Nullable<decimal> Intereses { get; set; }
        public Nullable<decimal> Impuestos { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> Saldo { get; set; }
        public Nullable<int> TipoMoneda { get; set; }
        public Nullable<int> IdCuentaCorrienteInversor { get; set; }
        public Nullable<decimal> Gastos { get; set; }
        public Nullable<decimal> Multas { get; set; }
        public Nullable<double> TasaInteres { get; set; }

        public virtual CuentaCorrienteInversor CuentaCorrienteInversor { get; set; }
        public virtual ICollection<OperacionInversor> OperacionInversor { get; set; }
    }
}
