﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domine
{
    public partial class CuentaCorrienteInversorMetadata
    {
        [IDProperty]
        public int IdCuentaCorrienteInversor { get; set; }
        public Nullable<int> IdInversor { get; set; }
        public Nullable<bool> Estado { get; set; }
        public int TipoMoneda { get; set; }
        public Nullable<System.DateTime> FechaAlta { get; set; }
        public int ClasificacionInversor { get; set; }

        public virtual Inversor Inversor { get; set; }
        public virtual ICollection<OperacionInversor> OperacionInversor { get; set; }
        public virtual ICollection<ResumenDiarioOperacionInversor> ResumenDiarioOperacionInversor { get; set; }
    }
}
