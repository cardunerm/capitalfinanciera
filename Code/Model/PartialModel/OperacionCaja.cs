﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
   public class OperacionCajaMetadata
    {
        [IDProperty]
        public int IdOperacionCaja { get; set; }
        public int IdCaja { get; set; }
        public System.DateTime Fecha { get; set; }
        public int TipoMovimiento { get; set; }
        public float Monto { get; set; }
        public string Descripcion { get; set; }
    }
}
