﻿using Domine.Attributes;
using System;

namespace Domine
{
    public partial class View_OperacionesInversorMetadata
    {
        [IDProperty]
        public int IdOperacionInversor { get; set; }
        public Nullable<int> TipoMovimiento { get; set; }
        public System.DateTime Fecha { get; set; }
        public int IdInversor { get; set; }
        public string CuitInversor { get; set; }
        public string Inversor { get; set; }
        public decimal Monto { get; set; }
        public int TipoMoneda { get; set; }
        public int Condicion { get; set; }
        public int IdCuentaCorrienteInversor { get; set; }
        public int IdHistorial { get; set; }
        public Nullable<decimal> TasaAplicada { get; set; }
        public int Estado { get; set; }
        public string Observaciones { get; set; }
        public int ClasificacionMovimiento { get; set; }
    }
}
