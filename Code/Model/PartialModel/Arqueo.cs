﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
    public class ArqueoMetadata
    {
        [IDProperty]
        public int IdArqueo { get; set; }
        public int IdEmpresa { get; set; }
        public int IdSucursal { get; set; }
        public int IdUsuario { get; set; }
        public System.DateTime Fecha { get; set; }
        public float Monto { get; set; }
        public int Estado { get; set; }
        public int CantidadCheques { get; set; }
        public string Descripcion { get; set; }
    }
}
