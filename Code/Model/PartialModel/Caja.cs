﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
    public class CajaMetadata
    {
        [IDProperty]
        public int IdCaja { get; set; }
        public int IdEmpresa { get; set; }
        public int IdSucursal { get; set; }
        public int IdUsuario { get; set; }
        public System.DateTime FechaApertura { get; set; }
        public System.DateTime FechaCierre { get; set; }
        public float MontoInicial { get; set; }
        public float MontoFinal { get; set; }
    }
}
