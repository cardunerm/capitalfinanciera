
namespace Domine
{
    using Domine.Attributes;
    using System.ComponentModel.DataAnnotations;

	public class CuentaCorrienteMetadata
	{
        [IDProperty]
        public long IdCuentaCorriente { get; set; }
        public int IdCliente { get; set; }
        public int Estado { get; set; }
        
    }
}