
namespace Domine
{
    using Domine.Attributes;
    using System.ComponentModel.DataAnnotations;

	public partial class TasaMetadata
	{
        [IDProperty]
        public int Id { get; set; }
    }
}