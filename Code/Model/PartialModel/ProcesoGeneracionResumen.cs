﻿using System;

namespace Domine
{
    public partial class ProcesoGeneracionResumenMetadata
    {
        public int IdProceso { get; set; }
        public System.DateTime FechaInicio { get; set; }
        public System.DateTime FechaFin { get; set; }
        public DateTime? FechaCorrida { get; set; }
    }
}
