﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domine
{
    public partial class OperacionInversorMetadata
    {
        [IDProperty]
        public int IdOperacionInversor { get; set; }
        public Nullable<long> IdUsuario { get; set; }
        public System.DateTime Fecha { get; set; }
        [Display(ResourceType = typeof(Resource), Name = "Observacion")]
        public string Observacion { get; set; }
        public int Estado { get; set; }
        public Nullable<int> IdEmpresa { get; set; }
        public Nullable<decimal> Monto { get; set; }
        public Nullable<int> IdSucursal { get; set; }
        [Display(ResourceType = typeof(Resource), Name = "CuentaCorriente")]
        public Nullable<int> IdCuentaCorrienteInversor { get; set; }
        public Nullable<int> TipoMovimiento { get; set; }
        public Nullable<int> IdHistorialTasaInteresInversorAplicado { get; set; }
        public int Condicion { get; set; }
        public int ClasificacionMovimiento { get; set; }
        public Nullable<int> IdResumen { get; set; }
        public bool EsOperacionGarantia { get; set; }

        public virtual CuentaCorrienteInversor CuentaCorrienteInversor { get; set; }
        public virtual ICollection<DetalleOperacion> DetalleOperacion { get; set; }
        public virtual Empresa Empresa { get; set; }
        public virtual HistorialTasaInteresInversor HistorialTasaInteresInversor { get; set; }
        public virtual ResumenDiarioOperacionInversor ResumenDiarioOperacionInversor { get; set; }
        public virtual Sucursal Sucursal { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
