﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.PartialModel
{
    public partial class FacturaTasaMetadata
    {
        [IDProperty]
        public int IdFacturaTasa { get; set; }
    }
}
