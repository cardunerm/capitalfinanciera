﻿using Domine.Attributes;

namespace Domine
{
    public class OperacionMetadata
    {
        [IDProperty]
        public long Id { get; set; }
        public int IdCliente { get; set; }
        public System.DateTime Fecha { get; set; }    
        public string Observacion { get; set; }
    }
}
