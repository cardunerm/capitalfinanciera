﻿
namespace Domine
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Domine.Attributes;
    using Resources;
    using Newtonsoft.Json;

    public partial class ChequeRechazadoMetadata
    {
        [IDProperty]
        public int IdChequeRechazado { get; set; }

        public int IdCheque { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(250)]
        [StringLength(250, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]

        [Display(ResourceType = typeof(Resource), Name = "Motivo")]
        public string Motivo { get; set; }
        
        [Display(ResourceType = typeof(Resource), Name = "Gasto")]
        public double Gasto { get; set; }
              
        public System.DateTime Fecha { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Impuesto")]
        public double? Impuesto { get; set; }

        [Display(ResourceType = typeof(Resource), Name = "Multa")]
        public double? Multa { get; set; }

        [StatusProperty]
        public int Estado { get; set; }

    }
}