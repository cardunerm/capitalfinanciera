﻿using System;

namespace Domine
{
    public partial class HistorialTasaInteresInversorMetadata
    {
        public int IdHistorial { get; set; }
        public Nullable<int> TipoMonedaTasa { get; set; }
        public Nullable<decimal> ValorTasa { get; set; }
        public Nullable<System.DateTime> FechaDesde { get; set; }
        public System.DateTime FechaHasta { get; set; }
        public int IdInversor { get; set; }
    }
}
