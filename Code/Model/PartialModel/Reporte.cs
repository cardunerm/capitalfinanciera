﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.PartialModel
{
    public class Reporte
    {

        public string NombreInversor { get; set; }

        public DateTime Fecha { get; set; }

        public float MontoIncial { get; set; }              

        public float Ingresos { get; set; }

        public float Tasa { get; set; }

        public float GananciaDia { get; set; }

        public float Egresos { get; set; }       

        public float SaldoFinal { get; set; }

    }
}
