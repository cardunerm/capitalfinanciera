﻿
namespace Domine
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Domine.Attributes;
    using Resources;
    using Newtonsoft.Json;

    public partial class PerfilMetadata
    {
        [IDProperty]
        public long IdPerfil { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [Display(ResourceType = typeof(Resource), Name = "Nombre")]
        public string Nombre { get; set; }

        public System.DateTime FechaCreacion { get; set; }
        public Nullable<System.DateTime> FechaEdicion { get; set; }

        [StatusProperty]
        public int Estado { get; set; }

        [JsonIgnore]
        public virtual ICollection<PerfilModulo> PerfilModulo { get; set; }
        [JsonIgnore]
        public virtual ICollection<PerfilValidacion> PerfilValidacion { get; set; }
        [JsonIgnore]
        public virtual ICollection<UsuarioPerfil> UsuarioPerfil { get; set; }
    }
}