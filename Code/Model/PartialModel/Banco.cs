﻿
namespace Domine
{
    using System.ComponentModel.DataAnnotations;
    using Domine.Attributes;
    using Resources;
    using Newtonsoft.Json;

    public partial class BancoMetadata
    {
        [IDProperty]
        public int IdBanco { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Nombre")]
        public string Nombre { get; set; }

        //public System.DateTime FechaCreacion { get; set; }
        //public Nullable<System.DateTime> FechaEdicion { get; set; }

        [StatusProperty]
        public bool Activo { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Codigo")]
        public string Codigo { get; set; }

    }
}