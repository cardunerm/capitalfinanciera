﻿
namespace Domine
{
    using System;
    using Domine.Attributes;
    using Newtonsoft.Json;

    public partial class AuditoriaMetadata
    {
        [IDProperty]
        public long IdAuditoria { get; set; }        
        [JsonIgnore]
        public virtual Modulo Modulo { get; set; }
        [JsonIgnore]
        public virtual Usuario Usuario { get; set; }
    }
}