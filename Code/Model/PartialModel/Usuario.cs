﻿namespace Domine
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Domine.Attributes;
    using Resources;
    using Newtonsoft.Json;

    public partial class UsuarioMetadata
    {
        [IDProperty]
        public int IdUsuario { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Apellido")]
        public string Apellido { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(20)]
        [StringLength(20, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "NombreUsuario")]
        public string NombreUsuario { get; set; }

        //[Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(32)]
        [StringLength(32, MinimumLength = 6, ErrorMessageResourceName = "MinPassword", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Contrasenia")]
        [DataType(DataType.Password)]
        public string Contrasenia { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(150)]
        [StringLength(150, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Email")]
        [EmailAddress(ErrorMessageResourceName = "EmailRequerido", ErrorMessageResourceType = typeof(Resource), ErrorMessage = null)]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Empresa")]
        public int IdEmpresa { get; set; }

        public System.DateTime FechaCreacion { get; set; }
        public Nullable<System.DateTime> FechaEdicion { get; set; }

        [StatusProperty]
        public int Estado { get; set; }

        [JsonIgnore]
        public virtual ICollection<Auditoria> Auditoria { get; set; }
        [JsonIgnore]
        public ICollection<UsuarioPerfil> UsuarioPerfil { get; set; }
        public virtual ICollection<Caja> Caja { get; set; }
        [JsonIgnore]
        public virtual ICollection<DetalleOperacion> DetalleOperacion { get; set; }
        [JsonIgnore]
        public virtual ICollection<Operacion> Operacion { get; set; }
        [JsonIgnore]
        public virtual ICollection<Operacion> Operacion1 { get; set; }
        [JsonIgnore]
        public virtual ICollection<Operacion> Operacion2 { get; set; }
    }
}
