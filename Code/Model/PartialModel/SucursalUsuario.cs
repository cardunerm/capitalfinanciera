﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
    public class SucursalUsuarioMetadata
    {
        [IDProperty]
        public int IdSucursalUsuario { get; set; }
        public int IdUsuario { get; set; }
        public int IdSucursal { get; set; }
    }
}
