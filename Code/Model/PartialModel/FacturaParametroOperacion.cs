﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.PartialModel
{
    public class FacturaParametroOperacionMetadata
    {
        [IDProperty]
        public int IdFacturaParametroOperacion { get; set; }
        public Nullable<int> IdFacturaOperacion { get; set; }
        public Nullable<int> IdFacturaParametro { get; set; }
        public Nullable<decimal> Valor { get; set; }
    }
}
