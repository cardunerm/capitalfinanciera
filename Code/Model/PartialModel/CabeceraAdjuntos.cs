﻿using Domine.Attributes;
using System.Collections.Generic;

namespace Domine
{
    public class CabeceraAdjuntosMetadata
    {
        [IDProperty]
        public int Id { get; set; }
        public int IdEntidadEnum { get; set; }
        public string PathRaiz { get; set; }

        public virtual ICollection<DetalleArchivoAdjunto> DetalleArchivoAdjunto { get; set; }
    }
}
