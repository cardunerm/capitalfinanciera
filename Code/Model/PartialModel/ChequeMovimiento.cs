namespace Domine
{
    using Attributes;
    using System.ComponentModel.DataAnnotations;

    public partial class ChequeMovimientoMetadata
	{
        [IDProperty]
        public int Id { get; set; }
    }
}