﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.PartialModel
{
    public class FacturaDeudoMetadata
    {
        [IDProperty]
        public int IdFacturaDeudo { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(100)]
        [StringLength(100, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Nombre")]
        public string Nombre { get; set; }

        [MaxLength(11)]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(Name = "Cuit")]
        public string CUIT { get; set; }
        public string Domicilio { get; set; }
        [Display(Name = "Teléfono fijo")]
        public string TelefonoFijo { get; set; }
        [Display(Name = "Teléfon movil")]
        public string TelefonoMovil { get; set; }
        [Display(Name = "Correo")]
        public string Email { get; set; }
        public Nullable<bool> Activo { get; set; }

        public virtual ICollection<FacturaOperacion> FacturaOperacion { get; set; }
    }
}
