﻿using Domine.Attributes;
using Resources;
using System.ComponentModel.DataAnnotations;

namespace Domine
{
    public partial class InversorMetadata
        
    {
        [IDProperty]
        public int IdInversor { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(11)]
        [StringLength(11, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
       // [RegularExpression(@"^[0-9]{11}$", ErrorMessageResourceName = "ValorIncorrecto", ErrorMessageResourceType = (typeof(Resource)))]
        [Display(ResourceType = typeof(Resource), Name = "CUIT")]
        public string CUIT { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(150)]
        [StringLength(150, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Domicilio")]
        public string Domicilio { get; set; }

        //[Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "TelefonoFijo")]
        public string TelefonoFijo { get; set; }

        //[Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "TelefonoMovil")]
        public string TelefonoMovil { get; set; }


        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(150)]
        [StringLength(150, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Email")]
        [EmailAddress(ErrorMessageResourceName = "EmailRequerido", ErrorMessageResourceType = typeof(Resource), ErrorMessage = null)]
        public string Email { get; set; }

        [Display(Name = "InversoresClasificacion", ResourceType = typeof(Resource))]        
        public int Clasificacion { get; set; }

        
        public int Estado { get; set; }

        [Display(Name = "InversoresTasa", ResourceType = typeof(Resource))]
        public decimal Tasa { get; set; }

        [Display(Name = "InversoresTasaUSD", ResourceType = typeof(Resource))]
        public decimal TasaUSD { get; set; }

        [Display(Name = "InversoresTasaMora", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public decimal TasaMora { get; set; }

        [Display(Name = "InversoresTasaMoraUSD", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public decimal TasaMoraUSD { get; set; }

        [Display(Name = "InversoreLimiteDescubierto", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public decimal LimiteDescubierto { get; set; }

        [Display(Name = "InversoreLimiteDescubiertoUSD", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        public decimal LimiteDescubiertoUSD { get; set; }
        public bool Activo { get; set; }

        [Display(Name = "InversorEnvioMail", ResourceType = typeof(Resource))]
        public bool EnvioMail { get; set; }

        [Display(Name = "Clasificacion", ResourceType = typeof(Resource))]
        public int ClasificacionInversor { get; set; }

        [Display(Name = "RelacionGarantia", ResourceType = typeof(Resource))]
        public decimal PorcentajeBalanceGarantiaOperativo { get; set; }
    }
}
