﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.PartialModel
{
    public class FacturaParametroMetadata
    {
        [IDProperty]
        public int IdFacturaParametro { get; set; }
        public Nullable<int> Codigo { get; set; }
        public string Nombre { get; set; }
        public Nullable<decimal> Importe { get; set; }
        public Nullable<bool> Activo { get; set; }
    }
}
