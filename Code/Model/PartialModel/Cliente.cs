﻿
namespace Domine
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Domine.Attributes;
    using Resources;
    using Newtonsoft.Json;

    public partial class ClienteMetadata
    {
        [IDProperty]
        public int IdCliente { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(11)]
        [StringLength(11, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        //[RegularExpression(@"^[0-9]{11}$", ErrorMessageResourceName = "ValorIncorrecto", ErrorMessageResourceType = (typeof(Resource)))]
        [Display(ResourceType = typeof(Resource), Name = "CUIT")]
        public string CUIT { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Domicilio")]
        public string Domicilio { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "TelefonoFijo")]
        public string TelefonoFijo { get; set; }

        //[Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "TelefonoMovil")]
        public string TelefonoMovil { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(150)]
        [StringLength(150, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Email")]
        [EmailAddress(ErrorMessageResourceName = "EmailRequerido", ErrorMessageResourceType = typeof(Resource), ErrorMessage = null)]
        public string Email { get; set; }
        
        [Display(ResourceType = typeof(Resource), Name = "LimiteDescubierto")]        
        public double LimiteDescubierto { get; set; }

        //[Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        //[Range(1, 31, ErrorMessageResourceName = "RangeGreaterThanValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Clasificacion")]
        public int Clasificacion { get; set; }

        [StatusProperty]
        public int Estado { get; set; }


        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "TasaAsignada")]
        public int IdTasa { get; set; }

    }
}