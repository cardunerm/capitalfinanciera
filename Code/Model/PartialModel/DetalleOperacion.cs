using System;

namespace Domine
{
    public partial class DetalleOperacionMetadata
	{
        public int Id { get; set; }
        public Nullable<int> IdCheque { get; set; }
        public short Estado { get; set; }
        public Nullable<decimal> MontoAprobado { get; set; }
        public Nullable<decimal> MontoAPagar { get; set; }
        public string Observacion { get; set; }
        public Nullable<decimal> TasaInteres { get; set; }
        public Nullable<int> DiasClering { get; set; }
        public Nullable<long> IdOperacion { get; set; }
        public Nullable<long> IdUsuario { get; set; }
        public decimal Gastos { get; set; }
        public decimal Impuestos { get; set; }
        public decimal ImpuestosCheque { get; set; }
        public Nullable<int> IdOperacionInversor { get; set; }
        public Nullable<int> IdResumen { get; set; }
    }
}