﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
    public partial class TasaInversoresMetadata
    {
        [IDProperty]
        public int IdTasaInversores { get; set; }

        public int IdInversores { get; set; }

        [Display(Name = "TasaInversoresTasa", ResourceType = typeof(Resource))]
        public decimal Tasa { get; set; }

        [Display(Name = "TasaInversoresFechaValidacion", ResourceType = typeof(Resource))]
        public System.DateTime FechaValidacion { get; set; }

        public bool Estado { get; set; }


    }
}
