﻿using Domine.Attributes;
using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.PartialModel
{
    public class FacturaOperacionMetadata
    {
        [IDProperty]
        public int IdFacturaOperacion { get; set; }

        [Display(Name = "Cliente")]
        public Nullable<int> IdFacturaCliente { get; set; }

        [Display(Name = "Deudo")]
        public Nullable<int> IdFacturaDeudo { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(Name = "Número")]
        public Nullable<long> FacturaNumero { get; set; }

        [Display(Name = "Fecha")]
        public Nullable<System.DateTime> FacturaFecha { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(Name = "Monto")]
        public Nullable<decimal> FacturaMonto { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(Name = "Retención")]
        public Nullable<decimal> FacturaRetencion { get; set; }

        [Display(Name = "F. Cobro")]
        public Nullable<System.DateTime> FacturaFechaCobro { get; set; }

        [Display(Name = "Factura")]
        public string FacturaImagen { get; set; }

        public Nullable<bool> FacturaActivo { get; set; }
        public Nullable<System.DateTime> FacturaFechaAlta { get; set; }
        public Nullable<int> FacturaIdUsuarioAlta { get; set; }
        public Nullable<System.DateTime> FacturaFechaUltimaActualizacion { get; set; }
        public Nullable<int> FacturaIdUsuarioUltimaActualizacion { get; set; }

        public virtual FacturaCliente FacturaCliente { get; set; }
        public virtual ICollection<FacturaCobro> FacturaCobro { get; set; }
        public virtual ICollection<FacturaDesembolso> FacturaDesembolso { get; set; }
        public virtual FacturaDeudo FacturaDeudo { get; set; }
    }
}
