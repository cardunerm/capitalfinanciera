
namespace Domine
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class RecepcionChequeMetadata
	{
        
        public int IdRecepcionCheque { get; set; }
        public System.DateTime Fecha { get; set; }
        public Nullable<int> IdCliente { get; set; }
        public int Estado { get; set; }
        public Nullable<int> IdInversor { get; set; }

        public virtual ICollection<Cheque> Cheque { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Inversor Inversor { get; set; }
    }
}