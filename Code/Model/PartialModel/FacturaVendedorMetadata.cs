﻿using Domine.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.PartialModel
{
    public class FacturaVendedorMetadata
    {
        [IDProperty]
        public int IdVendedor { get; set; }
    }
}
