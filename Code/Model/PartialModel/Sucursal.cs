﻿using Domine.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine
{
    public class SucursalMetadata
    {
        [IDProperty]
        public int IdSucursal { get; set; }
        [Display(Name = "Empresa")]
        public int IdEmpresa { get; set; }
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

        [Display(Name =  "Número")]
        public string Numero { get; set; }

        [StatusProperty]
        public int Estado { get; set; }

        [JsonIgnore]
        public virtual Empresa Empresa { get; set; }
    }
}
