﻿
namespace Domine
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Resources;
    using Newtonsoft.Json;
    using Attributes;

    public partial class FeriadoMetadata
    {
        [IDProperty]
        public int IdFeriado { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [MaxLength(50)]
        [StringLength(50, ErrorMessageResourceName = "MaxString", ErrorMessageResourceType = typeof(Resource))]
        [Display(ResourceType = typeof(Resource), Name = "Descripcion")]
        public string Descripcion { get; set; }

        [Required(ErrorMessageResourceName = "Requerido", ErrorMessageResourceType = typeof(Resource), AllowEmptyStrings = false)]
        [Display(ResourceType = typeof(Resource), Name = "Fecha")]
        public DateTime Fecha { get; set; }

        [StatusProperty]
        public int Estado { get; set; }

    }
}