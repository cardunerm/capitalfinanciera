
namespace Domine
{
    using Domine.Attributes;
    using System.ComponentModel.DataAnnotations;

	public partial class OperacionCuentaCorrienteMetadata
	{

        [IDProperty]
        public int IdOperacionCuentaCorriente { get; set; }
    }
}