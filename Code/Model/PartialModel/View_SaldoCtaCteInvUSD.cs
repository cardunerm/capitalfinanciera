﻿namespace Domine
{
    public partial class View_SaldoCtaCteInvUSD
    {
        public double Saldo
        {
            get
            {
                return Credito.Value - Debito.Value;
            }
        }
    }
}
