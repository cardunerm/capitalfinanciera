﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domine.PartialModel
{
    public partial class ViewSaldoInversor
    {

        public Nullable<decimal> Credito { get; set; }
        public Nullable<decimal> Debito { get; set; }
        public Nullable<decimal> Rechazado { get; set; }
        public Nullable<int> CantRechazados { get; set; }
        public int IdInversor { get; set; }

    }
}
