﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scanner.Interface;
using Luma.Utils;

namespace Testing
{
    [TestClass]
    public class ScannerTest
    {
        [TestMethod]
        public void Scan()
        {
            IScanner scanner = Scanner.Implementation.BaseScanner.GetInstance(null);
            var cheque = scanner.Scan();
        }

        [TestMethod]
        public void TestCalculoInteres()
        {
            decimal capital = 112639;
            decimal tasa = 2;
            decimal interes = tasa/100;
            int cantDias = 1;
            double result = UtilHelper.CalcularInteres(capital, interes, cantDias);
            decimal resultDecimal = Convert.ToDecimal(result);
        }
    }
}
