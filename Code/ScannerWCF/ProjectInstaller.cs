﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace ScannerWCF
{
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = "Scanner Service Financiera";
            Installers.Add(process);
            Installers.Add(service);
        }
    }
}
