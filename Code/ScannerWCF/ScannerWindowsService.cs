﻿using System.ServiceModel;
using System.ServiceProcess;

namespace ScannerWCF
{
    public class ScannerWindowsService : ServiceBase
    {

        public ServiceHost serviceHost = null;
        public ScannerWindowsService()
        {
            ServiceName = "Scanner Service Financiera";
        }
    
        public static void Main()
        {
            ServiceBase.Run(new ScannerWindowsService());
        }

        // Start the Windows service.
        protected override void OnStart(string[] args)
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
            }

            // Create a ServiceHost for the CalculatorService type and 
            // provide the base address.
            serviceHost = new ServiceHost(typeof(ScannerService));

            // Open the ServiceHostBase to create listeners and start 
            // listening for messages.
            serviceHost.Open();
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
        }
    }
}
