﻿using System;
using Scanner.Implementation;
using Scanner.Interface;

namespace ScannerWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class ScannerService : IScannerService
    {
        public Cheque Get()
        {
            IScanner scanner = BaseScanner.GetInstance(new Scanner.Utils.ConfigurationScanner());
            var cheque = scanner.Scan();
            scanner.Dispose();
            return new Cheque
            {
                ChequeResponse = cheque
            };
        }

        public Cheque Options()
        {
            return null;
        }
        
    }
}
