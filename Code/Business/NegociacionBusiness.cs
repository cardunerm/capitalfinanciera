﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DataAccess.Pages;
using Luma.Utils.Logs;
using CurrentContext;
using Utils.Enumeraciones;

namespace Business
{
    public class NegociacionBusiness : GenericBusiness<NegociacionBusiness, Negociacion>
    {

        public PageResult<View_Negociado> GetByFilters(string tiponegociacion, string banco, string mayorista, string proveedor, int skip, int top, ref int rowCount)
        {
            try
            {

                return DataAccessManager.GetByConditions<View_Negociado, DateTime>(
                       op => (tiponegociacion == "Indistinto" || op.TipoOperacion.ToUpper().Contains(tiponegociacion))
                             && (banco == "Indistinto" || op.Banco.ToUpper().Contains(banco))
                             && (mayorista == "Indistinto" || op.Mayorista.ToUpper().Contains(mayorista))
                             && (proveedor == "Indistinto" || op.Proveedor.ToUpper().Contains(proveedor))
                             && op.Estado != null
                             && op.Estado != "Cancelado"                          
                               , skip, top, op => (DateTime)op.Fecha, true);
                     
               
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }



        public bool Save(int IdNegociacion,Negociacion negociacion)
        {

            try
            {
                Negociacion ent = NegociacionBusiness.Instance.GetEntity(IdNegociacion);

                if (negociacion.IdBanco == -1)
                    ent.IdBanco = null;
                else
                    ent.IdBanco = negociacion.IdBanco;

                if (negociacion.IdMayorista == -1)
                    ent.IdMayorista = null;
                else
                    ent.IdMayorista = negociacion.IdMayorista;

                if (negociacion.IdProveedor == -1)
                    ent.IdProveedor = null;
                else
                    ent.IdProveedor = negociacion.IdProveedor;

                ent.IdTipoOperacion = negociacion.IdTipoOperacion;
                ent.Observacion = negociacion.Observacion;
                ent.IdEstado = (int)Enumerations.EstadoNegocion.Negociado;
                NegociacionBusiness.Instance.SaveOrUpdate(ent);

                foreach (var item in ent.NegociacionDetalle)
                {
                    item.IdEstado = (int)Enumerations.EstadoCheque.Negociado;
                    NegociacionDetalleBusiness.Instance.SaveOrUpdate(item);


                    item.Cheque.IdEstado = (int)Enumerations.EstadoCheque.Negociado;
                    ChequeBusiness.Instance.SaveOrUpdate(item.Cheque);
                }

                return true;
            }
            catch (Exception ex)
            {

                return false;
            }


        }



        public void Cerrar(string IdNegociacion, string Monto)
        {
            try
            {
                /*Cambiar estado de la negociacion y de los detalles-cheques*/
                Negociacion entNegociacion = this.GetEntity(long.Parse(IdNegociacion));
                entNegociacion.IdEstado = (int)Enumerations.EstadoNegocion.Cobrado;
                entNegociacion.TotalNegociado = decimal.Parse(Monto);

                foreach (var item in entNegociacion.NegociacionDetalle)
                {
                    item.IdEstado = (int)Enumerations.EstadoCheque.Cobrado;
                    item.Cheque.IdEstado = (int)Utils.Enumeraciones.Estados.eEstadosCheque.Cobrado;

                    NegociacionDetalleBusiness.Instance.SaveOrUpdate(item);
                    //ChequeBusiness.Instance.SaveOrUpdate(item.Cheque);

                    //Cambio es estado del cheque
                    ChequeBusiness.Instance.ChangeStatus((long)item.Cheque.IdCheque, Estados.eEstadosCheque.Cobrado);
                }

                /*Crear el movimiento de la caja*/
                OperacionCaja entity;
                Caja caja = CajaBusiness.Instance.GetUltimaCaja();

                
                entity = new OperacionCaja
                {
                    IdCaja = CajaBusiness.Instance.GetUltimaCaja().IdCaja,
                    TipoMovimiento = 1,
                    Monto = decimal.Parse(Monto),
                    Fecha = DateTime.Now,
                    Descripcion = "Ingreso por Operación N°:" + IdNegociacion,
                    Caja = caja
                };

                OperacionCajaBusiness.Instance.SaveOrUpdate(entity,null);


                /*Actualizo la negociación con el Id que se genero del ingreso.*/

                entNegociacion.IdOperacionCaja = entity.IdOperacionCaja;
                this.SaveOrUpdate(entNegociacion);

            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }



        }


        public void Cancelar(long IdNegociacion)
        {
            try
            {
                /*Cambiar estado de la negociacion y de los detalles-cheques*/
                Negociacion entNegociacion = this.GetEntity(IdNegociacion);
                entNegociacion.IdEstado = (int)Enumerations.EstadoNegocion.Cancelado;
                foreach (var item in entNegociacion.NegociacionDetalle)
                {
                    //Cambio es estado del cheque
                    ChequeBusiness.Instance.ChangeStatus((long)item.Cheque.IdCheque, Estados.eEstadosCheque.Cartera);
                }

               


                this.SaveOrUpdate(entNegociacion);
              

            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }

         }


        public void EliminarChequeSeleccionado(int IdNegociacionDetalle)
        {
            try
            {
                /*Cambiar estado de la negociacion y de los detalles-cheques*/
                var entND = NegociacionDetalleBusiness.Instance.GetEntity(long.Parse(IdNegociacionDetalle.ToString()));
                //entND.Cheque.IdEstado = (int)Utils.Enumeraciones.Estados.eEstadosCheque.Cartera;
                entND.IdEstado = (int)Enumerations.EstadoNegocion.Cancelado;
                NegociacionDetalleBusiness.Instance.SaveOrUpdate(entND);
                //ChequeBusiness.Instance.SaveOrUpdate(entND.Cheque);

                //Cambio es estado del cheque
                ChequeBusiness.Instance.ChangeStatus((long)entND.Cheque.IdCheque, Estados.eEstadosCheque.Cartera);

            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }




        }

    }
}
