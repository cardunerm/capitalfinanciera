﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class FacturaVendedorBusiness : GenericBusiness<FacturaVendedorBusiness, FacturaVendedor>
    {
        #region methods

        public List<FacturaVendedor> GetByFilters(string nombre, string cuit, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<FacturaVendedor> query = DataAccessManager.GetByConditions<FacturaVendedor>(p =>
                                                p.Activo == true &&
                                                p.Nombre.Contains(nombre) &&
                                                p.Apellido.Contains(cuit));

                query = query.OrderBy(p => p.Nombre);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FacturaVendedor> GetClientes()
        {
            var query = DataAccessManager.GetByConditions<FacturaVendedor>(p => p.Activo == true);
            return query;
        }

        #endregion 
    }
}
