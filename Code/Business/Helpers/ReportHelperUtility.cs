﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Enumerations;
using System;
using System.Configuration;
using System.IO;
using System.Net.Mime;
using System.Web;

namespace Bussines.Helpers
{
    public class ReportHelperUtility
    {
        public static ReportClass LoadReport(string reportName)
        {
            try
            {
                ReportClass reportClass = new ReportClass();
                reportClass.FileName = ConfigurationManager.AppSettings["ReportPath"] + reportName;
                //reportClass.FileName = HttpContext.Current.Server.MapPath(@"~\Reporte\" + reportName + ".rpt");
                reportClass.Load();

                return reportClass;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Stream ShowReport(ReportClass reportClass, ReportFormat reportFormat)
        {
            try
            {
                switch ((ReportFormat)reportFormat)
                {
                    case ReportFormat.Excel:
                        return reportClass.ExportToStream(ExportFormatType.Excel);
                    default: // PDF by default
                        return reportClass.ExportToStream(ExportFormatType.PortableDocFormat);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetContentType(string fileExtension)
        {
            try
            {
                switch (fileExtension.ToLower())
                {
                    case "pdf":
                        return MediaTypeNames.Application.Pdf;
                    case "gif":
                        return MediaTypeNames.Image.Gif;
                    case "jpeg":
                    case "jpg":
                        return MediaTypeNames.Image.Jpeg;
                    case "tiff":
                        return MediaTypeNames.Image.Tiff;
                    case "txt":
                        return MediaTypeNames.Text.Plain;
                    case "xml":
                        return MediaTypeNames.Text.Xml;
                    default:
                        return MediaTypeNames.Application.Octet;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
