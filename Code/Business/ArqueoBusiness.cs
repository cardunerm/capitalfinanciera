﻿using Domine;
using System.Linq;
using System.Collections.Generic;
using Domine.Dto;
using System;
using DataAccess.Pages;
using System.Data.Entity;
using Luma.Utils.Logs;
using Utils.Enumeraciones;
using System.ComponentModel.DataAnnotations;
using CurrentContext;

namespace Business
{
    public class ArqueoBusiness : GenericBusiness<ArqueoBusiness, Arqueo>
    {
        public List<Arqueo> GetArqueosByFilters(DateTime? fecha, Usuario usuario, int skip, int top)
        {
            try
            {
                IEnumerable<Arqueo> query = DataAccessManager.GetByConditions<Arqueo>(a => a.IdUsuario == usuario.IdUsuario && a.IdEmpresa == SessionContext.IdEmpresaActual && a.IdSucursal == SessionContext.IdSucursalOperacionActual);

                if (fecha != null)
                {
                    query = query.Where(c => c.Fecha <=fecha.Value.AddDays(1));
                }

                query = query.OrderBy(p => p.IdArqueo);

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cheque> GetChequesArqueo(int idArqueo, int skip, int top)
        {
            try
            {
                List<ArqueoDetalle> arqueoDetalle = DataAccessManager.GetByConditions<ArqueoDetalle>(a => a.IdArqueo == idArqueo);
                List<Cheque> listCheques = new List<Cheque>();
                Cheque chequeBD = new Cheque() { Activo = true };

                foreach (var item in arqueoDetalle)
                {
                    chequeBD = ChequeBusiness.Instance.GetCheque(item.IdCheque);

                    listCheques.Add(chequeBD);
                }

                return (top > 0 ? listCheques.Skip(skip).Take(top) : listCheques).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public int CreateArqueo(List<Cheque> listCheques, Arqueo arqueoModel, Usuario user) 
        {
            using (var ctx = DataAccessManager.GetContext() as DCREntities)
            {
                using (var tx = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        Arqueo arqueo = new Arqueo
                        {
                            IdUsuario = user.IdUsuario,
                            Estado = (int)Estados.eEstadosArqueo.Pendiente,
                            Fecha = DateTime.Now,
                            CantidadCheques = listCheques.Count,
                            Monto = listCheques.Sum(det => det.Monto),
                            Descripcion =  arqueoModel.Descripcion,
                            IdEmpresa = Convert.ToInt32(SessionContext.IdEmpresaActual),
                            IdSucursal = Convert.ToInt32(SessionContext.IdSucursalOperacionActual)
                        };

                        arqueo.ArqueoDetalle = new List<ArqueoDetalle>();

                        foreach (var det in listCheques)
                        {
                            var detArq = new ArqueoDetalle
                            {
                                Arqueo = arqueo,
                                IdCheque = det.IdCheque
                            };

                            arqueo.ArqueoDetalle.Add(detArq);
                        }
                        ctx.Arqueo.Add(arqueo);
                        ctx.SaveChanges();
                        tx.Commit();
                        return arqueo.IdArqueo;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        LogManager.Log.RegisterError(ex);
                        throw ex;
                    }
                }
            }
        
        }
    }
}
