﻿using Bussines.Helpers;
using CrystalDecisions.CrystalReports.Engine;
using Domine;
using Domine.Dto;
using Enumerations;
using Luma.Utils;
using Luma.Utils.Logs;
using MailSender;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Utils.Enumeraciones;
using static Utils.Enumeraciones.Estados;

namespace Business
{
    [DisallowConcurrentExecution]
    public class JobProcesoResumenDiario : IJob
    {

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                LogManager.Log.RegisterInfo("Inicio el proceso " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                RunProcess();
                LogManager.Log.RegisterInfo("Termino el proceso " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
            }
        }

        public void RunProcess()
        {
            using (var context = new DCREntities())
            {
                List<int> horaEjecucionProceso = getHoraEjecucionProceso();

                //Mantener fecha actual en la que comenzó la ejecución por las dudas de que cambie la fecha durante la ejecución del proceso
                DateTime fechaEjecucionActual = DateTime.Now;

                //Recuperar de la base de datos el último registro generado
                ProcesoGeneracionResumen ultimoProcesoEjecutado = (from upe in context.ProcesoGeneracionResumen select upe).ToList().OrderByDescending(p => p.FechaFin).FirstOrDefault();

                //Almacenar la fecha de ultima ejecucion
                DateTime fechaUltimaEjecucion = ultimoProcesoEjecutado != null ? ultimoProcesoEjecutado.FechaCorrida.Value.Date : fechaEjecucionActual.AddDays(-1).Date;

                //Inicializar un contador de días
                int contDias = 1;

                //Verifica si el proceso debe correr por el día actual pero la hora aún no es la correcta
                if (!(fechaUltimaEjecucion.AddDays(contDias).Date == fechaEjecucionActual.Date && fechaEjecucionActual.TimeOfDay < new TimeSpan(horaEjecucionProceso[0], horaEjecucionProceso[1], horaEjecucionProceso[2])))
                {
                    //Estructura While que calculará los procesos desde la última fecha de ejecución hasta la actual
                    while (fechaUltimaEjecucion.AddDays(contDias).Date <= fechaEjecucionActual.Date)
                    {
                        using (var tx = context.Database.BeginTransaction())
                        {
                            try
                            {
                                //Determina el día del proceso que se va a ejecutar
                                DateTime fechaDeCorrida = fechaUltimaEjecucion.AddDays(contDias).Date == fechaEjecucionActual.Date ? fechaEjecucionActual.Date : fechaUltimaEjecucion.AddDays(contDias).Date;

                                ProcesarMovimientosDeCuentas(fechaDeCorrida, fechaEjecucionActual, context);

                                RegistrarProcesoEjecutado(fechaDeCorrida, fechaEjecucionActual, context);

                                LiberarChequesEnGarantiaClasificacionBBB(fechaDeCorrida, fechaEjecucionActual, context);

                                //Confirmamos la transacción para la fecha de corrida actual
                                tx.Commit();

                                ++contDias;
                            }
                            catch (Exception ex)
                            {
                                tx.Rollback();
                                LogManager.Log.RegisterError(ex);
                                throw ex;
                            }
                        }
                    }
                }
            }
        }

        private void LiberarChequesEnGarantiaClasificacionBBB(DateTime fechaDeCorrida, DateTime fechaEjecucionActual, DCREntities context)
        {
            try
            {
                //Recupero las cuentas de clase BBB
                var cuentasBBB = from ctacte in context.CuentaCorrienteInversor
                                 join inversor in context.Inversor on ctacte.IdInversor equals inversor.IdInversor
                                 where ctacte.Estado == true && ctacte.ClasificacionInversor == (int)ClasificacionInversor.BBB
                                 select ctacte;

                //Proceso los cheque de cada cuenta
                foreach (var ctaBBB in cuentasBBB)
                {
                    //join ch in context.Cheque on det.IdCheque equals ch.IdCheque
                    var listaChequesEnCartera = from ch in context.Cheque
                                                join detOp in context.DetalleOperacion on ch.IdCheque equals detOp.IdCheque
                                                join opInv in context.OperacionInversor on detOp.IdOperacionInversor equals opInv.IdOperacionInversor
                                                join ctacte in context.CuentaCorrienteInversor on opInv.IdCuentaCorrienteInversor equals ctacte.IdCuentaCorrienteInversor
                                                where
                                                ctacte.IdCuentaCorrienteInversor == ctaBBB.IdCuentaCorrienteInversor &&
                                                ch.FechaVencimiento.Date > fechaDeCorrida.Date
                                                select ch;

                    List<Cheque> chequesOperativos = new List<Cheque>();
                    List<Cheque> chequesEnGarantia = new List<Cheque>();

                    decimal porcentajeBalance = ctaBBB.Inversor.PorcentajeBalanceGarantiaOperativo;

                    decimal montoOperativo = 0;
                    decimal montoGarantia = 0;

                    foreach (var chequeEnCartera in listaChequesEnCartera)
                    {
                        if (chequeEnCartera.Clasificacion == (int)ClasificacionCheque.Garantia)
                        {
                            montoGarantia = montoGarantia + chequeEnCartera.Monto;
                            chequesEnGarantia.Add(chequeEnCartera);
                        }
                        else
                        {
                            montoOperativo = montoOperativo + chequeEnCartera.Monto;
                            chequesOperativos.Add(chequeEnCartera);
                        }
                    }

                    //Ordeno las lista de cheques en garantia por Fecha de Cobro
                    chequesEnGarantia = chequesEnGarantia.OrderBy(c => c.FechaVencimiento).ToList();

                    //Valido si paso o no garantias a operativos
                    decimal porcentajeProporcionActual = ((montoGarantia * 100) / montoOperativo);
                    if (porcentajeProporcionActual > porcentajeBalance)
                    {
                        foreach (var chequeGarantia in chequesEnGarantia)
                        {
                            montoGarantia = montoGarantia - chequeGarantia.Monto;
                            montoOperativo = montoOperativo + chequeGarantia.Monto;
                            porcentajeProporcionActual = ((montoGarantia * 100) / montoOperativo);
                            if (porcentajeProporcionActual >= porcentajeBalance)
                            {
                                chequeGarantia.Clasificacion = (int)ClasificacionCheque.Activo;
                                context.SaveChanges();
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void ProcesarMovimientosDeCuentas(DateTime fechaDeCorrida, DateTime fechaEjecucionActual, DCREntities context)
        {
            decimal montoInteresCalculado = 0;
            bool enviarMail = false;

            fechaDeCorrida = DateTime.Now;

            //Obtengo las cuentas corrientes a procesar
            var cuentas = from ctacte in context.CuentaCorrienteInversor
                          where ctacte.Estado == true && ctacte.FechaAlta <= fechaDeCorrida
                         // && ctacte.IdInversor == 3064
                          select ctacte;

            //Proceso las transacciones correspondientes por cada cuenta
            foreach (var cuenta in cuentas)
            {

                LogManager.Log.RegisterInfo("Cuenta a procesar:" + cuenta.IdCuentaCorrienteInversor.ToString());

                var ultimoResumen = (from rd in context.ResumenDiarioOperacionInversor
                                     where rd.IdCuentaCorrienteInversor == cuenta.IdCuentaCorrienteInversor
                                     orderby rd.IdResumen descending
                                     select rd).ToList().FirstOrDefault();

                double interesAplicado = 0;

                montoInteresCalculado = AcreditarIntereses(ultimoResumen, cuenta, fechaDeCorrida, fechaEjecucionActual, context, out interesAplicado);

                ResumenDiarioOperacionInversor resumen = GenerarResumenesDiarios(montoInteresCalculado, interesAplicado, ultimoResumen, cuenta, fechaDeCorrida, fechaEjecucionActual, context, out enviarMail);

                bool configEnviarMailProyeccionCuenta = Convert.ToBoolean(ConfigurationManager.AppSettings["EnviarMailProyeccionCuenta"]);
                if (configEnviarMailProyeccionCuenta)
                {
                    if (enviarMail)
                        EnviarMailIngresoEgreso(cuenta, resumen, context);
                }

            }


            //Obtengo las cuentas corrientes a procesar
            var clientes = from cliente in context.Cliente
                          where cliente.Estado == 1 
                          select cliente;


            //Proceso las transacciones correspondientes por cada cliente

            foreach (var item in clientes)
            {
                Tasa tasa = context.Tasa.Single(p => p.Id == item.IdTasa);
                

                foreach (var itemCtaCte in item.CuentaCorriente)
                {
                    var positivo = itemCtaCte.OperacionCuentaCorriente.Where(p => p.TipoMovimiento == 2).Sum(p => p.Monto);
                    var negativo = itemCtaCte.OperacionCuentaCorriente.Where(p => p.TipoMovimiento == 1).Sum(p => p.Monto);

                    var total = positivo - negativo;

                    if (total < 0)
                    {
                        OperacionCuentaCorriente OpCc = new OperacionCuentaCorriente
                        {
                            //Monto = ((total * (tasa.Value/30)) / 100),
                            Monto = ((tasa.Value / 30) / 100) * total,
                            Fecha = DateTime.Now,
                            Descripcion = "Intereses por Mora",
                            Estado = 1,
                            IdCuentaCorriente = itemCtaCte.IdCuentaCorriente,
                            TipoMovimiento = 2
                        };

                        context.OperacionCuentaCorriente.Add(OpCc);
                        context.SaveChanges();
                    }                 

                }
            }



        }

        private decimal AcreditarIntereses(ResumenDiarioOperacionInversor ultimoResumen, CuentaCorrienteInversor cuenta, DateTime fechaDeCorrida, DateTime fechaEjecucionActual, DCREntities context, out double interesAplicado)
        {
            try
            {
                decimal montoInteres = 0;

                //Buscar tasa interes vigente para la fecha de corrida

                HistorialTasaInteresInversor tasa = null;
                
                tasa = (from h in context.HistorialTasaInteresInversor
                        where h.IdInversor == cuenta.IdInversor &&
                              h.TipoMonedaTasa == cuenta.TipoMoneda &&
                              ((fechaDeCorrida >= h.FechaDesde && fechaDeCorrida <= h.FechaHasta) || (fechaDeCorrida >= h.FechaDesde && h.FechaHasta == null))
                        select h).OrderByDescending(t => t.IdInversor).FirstOrDefault();


                if (tasa == null)
                {
                    throw new NullReferenceException();
                }

                //Cálculo del interes a aplicar
                if (ultimoResumen != null && ultimoResumen.Saldo.Value != 0)
                    montoInteres = Convert.ToDecimal(UtilHelper.CalcularInteres(ultimoResumen.Saldo.Value, (tasa.ValorTasa.Value / 100), 1));

                //Crear operación de inversor por intereses
                OperacionInversor opInv = new OperacionInversor
                {
                    IdUsuario = null,
                    Fecha = fechaDeCorrida,
                    Observacion = string.Format(Resources.Resource.AcreditacionIntereses, fechaDeCorrida.Date.ToString(), cuenta.IdCuentaCorrienteInversor.ToString("000-0000000")),
                    Estado = (int)Estados.eEstadosOp.Aprobado,
                    Monto = montoInteres,
                    IdCuentaCorrienteInversor = cuenta.IdCuentaCorrienteInversor,
                    TipoMovimiento = (int)Estados.mTipoMovimientoCuentaCorriente.Credito,
                    IdHistorialTasaInteresInversorAplicado = tasa.IdHistorial,
                    Condicion = (int)Condicion.Efectivo,
                    ClasificacionMovimiento = (int)ClasificacionMovimiento.Intereses
                };

                interesAplicado = Convert.ToDouble(tasa.ValorTasa.Value / 100);

                context.OperacionInversor.Add(opInv);
                context.SaveChanges();

                return montoInteres;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private ResumenDiarioOperacionInversor GenerarResumenesDiarios(decimal montoInteresCalculado, double interesAplicado, ResumenDiarioOperacionInversor ultimoResumen, CuentaCorrienteInversor cuenta, DateTime fechaDeCorrida, DateTime fechaEjecucionActual, DCREntities context, out bool enviarMail)
        {
            //ACLARACIÓN: Las operaciones de cheques no son marcadas en el atributo Operacion.IdResumen, sino que son marcadas en el atributo DetalleOperacion.IdResumen

            //Defino acumuladores por clasificacion de movimiento
            decimal ingresos = 0;
            decimal egresos = 0;
            decimal intereses = montoInteresCalculado;
            decimal impuestos = 0;
            decimal gastos = 0;
            decimal multas = 0;
            enviarMail = false;

            //Se crea el resumen de operacion diaria
            ResumenDiarioOperacionInversor resumenDiario = new ResumenDiarioOperacionInversor();
            resumenDiario.Fecha = fechaDeCorrida;
            resumenDiario.TipoMoneda = cuenta.TipoMoneda;
            resumenDiario.IdCuentaCorrienteInversor = cuenta.IdCuentaCorrienteInversor;

            context.ResumenDiarioOperacionInversor.Add(resumenDiario);
            context.SaveChanges();

            //Recupero todas las operaciones con fechas anteriores a la de la corrida cuyos cheques vencen el día actual de corrida
            List<DetalleOperacion> detallesAnterioresCheques = new List<DetalleOperacion>();
            detallesAnterioresCheques = (from det in context.DetalleOperacion
                                         join ch in context.Cheque on det.IdCheque equals ch.IdCheque
                                         join op in context.OperacionInversor on det.IdOperacionInversor equals op.IdOperacionInversor
                                         where (op.IdCuentaCorrienteInversor == cuenta.IdCuentaCorrienteInversor &&
                                                op.Fecha < fechaDeCorrida.Date &&
                                                op.ClasificacionMovimiento == (int)ClasificacionMovimiento.Ingreso && op.Condicion == (int)Condicion.Cheque &&
                                                det.IdResumen == null &&
                                                (op.Estado != (int)eEstadosOp.Cancelada && op.Estado != (int)eEstadosOp.Rechazado) &&
                                                (det.Estado != (int)eEstadosOpDetalle.Cancelado && det.Estado != (int)eEstadosOpDetalle.Rechazado && det.Estado != (int)eEstadosOpDetalle.Pagada) &&
                                                ch.FechaVencimiento == fechaDeCorrida.Date)
                                         select det).ToList();

            //Recorro cada detalle anterior para acreditar el monto
            foreach (var detalle in detallesAnterioresCheques)
            {
                ingresos = ingresos + detalle.Cheque.Monto;
                detalle.IdResumen = resumenDiario.IdResumen;
                
                //Paso el cheque a operativo si actualmente está en garantía
                if(detalle.Cheque.Clasificacion == (int)ClasificacionCheque.Garantia)
                    detalle.Cheque.Clasificacion = (int)ClasificacionCheque.Activo;

            }

            context.SaveChanges();

            DateTime fechaHastaConsultaOperaciones = fechaDeCorrida.Date.AddDays(1);
            //Busco las operaciones realizadas en la fecha de ejecución de la corrida
            List<OperacionInversor> operaciones = (from op in context.OperacionInversor
                                                   where op.IdCuentaCorrienteInversor == cuenta.IdCuentaCorrienteInversor
                                                   && (op.Estado == (int)eEstadosOp.Pendiente )
                                                   && (op.Fecha < fechaHastaConsultaOperaciones)
                                                   //  && (op.Fecha >= fechaDeCorrida.Date && op.Fecha < fechaHastaConsultaOperaciones) 
                                                   && op.Estado != (int)eEstadosOp.Cancelada && op.Estado != (int)eEstadosOp.Rechazado && op.ClasificacionMovimiento != (int)ClasificacionMovimiento.Cancelacion
                                                   select op).ToList();





            foreach (var operacion in operaciones)
            {
                List<DetalleOperacion> detallesOp = new List<DetalleOperacion>();

                //Recupero los detalles si los tiene, a partir de saber si la operacion es ingreso de cheque
                if (operacion.ClasificacionMovimiento == (int)ClasificacionMovimiento.Ingreso && operacion.Condicion == (int)Condicion.Cheque)
                {
                    detallesOp = (from det in context.DetalleOperacion
                                  join ch in context.Cheque on det.IdCheque equals ch.IdCheque
                                  where det.IdOperacionInversor == operacion.IdOperacionInversor && det.Estado != (int)eEstadosOpDetalle.Cancelado && det.Estado != (int)eEstadosOpDetalle.Rechazado && det.Estado != (int)eEstadosOpDetalle.Pagada
                                  select det).ToList();

                    foreach (var detalle in detallesOp)
                    {
                        if (detalle.Cheque.FechaVencimiento.Date <= fechaDeCorrida.Date && detalle.IdResumen == null)
                        {
                            ingresos = ingresos + detalle.Cheque.Monto;
                            detalle.IdResumen = resumenDiario.IdResumen;

                            //Paso el cheque a operativo si actualmente está en garantía
                            if (detalle.Cheque.Clasificacion == (int)ClasificacionCheque.Garantia)
                                detalle.Cheque.Clasificacion = (int)ClasificacionCheque.Activo;
                        }
                    }
                }
                else
                {
                    switch (operacion.ClasificacionMovimiento)
                    {
                        case (int)ClasificacionMovimiento.Ingreso:
                            ingresos = ingresos + operacion.Monto;
                            break;
                        case (int)ClasificacionMovimiento.Egreso:
                            egresos = egresos + operacion.Monto;
                            break;
                        //NO se considera porque ya fueron calculados
                        //case (int)ClasificacionMovimiento.Intereses:
                        //    intereses = intereses + operacion.Monto;
                        //    break;
                        case (int)ClasificacionMovimiento.Impuestos:
                            impuestos = impuestos + operacion.Monto;
                            break;
                        case (int)ClasificacionMovimiento.Gastos:
                            gastos = gastos + operacion.Monto;
                            break;
                        case (int)ClasificacionMovimiento.Multas:
                            multas = multas + operacion.Monto;
                            break;
                    }

                    //Actualizo la operación para marcarla con el resumen por el que fue procesada
                    operacion.IdResumen = resumenDiario.IdResumen;
                }

                //Actualizo el estado de la operacion
                operacion.Estado = (int)eEstadosOp.Aprobado;
                OperacionInversoresBussines.Instance.SaveOrUpdate(operacion);

            }

            resumenDiario.TotalIngresos = ingresos;
            resumenDiario.TotalEgresos = egresos;
            resumenDiario.Intereses = intereses;
            resumenDiario.Impuestos = impuestos;
            resumenDiario.Gastos = gastos;
            resumenDiario.Multas = multas;
            resumenDiario.Total = ingresos - egresos + intereses - impuestos - gastos - multas;
            resumenDiario.Saldo = ultimoResumen != null ? ultimoResumen.Saldo + resumenDiario.Total : resumenDiario.Total;
            resumenDiario.TasaInteres = interesAplicado;

            if (ingresos != 0 || egresos != 0)
                enviarMail = true;

            context.SaveChanges();

            return resumenDiario;

        }

        private void RegistrarProcesoEjecutado(DateTime fechaDeCorrida, DateTime fechaEjecucionActual, DCREntities context)
        {
            ProcesoGeneracionResumen proceso = new ProcesoGeneracionResumen
            {
                FechaCorrida = fechaDeCorrida.Date,
                FechaInicio = fechaEjecucionActual,
                FechaFin = DateTime.Now
            };

            context.ProcesoGeneracionResumen.Add(proceso);
            context.SaveChanges();
        }

        private List<int> getHoraEjecucionProceso()
        {
            List<string> horaListStr = ConfigurationManager.AppSettings["HoraEjecucionProcesoResumenDiario"].Split(':').ToList();

            List<int> horaListInt = new List<int>();

            for (int i = 0; i < horaListStr.Count; ++i)
            {
                horaListInt.Add(int.Parse(horaListStr[i]));
            }

            return horaListInt;
        }

        private void EnviarMailIngresoEgreso(CuentaCorrienteInversor cuenta, ResumenDiarioOperacionInversor resumen, DCREntities context)
        {
            try
            {
                List<Model.ProyeccionFinancieraModel> result = new List<Model.ProyeccionFinancieraModel>();
                //cuenta.Inversor.Email
                int idInversor = cuenta.IdInversor.Value;
                int idCuenta = cuenta.IdCuentaCorrienteInversor;

                List<DtoProyeccionFinanciera> proyeccion = ProyeccionFinancieraBusiness.Instance.GetProyeccion(1,idInversor, idCuenta, resumen, context);

                foreach (var item in proyeccion)
                {
                    result.Add((Model.ProyeccionFinancieraModel)WebHelperUtility.GenericLoad(item, new Model.ProyeccionFinancieraModel()));
                }
                //Armar el reporte a partir del datasource proyección

                //Obtener el PDF del reporte
                string temporalPath = AppDomain.CurrentDomain.BaseDirectory + @"CarpetaTemporal\";

                if (!Directory.Exists(temporalPath))
                {
                    Directory.CreateDirectory(temporalPath);
                }

                //byte[] file = File.ReadAllBytes("");

                string fileName = "ProyeccionReport.pdf";

                ReportClass reportClass = ReportHelperUtility.LoadReport("RpProyeccionFinanciera.rpt");
                reportClass.SetDataSource(result);
                reportClass.SetParameterValue("inversor", cuenta.Inversor.Nombre);
                reportClass.SetParameterValue("cuentaCorrienteInversor", cuenta.NumeroCtaCteStr);

                reportClass.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, temporalPath + fileName);

                byte[] file = File.ReadAllBytes(temporalPath + fileName);

                AttachmentFile[] pdf = new AttachmentFile[1];

                AttachmentFile archivo1 = new AttachmentFile();

                archivo1.Content = Convert.ToBase64String(file);

                archivo1.Name = "Resumen Egreso Ingreso";

                pdf[0] = archivo1;

                //Armar y enviar el mail
                MailBusiness mail = new MailBusiness();

                if (cuenta.Inversor.EnvioMail)
                {
                    mail.SendMail(cuenta.Inversor.Email, string.Empty, string.Empty, ConfigurationManager.AppSettings["Subjet"], ConfigurationManager.AppSettings["Body"], pdf, ConfigurationManager.AppSettings["EmailSMTPHost"], ConfigurationManager.AppSettings["EmailSMTPPort"], ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailUserName"], ConfigurationManager.AppSettings["EmailContrasenia"], false, null);
                }

                File.Delete(temporalPath + fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
