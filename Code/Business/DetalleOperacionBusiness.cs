﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class DetalleOperacionBusiness : GenericBusiness<DetalleOperacionBusiness, DetalleOperacion>
    {

        public List<DetalleOperacion> GetDetalleOperacion(int IdOperacionInversor)
        {

            try
            {

                IList<DetalleOperacion> list = DataAccessManager.GetByConditions<DetalleOperacion>(p => p.IdOperacionInversor == IdOperacionInversor);

                return list.ToList();

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}
