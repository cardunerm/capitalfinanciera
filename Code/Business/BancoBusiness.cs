﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Business
{
    public class BancoBusiness : GenericBusiness<BancoBusiness, Banco>
    {
        #region Methods

        public List<Banco> GetByFilters(string nombre, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<Banco> query = DataAccessManager.GetByConditions<Banco>(p =>
                                                p.Estado == (int)EntityStatus.Active &&
                                                p.Nombre.Contains(nombre));

                query = query.OrderBy(p => p.Nombre);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Banco> GetBancosList()
        {
            try
            {
                IEnumerable<Banco> query = DataAccessManager.GetByConditions<Banco>(p =>
                                                p.Activo == true);

                query = query.OrderBy(p => p.Nombre);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveOrUpdateBanco(Banco banco)
        {
            Banco entity;

            if (banco.IdBanco > 0)
            {
                entity = GetEntity(banco.IdBanco);
                entity.Activo = banco.Activo;
                entity.Contacto = banco.Contacto;
                entity.Direccion = banco.Direccion;
                entity.Nombre = banco.Nombre;
                entity.Observaciones = banco.Observaciones;
                entity.Telefono = banco.Telefono;
                entity.Codigo = banco.Codigo;
            }
            else
            {
                entity = new Banco
                {
                    Activo = true,
                    Contacto = banco.Contacto,
                    Direccion = banco.Direccion,
                    Nombre = banco.Nombre,
                    Observaciones = banco.Observaciones,
                    Telefono = banco.Telefono,
                    Estado = 1,
                    Codigo = banco.Codigo
                };
            }

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaBanco(banco.IdBanco > 0 ? AuditAction.Edit : AuditAction.Create, entity);
        }

        private bool ValidateSaveOrUpdate(Banco entity)
        {
            if (!BancoNombreExist(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException("Ya existe un banco con ese nombre");
            }
        }

        private bool BancoNombreExist(Banco entity)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Banco>(p =>
                                p.Activo == true &&
                                p.Nombre.Equals(entity.Nombre) &&
                                p.IdBanco != entity.IdBanco);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(Banco entity)
        {
            return true;
            //if (!BancoHasChequesOrCBUCliente(entity))
            //{
            //    return true;
            //}
            //else
            //{
            //    throw new ValidationException(string.Format("No se puede eliminar el registro.", "El banco tiene cheques asociados y/o CBU asociados"));
            //}
        }

        //private bool BancoHasChequesOrCBUCliente(Banco entity)
        //{
        //    try
        //    {
        //        return (entity.Cheques.Any(p => p.Estado == (int)EntityStatus.Active) || entity.CBUClientes.Any());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public Banco BancoExist(string nombre)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Banco>(p =>
                                p.Activo == true &&
                                p.Nombre.Equals(nombre));

                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}