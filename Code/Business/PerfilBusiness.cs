﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Business
{
    public class PerfilBusiness : GenericBusiness<PerfilBusiness, Perfil>
    {
        #region Methods

        public List<Perfil> GetByFilters(string nombre, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<Perfil> query = DataAccessManager.GetByConditions<Perfil>(p =>
                                                p.Estado == (int)EntityStatus.Active &&
                                                p.Nombre.Contains(nombre));

                query = query.OrderBy(p => p.Nombre);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveOrUpdatePerfil(Perfil perfil, List<PerfilModulo> permisos)
        {
            Perfil entity;
            //IEnumerable<PerfilModulo> oldPermisos = null;

            if (perfil.IdPerfil > 0)
            {
                entity = GetEntity(perfil.IdPerfil);
                entity.FechaEdicion = DateTime.Now;

                PerfilModuloBusiness.Instance.DeletePerfilModulos(entity);
                //oldPermisos = PerfilModuloBusiness.Instance.DeletePerfilModulos(entity);
            }
            else
            {
                entity = new Perfil
                {
                    FechaCreacion = DateTime.Now,
                    Estado = (int)EntityStatus.Active
                };
            }

            entity.Nombre = perfil.Nombre;

            permisos.ForEach(p => entity.PerfilModulo.Add(new PerfilModulo
            {
                IdModulo = p.IdModulo,
                Permiso = p.Permiso,
                FechaCreacion = DateTime.Now,
                Estado = (int)EntityStatus.Active
            }));

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaPerfil(perfil.IdPerfil > 0 ? AuditAction.Edit : AuditAction.Create, entity);
            //AuditoriaBusiness.Instance.AuditoriaPerfil(perfil.IdPerfil > 0 ? AuditAction.Edit : AuditAction.Create, entity, oldPermisos);
        }

        private bool ValidateSaveOrUpdate(Perfil perfil)
        {
            if (!PerfilNombreExist(perfil))
            {
                return true;
            }
            else
            {
                throw new ValidationException("El nombre del perfil ya existe");
            }
        }

        private bool PerfilNombreExist(Perfil perfil)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Perfil>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Nombre.Equals(perfil.Nombre) &&
                                p.IdPerfil != perfil.IdPerfil);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(Perfil entity)
        {
            if (!PerfilHasUsuarios(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException(string.Format("No se puede eliminar el registro", "El perfil tiene usuarios asociados"));
            }
        }

        private bool PerfilHasUsuarios(Perfil entity)
        {
            try
            {
                return entity.UsuarioPerfil.Any(p => p.Estado == (int)EntityStatus.Active &&
                                                    p.Usuario.Estado == (int)EntityStatus.Active);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Perfil> GetPerfilByUsuario()
        {
            try
            {
               List<Perfil> query = DataAccessManager.GetByConditions<Perfil>(p=> p.Estado == 1).ToList();                              

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}