﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Utils.Enumeraciones;

namespace Business
{
    public class ClienteBusiness : GenericBusiness<ClienteBusiness, Cliente>
    {
        #region Methods

        public List<Cliente> GetByFilters(string nombre, string cuit, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<Cliente> query = DataAccessManager.GetByConditions<Cliente>(p =>
                                                p.Estado == (int)EntityStatus.Active &&
                                                p.Nombre.Contains(nombre) &&
                                                p.CUIT.Contains(cuit));

                query = query.OrderBy(p => p.Nombre).ThenBy(x => x.CUIT);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> GetClientesList()
        {
            try
            {
                IEnumerable<Cliente> query = DataAccessManager.GetByConditions<Cliente>(p =>
                                                p.Estado == (int)EntityStatus.Active);

                query = query.OrderBy(p => p.Nombre).ThenBy(x => x.CUIT);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Cliente SaveOrUpdateCliente(Cliente cliente)
        {
            Cliente entity;

            if (cliente.IdCliente > 0)
            {
                entity = GetEntity(cliente.IdCliente);
                entity.Clasificacion = cliente.Clasificacion;
                entity.CUIT = cliente.CUIT;
                entity.Domicilio = cliente.Domicilio;
                entity.Email = (cliente.Email == null) ? "" : cliente.Email;
                entity.LimiteDescubierto = cliente.LimiteDescubierto;
                entity.Nombre = cliente.Nombre;
                entity.TelefonoFijo = cliente.TelefonoFijo;
                entity.TelefonoMovil = cliente.TelefonoMovil;
                entity.IdTasa = cliente.IdTasa;
                entity.IdTasaMora = cliente.IdTasaMora;
                entity.IdCondicionTributaria = cliente.IdCondicionTributaria;
            }
            else
            {
                var tasa = (DataAccessManager.GetContext() as DCREntities).Tasa.Max(p => p.Value);
                var tasaItem = GetByConditions<Tasa>(p => p.Value == tasa).FirstOrDefault();

                entity = new Cliente
                {
                    Clasificacion = cliente.Clasificacion,
                    CUIT = cliente.CUIT,
                    Domicilio = cliente.Domicilio,
                    Email = (cliente.Email == null) ? "" : cliente.Email,
                    LimiteDescubierto = cliente.LimiteDescubierto,
                    Nombre = cliente.Nombre,
                    TelefonoFijo = cliente.TelefonoFijo,
                    TelefonoMovil = cliente.TelefonoMovil,
                    Estado = (int)EntityStatus.Active,
                    IdTasa = cliente.IdTasa ?? tasaItem.Id,
                    IdTasaMora = cliente.IdTasa ?? tasaItem.Id,
                    IdCondicionTributaria = cliente.IdCondicionTributaria
            };
                entity.CuentaCorriente.Add
                    (
                        new CuentaCorriente
                        {
                            Cliente = entity, Estado = 1
                        }
                    );
            }
            
            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaCliente(cliente.IdCliente > 0 ? AuditAction.Edit : AuditAction.Create, entity);

            return entity;
        }

        private bool ValidateSaveOrUpdate(Cliente entity)
        {
            if (!ClienteNombreExist(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException("Ya existe un cliente con ese nombre");
            }
        }

        private bool ClienteNombreExist(Cliente entity)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Cliente>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Nombre.Equals(entity.Nombre) &&
                                p.CUIT.Equals(entity.CUIT) &&
                                p.IdCliente != entity.IdCliente);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(Cliente entity)
        {
           // if (!ClienteHasCheques(entity))
           // {
                return true;
           // }
           // else
           // {
           //     throw new ValidationException(string.Format("No se puede eliminar el registro.", "El cliente tiene cheques asociados"));
           // }
        }

        //private bool ClienteHasCheques(Cliente entity)
        //{
        //    try
        //    {
        //        return entity.RecepcionCheques.Any();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public Cliente ClienteExist(string nombre, string cuit)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Cliente>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Nombre.Equals(nombre) &&
                                p.CUIT.Equals(cuit));

                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public decimal GetTasaCliente(int idcliente)
        {
            try
            {
                var cliente = DataAccessManager.GetById<Cliente>(idcliente);
                return DataAccessManager.GetById<Tasa>(long.Parse(cliente.IdTasa.ToString())).Value;

            }
            catch (Exception)
            {

                throw;
            }


        }

        #endregion
    }
}