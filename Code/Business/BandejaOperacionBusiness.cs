﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domine.Extensions;
using DataAccess.Pages;
using Domine;
using Utils.Enumeraciones;
using AutoMapper;
using Domine.Dto;
using Enumerations;
using System.Transactions;
using static Utils.Enumeraciones.Estados;

namespace Business
{
    public class BandejaOperacionBusiness : GenericBusiness<BandejaOperacionBusiness, View_OperacionesInversor>
    {

        public List<View_OperacionesInversor> GetBandejaOperacionByFilters(int? idEstado, DateTime? fechaDesde, DateTime? fechaHasta, string inversor, int? tipoMoneda, int? condicion, int skip, int top, ref int rowCount)
        {

            try
            {

                IEnumerable<View_OperacionesInversor> query = DataAccessManager.GetByConditions<View_OperacionesInversor>(p =>
                                        (p.Fecha >= fechaDesde || fechaDesde == null) &&
                                        (p.Fecha <= fechaHasta || fechaHasta == null) &&
                                        (p.Inversor == inversor) &&
                                        (p.TipoMoneda == tipoMoneda) &&
                                        (p.Condicion == condicion || condicion == null) &&
                                        (p.Estado == idEstado || idEstado == null));

                query = query.OrderByDescending(p => p.IdOperacionInversor).ThenBy(x => x.CuitInversor);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public dynamic GetTipoMoneda(Enumerations.TipoMoneda? moneda)
        {
            try
            {
                var result = Enum.GetValues(typeof(Enumerations.TipoMoneda)).Cast<Enumerations.TipoMoneda>();

                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public List<DtoCheque> GetCheques(int IdOperacionInversor)
        {
            try
            {
                List<string> includes = new List<string>();
                includes.Add("Cheque");
                List<DetalleOperacion> detalles = DataAccessManager.GetByConditions<DetalleOperacion>(d => d.IdOperacionInversor == IdOperacionInversor, includes).ToList();

                List<Cheque> listaCheques = new List<Cheque>();
                foreach (var detalle in detalles)
                {
                    listaCheques.Add(detalle.Cheque);
                }

                List<DtoCheque> dtoList = new List<DtoCheque>();
                dtoList = Mapper.Map<List<Cheque>, List<DtoCheque>>(listaCheques).OrderBy(c => c.IdCheque).ToList();



                return dtoList;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RechazoCheque(int IdCheque)
        {
            try
            {
                Cheque entidad = ChequeBusiness.Instance.GetEntity(IdCheque);
                entidad.IdEstado = 10;
                ChequeBusiness.Instance.SaveOrUpdateCheque(entidad);

                //Cargo en la Tabla ChequeMovimiento

                //ChequeMovimiento entidadMovimiento = new ChequeMovimiento();
                //entidadMovimiento.Cheque = entidad;
                //entidadMovimiento.ChequeEstado = ChequeEstadoBusiness.Instance.GetEntity((long)newstatus);
                //entidadMovimiento.Fecha = DateTime.Now;
                //ChequeMovimientoBusiness.Instance.SaveOrUpdate(entidadMovimiento);


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DtoDataInversor GetDataInversor(int idInversor, int tipoMoneda)
        {
            using (TransactionScope tx = new TransactionScope())
            {
                try
                {
                    var ctacte = DataAccessManager.GetByConditions<CuentaCorrienteInversor>(c => c.IdInversor == idInversor && c.TipoMoneda == tipoMoneda).FirstOrDefault();
                    var saldo = ResumenDiarioOperacionInversorBusiness.Instance.GetSaldoByIdCuenta(ctacte.IdCuentaCorrienteInversor, ctacte.TipoMoneda);

                    DtoDataInversor dtoData = new DtoDataInversor
                    {

                        Nombre = ctacte.Inversor.Nombre,
                        Cuit = ctacte.Inversor.CUIT,
                        Numero = ctacte.IdCuentaCorrienteInversor,
                        Saldo = saldo

                    };

                    tx.Complete();
                    return dtoData;
                    
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }

        public void CancelarOperacion(int IdOperacionInversor)
        {
            try
            {
                OperacionInversor entidad = OperacionInversoresBussines.Instance.GetEntity(IdOperacionInversor);

                entidad.Estado = (int)eEstadosOp.Cancelada;

                if(entidad.Condicion == (int)Condicion.Cheque)
                {
                    foreach (var detalle in entidad.DetalleOperacion)
                    {
                        detalle.Estado = (int)eEstadosOpDetalle.Cancelado;
                        detalle.Cheque.IdEstado = (int)EstadoCheque.Cancelado;
                    }
                }

                OperacionInversoresBussines.Instance.SaveOrUpdateOperacion(entidad);

                OperacionInversoresBussines.Instance.BalanceCancelar(IdOperacionInversor);
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public decimal GetTasaInversor(string key)
        {
            return DataAccessManager.GetById<Inversor>(Convert.ToInt32(key)).Tasa;
        }

    }
}
