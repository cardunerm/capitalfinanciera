﻿using System;
using System.Collections.Generic;
using System.Linq;
using Enumerations;
using Domine;

namespace Business
{
    public class PerfilModuloBusiness : GenericBusiness<PerfilModuloBusiness, PerfilModulo>
    {
        #region Methods

        public IEnumerable<PerfilModulo> DeletePerfilModulos(Perfil perfil)
        {
            try
            {
                var list = perfil.PerfilModulo.Where(p => p.Estado == (int)EntityStatus.Active);

                foreach (var item in list)
                {
                    item.FechaEdicion = DateTime.Now;
                    item.Estado = (int)EntityStatus.Deleted;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}