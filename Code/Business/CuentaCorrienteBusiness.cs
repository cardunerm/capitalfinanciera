﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Luma.Utils.Logs;
using Utils.Enumeraciones;

namespace Business
{
    public class CuentaCorrienteBusiness : GenericBusiness<CuentaCorrienteBusiness, CuentaCorriente>
    {
        #region Methods

        // public List<CuentaCorriente> GetByFilters(string nombre, string cuit, int skip, int top, ref int rowCount)
        //{
        //    try
        //    {
        //        IEnumerable<CuentaCorriente> query = DataAccessManager.GetByConditions<CuentaCorriente>(p =>
        //                                        p.Estado == (int)EntityStatus.Active &&
        //                                        p.Nombre.Contains(nombre);

        //        query = query.OrderBy(p => p.Nombre);
        //        rowCount = query.Count();

        //        return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public List<View_SaldoCtaCte> GetCuentaCorrienteList(string cliente)
        {
            try
            {
                IEnumerable<View_SaldoCtaCte> query = DataAccessManager.GetByConditions<View_SaldoCtaCte>(p=> p.NombreCliente.Contains(cliente));

                query = query.OrderBy(p => p.IdCuentaCorriente);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveOrUpdateCuentaCorriente(CuentaCorriente cuenta)
        {
            CuentaCorriente entity;

            if (cuenta.IdCliente > 0)
            {
                entity = GetEntity(cuenta.IdCuentaCorriente);
                entity.Estado = cuenta.Estado;
                entity.IdCliente = cuenta.IdCliente;
            }
            else
            {
                entity = new CuentaCorriente
                {
                    IdCliente = cuenta.IdCliente,
                    Estado = (int)EntityStatus.Active,
                };
            }

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaCuentaCorriente(cuenta.IdCliente > 0 ? AuditAction.Edit : AuditAction.Create, entity);
        }

        private bool ValidateSaveOrUpdate(CuentaCorriente entity)
        {
            //if (!ClienteNombreExist(entity))
            //{
            return true;
            //}
            //else
            //{
            //    throw new ValidationException("Ya existe un cliente con ese nombre");
            //}
        }

        public bool ValidateDelete(CuentaCorriente entity)
        {
            if (!ClienteHasOperaciones(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException(string.Format("No se puede eliminar el registro.", "La cuenta corriente tiene operaciones asociadas"));
            }
        }

        private bool ClienteHasOperaciones(CuentaCorriente entity)
        {
            try
            {
                return entity.OperacionCuentaCorriente.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetCtaCteSaldoDebito(DateTime? fechaDesde, DateTime? fechaHasta, int idCuentaCorriente)
        {
            List<OperacionCuentaCorriente> query = DataAccessManager.GetByConditions<OperacionCuentaCorriente>(p => p.TipoMovimiento == 1
                                                                                                               && p.Fecha >= fechaDesde
                                                                                                               && p.Fecha <= fechaHasta
                                                                                                               && p.IdCuentaCorriente == idCuentaCorriente);

            return query.Sum(op => op.Monto);
        }

        public decimal GetCtaCteSaldoCredito(DateTime? fechaDesde, DateTime? fechaHasta, int idCuentaCorriente)
        {
            List<OperacionCuentaCorriente> query = DataAccessManager.GetByConditions<OperacionCuentaCorriente>(p => p.TipoMovimiento == 2
                                                                                                               && p.Fecha >= fechaDesde
                                                                                                               && p.Fecha <= fechaHasta
                                                                                                               && p.IdCuentaCorriente == idCuentaCorriente);

            return query.Sum(op => op.Monto);
        }

        public void DeleteLogicCuentaCorrienteByCliente(long idCliente)
        {
            try
            {
                List<CuentaCorriente> listCtaCte = DataAccessManager.GetByConditions<CuentaCorriente>(p =>
                                                p.IdCliente == idCliente);

                foreach (var item in listCtaCte)
                {
                    CuentaCorriente ctaCte = this.GetEntity(item.IdCuentaCorriente);
                    ctaCte.Estado = (int)EntityStatus.Deleted;
                    SaveOrUpdate(ctaCte, ValidateSaveOrUpdate);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        public int OperationClient(int idCliente, decimal importe, int tipo)
        {
            try
            {
                CuentaCorriente ctaCte = DataAccessManager.GetByConditions<CuentaCorriente>(p =>
                                              p.IdCliente == idCliente).FirstOrDefault();
                if (ctaCte == null)
                {
                    var ex = new Exception("No hay cuenta corriente para el cliente");
                    LogManager.Log.RegisterError(ex);
                    throw ex;
                }

                OperacionCuentaCorriente opCtaCte = new OperacionCuentaCorriente()
                {
                    IdCuentaCorriente = ctaCte.IdCuentaCorriente,
                    Descripcion = ((tipo == 1) ? "Extracción" : "Depósito") + " de efectivo por caja",
                    Estado = 1,
                    Fecha = DateTime.Now,
                    Monto = importe,
                    TipoMovimiento = (int)(tipo == 1 ? Estados.mTipoMovimientoCuentaCorriente.Debito : Estados.mTipoMovimientoCuentaCorriente.Credito)
                };

                using (DCREntities context = new DCREntities())
                {
                    using (var tx = context.Database.BeginTransaction())
                    {
                        try {
                            var imp = importe;
                            var operaciones = context.Operacion.Where(op => op.Saldo > 0 && op.Estado == (int)Estados.eEstadosOp.Aprobado).OrderBy(op => op.Fecha);
                            foreach (var op in operaciones)
                            {
                                if (op.Saldo <= imp)
                                {
                                    imp -= (op.Saldo ?? 0);
                                    op.Saldo = 0;
                                    op.Estado = (int)Estados.eEstadosOp.Pagada;
                                }
                                else
                                {
                                    op.Saldo -= imp;
                                    imp = 0;
                                }

                                if (imp <= 0)
                                    break;
                            }

                            var opCaja = new OperacionCaja
                            {
                                IdCaja = CajaBusiness.Instance.GetUltimaCaja().IdCaja,
                                TipoMovimiento = (int)(tipo == 1 ? Estados.mTipoMovimientoCuentaCorriente.Credito : Estados.mTipoMovimientoCuentaCorriente.Debito) ,
                                Monto = importe,
                                Fecha = DateTime.Now,
                                Descripcion = opCtaCte.Descripcion                               
                            };

                            context.OperacionCaja.Add(opCaja);
                            context.OperacionCuentaCorriente.Add(opCtaCte);
                            context.SaveChanges();
                            tx.Commit();

                            return opCtaCte.IdOperacionCuentaCorriente;
                        }
                        catch(Exception ex)
                        {
                            tx.Rollback();
                            throw ex;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetSaldoByFecha(DateTime? fechaHasta, int idCuentaCorriente)
        {
            decimal debito = 0.0M;
            decimal credito = 0.0M;

            List<OperacionCuentaCorriente> queryCredito = DataAccessManager.GetByConditions<OperacionCuentaCorriente>(p => p.TipoMovimiento == 2
                                                                                                               && p.Fecha < fechaHasta
                                                                                                               && p.IdCuentaCorriente == idCuentaCorriente);

            List<OperacionCuentaCorriente> queryDebito = DataAccessManager.GetByConditions<OperacionCuentaCorriente>(p => p.TipoMovimiento == 1
                                                                                                               && p.Fecha < fechaHasta
                                                                                                               && p.IdCuentaCorriente == idCuentaCorriente);

            debito = queryDebito.Sum(op => op.Monto);

            credito = queryCredito.Sum(op => op.Monto);


            return credito - debito;
        }
        
        #endregion
    } 
}
