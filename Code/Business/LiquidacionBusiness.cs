﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class LiquidacionBusiness : GenericBusiness<LiquidacionBusiness, Liquidacion>
    {
        public Liquidacion GetLiquidacion(int id)
        {
            return DataAccessManager.GetById<Liquidacion>(id);
        }

        public List<Liquidacion> GetLiquidaciones(string num, int jtStartIndex, int jtPageSize, ref int rowCount)
        {
            try
            {
                IQueryable<Liquidacion> query = DataAccessManager.GetByConditions<Liquidacion>(p => p.Numero.Contains(num)).AsQueryable();

                rowCount = query.Count();

                return query.Skip(jtStartIndex).Take(jtPageSize).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateLiquidacion(Liquidacion LiquidacionCheques, LiquidacionDetalle[] liquidacionDetalleList)
        {

            if (liquidacionDetalleList != null && liquidacionDetalleList.Any())
            {
                Liquidacion entidad = new Liquidacion
                {
                    Numero = LiquidacionCheques.Numero,
                    DiaLiquidacion = LiquidacionCheques.DiaLiquidacion,
                    Estado = (int)EntityStatus.Active,
                    Facturacion = LiquidacionCheques.Facturacion
                };

                foreach (var detalle in liquidacionDetalleList)
                {
                    LiquidacionDetalle liquidacionDetalle = new LiquidacionDetalle();

                    liquidacionDetalle.Liquidacion = entidad;
                    liquidacionDetalle.Comision =detalle.Comision;
                    liquidacionDetalle.Gastos = detalle.Gastos;
                    liquidacionDetalle.IdCheque = detalle.IdCheque;
                    liquidacionDetalle.Importe = detalle.Importe;
                    liquidacionDetalle.Tasa = detalle.Tasa;
                    liquidacionDetalle.Estado =  (int)EstadoCheque.Cartera;

                    //LiquidacionDetalleBusiness.Instance.SaveOrUpdateCheque(liquidacionDetalle);

                    //Cheque che = ChequeBusiness.Instance.GetCheque(detalle.IdCheque);

                    //che.IdEstado = (int)Utils.Enumeraciones.Estados.eEstadosCheque.Aceptado;

                    //ChequeBusiness.Instance.SaveOrUpdateCheque(che);

                    //OperacionCuentaCorrienteBusiness.Instance.CreateEgreso(liquidacionDetalle, che)
                }

                SaveOrUpdate(entidad, ValidateSaveOrUpdate);

                AuditoriaBusiness.Instance.AuditoriaLiquidacion(AuditAction.Create, entidad);
            }
        }

        private bool ValidateSaveOrUpdate(Liquidacion entity)
        {
            if (true)
            {
                return true;
            }
            else
            {
                throw new ValidationException("No se puede registrar la liquidacion.");
            }
        }

        public void UpdateLiquidacion(Liquidacion liquidacion)
        {
            try
            {
                Liquidacion LiquidacionDB = this.GetLiquidacion(liquidacion.IdLiquidacion);

                if (LiquidacionDB != null)
                {
                    LiquidacionDB.DiaLiquidacion = liquidacion.DiaLiquidacion;
                    LiquidacionDB.Estado = liquidacion.Estado;
                    LiquidacionDB.Facturacion = liquidacion.Facturacion;
                    LiquidacionDB.Numero = liquidacion.Numero;

                    SaveOrUpdate(LiquidacionDB, ValidateSaveOrUpdate);
                }
                else
                {
                    throw new Exception("La Liquidacion no existe");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(Liquidacion entity)
        {
            if (!LiquidacionHasDetalle(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException(string.Format("No se puede eliminar el registro.", "El registro tiene detalles asociados"));
            }
        }

        private bool LiquidacionHasDetalle(Liquidacion entity)
        {
            try
            {
                return entity.LiquidacionDetalle.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
