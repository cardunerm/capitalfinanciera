﻿using CurrentContext;
using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class SucursalUsuarioBusiness : GenericBusiness<SucursalUsuarioBusiness, SucursalUsuario>
    {
        public SucursalUsuario GetCurrentUsuarioDefaultSucursal()
        {
            try
            {
                var query = DataAccessManager.GetByConditions<SucursalUsuario>(p =>
                                    p.IdUsuario == SessionContext.IdUsuarioActual &&
                                    p.Sucursal.IdEmpresa == SessionContext.IdEmpresaActual
                                    && p.Sucursal.Estado == 1
                                    , p => p.Sucursal);


                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SucursalUsuario> GetUsuarioSucursal()
        {
            try
            {
                var query = DataAccessManager.GetByConditions<SucursalUsuario>(p =>
                                    p.IdUsuario == SessionContext.UsuarioActual.IdUsuario &&
                                    p.Sucursal.IdEmpresa == SessionContext.IdEmpresaActual && p.Sucursal.Estado == 1,
                                            p => p.Sucursal);

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool HasUsuarioSucursalAssigned(long idSucursal)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<SucursalUsuario>(p =>
                                    p.IdUsuario == SessionContext.UsuarioActual.IdUsuario &&
                                    p.IdSucursal == idSucursal &&
                                    p.Sucursal.IdEmpresa == SessionContext.IdEmpresaActual);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


}
