﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ComisionBusiness : GenericBusiness<ComisionBusiness, Comision>
    {

        #region methods

        public List<Comision> GetByFilters(string vendedor,  int skip, int top, ref int rowCount)
        {
            List<string> _includes = new List<string>();
            _includes.Add("ComisionDetalle");
            int _idVendedor = (vendedor == "")? 0 : int.Parse(vendedor);

            try
            {
                IEnumerable<Comision> query = DataAccessManager.GetByConditions<Comision>(p => p.Activo == true && (_idVendedor == 0 || p.IdVendedor == _idVendedor) ,_includes);
                query = query.OrderByDescending(p => p.Id);
                rowCount = query.Count();
                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }       

        #endregion 

    }
}
