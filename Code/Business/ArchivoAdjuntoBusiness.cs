﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ArchivoAdjuntoBusiness: GenericBusiness<ArchivoAdjuntoBusiness, DetalleArchivoAdjunto>
    {
        public List<DetalleArchivoAdjunto> GetAttachedfiles(int idRecord, int idEntity)
        {
            using (var dbContext = DataAccessManager.GetContext() as DCREntities)
            {

                var query = (from details in dbContext.DetalleArchivoAdjunto
                             join attachedHead in dbContext.CabeceraAdjuntos on details.IdCabeceraAdjuntos equals attachedHead.Id
                             where attachedHead.IdEntidadEnum == idEntity && details.IdEntidadRegistro == idRecord
                             select details);

                if (query == null)
                    return null;
                else
                    return query.ToList() as List<DetalleArchivoAdjunto>;

            }
        }
    }
}
