﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Business
{
    public class EmisorValorBusiness : GenericBusiness<EmisorValorBusiness, EmisorValor>
    {
        #region Methods

        public List<EmisorValor> GetByFilters(string numero, string cuit, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<EmisorValor> query = DataAccessManager.GetByConditions<EmisorValor>(p =>
                                                p.Estado == (int)EntityStatus.Active &&
                                                p.NumeroEmisor.Contains(numero) &&
                                                p.CUIT.Contains(cuit));

                query = query.OrderBy(p => p.NumeroEmisor).ThenBy(x => x.CUIT);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmisorValor> GetEmisoresValorList()
        {
            try
            {
                IEnumerable<EmisorValor> query = DataAccessManager.GetByConditions<EmisorValor>(p =>
                                                p.Estado == (int)EntityStatus.Active);

                query = query.OrderBy(p => p.NumeroEmisor).ThenBy(x => x.CUIT);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveOrUpdateEmisorValor(EmisorValor emisorValor)
        {
            EmisorValor entity;

            if (emisorValor.IdEmisorValor > 0)
            {
                entity = GetEntity(emisorValor.IdEmisorValor);
                entity.Clasificacion = emisorValor.Clasificacion;
                entity.CUIT = emisorValor.CUIT;
                entity.NumeroEmisor = emisorValor.NumeroEmisor;
                entity.TasaAsignada = emisorValor.TasaAsignada;
                entity.Observaciones = emisorValor.Observaciones;
            }
            else
            {
                entity = new EmisorValor
                {
                    Clasificacion = emisorValor.Clasificacion,
                    CUIT = emisorValor.CUIT,
                    NumeroEmisor = emisorValor.NumeroEmisor,
                    TasaAsignada = emisorValor.TasaAsignada,
                    Observaciones = emisorValor.Observaciones,
                    Estado = (int)EntityStatus.Active,
                };
            }

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaEmisorValor(emisorValor.IdEmisorValor > 0 ? AuditAction.Edit : AuditAction.Create, entity);
        }

        private bool ValidateSaveOrUpdate(EmisorValor entity)
        {
            if (!EmisorValorExist(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException("Ya existe un emisor de valor con ese CUIT");
            }
        }

        public bool EmisorValorExist(EmisorValor entity)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Cliente>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.CUIT.Equals(entity.CUIT));

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(EmisorValor entity)
        {
            //if (!ClienteHasCheques(entity))
            //{
                return true;
            //}
            //else
            //{
            //    throw new ValidationException(string.Format("No se puede eliminar el registro.", "El cliente tiene cheques asociados"));
            //}
        }             
       
        #endregion
    }
}