﻿using Domine;
using Enumerations;
using Luma.Utils.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using Utils.Enumeraciones;

namespace Business
{
    public class CtaCteInvUSDBussines : GenericBusiness<CtaCteInvUSDBussines, CuentaCorrienteInversor>
    {

        #region Methods

        public List<View_SaldoCtaCteInvUSD> GetCuentaCorrienteList(string inversor)
        {
            try
            {
                IEnumerable<View_SaldoCtaCteInvUSD> query = DataAccessManager.GetByConditions<View_SaldoCtaCteInvUSD>(p => p.NombreInversor.Contains(inversor));

                query = query.OrderBy(p => p.IdCtaCteInvUSD);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveOrUpdateCuentaCorriente(CuentaCorrienteInversor cuenta)
        {
            CuentaCorrienteInversor entity;

            if (cuenta.IdInversor > 0)
            {
                entity = GetEntity(cuenta.IdCuentaCorrienteInversor);
                entity.Estado = cuenta.Estado;
                entity.IdInversor = cuenta.IdInversor;
            }
            else
            {
                entity = new CuentaCorrienteInversor
                {
                    IdInversor = cuenta.IdInversor,
                    Estado = true
                };
            }

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            //AuditoriaBusiness.Instance.AuditoriaCtaCteInversorUSD(cuenta.IdInversor > 0 ? AuditAction.Edit : AuditAction.Create, entity);
        }


        private bool ValidateSaveOrUpdate(CuentaCorrienteInversor entity)
        {
            //if (!ClienteNombreExist(entity))
            //{
            return true;
            //}
            //else
            //{
            //    throw new ValidationException("Ya existe un cliente con ese nombre");
            //}
        }

        public decimal? GetCtaCteSaldoDebito(DateTime? fechaDesde, DateTime? fechaHasta, int IdCtaCteInvUSD)
        {
            //List<CuentaCorrienteInversor> query = DataAccessManager.GetByConditions<CuentaCorrienteInversor>(p => p.TipoMovimiento == 1
            //                                                                                                   && p.Fecha >= fechaDesde
            //                                                                                                   && p.Fecha <= fechaHasta
            //                                                                                                   && p.IdCtaCteInvUSD == IdCtaCteInvUSD);

            //return query.Sum(op => op.M);
            return null;
        }

        public decimal? GetCtaCteSaldoCredito(DateTime? fechaDesde, DateTime? fechaHasta, int IdCtaCteInvUSD)
        {
            //List<OperacionCtaCteInvUSD> query = DataAccessManager.GetByConditions<OperacionCtaCteInvUSD>(p => p.TipoMovimiento == 2
            //                                                                                                   && p.Fecha >= fechaDesde
            //                                                                                                   && p.Fecha <= fechaHasta
            //                                                                                                   && p.IdCtaCteInvUSD == IdCtaCteInvUSD);

            //return query.Sum(op => op.Monto);
            return null;
        }


        public void DeleteLogicCuentaCorrienteInversortByInversor(long IdInversor)
        {
            //try
            //{
            //    List<CtaCteInvUSD> listCtaCte = DataAccessManager.GetByConditions<CtaCteInvUSD>(p =>
            //                                    p.IdInversor == IdInversor);

            //    foreach (var item in listCtaCte)
            //    {
            //        CtaCteInvUSD ctaCte = this.GetEntity(item.IdCtaCteInvUSD);
            //        ctaCte.Estado = false;
            //        SaveOrUpdate(ctaCte, ValidateSaveOrUpdate);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        public int OperationInversor(int IdInversor, decimal importe, int tipo)
        {
            //try
            //{
            //    CtaCteInvUSD ctaCte = DataAccessManager.GetByConditions<CtaCteInvUSD>(p =>
            //                                  p.IdInversor == IdInversor).FirstOrDefault();
            //    if (ctaCte == null)
            //    {
            //        var ex = new Exception("No hay cuenta corriente para el cliente");
            //        LogManager.Log.RegisterError(ex);
            //        throw ex;
            //    }

            //    OperacionCtaCteInvUSD opCtaCte = new OperacionCtaCteInvUSD()
            //    {
            //        IdCtaCteInvUSD = ctaCte.IdCtaCteInvUSD,
            //        Descripcion = ((tipo == 1) ? "Extracción" : "Depósito") + " de efectivo por caja",
            //        Estado = true,
            //        Fecha = DateTime.Now,
            //        Monto = importe,
            //        TipoMovimiento = (int)(tipo == 1 ? Estados.mTipoMovimientoCuentaCorriente.Debito : Estados.mTipoMovimientoCuentaCorriente.Credito)
            //    };

            //    using (DCREntities context = new DCREntities())
            //    {
            //        using (var tx = context.Database.BeginTransaction())
            //        {
            //            try
            //            {
            //                var imp = importe;
            //                var operaciones = context.Operacion.Where(op => op.Saldo > 0 && op.Estado == (int)Estados.eEstadosOp.Aprobado).OrderBy(op => op.Fecha);
            //                foreach (var op in operaciones)
            //                {
            //                    if (op.Saldo <= imp)
            //                    {
            //                        imp -= (op.Saldo ?? 0);
            //                        op.Saldo = 0;
            //                        op.Estado = (int)Estados.eEstadosOp.Pagada;
            //                    }
            //                    else
            //                    {
            //                        op.Saldo -= imp;
            //                        imp = 0;
            //                    }

            //                    if (imp <= 0)
            //                        break;
            //                }

            //                var opCaja = new OperacionCaja
            //                {
            //                    IdCaja = CajaBusiness.Instance.GetUltimaCaja().IdCaja,
            //                    TipoMovimiento = (int)(tipo == 1 ? Estados.mTipoMovimientoCuentaCorriente.Credito : Estados.mTipoMovimientoCuentaCorriente.Debito),
            //                    Monto = importe,
            //                    Fecha = DateTime.Now,
            //                    Descripcion = opCtaCte.Descripcion
            //                };

            //                context.OperacionCaja.Add(opCaja);
            //                context.OperacionCtaCteInvUSD.Add(opCtaCte);
            //                context.SaveChanges();
            //                tx.Commit();

            //                return opCtaCte.IdOperacionCtaCteInvUSD;
            //            }
            //            catch (Exception ex)
            //            {
            //                tx.Rollback();
            //                throw ex;
            //            }
            //        }
            //    }

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            return 0;
        }

        public decimal? GetSaldoByFecha(DateTime? fechaHasta, int IdCtaCteInvUSD)
        {
            //decimal? debito = 0.0M;
            //decimal? credito = 0.0M;

            //List<OperacionCtaCteInvUSD> queryCredito = DataAccessManager.GetByConditions<OperacionCtaCteInvUSD>(p => p.TipoMovimiento == 2
            //                                                                                                   && p.Fecha < fechaHasta
            //                                                                                                   && p.IdCtaCteInvUSD == IdCtaCteInvUSD);

            //List<OperacionCtaCteInvUSD> queryDebito = DataAccessManager.GetByConditions<OperacionCtaCteInvUSD>(p => p.TipoMovimiento == 1
            //                                                                                                   && p.Fecha < fechaHasta
            //                                                                                                   && p.IdCtaCteInvUSD == IdCtaCteInvUSD);

            //debito = queryDebito.Sum(op => op.Monto);

            //credito = queryCredito.Sum(op => op.Monto);


            //return credito - debito;
            return null;
        }

        #endregion
    }
}
