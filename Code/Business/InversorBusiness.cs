﻿using Domine;
using Enumerations;
using MailSender;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Business
{
    public class InversorBussines : GenericBusiness<InversorBussines, Inversor>
    {
        #region Methods
        public List<Inversor> GetByFilters(string inversor, string cuit, int skip, int top, ref int rowCount, int clasificacionInversor = -1)
        {
            try
            {
                IEnumerable<Inversor> query = DataAccessManager.GetByConditions<Inversor>(p =>
                                                p.Estado == (int)EntityStatus.Active &&
                                                ((p.Nombre.Contains(inversor) || inversor == "") ||
                                                 (p.CUIT.Contains(inversor) || inversor == "")) &&
                                                 (p.ClasificacionInversor == clasificacionInversor || clasificacionInversor == -1));         

                query = query.OrderBy(p => p.Nombre).ThenBy(x => x.CUIT);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Inversor> GetInversoresList()
        {
            try
            {
                IEnumerable<Inversor> query = DataAccessManager.GetByConditions<Inversor>(p =>
                                                p.Estado == (int)EntityStatus.Active);

                query = query.OrderBy(p => p.Nombre).ThenBy(x => x.CUIT);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Inversor SaveOrUpdateInversores(Inversor inversor)
        {

            bool createUser = false;
            

            using (TransactionScope tx = new TransactionScope())
            {
                try
                {
                    Inversor entity;

                    if (inversor.IdInversor > 0)
                    {
                        entity = GetEntity(inversor.IdInversor);
                        entity.Nombre = inversor.Nombre;
                        entity.CUIT = inversor.CUIT;
                        entity.Domicilio = inversor.Domicilio;
                        entity.TelefonoFijo = inversor.TelefonoFijo;
                        entity.TelefonoMovil = inversor.TelefonoMovil;
                        entity.Email = (inversor.Email == null) ? "" : inversor.Email;
                        entity.Clasificacion = inversor.Clasificacion;
                        entity.Tasa = inversor.Tasa;
                        entity.TasaUSD = inversor.TasaUSD;
                        entity.TasaMora = inversor.TasaMora;
                        entity.TasaMoraUSD = inversor.TasaMoraUSD;
                        entity.LimiteDescubierto = inversor.LimiteDescubierto;
                        entity.LimiteDescubiertoUSD = inversor.LimiteDescubiertoUSD;
                        entity.EnvioMail = inversor.EnvioMail;
                        entity.PorcentajeBalanceGarantiaOperativo = inversor.PorcentajeBalanceGarantiaOperativo;
                        entity.Alias = inversor.Alias;
                        entity.Contrato = (inversor.Contrato == "")? entity.Contrato : inversor.Contrato;

                        var context = DataAccessManager.GetContext() as DCREntities;
                        var historialAnteriorDolar = context.HistorialTasaInteresInversor.Where(p => p.IdInversor == inversor.IdInversor && p.TipoMonedaTasa == (int)TipoMoneda.Dolar).OrderByDescending(p => p.IdHistorial).FirstOrDefault();
                        var historialAnteriorPeso = context.HistorialTasaInteresInversor.Where(p => p.IdInversor == inversor.IdInversor && p.TipoMonedaTasa == (int)TipoMoneda.Peso).OrderByDescending(p => p.IdHistorial).FirstOrDefault();

                        if(historialAnteriorDolar.FechaDesde.Date < DateTime.Now.Date)
                        {
                            historialAnteriorDolar.FechaHasta = DateTime.Now.Date.AddSeconds(-1);
                            DataAccessManager.SaveOrUpdate(historialAnteriorDolar);
                            entity.HistorialTasaInteresInversor.Add
                            (
                                new HistorialTasaInteresInversor
                                {

                                    TipoMonedaTasa = (int)TipoMoneda.Dolar,
                                    ValorTasa = entity.TasaUSD,
                                    FechaDesde = DateTime.Now.Date,
                                    IdInversor = entity.IdInversor
                                }
                            );
                        }
                        else
                        {
                            historialAnteriorDolar.ValorTasa = inversor.TasaUSD;
                            DataAccessManager.SaveOrUpdate(historialAnteriorDolar);
                        }

                        if (historialAnteriorPeso.FechaDesde.Date < DateTime.Now.Date)
                        {
                            historialAnteriorPeso.FechaHasta = DateTime.Now.Date.AddSeconds(-1);
                            DataAccessManager.SaveOrUpdate(historialAnteriorPeso);
                            entity.HistorialTasaInteresInversor.Add
                                (
                                    new HistorialTasaInteresInversor
                                    {

                                        TipoMonedaTasa = (int)TipoMoneda.Peso,
                                        ValorTasa = entity.Tasa,
                                        FechaDesde = DateTime.Now.Date,
                                        IdInversor = entity.IdInversor
                                    }
                                );
                        }
                        else
                        {
                            historialAnteriorPeso.ValorTasa = inversor.Tasa;
                            DataAccessManager.SaveOrUpdate(historialAnteriorPeso);
                        }
                    }
                    else
                    {
                        createUser = true;


                        if (Any<Inversor>(e => e.CUIT == inversor.CUIT && e.Activo == true))
                        {
                            throw new ValidationException("Ya existe un Inversor con ese CUIT");
                        }

                        entity = new Inversor
                        {
                            Clasificacion = inversor.Clasificacion,
                            CUIT = inversor.CUIT,
                            Domicilio = inversor.Domicilio,
                            Email = (inversor.Email == null) ? "" : inversor.Email,
                            Nombre = inversor.Nombre,
                            TelefonoFijo = inversor.TelefonoFijo,
                            TelefonoMovil = inversor.TelefonoMovil,
                            Estado = (int)EntityStatus.Active,
                            Tasa = inversor.Tasa,
                            TasaUSD = inversor.TasaUSD,
                            TasaMora = inversor.TasaMora,
                            TasaMoraUSD = inversor.TasaMoraUSD,
                            LimiteDescubierto = inversor.LimiteDescubierto,
                            LimiteDescubiertoUSD = inversor.LimiteDescubiertoUSD,
                            EnvioMail = inversor.EnvioMail,
                            ClasificacionInversor = inversor.ClasificacionInversor,
                            PorcentajeBalanceGarantiaOperativo = inversor.PorcentajeBalanceGarantiaOperativo,
                            Alias = inversor.Alias
                        };
                        //Creacion Cta Cte Peso
                        entity.CuentaCorrienteInversor.Add
                            (
                                new CuentaCorrienteInversor
                                {
                                    Inversor = entity,
                                    Estado = true,
                                    TipoMoneda = (int)TipoMoneda.Peso,
                                    FechaAlta = DateTime.Now.Date,
                                    ClasificacionInversor = inversor.ClasificacionInversor
                                }
                            );

                        //Creacion Cta Cte Dolar
                        entity.CuentaCorrienteInversor.Add
                            (
                                new CuentaCorrienteInversor
                                {
                                    Inversor = entity,
                                    Estado = true,
                                    TipoMoneda = (int)TipoMoneda.Dolar,
                                    FechaAlta = DateTime.Now.Date,
                                    ClasificacionInversor = inversor.ClasificacionInversor
                                }
                            );

                        //Creacion Historial Tasa de Interes
                        entity.HistorialTasaInteresInversor.Add
                            (
                                new HistorialTasaInteresInversor
                                {

                                    TipoMonedaTasa = (int)TipoMoneda.Dolar,
                                    ValorTasa = entity.TasaUSD,
                                    FechaDesde = DateTime.Now.Date,
                                    IdInversor = entity.IdInversor
                                }
                            );

                        //Creacion Historial Tasa de Interes
                        entity.HistorialTasaInteresInversor.Add
                            (

                                new HistorialTasaInteresInversor
                                {
                                    TipoMonedaTasa = (int)TipoMoneda.Peso,
                                    ValorTasa = entity.Tasa,
                                    FechaDesde = DateTime.Now.Date,
                                    IdInversor = entity.IdInversor
                                }
                            );


                        entity.Usuario = new Usuario() { Nombre = inversor.Nombre, Apellido = inversor.Nombre, NombreUsuario = inversor.CUIT, Contrasenia = "202CB962AC59075B964B07152D234B70", Email = inversor.Email, FechaCreacion = DateTime.Now, Estado = 1, IdEmpresa = 1 };
                        entity.Usuario.UsuarioPerfil.Add(new UsuarioPerfil { Usuario = entity.Usuario, IdPerfil = 7, FechaCreacion = DateTime.Now, FechaEdicion = null, Estado = 1 });
                        entity.Usuario.SucursalUsuario.Add(new SucursalUsuario { Usuario = entity.Usuario, IdSucursal = 5 });
                    }

                    SaveOrUpdate(entity, ValidateSaveOrUpdate);


                    //Notificar al Inversor sus datos por mail
                    if (createUser)
                    {
                        MailBusiness mail = new MailBusiness();
                        string body = Resources.Resource.MailInversor.Replace("@@nombre", inversor.Nombre).Replace("@@usuario", inversor.CUIT).Replace("@@nclave", "123");
                        mail.SendMail(inversor.Email, "", "", "Se a creado su cuenta en Capital Arg.", body, null, true);

                    }


                    AuditoriaBusiness.Instance.AuditoriaInversores(inversor.IdInversor > 0 ? AuditAction.Edit : AuditAction.Create, entity);

                    tx.Complete();

                    return entity;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        private bool ValidateSaveOrUpdate(Inversor entity)
        {

            return true;
            //if (!InversorNombreExist(entity))
            //{
            //    return true;
            //}
            //else
            //{
            //    throw new ValidationException("Ya existe un inversor con ese nombre");
            //}
        }

        private bool InversorNombreExist(Inversor entity)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Inversor>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Nombre.Equals(entity.Nombre) &&
                                p.CUIT.Equals(entity.CUIT) &&
                                p.IdInversor != entity.IdInversor);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(Inversor entity)
        {
            // if (!ClienteHasCheques(entity))
            // {
            return true;
            // }
            // else
            // {
            //     throw new ValidationException(string.Format("No se puede eliminar el registro.", "El cliente tiene cheques asociados"));
            // }
        }

        //private bool ClienteHasCheques(Cliente entity)
        //{
        //    try
        //    {
        //        return entity.RecepcionCheques.Any();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public Inversor InversorExist(string nombre, string cuit)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Inversor>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Nombre.Equals(nombre) &&
                                p.CUIT.Equals(cuit));

                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
