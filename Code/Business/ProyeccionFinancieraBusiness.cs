﻿using AutoMapper;
using Domine;
using Domine.Dto;
using Luma.Utils.Logs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Utils.Enumeraciones;

namespace Business
{
    public class ProyeccionFinancieraBusiness : GenericBusiness<ProyeccionFinancieraBusiness, ResumenDiarioOperacionInversor>
    {
        public List<DtoProyeccionFinanciera> GetProyeccion(int idTipomodenda = 1, int idInversor = -1, int idCuenta = -1, ResumenDiarioOperacionInversor resumenExtra = null, DCREntities context = null)
        {
            List<DtoProyeccionFinanciera> dtoListFinal = new List<DtoProyeccionFinanciera>();
            List<DtoProyeccionFinanciera> dtoList = new List<DtoProyeccionFinanciera>();

            if (idInversor != -1 || idCuenta != -1)
            {
                try
                {
                    int diasAnterioreres = int.Parse(ConfigurationManager.AppSettings["DiasConsultaMovimientosHaciaAtras"]);
                    int diasPosteriores = int.Parse(ConfigurationManager.AppSettings["DiasProyeccionHaciaAdelante"]);

                    DateTime fechaActual = DateTime.Now;
                    DateTime fechaDesde = fechaActual.AddDays(-diasAnterioreres).Date;
                    DateTime fechaHasta = fechaActual.AddDays(diasPosteriores).Date;

                    List<ResumenDiarioOperacionInversor> list = new List<ResumenDiarioOperacionInversor>();

                    if (context == null)
                    {
                        list = DataAccessManager.GetByConditions<ResumenDiarioOperacionInversor>(r => r.IdCuentaCorrienteInversor == idCuenta && (r.Fecha >= fechaDesde && r.Fecha <= fechaActual));
                    }
                    else
                    {
                        list = (from rd in context.ResumenDiarioOperacionInversor
                                where rd.IdCuentaCorrienteInversor == idCuenta &&
                                (rd.Fecha >= fechaDesde && rd.Fecha <= fechaActual)
                                select rd).ToList();
                    }

                    if (list.Count != 0 && list != null)
                    {
                        if (resumenExtra != null)
                            list.Add(resumenExtra);

                        list = list.OrderBy(r => r.IdResumen).ToList();
                    }

                    dtoList = Mapper.Map<List<DtoProyeccionFinanciera>>(list).OrderBy(r => r.IdResumen).ToList();

                    int tipoMoneda = idTipomodenda;

                    if (context == null)
                    {
                        tipoMoneda = CuentaCorrienteInversorBussines.Instance.GetByConditions(c => c.IdCuentaCorrienteInversor == idCuenta).FirstOrDefault().TipoMoneda;
                    }
                    else
                    {
                        tipoMoneda = (from ctacteInv in context.CuentaCorrienteInversor
                                      where ctacteInv.IdCuentaCorrienteInversor == idCuenta
                                      select ctacteInv).FirstOrDefault().TipoMoneda;
                    }

                    decimal interes = 0;

                    if (context == null)
                    {
                        interes = HistorialTasaInteresBusiness.Instance.GetByConditions<HistorialTasaInteresInversor>(t => t.IdInversor == idInversor && t.FechaHasta == null && t.TipoMonedaTasa == tipoMoneda).ToList().OrderByDescending(t => t.IdHistorial).First().ValorTasa.Value;
                    }
                    else
                    {
                        interes = (from hti in context.HistorialTasaInteresInversor
                                   where (hti.IdInversor == idInversor &&
                                         hti.FechaHasta == null &&
                                         hti.TipoMonedaTasa == tipoMoneda)
                                   select hti
                                   ).ToList().OrderByDescending(t => t.IdHistorial).First().ValorTasa.Value;
                    }



                    
                    DateTime contadorFecha = dtoList.Count != 0 && dtoList.Last().Fecha.Date == fechaActual.Date ? fechaActual.Date.AddDays(1) : fechaActual.Date;
                    decimal _ingreso, _egreso, intereses, saldoAcumulado = 0;
                    bool calculaInteres = true;
                    var _operacionesInversorDia = OperacionInversoresBussines.Instance.GetAll().Where(p => p.IdCuentaCorrienteInversor == idCuenta && p.Estado == (int)Estados.eEstadosOp.Pendiente).ToList();

                    if (_operacionesInversorDia.Any())
                    {
                        contadorFecha = _operacionesInversorDia.OrderByDescending(p => p.Fecha).First().Fecha;
                        saldoAcumulado = dtoList.Where(p => p.Fecha <= contadorFecha).Sum(p => p.Saldo); 
                    }


                    //Calcular proyeccion
                    while (contadorFecha <= fechaHasta)
                    {
                        _ingreso = 0;
                        _egreso = 0;
                        intereses = 0;
                        calculaInteres = true;
                        
                        //if (_operacionesInversorDia.Count > 0 && contadorFecha.ToString("yyyMMdd") == DateTime.Now.ToString("yyyyMMdd"))
                        if (_operacionesInversorDia.Count > 0)
                        {
                            foreach (var item in _operacionesInversorDia.OrderBy(p=>p.Fecha))
                            {
                                if(item.Fecha.ToString("yyyyMMdd") == contadorFecha.ToString("yyyMMdd") && item.TipoMovimiento == 2)
                                {
                                    calculaInteres = false;
                                    _ingreso = item.Monto;
                                    intereses = saldoAcumulado * (interes / 100);
                                    saldoAcumulado = saldoAcumulado + _ingreso;
                                    

                                }


                                var resul = dtoList.SingleOrDefault(p => p.Fecha.ToString("yyyyMMdd") == contadorFecha.ToString("yyyyMMdd") );
                                if (resul != null)
                                {
                                    _ingreso = resul.Ingreso;
                                    _egreso = resul.Egreso;
                                    saldoAcumulado = saldoAcumulado + _ingreso - _egreso;
                                }


                                if (item.Fecha.ToString("yyyyMMdd") == contadorFecha.ToString("yyyMMdd") && item.TipoMovimiento == 1)
                                {

                                    _egreso = item.Monto; 
                                    saldoAcumulado = saldoAcumulado - _egreso;
                                }                               

                            }                            
                        }
                       
                        dtoListFinal.Add(new DtoProyeccionFinanciera
                        {
                            Fecha = contadorFecha,
                            TasaInteres = interes / 100,
                            Ingreso = _ingreso,
                            Egreso = _egreso,
                            Intereses = (calculaInteres)? saldoAcumulado * (interes / 100) : intereses,
                            Impuestos = 0,
                            Gastos = 0,
                            Multas = 0,
                            Total = (calculaInteres) ? saldoAcumulado * (interes / 100) : intereses,                            
                            Saldo = (calculaInteres) ? (saldoAcumulado + (saldoAcumulado * (interes / 100))) : saldoAcumulado,
                            EsCalculado = true
                        });

                        //Acumular saldo
                        saldoAcumulado = saldoAcumulado + (calculaInteres ? (saldoAcumulado * (interes / 100)) : 0);

                        //Incrementar contador de fecha
                        contadorFecha = contadorFecha.AddDays(1);
                    }


                    return dtoListFinal;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                return dtoList;
            }
        }

        public List<DtoProyeccionFinanciera> GetProyeccionResumen(int tipomoneda = 0, ResumenDiarioOperacionInversor resumenExtra = null, DCREntities context = null)
        {
                List<DtoProyeccionFinanciera> dtoList = new List<DtoProyeccionFinanciera>();
                List<DtoProyeccionFinanciera> dtoListFinal = new List<DtoProyeccionFinanciera>();


            try
                {
                    int diasAnterioreres = int.Parse(ConfigurationManager.AppSettings["DiasConsultaMovimientosHaciaAtras"]);
                    int diasPosteriores = int.Parse(ConfigurationManager.AppSettings["DiasProyeccionHaciaAdelante"]);

                    DateTime fechaActual = DateTime.Now;
                    DateTime fechaDesde = fechaActual.AddDays(-diasAnterioreres).Date;
                    DateTime fechaHasta = fechaActual.AddDays(diasPosteriores).Date;

                    List<ResumenDiarioOperacionInversor> list = new List<ResumenDiarioOperacionInversor>();

                    
                    //Obtengo el listado de los inversores 
                    var inversoresList = InversorBussines.Instance.GetInversoresList().Where(p=>p.ClasificacionInversor == 1);
                    int idInversor = 0;
                    int idCuenta = 0;

                    foreach (var item in inversoresList)
                    {

                    //if (item.ClasificacionInversor == 2)
                    //    continue;
                    //1 == Peso, 2 == Dolar
                        idInversor = item.IdInversor;
                        idCuenta = item.CuentaCorrienteInversor.SingleOrDefault(p => p.TipoMoneda == tipomoneda).IdCuentaCorrienteInversor;

                        if (context == null)
                        {
                            list = DataAccessManager.GetByConditions<ResumenDiarioOperacionInversor>(r => r.IdCuentaCorrienteInversor == idCuenta && (r.Fecha >= fechaDesde && r.Fecha <= fechaActual));
                            //list = DataAccessManager.GetByConditions<ResumenDiarioOperacionInversor>(r=> (r.Fecha >= fechaDesde && r.Fecha <= fechaActual));
                        }
                        else
                        {
                            list = (from rd in context.ResumenDiarioOperacionInversor
                                    where rd.IdCuentaCorrienteInversor == idCuenta &&
                                    (rd.Fecha >= fechaDesde && rd.Fecha <= fechaActual)
                                    select rd).ToList();

                    }

                        if (list.Count != 0 && list != null)
                        {
                            if (resumenExtra != null)
                                list.Add(resumenExtra);

                            list = list.OrderBy(r => r.IdResumen).ToList();
                        }

                        dtoList = Mapper.Map<List<DtoProyeccionFinanciera>>(list).OrderBy(r => r.IdResumen).ToList();

                        int tipoMoneda = 0;

                        if (context == null)
                        {
                            tipoMoneda = CuentaCorrienteInversorBussines.Instance.GetByConditions(c => c.IdCuentaCorrienteInversor == idCuenta).FirstOrDefault().TipoMoneda;
                        }
                        else
                        {
                            tipoMoneda = (from ctacteInv in context.CuentaCorrienteInversor
                                          where ctacteInv.IdCuentaCorrienteInversor == idCuenta
                                          select ctacteInv).FirstOrDefault().TipoMoneda;
                        }

                        decimal interes = 0;

                        if (context == null)
                        {
                            interes = HistorialTasaInteresBusiness.Instance.GetByConditions<HistorialTasaInteresInversor>(t => t.IdInversor == idInversor && t.FechaHasta == null && t.TipoMonedaTasa == tipoMoneda).ToList().OrderByDescending(t => t.IdHistorial).First().ValorTasa.Value;
                        }
                        else
                        {
                            interes = (from hti in context.HistorialTasaInteresInversor
                                       where (hti.IdInversor == idInversor &&
                                             hti.FechaHasta == null &&
                                             hti.TipoMonedaTasa == tipoMoneda)
                                       select hti
                                       ).ToList().OrderByDescending(t => t.IdHistorial).First().ValorTasa.Value;
                        }

                        decimal saldoAcumulado = dtoList.Count == 0 ? 0 : dtoList.Last().Saldo;
                        DateTime contadorFecha = dtoList.Count != 0 && dtoList.Last().Fecha.Date == fechaActual.Date ? fechaActual.Date.AddDays(1) : fechaActual.Date;

                    //Obtengo las Operaciones del día
                    decimal _ingreso = 0;
                    var _operacionesInversorDia = OperacionInversoresBussines.Instance.GetAll().Where(p => p.IdCuentaCorrienteInversor == idCuenta 
                                                                                                    && p.Fecha.Year == DateTime.Now.Year
                                                                                                    && p.Fecha.Month == DateTime.Now.Month
                                                                                                    && p.Fecha.Day == DateTime.Now.Day).ToList();

                    //Calcular proyeccion
                    while (contadorFecha <= fechaHasta)
                        {

                            if (_operacionesInversorDia.Count > 0 && contadorFecha.ToString("yyyMMdd") == DateTime.Now.ToString("yyyyMMdd"))
                            {
                                _ingreso = _operacionesInversorDia.Sum(p => p.Monto);
                                saldoAcumulado = saldoAcumulado + _ingreso;
                            }
                            else
                                _ingreso = 0;

                            

                            dtoList.Add(new DtoProyeccionFinanciera
                            {
                                Fecha = contadorFecha,
                                TasaInteres = interes / 100,
                                Ingreso = _ingreso,
                                Egreso = 0,
                                Intereses = saldoAcumulado * (interes / 100),
                                Impuestos = 0,
                                Gastos = 0,
                                Multas = 0,
                                Total = saldoAcumulado * (interes / 100),
                                Saldo = saldoAcumulado + (saldoAcumulado * (interes / 100)),
                                EsCalculado = true
                            });




                            dtoListFinal.Add(new DtoProyeccionFinanciera
                            {
                                Fecha = contadorFecha,
                                TasaInteres = interes / 100,
                                Ingreso = _ingreso,
                                Egreso = 0,
                                Intereses = saldoAcumulado * (interes / 100),
                                Impuestos = 0,
                                Gastos = 0,
                                Multas = 0,
                                Total = saldoAcumulado * (interes / 100),
                                Saldo = saldoAcumulado + (saldoAcumulado * (interes / 100)),
                                EsCalculado = true
                            });

                            //Acumular saldo
                            saldoAcumulado = saldoAcumulado + (saldoAcumulado * (interes / 100));

                            //Incrementar contador de fecha
                            contadorFecha = contadorFecha.AddDays(1);                            
                        }                      
                    }

                    return dtoListFinal;

                    }
                    catch (Exception)
                    {
                        throw;
                    }
            
        }

        public List<DtoProyeccionFinanciera> GetProyeccionBBB(int idInversor = -1, int idCuenta = -1, ResumenDiarioOperacionInversor resumenExtra = null, DCREntities context = null)
        {
            List<DtoProyeccionFinanciera> dtoList = new List<DtoProyeccionFinanciera>();

            if (idInversor != -1 || idCuenta != -1)
            {
                try
                {
                    int diasAnterioreres = int.Parse(ConfigurationManager.AppSettings["DiasConsultaMovimientosHaciaAtras"]);
                    int diasPosteriores = int.Parse(ConfigurationManager.AppSettings["DiasProyeccionHaciaAdelante"]);

                    DateTime fechaActual = DateTime.Now;
                    DateTime fechaDesde = fechaActual.AddDays(-diasAnterioreres).Date;
                    DateTime fechaHasta = fechaActual.AddDays(diasPosteriores).Date;

                    List<ResumenDiarioOperacionInversor> list = new List<ResumenDiarioOperacionInversor>();

                    if (context == null)
                    {
                        list = DataAccessManager.GetByConditions<ResumenDiarioOperacionInversor>(r => r.IdCuentaCorrienteInversor == idCuenta && (r.Fecha >= fechaDesde && r.Fecha <= fechaActual));
                    }
                    else
                    {
                        list = (from rd in context.ResumenDiarioOperacionInversor
                                where rd.IdCuentaCorrienteInversor == idCuenta &&
                                (rd.Fecha >= fechaDesde && rd.Fecha <= fechaActual)
                                select rd).ToList();
                    }

                    if (list.Count != 0 && list != null)
                    {
                        if (resumenExtra != null)
                            list.Add(resumenExtra);

                        list = list.OrderBy(r => r.IdResumen).ToList();
                    }

                    dtoList = Mapper.Map<List<DtoProyeccionFinanciera>>(list).OrderBy(r => r.IdResumen).ToList();

                    int tipoMoneda = 0;

                    if (context == null)
                    {
                        tipoMoneda = CuentaCorrienteInversorBussines.Instance.GetByConditions(c => c.IdCuentaCorrienteInversor == idCuenta).FirstOrDefault().TipoMoneda;
                    }
                    else
                    {
                        tipoMoneda = (from ctacteInv in context.CuentaCorrienteInversor
                                      where ctacteInv.IdCuentaCorrienteInversor == idCuenta
                                      select ctacteInv).FirstOrDefault().TipoMoneda;
                    }

                    decimal interes = 0;

                    if (context == null)
                    {
                        interes = HistorialTasaInteresBusiness.Instance.GetByConditions<HistorialTasaInteresInversor>(t => t.IdInversor == idInversor && t.FechaHasta == null && t.TipoMonedaTasa == tipoMoneda).ToList().OrderByDescending(t => t.IdHistorial).First().ValorTasa.Value;
                    }
                    else
                    {
                        interes = (from hti in context.HistorialTasaInteresInversor
                                   where (hti.IdInversor == idInversor &&
                                         hti.FechaHasta == null &&
                                         hti.TipoMonedaTasa == tipoMoneda)
                                   select hti
                                   ).ToList().OrderByDescending(t => t.IdHistorial).First().ValorTasa.Value;
                    }

                    decimal saldoAcumulado = dtoList.Count == 0 ? 0 : dtoList.Last().Saldo;
                    DateTime contadorFecha = dtoList.Count != 0 && dtoList.Last().Fecha.Date == fechaActual.Date ? fechaActual.Date.AddDays(1) : fechaActual.Date;

                    //Calcular proyeccion
                    while (contadorFecha <= fechaHasta)
                    {
                        dtoList.Add(new DtoProyeccionFinanciera
                        {
                            Fecha = contadorFecha,
                            TasaInteres = interes / 100,
                            Ingreso = 0,
                            Egreso = 0,
                            Intereses = saldoAcumulado * (interes / 100),
                            Impuestos = 0,
                            Gastos = 0,
                            Multas = 0,
                            Total = saldoAcumulado * (interes / 100),
                            Saldo = saldoAcumulado + (saldoAcumulado * (interes / 100)),
                            EsCalculado = true
                        });

                        //Acumular saldo
                        saldoAcumulado = saldoAcumulado + (saldoAcumulado * (interes / 100));

                        //Incrementar contador de fecha
                        contadorFecha = contadorFecha.AddDays(1);
                    }

                    //Insertamos, en el objeto a devolver, la información de los cheques vencidos y por vencer (Todos los cheques en cartera)
                    List<Cheque> chequesEnCartera = GetChequesEnCartera(idCuenta, fechaDesde, fechaHasta);
                    
                    foreach (var cheque in chequesEnCartera)
                    {
                        string moneda = tipoMoneda == 1 ? "ARS" : "USD";
                        DateTime fechaCheque = cheque.FechaVencimiento.Date;
                        string infoCheque = cheque.Numero + " (" + moneda + " " + cheque.Monto + ")";
                        if (string.IsNullOrEmpty(dtoList.Find(d => d.Fecha == fechaCheque).ChequesPorVencer))
                        {
                            dtoList.Find(d => d.Fecha == fechaCheque).ChequesPorVencer = infoCheque;
                        }
                        else
                        {
                            dtoList.Find(d => d.Fecha == fechaCheque).ChequesPorVencer = dtoList.Find(d => d.Fecha == fechaCheque).ChequesPorVencer + ", " + infoCheque;
                        }
                    }

                    return dtoList;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                return dtoList;
            }
        }

        public List<Cheque> GetChequesEnCartera(int idCuenta, DateTime? fechaDesde_remote = null, DateTime? fechaHasta_remote = null, int Clasificacion = 0)
        {
            try
            {
                DateTime fechaDesde;
                DateTime fechaHasta;

                if (fechaDesde_remote == null && fechaHasta_remote == null)
                {
                    int diasAnterioreres = int.Parse(ConfigurationManager.AppSettings["DiasConsultaMovimientosHaciaAtras"]);
                    int diasPosteriores = int.Parse(ConfigurationManager.AppSettings["DiasProyeccionHaciaAdelante"]);

                    DateTime fechaActual_local = DateTime.Now;
                    DateTime fechaDesde_local = fechaActual_local.AddDays(-diasAnterioreres).Date;
                    DateTime fechaHasta_local = fechaActual_local.AddDays(diasPosteriores).Date;

                    fechaDesde = fechaDesde_local;
                    fechaHasta = fechaHasta_local;
                }
                else
                {
                    fechaDesde = fechaDesde_remote.Value;
                    fechaHasta = fechaHasta_remote.Value;
                }

                List<Cheque> chequesEnCartera = new List<Cheque>();
                fechaHasta = fechaHasta.AddDays(1).Date;
                using (DCREntities context = new DCREntities())
                {
                    chequesEnCartera = (from ch in context.Cheque
                                             join detOp in context.DetalleOperacion on ch.IdCheque equals detOp.IdCheque
                                             join opInv in context.OperacionInversor on detOp.IdOperacionInversor equals opInv.IdOperacionInversor
                                             join ctacte in context.CuentaCorrienteInversor on opInv.IdCuentaCorrienteInversor equals ctacte.IdCuentaCorrienteInversor
                                             where
                                             ctacte.IdCuentaCorrienteInversor == idCuenta &&
                                             ch.FechaVencimiento >= fechaDesde &&
                                             ch.FechaVencimiento <= fechaHasta &&
                                             (Clasificacion == 0 || ch.Clasificacion == Clasificacion) &&
                                             opInv.Estado == 1
                                             select ch).ToList();
                }

                return chequesEnCartera;
                
            }
            catch(Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw;
            }
        }
    }
}
