﻿using Domine;
using Resources;
using System;
using System.Configuration;
using System.Net.Mail;
using System.Text;

namespace Business
{
    public class SendEmailBusiness
    {
        #region Singleton

        private static readonly SendEmailBusiness instance = new SendEmailBusiness();

        public static SendEmailBusiness Instance { get { return instance; } }

        private SendEmailBusiness() { }

        static SendEmailBusiness() { }

        #endregion

        #region Methods

        public void SendEmail(string emailTo, string subject, string emailBody)
        {
            try
            {
                MailMessage message = new MailMessage();
                message.To.Add(emailTo);

                SendEmail(message, subject, emailBody);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendEmail(MailMessage message, string subject, string emailBody)
        {
            try
            {
                string sendEmailEnabledStr = ConfigurationManager.AppSettings["SendEmailEnabled"];
                bool sendEmailEnabled;

                if (bool.TryParse(sendEmailEnabledStr, out sendEmailEnabled) && sendEmailEnabled)
                {
                    string addressFrom = ConfigurationManager.AppSettings["EmailAccount"];
                    string passwordFrom = ConfigurationManager.AppSettings["EmailContrasenia"];
                    string smtpHost = ConfigurationManager.AppSettings["EmailSMTPHost"];
                    int smtpPort = int.Parse(ConfigurationManager.AppSettings["EmailSMTPPort"]);

                    message.From = new MailAddress(addressFrom);
                    message.Subject = subject;
                    message.Body = emailBody;
                    message.BodyEncoding = UTF8Encoding.UTF8;

                    SmtpClient smtpClient = new SmtpClient();
                    smtpClient.Host = smtpHost;
                    smtpClient.Port = smtpPort;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.Timeout = 10000;
                    smtpClient.EnableSsl = true;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential(addressFrom, passwordFrom);

                    smtpClient.Send(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void NotifyPasswordChanged(Usuario usuario, string newContrasenia)
        {
            try
            {
                string emailTo = usuario.Email;
                string subject = Resource.ContraseniaReset;
                string emailBody = string.Format(Resource.ResetContraseniaEmailBody, usuario.NombreUsuario, newContrasenia);

                SendEmail(emailTo, subject, emailBody);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}