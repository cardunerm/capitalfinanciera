﻿using Enumerations;
using CurrentContext;
using Domine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class AuditoriaBusiness : GenericBusiness<AuditoriaBusiness, Auditoria>
    {
        #region Save Methods

        private void SaveAuditoria(Auditoria auditoria)
        {
            try
            {
                DataAccessManager.Save(auditoria);
                DataAccessManager.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveAuditoria(List<Auditoria> auditoriaList)
        {
            try
            {
                foreach (var item in auditoriaList)
                {
                    DataAccessManager.Save(item);
                }

                DataAccessManager.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Auditoria Methods

        //public void AuditoriaBanco(AuditAction accion, Banco entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Bancos,
        //            IdEntidadRelacionada = entity.IdBanco,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaBancoSucursal(AuditAction accion, BancoSucursal entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.BancoSucursales,
        //            IdEntidadRelacionada = entity.IdBancoSucursal,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaClasificacion(AuditAction accion, Clasificacion entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Clasificaciones,
        //            IdEntidadRelacionada = entity.IdClasificacion,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaCotizador(AuditAction accion, Cotizador entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Cotizadores,
        //            IdEntidadRelacionada = entity.IdCotizador,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaDeclaracionJurada(AuditAction accion, DeclaracionJurada entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.DeclaracionesJuradas,
        //            IdEntidadRelacionada = entity.IdDeclaracionJurada,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaElemento(AuditAction accion, Elemento entity)
        //{
        //    try
        //    {
        //        List<Elemento> list = new List<Elemento>() { entity };

        //        AuditoriaElemento(accion, list);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaElemento(AuditAction accion, IEnumerable<Elemento> entities)
        //{
        //    try
        //    {
        //        List<Auditoria> list = new List<Auditoria>();

        //        foreach (var item in entities)
        //        {
        //            list.Add(new Auditoria
        //            {
        //                IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //                IPUsuario = SessionContext.DireccionIPActual,
        //                Accion = (int)accion,
        //                IdModulo = (int)ModuloSistema.Elementos,
        //                IdEntidadRelacionada = item.IdElemento,
        //                Fecha = DateTime.Now,
        //                Modificacion = item.GetFieldsAsText()
        //            });
        //        }

        //        SaveAuditoria(list);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaElementoActividad(AuditAction accion, ElementoActividad entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Elementos,
        //            IdEntidadRelacionada = entity.IdElementoActividad,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaEmpresa(AuditAction accion, Empresa entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Empresas,
        //            IdEntidadRelacionada = entity.IdEmpresa,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaEmpresaMedio(Empresa entity, IEnumerable<EmpresaMedio> oldMedios)
        //{
        //    try
        //    {
        //        List<Auditoria> list = new List<Auditoria>();

        //        if (oldMedios != null)
        //        {
        //            foreach (var item in oldMedios)
        //            {
        //                list.Add(new Auditoria
        //                {
        //                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //                    IPUsuario = SessionContext.DireccionIPActual,
        //                    Accion = (int)AuditAction.Delete,
        //                    IdModulo = (int)ModuloSistema.EmpresaMedios,
        //                    IdEntidadRelacionada = entity.IdEmpresa,
        //                    Descripcion = string.Format(Resource.MedioDesasignadoDeEmpresa,
        //                                                item.NombreMedio,
        //                                                entity.Nombre),
        //                    Fecha = DateTime.Now
        //                });
        //            }
        //        }

        //        foreach (var item in entity.EmpresaMedio.Where(p => p.Estado == (int)EntityStatus.Active))
        //        {
        //            list.Add(new Auditoria
        //            {
        //                IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //                IPUsuario = SessionContext.DireccionIPActual,
        //                Accion = (int)AuditAction.Create,
        //                IdModulo = (int)ModuloSistema.EmpresaMedios,
        //                IdEntidadRelacionada = entity.IdEmpresa,
        //                Descripcion = string.Format(Resource.MedioAsignadoAEmpresa,
        //                                            item.NombreMedio,
        //                                            entity.Nombre),
        //                Fecha = DateTime.Now
        //            });
        //        }

        //        SaveAuditoria(list);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaEspecificacionTecnica(AuditAction accion, EspecificacionTecnica entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.EspecificacionesTecnicas,
        //            IdEntidadRelacionada = entity.IdEspecificacionTecnica,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaFacturacionPauta(AuditAction accion, FacturacionPauta entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.FacturacionPautas,
        //            IdEntidadRelacionada = entity.IdFacturacionPauta,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaGrupoElemento(AuditAction accion, GrupoElemento entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.GruposElementos,
        //            IdEntidadRelacionada = entity.IdGrupoElemento,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaGrupoElementoContenido(GrupoElemento entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)AuditAction.Edit,
        //            IdModulo = (int)ModuloSistema.GruposElementosContenido,
        //            IdEntidadRelacionada = entity.IdGrupoElemento,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaGrupoElementoContenido(GrupoElemento entity, IEnumerable<GrupoElementoContenido> oldContent)
        //{
        //	try
        //	{
        //		List<Auditoria> list = new List<Auditoria>();

        //		if (oldContent != null)
        //		{
        //			foreach (var item in oldContent)
        //			{
        //				var tipoElemento = TipoElementoBusiness.Instance.GetEntity(item.IdTipoElemento);

        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Delete,
        //					IdModulo = (int)ModuloSistema.GruposElementosContenido,
        //					IdEntidadRelacionada = entity.IdGrupoElemento,
        //					Descripcion = string.Format(Resource.ContentUnassignedFromGrupoElemento,
        //												tipoElemento.Nombre,
        //												entity.Nombre),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		foreach (var item in entity.GrupoElementoContenido.Where(p => p.Estado == (int)EntityStatus.Active))
        //		{
        //			var tipoElemento = TipoElementoBusiness.Instance.GetEntity(item.IdTipoElemento);

        //			list.Add(new Auditoria
        //			{
        //				IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //				IPUsuario = SessionContext.DireccionIPActual,
        //				Accion = (int)AuditAction.Create,
        //				IdModulo = (int)ModuloSistema.GruposElementosContenido,
        //				IdEntidadRelacionada = entity.IdGrupoElemento,
        //				Descripcion = string.Format(Resource.ContentAssignedToGrupoElemento,
        //											tipoElemento.Nombre,
        //											entity.Nombre),
        //				Fecha = DateTime.Now
        //			});
        //		}

        //		SaveAuditoria(list);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaLocacion(AuditAction accion, Locacion entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Locaciones,
        //            IdEntidadRelacionada = entity.IdLocacion,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocacionContrato(AuditAction accion, LocacionContrato entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.LocacionContratos,
        //            IdEntidadRelacionada = entity.IdLocacionContrato,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocacionContratoMonto(AuditAction accion, LocacionContratoMonto entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.LocacionContratosMontos,
        //            IdEntidadRelacionada = entity.IdLocacionContratoMonto,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocacionElemento(AuditAction accion, LocacionElemento entity)
        //{
        //    try
        //    {
        //        List<LocacionElemento> list = new List<LocacionElemento>() { entity };

        //        AuditoriaLocacionElemento(accion, list);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocacionElemento(AuditAction accion, List<LocacionElemento> entities)
        //{
        //    try
        //    {
        //        List<Auditoria> list = new List<Auditoria>();

        //        foreach (var entity in entities)
        //        {
        //            list.Add(new Auditoria
        //            {
        //                IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //                IPUsuario = SessionContext.DireccionIPActual,
        //                Accion = (int)accion,
        //                IdModulo = (int)ModuloSistema.LocacionElementos,
        //                IdEntidadRelacionada = entity.IdLocacionElemento,
        //                Fecha = DateTime.Now,
        //                Modificacion = entity.GetFieldsAsText()
        //            });
        //        }

        //        SaveAuditoria(list);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocacionElemento(AuditAction accion, List<LocacionElemento> entities, IEnumerable<LocacionElementoEspecificacionTecnica> oldEspecificacionTecnicas = null)
        //{
        //	try
        //	{
        //		List<Auditoria> list = new List<Auditoria>();

        //		foreach (var entity in entities)
        //		{
        //			list.Add(new Auditoria
        //			{
        //				IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //				IPUsuario = SessionContext.DireccionIPActual,
        //				Accion = (int)accion,
        //				IdModulo = (int)ModuloSistema.LocacionElementos,
        //				IdEntidadRelacionada = entity.IdLocacionElemento,
        //				Fecha = DateTime.Now,
        //				Modificacion = entity.GetFieldsAsText()
        //			});

        //			if (accion == AuditAction.Create && entity.ElementoActividad.Any())
        //			{
        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)accion,
        //					IdModulo = (int)ModuloSistema.Elementos,
        //					IdEntidadRelacionada = entity.ElementoActividad.Last().IdElementoActividad,
        //					Fecha = DateTime.Now
        //				});
        //			}

        //			var elemento = ElementoBusiness.Instance.GetEntity(entity.IdElemento);
        //			var locacion = LocacionBusiness.Instance.GetEntity(entity.IdLocacion);

        //			if (accion == AuditAction.Edit && oldEspecificacionTecnicas != null)
        //			{
        //				foreach (var item in oldEspecificacionTecnicas)
        //				{
        //					var especificacionTecnica = EspecificacionTecnicaBusiness.Instance.GetEntity(item.IdEspecificacionTecnica);

        //					list.Add(new Auditoria
        //					{
        //						IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //						IPUsuario = SessionContext.DireccionIPActual,
        //						Accion = (int)AuditAction.Delete,
        //						IdModulo = (int)ModuloSistema.LocacionElementos,
        //						IdEntidadRelacionada = entity.IdLocacionElemento,
        //						Descripcion = string.Format(Resource.EspecificacionTecnicaUnassignedFromLocacionElemento,
        //													especificacionTecnica.Nombre,
        //													item.Valor,
        //													elemento.CodigoTipoElementoNombre,
        //													locacion.Codigo),
        //						Fecha = DateTime.Now
        //					});
        //				}
        //			}

        //			if (accion == AuditAction.Create || accion == AuditAction.Edit)
        //			{
        //				foreach (var item in entity.LocacionElementoEspecificacionTecnica.Where(p => p.Estado == (int)EntityStatus.Active))
        //				{
        //					var especificacionTecnica = EspecificacionTecnicaBusiness.Instance.GetEntity(item.IdEspecificacionTecnica);

        //					list.Add(new Auditoria
        //					{
        //						IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //						IPUsuario = SessionContext.DireccionIPActual,
        //						Accion = (int)AuditAction.Create,
        //						IdModulo = (int)ModuloSistema.LocacionElementos,
        //						IdEntidadRelacionada = entity.IdLocacionElemento,
        //						Descripcion = string.Format(Resource.EspecificacionTecnicaAssignedToLocacionElemento,
        //													especificacionTecnica.Nombre,
        //													item.Valor,
        //													elemento.CodigoTipoElementoNombre,
        //													locacion.Codigo),
        //						Fecha = DateTime.Now
        //					});
        //				}
        //			}
        //		}

        //		SaveAuditoria(list);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaLocacionElementoImagen(AuditAction accion, LocacionElementoImagen entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.LocacionElementoImagenes,
        //            IdEntidadRelacionada = entity.IdLocacionElementoImagen,
        //            Fecha = DateTime.Now
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocacionImagen(AuditAction accion, LocacionImagen entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.LocacionImagenes,
        //            IdEntidadRelacionada = entity.IdLocacionImagen,
        //            Fecha = DateTime.Now
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocacionMedidorElectrico(AuditAction accion, LocacionMedidorElectrico entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.LocacionMedidoresElectricos,
        //            IdEntidadRelacionada = entity.IdLocacionMedidorElectrico,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocacionProveedor(AuditAction accion, LocacionProveedor entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.LocacionProveedores,
        //            IdEntidadRelacionada = entity.IdLocacionProveedor,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocacionReceptorPago(AuditAction accion, LocacionReceptorPago entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.LocacionContratos,
        //            IdEntidadRelacionada = entity.IdLocacionReceptorPago,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocalidad(AuditAction accion, Localidad entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Localidades,
        //            IdEntidadRelacionada = entity.IdLocalidad,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLocalidadTasa(AuditAction accion, LocalidadTasa entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.LocalidadTasas,
        //            IdEntidadRelacionada = entity.IdLocalidadTasa,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void AuditoriaLogin()
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    Accion = (int)AuditAction.Login,
                    IdModulo = (int)ModuloSistema.Principal,
                    Fecha = DateTime.Now
                };

                SaveAuditoria(auditoria);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AuditoriaLogout()
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    Accion = (int)AuditAction.Logout,
                    IdModulo = (int)ModuloSistema.Principal,
                    Fecha = DateTime.Now
                };

                SaveAuditoria(auditoria);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void AuditoriaLotePago(AuditAction accion, LotePago entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.LotesPago,
        //            IdEntidadRelacionada = entity.IdLotePago,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        if (accion == AuditAction.Edit && entity.EstadoActual.EstadoLote == (int)EstadoLote.Cerrado)
        //        {
        //            auditoria.Descripcion = Resource.LotePagoClose;
        //        }

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLotePagoAnulacionNoCerrado(LotePago entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)AuditAction.Edit,
        //            IdModulo = (int)ModuloSistema.AnulacionLotesPagoNoCerrados,
        //            IdEntidadRelacionada = entity.IdLotePago,
        //            Descripcion = Resource.LotePagoNoCerradoAnulado,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaLotePagoAnulacionProvisionado(LotePago entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)AuditAction.Edit,
        //            IdModulo = (int)ModuloSistema.AnulacionLotesPagoProvisionados,
        //            IdEntidadRelacionada = entity.IdLotePago,
        //            Descripcion = Resource.LotePagoProvisionadoAnulado,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaMovimientoLocacion(AuditAction accion, MovimientoLocacion entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.MovimientosLocacion,
        //            IdEntidadRelacionada = entity.IdMovimientoLocacion,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaMovimientoLocalidad(AuditAction accion, MovimientoLocalidad entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.MovimientosLocalidad,
        //            IdEntidadRelacionada = entity.IdMovimientoLocalidad,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaPais(AuditAction accion, Pais entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Paises,
        //            IdEntidadRelacionada = entity.IdPais,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaPautaExhibicion(AuditAction accion, PautaExhibicion entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.PautasExhibicion,
        //            IdEntidadRelacionada = entity.IdPautaExhibicion,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        if (entity.EstadoActual == (int)EstadoPauta.AnuladoReprocesado)
        //        {
        //            auditoria.Descripcion = Resource.PautaReprocesada;
        //        }
        //        else if (entity.EstadoActual == (int)EstadoPauta.Refacturado)
        //        {
        //            auditoria.Descripcion = Resource.PautaRefacturada;
        //        }

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaPautaProduccion(AuditAction accion, PautaProduccion entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.PautasProduccion,
        //            IdEntidadRelacionada = entity.IdPautaProduccion,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        if (entity.EstadoActual == (int)EstadoPauta.AnuladoReprocesado)
        //        {
        //            auditoria.Descripcion = Resource.PautaReprocesada;
        //        }
        //        else if (entity.EstadoActual == (int)EstadoPauta.Refacturado)
        //        {
        //            auditoria.Descripcion = Resource.PautaRefacturada;
        //        }

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void AuditoriaPerfil(AuditAction accion, Perfil entity)
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    Accion = (int)accion,
                    IdModulo = (int)ModuloSistema.Perfiles,
                    //IdEntidadRelacionada = entity.IdPerfil,
                    Fecha = DateTime.Now,
                    //Modificacion = entity.GetFieldsAsText()
                };

                SaveAuditoria(auditoria);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void AuditoriaPerfil(AuditAction accion, Perfil entity)
        //{
        //	try
        //	{
        //		AuditoriaPerfil(accion, entity, null);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaPerfil(AuditAction accion, Perfil entity, IEnumerable<PerfilModulo> oldPermisos = null)
        //{
        //	try
        //	{
        //		List<Auditoria> list = new List<Auditoria>();

        //		list.Add(new Auditoria
        //		{
        //			IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //			IPUsuario = SessionContext.DireccionIPActual,
        //			Accion = (int)accion,
        //			IdModulo = (int)ModuloSistema.Perfiles,
        //			IdEntidadRelacionada = entity.IdPerfil,
        //			Fecha = DateTime.Now,
        //			Modificacion = entity.GetFieldsAsText()
        //		});

        //		//if (accion == AuditAction.Edit && oldPermisos != null)
        //		//{
        //		//	foreach (var item in oldPermisos)
        //		//	{
        //		//		var modulo = ModuloBusiness.Instance.GetEntity(item.IdModulo);

        //		//		list.Add(new Auditoria
        //		//		{
        //		//			IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //		//			IPUsuario = SessionContext.DireccionIPActual,
        //		//			Accion = (int)AuditAction.Delete,
        //		//			IdModulo = (int)ModuloSistema.Perfiles,
        //		//			IdEntidadRelacionada = entity.IdPerfil,
        //		//			Descripcion = string.Format(Resource.PermisoUnassignedFromPerfil,
        //		//										EnumerationsHelper.GetPermissionName((Permission)item.Permiso, true),
        //		//										modulo.Nombre,
        //		//										entity.Nombre),
        //		//			Fecha = DateTime.Now
        //		//		});
        //		//	}
        //		//}

        //		//if (accion == AuditAction.Create || accion == AuditAction.Edit)
        //		//{
        //		//	foreach (var item in entity.PerfilModulo.Where(p => p.Estado == (int)EntityStatus.Active))
        //		//	{
        //		//		var modulo = ModuloBusiness.Instance.GetEntity(item.IdModulo);

        //		//		list.Add(new Auditoria
        //		//		{
        //		//			IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //		//			IPUsuario = SessionContext.DireccionIPActual,
        //		//			Accion = (int)AuditAction.Create,
        //		//			IdModulo = (int)ModuloSistema.Perfiles,
        //		//			IdEntidadRelacionada = entity.IdPerfil,
        //		//			Descripcion = string.Format(Resource.PermisoAssignedToPerfil,
        //		//										EnumerationsHelper.GetPermissionName((Permission)item.Permiso, true),
        //		//										modulo.Nombre,
        //		//										entity.Nombre),
        //		//			Fecha = DateTime.Now
        //		//		});
        //		//	}
        //		//}

        //		SaveAuditoria(list);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        public void AuditoriaPerfilValidacion(Perfil entity)
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    Accion = (int)AuditAction.Edit,
                    IdModulo = (int)ModuloSistema.ValidacionesAutorizadasPerfil,
                    //IdEntidadRelacionada = entity.IdPerfil,
                    Fecha = DateTime.Now,
                    //Modificacion = entity.GetFieldsAsText()
                };

                SaveAuditoria(auditoria);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void AuditoriaPerfilValidacion(Perfil entity, IEnumerable<PerfilValidacion> oldValidaciones)
        //{
        //	try
        //	{
        //		List<Auditoria> list = new List<Auditoria>();

        //		if (oldValidaciones != null)
        //		{
        //			foreach (var item in oldValidaciones)
        //			{
        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Delete,
        //					IdModulo = (int)ModuloSistema.ValidacionesAutorizadasPerfil,
        //					IdEntidadRelacionada = entity.IdPerfil,
        //					Descripcion = string.Format(Resource.ValidacionNoAutorizadaAPerfil,
        //												item.NombreValidacion,
        //												entity.Nombre),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		foreach (var item in entity.PerfilValidacion.Where(p => p.Estado == (int)EntityStatus.Active))
        //		{
        //			list.Add(new Auditoria
        //			{
        //				IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //				IPUsuario = SessionContext.DireccionIPActual,
        //				Accion = (int)AuditAction.Create,
        //				IdModulo = (int)ModuloSistema.ValidacionesAutorizadasPerfil,
        //				IdEntidadRelacionada = entity.IdPerfil,
        //				Descripcion = string.Format(Resource.ValidacionAutorizadaAPerfil,
        //											item.NombreValidacion,
        //											entity.Nombre),
        //				Fecha = DateTime.Now
        //			});
        //		}

        //		SaveAuditoria(list);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaProgramaRadio(AuditAction accion, ProgramaRadio entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.ProgramasRadio,
        //            IdEntidadRelacionada = entity.IdProgramaRadio,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaProgramaRadio(AuditAction accion, ProgramaRadio entity)
        //{
        //	try
        //	{
        //		AuditoriaProgramaRadio(accion, entity, null);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaProgramaRadio(AuditAction accion, ProgramaRadio entity, IEnumerable<ProgramaRadioDia> oldDias = null)
        //{
        //	try
        //	{
        //		List<Auditoria> list = new List<Auditoria>();

        //		list.Add(new Auditoria
        //		{
        //			IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //			IPUsuario = SessionContext.DireccionIPActual,
        //			Accion = (int)accion,
        //			IdModulo = (int)ModuloSistema.ProgramasRadio,
        //			IdEntidadRelacionada = entity.IdProgramaRadio,
        //			Fecha = DateTime.Now,
        //			Modificacion = entity.GetFieldsAsText()
        //		});

        //		if (accion == AuditAction.Edit && oldDias != null)
        //		{
        //			foreach (var item in oldDias)
        //			{
        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Delete,
        //					IdModulo = (int)ModuloSistema.ProgramasRadio,
        //					IdEntidadRelacionada = entity.IdProgramaRadio,
        //					Descripcion = string.Format(Resource.DiaDesasignadoDelPrograma,
        //												item.NombreDia,
        //												entity.Nombre),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		if (accion == AuditAction.Create || accion == AuditAction.Edit)
        //		{
        //			foreach (var item in entity.ProgramaRadioDia.Where(p => p.Estado == (int)EntityStatus.Active))
        //			{
        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Create,
        //					IdModulo = (int)ModuloSistema.ProgramasRadio,
        //					IdEntidadRelacionada = entity.IdProgramaRadio,
        //					Descripcion = string.Format(Resource.DiaAsignadoAPrograma,
        //												item.NombreDia,
        //												entity.Nombre),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		SaveAuditoria(list);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaProvincia(AuditAction accion, Provincia entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Provincias,
        //            IdEntidadRelacionada = entity.IdProvincia,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaRevisarLotePago(AuditAction accion, LotePago entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.RevisarLotesPago,
        //            IdEntidadRelacionada = entity.IdLotePago,
        //            Fecha = DateTime.Now,
        //            Descripcion = Resource.LotePagoClose,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaRubro(AuditAction accion, Rubro entity)
        //{
        //    Auditoria auditoria = new Auditoria
        //    {
        //        IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //        IPUsuario = SessionContext.DireccionIPActual,
        //        Accion = (int)accion,
        //        IdModulo = (int)ModuloSistema.Rubro,
        //        IdEntidadRelacionada = entity.IdRubro,
        //        Fecha = DateTime.Now,
        //        Modificacion = entity.GetFieldsAsText()
        //    };

        //    SaveAuditoria(auditoria);
        //}

        //public void AuditoriaSucursalOperacion(AuditAction accion, SucursalOperacion entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.SucursalesOperacion,
        //            IdEntidadRelacionada = entity.IdSucursalOperacion,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaTipoCalidad(AuditAction accion, TipoCalidad entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.TiposCalidad,
        //            IdEntidadRelacionada = entity.IdTipoCalidad,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaTipoElemento(AuditAction accion, TipoElemento entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.TiposElementos,
        //            IdEntidadRelacionada = entity.IdTipoElemento,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaTipoElemento(AuditAction accion, TipoElemento entity)
        //{
        //	try
        //	{
        //		AuditoriaTipoElemento(accion, entity, null);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaTipoElemento(AuditAction accion, TipoElemento entity, IEnumerable<TipoElementoEspecificacionTecnica> oldEspecificacionTecnicas = null)
        //{
        //	try
        //	{
        //		List<Auditoria> list = new List<Auditoria>();

        //		list.Add(new Auditoria
        //		{
        //			IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //			IPUsuario = SessionContext.DireccionIPActual,
        //			Accion = (int)accion,
        //			IdModulo = (int)ModuloSistema.TiposElementos,
        //			IdEntidadRelacionada = entity.IdTipoElemento,
        //			Fecha = DateTime.Now,
        //			Modificacion = entity.GetFieldsAsText()
        //		});

        //		if (accion == AuditAction.Edit && oldEspecificacionTecnicas != null)
        //		{
        //			foreach (var item in oldEspecificacionTecnicas)
        //			{
        //				var especificacionTecnica = EspecificacionTecnicaBusiness.Instance.GetEntity(item.IdEspecificacionTecnica);

        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Delete,
        //					IdModulo = (int)ModuloSistema.TiposElementos,
        //					IdEntidadRelacionada = entity.IdTipoElemento,
        //					Descripcion = string.Format(Resource.EspecificacionTecnicaUnassignedFromTipoElemento,
        //												especificacionTecnica.Nombre,
        //												item.Valor,
        //												entity.Nombre),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		if (accion == AuditAction.Create || accion == AuditAction.Edit)
        //		{
        //			foreach (var item in entity.TipoElementoEspecificacionTecnica.Where(p => p.Estado == (int)EntityStatus.Active))
        //			{
        //				var especificacionTecnica = EspecificacionTecnicaBusiness.Instance.GetEntity(item.IdEspecificacionTecnica);

        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Create,
        //					IdModulo = (int)ModuloSistema.TiposElementos,
        //					IdEntidadRelacionada = entity.IdTipoElemento,
        //					Descripcion = string.Format(Resource.EspecificacionTecnicaAssignedToTipoElemento,
        //												especificacionTecnica.Nombre,
        //												item.Valor,
        //												entity.Nombre),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		SaveAuditoria(list);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaTipoElementoHistorial(AuditAction accion, TipoElementoHistorial entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.TiposElementosHistorial,
        //            IdEntidadRelacionada = entity.IdTipoElementoHistorial,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaTipoElementoReglaNegocio(AuditAction accion, TipoElementoReglaNegocio entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.TipoElementoReglasNegocio,
        //            IdEntidadRelacionada = entity.IdTipoElementoReglaNegocio,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void AuditoriaUbicacion(AuditAction accion, Ubicacion entity)
        //{
        //    try
        //    {
        //        Auditoria auditoria = new Auditoria
        //        {
        //            IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //            IPUsuario = SessionContext.DireccionIPActual,
        //            Accion = (int)accion,
        //            IdModulo = (int)ModuloSistema.Ubicaciones,
        //            IdEntidadRelacionada = entity.IdUbicacion,
        //            Fecha = DateTime.Now,
        //            Modificacion = entity.GetFieldsAsText()
        //        };

        //        SaveAuditoria(auditoria);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void AuditoriaUsuario(AuditAction accion, Usuario entity)
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    IdUsuario = (SessionContext.UsuarioActual != null ? SessionContext.UsuarioActual.IdUsuario : entity.IdUsuario),
                    IPUsuario = SessionContext.DireccionIPActual,
                    Accion = (int)accion,
                    IdModulo = (int)ModuloSistema.Usuarios,
                    //IdEntidadRelacionada = entity.IdUsuario,
                    Fecha = DateTime.Now,
                    //Modificacion = entity.GetFieldsAsText()
                };

                SaveAuditoria(auditoria);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void AuditoriaUsuario(AuditAction accion, Usuario entity)
        //{
        //	try
        //	{
        //		AuditoriaUsuario(accion, entity, null);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaUsuario(AuditAction accion, Usuario entity, IEnumerable<UsuarioPerfils> oldPerfiles = null)
        //{
        //	try
        //	{
        //		List<Auditoria> list = new List<Auditoria>();

        //		list.Add(new Auditoria
        //		{
        //			IdUsuario = (SessionContext.UsuarioActual != null ? SessionContext.UsuarioActual.IdUsuario : entity.IdUsuario),
        //			IPUsuario = SessionContext.DireccionIPActual,
        //			Accion = (int)accion,
        //			IdModulo = (int)ModuloSistema.Usuarios,
        //			IdEntidadRelacionada = entity.IdUsuario,
        //			Fecha = DateTime.Now,
        //			Modificacion = entity.GetFieldsAsText()
        //		});

        //		if (accion == AuditAction.Edit && oldPerfiles != null)
        //		{
        //			foreach (var item in oldPerfiles)
        //			{
        //				var perfil = PerfilBusiness.Instance.GetEntity(item.IdPerfil);

        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Delete,
        //					IdModulo = (int)ModuloSistema.Usuarios,
        //					IdEntidadRelacionada = entity.IdUsuario,
        //					Descripcion = string.Format(Resource.PerfilUnassignedFromUsuario,
        //												perfil.Nombre,
        //												entity.NombreUsuario),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		if (SessionContext.UsuarioActual != null && (accion == AuditAction.Create || accion == AuditAction.Edit))
        //		{
        //			foreach (var item in entity.UsuarioPerfils.Where(p => p.Estado == (int)EntityStatus.Active))
        //			{
        //				var perfil = PerfilBusiness.Instance.GetEntity(item.IdPerfil);

        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Create,
        //					IdModulo = (int)ModuloSistema.Usuarios,
        //					IdEntidadRelacionada = entity.IdUsuario,
        //					Descripcion = string.Format(Resource.PerfilAssignedToUsuario,
        //												perfil.Nombre,
        //												entity.NombreUsuario),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		SaveAuditoria(list);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        

        //public void AuditoriaUsuarioEmpresa(Usuario entity, IEnumerable<UsuarioEmpresa> oldEmpresas)
        //{
        //	try
        //	{
        //		List<Auditoria> list = new List<Auditoria>();

        //		if (oldEmpresas != null)
        //		{
        //			foreach (var item in oldEmpresas)
        //			{
        //				var empresa = EmpresaBusiness.Instance.GetEntity(item.IdEmpresa);

        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Delete,
        //					IdModulo = (int)ModuloSistema.AsignarEmpresaAUsuario,
        //					IdEntidadRelacionada = entity.IdUsuario,
        //					Descripcion = string.Format(Resource.EmpresaUnassignedFromUsuario,
        //												empresa.Nombre,
        //												entity.NombreUsuario),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		foreach (var item in entity.UsuarioEmpresa.Where(p => p.Estado == (int)EntityStatus.Active))
        //		{
        //			var empresa = EmpresaBusiness.Instance.GetEntity(item.IdEmpresa);

        //			list.Add(new Auditoria
        //			{
        //				IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //				IPUsuario = SessionContext.DireccionIPActual,
        //				Accion = (int)AuditAction.Create,
        //				IdModulo = (int)ModuloSistema.AsignarEmpresaAUsuario,
        //				IdEntidadRelacionada = entity.IdUsuario,
        //				Descripcion = string.Format(Resource.EmpresaAssignedToUsuario,
        //											empresa.Nombre,
        //											entity.NombreUsuario),
        //				Fecha = DateTime.Now
        //			});
        //		}

        //		SaveAuditoria(list);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

     

        //public void AuditoriaUsuarioSucursalOperacion(Usuario entity, IEnumerable<UsuarioSucursalOperacion> oldSucursalOperaciones)
        //{
        //	try
        //	{
        //		List<Auditoria> list = new List<Auditoria>();

        //		if (oldSucursalOperaciones != null)
        //		{
        //			foreach (var item in oldSucursalOperaciones)
        //			{
        //				var sucursalOperacion = SucursalOperacionBusiness.Instance.GetEntity(item.IdSucursalOperacion);

        //				list.Add(new Auditoria
        //				{
        //					IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //					IPUsuario = SessionContext.DireccionIPActual,
        //					Accion = (int)AuditAction.Delete,
        //					IdModulo = (int)ModuloSistema.AsignarSucursalOperacionAUsuario,
        //					IdEntidadRelacionada = entity.IdUsuario,
        //					Descripcion = string.Format(Resource.SucursalOperacionUnassignedFromUsuario,
        //												sucursalOperacion.Nombre,
        //												sucursalOperacion.Empresa.Nombre,
        //												entity.NombreUsuario),
        //					Fecha = DateTime.Now
        //				});
        //			}
        //		}

        //		foreach (var item in entity.UsuarioSucursalOperacion.Where(p => p.Estado == (int)EntityStatus.Active))
        //		{
        //			var sucursalOperacion = SucursalOperacionBusiness.Instance.GetEntity(item.IdSucursalOperacion);

        //			list.Add(new Auditoria
        //			{
        //				IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //				IPUsuario = SessionContext.DireccionIPActual,
        //				Accion = (int)AuditAction.Create,
        //				IdModulo = (int)ModuloSistema.AsignarSucursalOperacionAUsuario,
        //				IdEntidadRelacionada = entity.IdUsuario,
        //				Descripcion = string.Format(Resource.SucursalOperacionAssignedToUsuario,
        //											sucursalOperacion.Nombre,
        //											sucursalOperacion.Empresa.Nombre,
        //											entity.NombreUsuario),
        //				Fecha = DateTime.Now
        //			});
        //		}

        //		SaveAuditoria(list);
        //	}
        //	catch (Exception ex)
        //	{
        //		throw ex;
        //	}
        //}

        //public void AuditoriaZona(AuditAction accion, Zona entity)
        //{
        //    Auditoria auditoria = new Auditoria
        //    {
        //        IdUsuario = SessionContext.UsuarioActual.IdUsuario,
        //        IPUsuario = SessionContext.DireccionIPActual,
        //        Accion = (int)accion,
        //        IdModulo = (int)ModuloSistema.Zona,
        //        IdEntidadRelacionada = entity.IdZona,
        //        Fecha = DateTime.Now,
        //        Modificacion = entity.GetFieldsAsText()
        //    };

        //    SaveAuditoria(auditoria);
        //}

        public void AuditoriaCliente(AuditAction accion, Cliente entity)
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    Accion = (int)accion,
                    IdModulo = (int)ModuloSistema.Clientes,
                    IdEntidadRelacionada = entity.IdCliente,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };

                SaveAuditoria(auditoria);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void AuditoriaInversores(AuditAction accion, Inversor entity)
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    Accion = (int)accion,
                    IdModulo = (int)ModuloSistema.Inversores,
                    IdEntidadRelacionada = entity.IdInversor,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };

                SaveAuditoria(auditoria);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void AuditoriaBanco(AuditAction accion, Banco entity)
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    Accion = (int)accion,
                    IdModulo = (int)ModuloSistema.Bancos,
                    IdEntidadRelacionada = entity.IdBanco,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };

                SaveAuditoria(auditoria);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void AuditoriaCheque(AuditAction accion, Cheque entity) 
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.Cheques,
                    IdEntidadRelacionada = entity.IdCheque,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        
        }

        public void AuditoriaChequeRechazado(AuditAction accion, ChequeRechazado entity) 
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.ChequesRechazados,
                    IdEntidadRelacionada = entity.IdChequeRechazado,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()

                };
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        
        }        

        public void AuditoriaRecepcionCheque(AuditAction accion, RecepcionCheque entity) 
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.RecepcionCheques,
                    IdEntidadRelacionada = entity.IdRecepcionCheque,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()

                };
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        
        }
        
        public void AuditoriaLiquidacion(AuditAction accion, Liquidacion entity) 
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.Liquidacion,
                    IdEntidadRelacionada = entity.IdLiquidacion,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        
        }

        public void AuditoriaCuentaCorriente(AuditAction accion, CuentaCorriente entity) 
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.CuentaCorriente,
                    IdEntidadRelacionada = entity.IdCuentaCorriente,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        
        }

        public void AuditoriaCuentaCorrienteInversor(AuditAction accion, CuentaCorrienteInversor entity)
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.CuentaCorriente,
                    IdEntidadRelacionada = entity.IdCuentaCorrienteInversor,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void AuditoriaEmisorValor( AuditAction accion, EmisorValor entity) 
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.EmisoresDeValores,
                    IdEntidadRelacionada = entity.IdEmisorValor,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AuditoriaFeriado(AuditAction accion, Feriado entity) 
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.Feriados,
                    IdEntidadRelacionada = entity.IdFeriado,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AuditoriaSociedad(AuditAction accion, Sociedad entity) 
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.Sociedades,
                    IdEntidadRelacionada = entity.IdSociedad,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AuditoriaSucursal(AuditAction accion, Sucursal entity) 
        {
            try
            {
                Auditoria auditoria = new Auditoria
                {
                    Accion = (int)accion,
                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                    IPUsuario = SessionContext.DireccionIPActual,
                    IdModulo = (int)ModuloSistema.Sociedades,
                    IdEntidadRelacionada = entity.IdSucursal,
                    Fecha = DateTime.Now,
                    Modificacion = entity.GetFieldsAsText()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        

        #endregion

        #region Methods

        public List<Auditoria> GetByFilters(long idUsuario, string ipDomicilio, int accion, long idModulo, DateTime? fechaDesde, DateTime? fechaHasta, int skip, int top, ref int rowCount)
        {
            try
            {
                var query = GetByFilters(idUsuario, ipDomicilio, accion, idModulo, fechaDesde, fechaHasta);

                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Auditoria> GetByFilters(long idUsuario, string ipDomicilio, int accion, long idModulo, DateTime? fechaDesde, DateTime? fechaHasta)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Auditoria>(p =>
                                (idUsuario > 0 ? p.IdUsuario == idUsuario : true) &&
                                p.IPUsuario.Contains(ipDomicilio) &&
                                (accion > 0 ? p.Accion == accion : true) &&
                                (idModulo > 0 ? p.IdModulo == idModulo : true) &&
                                (fechaDesde.HasValue ? p.Fecha >= fechaDesde.Value : true) &&
                                (fechaHasta.HasValue ? p.Fecha <= fechaHasta.Value : true),
                                            p => p.Modulo,
                                            p => p.Usuario);

                return query.OrderByDescending(p => p.Fecha).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public Auditoria GetByIdLotePagoProvisionado(long idLotePago)
        //{
        //    try
        //    {
        //        var query = DataAccessManager.GetByConditions<Auditoria>(p =>
        //                            p.IdModulo == (int)ModuloSistema.RevisarLotesPago &&
        //                            p.IdEntidadRelacionada.HasValue &&
        //                            p.IdEntidadRelacionada.Value == idLotePago &&
        //                            p.Accion == (int)AuditAction.Edit,
        //                                                p => p.Usuario)
        //                                    .OrderByDescending(p => p.Fecha);

        //        return query.FirstOrDefault();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        #endregion
    }
}