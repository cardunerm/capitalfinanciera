﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Business
{
    public class FeriadoBusiness : GenericBusiness<FeriadoBusiness, Feriado>
    {
        #region Methods

        public List<Feriado> GetByFilters(string descripcion, DateTime fecha, int skip, int top, ref int rowCount)
        {
            try
            {

                IEnumerable<Feriado> query = DataAccessManager.GetByConditions<Feriado>(p =>
                                                p.Estado == (int)EntityStatus.Active &&
                                                p.Descripcion.Contains(descripcion));

                query = query.OrderBy(p => p.Fecha);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Feriado> GetFeriadosList()
        {
            try
            {
                IEnumerable<Feriado> query = DataAccessManager.GetByConditions<Feriado>(p =>
                                                p.Estado == (int)EntityStatus.Active);

                query = query.OrderBy(p => p.Fecha);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveOrUpdateFeriado(Feriado Feriado)
        {
            Feriado entity;

            if (Feriado.IdFeriado > 0)
            {
                entity = GetEntity(Feriado.IdFeriado);
                entity.Descripcion = Feriado.Descripcion;
                entity.Fecha = Feriado.Fecha;
            }
            else
            {
                entity = new Feriado
                {
                    Descripcion = Feriado.Descripcion,
                    Fecha = Feriado.Fecha,
                    Estado = (int)EntityStatus.Active,
                };
            }

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaFeriado(Feriado.IdFeriado > 0 ? AuditAction.Edit : AuditAction.Create, entity);
        }

        private bool ValidateSaveOrUpdate(Feriado entity)
        {
            if (!FeriadoNombreExist(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException("Ya existe un Feriado en esa fecha");
            }
        }

        private bool FeriadoNombreExist(Feriado entity)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Feriado>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Fecha.Equals(entity.Fecha) &&
                                p.IdFeriado != entity.IdFeriado);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(Feriado entity)
        {           
                return true;            
        }
               

        public Feriado FeriadoExist(DateTime fecha)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Feriado>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Fecha.Equals(fecha));

                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}