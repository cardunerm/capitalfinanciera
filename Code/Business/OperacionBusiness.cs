﻿using Domine;
using System.Linq;
using System.Collections.Generic;
using Domine.Dto;
using System;
using DataAccess.Pages;
using System.Data.Entity;
using Luma.Utils.Logs;
using Utils.Enumeraciones;
using CurrentContext;

namespace Business
{
    public class OperacionBusiness : GenericBusiness<OperacionBusiness, Operacion>
    {

        public DtoSaldoCliente GetSaldoCliente(int cliente)
        {
            var saldos = GetByConditions<View_SaldoClientes>(view => view.IdCliente == cliente).FirstOrDefault() ?? new View_SaldoClientes { Credito = 0, Debito = 0 };
            return new DtoSaldoCliente
            {
                Saldo = saldos.Credito.Value - saldos.Debito.Value,
                CantidadRechazos = saldos.CantRechazados ?? 0,
                MontoRechazos = saldos.Rechazado ?? 0
            };

        }

        public static int CalcularDiasClering(DateTime fechaLiquidacion, DateTime fechaPago)
        {
            fechaPago = CalcularFeriados(fechaPago, GetFeriados(fechaPago, fechaPago.AddDays(15)));
            var dias = fechaPago.Date.Subtract(fechaLiquidacion.Date).Days + 2; //dias de diferencia + 48hs del deposito    

            if (dias < 15)
                dias = 15;

            return dias;
        }

        private static List<Feriado> GetFeriados(DateTime desde, DateTime hasta)
        {
            return DataAccessManager.GetByConditions<Feriado>(f => f.Estado == 1 && DbFunctions.TruncateTime(f.Fecha) >= desde && DbFunctions.TruncateTime(f.Fecha) <= hasta).ToList();
        }

        public static DateTime CalcularFeriados(DateTime fechaPago, List<Feriado> feriados)
        {
            if (fechaPago.DayOfWeek == DayOfWeek.Thursday
                || fechaPago.DayOfWeek == DayOfWeek.Friday
                || fechaPago.DayOfWeek == DayOfWeek.Saturday
                || fechaPago.DayOfWeek == DayOfWeek.Sunday) //feriados.Count(f => f.Fecha.Date.CompareTo(fechaPago.Date) == 0) > 0)
            {
                fechaPago = fechaPago.AddDays(2);
                CalcularFeriados(fechaPago, feriados);
            }
            else
                return fechaPago;

            return fechaPago;
        }

        public List<ViewResumenDetalleOperacion> GetChequesByOperacion(int id)
        {
            try
            {
                return DataAccessManager.GetByConditions<ViewResumenDetalleOperacion>
                    (
                        det => det.IdOperacion == id
                    );
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public void UpdateDetalleOperacion(DtoDetalleOperacionAprobacion detalle, short? newStatus, Usuario user, Configuracion config)
        {
            try
            {
                var detalleOperacion = DataAccessManager.GetById<DetalleOperacion>(detalle.Id);
                if (detalleOperacion != null)
                {
                    detalle.TasaStr = detalle.TasaStr.Replace(',', '.');
                    detalle.GastoStr = detalle.GastoStr.Replace(',', '.');
                    detalle.MontoStr = detalle.MontoStr.Replace(',', '.');

                    //detalleOperacion.TasaInteres = decimal.Parse(detalle.TasaStr.Replace(',','.')); // detalle.Tasa;
                    detalleOperacion.TasaInteres = decimal.Parse(detalle.TasaStr); // detalle.Tasa;
                    detalleOperacion.MontoAprobado = (newStatus ?? 0) == (int)Estados.eEstadosOpDetalle.Rechazado ? 0 : CalcularMontoAPagar(decimal.Parse(detalle.MontoStr), detalle.FechaVencimiento, detalleOperacion.Operacion.Fecha, decimal.Parse(detalle.TasaStr), decimal.Parse(detalle.GastoStr), config);
                    detalleOperacion.IdUsuario = user.IdUsuario;
                    detalleOperacion.Observacion = detalle.Observacion;

                    //detalleOperacion.Gastos = Math.Round((decimal.Parse(detalle.GastoStr.Replace(',', '.')) * detalle.Monto) / 100,2);
                    detalleOperacion.Gastos = Math.Round((decimal.Parse(detalle.GastoStr) * decimal.Parse(detalle.MontoStr)) / 100, 2);
                    detalleOperacion.Impuestos = CalcularImpuestoDiario(detalle.DiasClearing.Value, decimal.Parse(detalle.MontoStr), decimal.Parse(detalle.TasaStr));
                    //detalleOperacion.Impuestos = CalcularImpuestoDiario(detalle.DiasClearingHidden.Value, detalle.Monto, decimal.Parse(detalle.TasaStr.Replace(',', '.')));

                    if (detalle.Numero != null)
                    {
                        int cp = 0;
                        detalleOperacion.Cheque.IdBanco = detalle.IdBanco;
                        detalleOperacion.Cheque.FechaVencimiento = detalle.FechaVencimiento;
                        //detalleOperacion.Cheque.Monto = detalle.Monto;
                        detalleOperacion.Cheque.Monto = decimal.Parse(detalle.MontoStr);
                        detalleOperacion.Cheque.NroCuenta = detalle.NumeroCuenta;
                        detalleOperacion.Cheque.TitularCuenta = detalle.TitularCuenta;
                        detalleOperacion.Cheque.Numero = detalle.Numero;
                        detalleOperacion.Cheque.SucursalEmision = detalle.SucursalEmision;
                        detalleOperacion.Cheque.CP = int.TryParse(detalle.Cp, out cp) ? (int?)cp : null;
                    }

                    if (newStatus != null)
                        detalleOperacion.Estado = newStatus.Value;

                    DataAccessManager.Update(detalleOperacion);



                    DataAccessManager.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        private decimal CalcularMontoAPagar(decimal monto, DateTime fechaVencimiento, DateTime fecha, decimal? tasa, decimal gasto, Configuracion configuracion)
        {

            var diasClering = CalcularDiasClering(fecha, fechaVencimiento);
            var impuestoDiario = CalcularImpuestoDiario(diasClering, monto, tasa.Value);
            var impuestoAlCheque = CalcularImpustoAlCheque(monto, configuracion);
            var impuestoPorGastos = CalcularImpustoPorGastosByGasto(monto, gasto);
            return (decimal)Math.Round(monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2);

        }

        public void Pagar(long idOp, Usuario user)
        {
            var operacion = GetByConditions<Operacion>(op => op.Id == idOp, new List<string> { { "DetalleOperacion" } }).FirstOrDefault();

            if (operacion == null)
                return;
            try
            {
                if (operacion.Estado == (int)Estados.eEstadosOp.Aprobado)
                {
                    operacion.IdUsuarioPago = user.IdUsuario;
                    operacion.FechaPago = DateTime.Now;
                    operacion.Estado = (int)Estados.eEstadosOp.Pagada;
                    operacion.Cliente.CuentaCorriente.FirstOrDefault().OperacionCuentaCorriente.Add
                        (
                             new OperacionCuentaCorriente
                             {
                                 Monto = operacion.Saldo.Value,
                                 Descripcion = "Pago Operación Nro: " + operacion.Id,
                                 Fecha = DateTime.Now,
                                 TipoMovimiento = 1
                             }
                        );                   

                    operacion.Saldo = 0;
                    SaveOrUpdate(operacion);
                    DataAccessManager.SaveChanges();


                    //Una vez guardada la operacion , guardo el movimiento en la caja
                    OperacionCaja opeCaja = new OperacionCaja();
                    opeCaja.Descripcion = "Pago Operacion Nro: " + operacion.Id;
                    opeCaja.Fecha = DateTime.Now;
                    opeCaja.Monto = (decimal)operacion.MontoAprobado;
                    opeCaja.TipoMovimiento = 2;
                    OperacionCajaBusiness.Instance.SaveOrUpdateOperacionCaja(opeCaja);

                    //Cambio en estado de los cheques
                    foreach (var item in operacion.DetalleOperacion)
                    {
                        ChequeBusiness.Instance.ChangeStatus(item.Cheque.IdCheque, Utils.Enumeraciones.Estados.eEstadosCheque.Cartera);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }

        }

        public bool Finalizar(DtoOperacionAprobacion dtoOp, Usuario user, ref string msg)
        {
            var operacion = GetByConditions<Operacion>(op => op.Id == dtoOp.Id, new List<string> { { "DetalleOperacion" }, { "DetalleOperacion.Cheque" } }).FirstOrDefault();

            if (operacion == null)
                return false;

            var ctx = (DataAccessManager.GetContext() as DCREntities);

            using (var tx = ctx.Database.BeginTransaction())
            {
                dtoOp.IdUser = operacion.IdUsuario;

                try
                {
                    if (operacion.Estado != dtoOp.IdEstado)
                    {
                        if (IsValidChange(operacion, dtoOp.IdEstado, ref msg))
                        {
                            operacion.IdUsuarioEvaluo = user.IdUsuario;
                            operacion.FechaEvaluacion = DateTime.Now;
                            if (dtoOp.IdEstado == (int)Estados.eEstadosOp.Aprobado || dtoOp.IdEstado == (int)Estados.eEstadosOp.Rechazado)
                            {
                                operacion.MontoAprobado = dtoOp.IdEstado == (int)Estados.eEstadosOp.Aprobado ? operacion.DetalleOperacion.Sum(det => (decimal)(det.MontoAprobado ?? 0)) : 0;
                                operacion.Saldo = operacion.MontoAprobado;

                                //EvaluarCheques(operacion,ctx);

                                foreach (var det in operacion.DetalleOperacion)
                                {
                                    if (det.Estado == (int)Estados.eEstadosOpDetalle.Pendiente)
                                    {
                                        det.Estado = (short)dtoOp.IdEstado;
                                        det.Observacion = dtoOp.ObservacionEvaluacion;
                                        if (det.Estado == (int)Estados.eEstadosOpDetalle.Rechazado)
                                        {
                                            det.Cheque.Activo = false;
                                        }
                                        else if (det.Estado == (int)Estados.eEstadosOpDetalle.Aprobado)
                                        {
                                            //if (det.Cheque.IdEstado == (int)Utils.Enumeraciones.Estados.eEstadosCheque.EnOperacion)
                                            //{
                                            //    det.Cheque.IdEstado = (int)Utils.Enumeraciones.Estados.eEstadosCheque.Aprobado;
                                            //}
                                        }
                                        DataAccessManager.Update(det);
                                    }
                                }

                                GenerarMovimientoCuentaCorriente(operacion);
                            }

                        }
                        else
                        {
                            return false;
                        }
                    }

                    operacion.ObservacionEvaluacion = dtoOp.ObservacionEvaluacion;
                    operacion.Estado = (short)dtoOp.IdEstado;

                    DataAccessManager.Update(operacion);
                    DataAccessManager.SaveChanges();
                    tx.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    LogManager.Log.RegisterError(ex);
                    throw ex;
                }
            }
        }



        private void GenerarMovimientoCuentaCorriente(Operacion operacion)
        {
            var detallesAprobados = operacion.DetalleOperacion.Where(cheque => cheque.Estado == (int)Estados.eEstadosOpDetalle.Aprobado || cheque.Estado == (int)Estados.eEstadosOpDetalle.AprobadoParcial);
            var montoAprobado = detallesAprobados.Sum(p => p.Cheque.Monto);
            var gastos = detallesAprobados.Sum(p => p.Gastos);
            var impuestos = detallesAprobados.Sum(p => p.Impuestos);
            var impuestoCheque = detallesAprobados.Sum(p => p.ImpuestosCheque);

            var cuentaCorrientCliente = GetByConditions<CuentaCorriente>(p => p.IdCliente == operacion.IdCliente).FirstOrDefault();

            if (cuentaCorrientCliente == null)
                throw new Exception("No hay cuenta corriente");

            DataAccessManager.Save(
                new OperacionCuentaCorriente
                {
                    Monto = montoAprobado,
                    CuentaCorriente = cuentaCorrientCliente,
                    Descripcion = "Operación Nro: " + operacion.Id + " - Fecha: " + operacion.Fecha.ToString("dd/MM/yyyy HH:mm"),
                    Fecha = DateTime.Now,
                    TipoMovimiento = 2
                });

            DataAccessManager.Save(
                new OperacionCuentaCorriente
                {
                    Monto = impuestos ,
                    CuentaCorriente = cuentaCorrientCliente,
                    Descripcion = "Intereses Operación Nro: " + operacion.Id + " - Fecha: " + operacion.Fecha.ToString("dd/MM/yyyy HH:mm"),
                    Fecha = DateTime.Now,
                    TipoMovimiento = 1
                });

            DataAccessManager.Save(
               new OperacionCuentaCorriente
               {
                   Monto = gastos + impuestoCheque,
                   CuentaCorriente = cuentaCorrientCliente,
                   Descripcion = "Comision y Gastos Operación Nro: " + operacion.Id + " - Fecha: " + operacion.Fecha.ToString("dd/MM/yyyy HH:mm"),
                   Fecha = DateTime.Now,
                   TipoMovimiento = 1
               });
        }

        public static string DescripcionCalculo(DetalleOperacion edit)
        {
            return
                "Monto - Inteeres - Imp. Cheque - Gastos </br> (" + edit.Cheque.Monto.ToString("C2")
                    + " - " + edit.Impuestos.ToString("C2")
                    + " - " + edit.ImpuestosCheque.ToString("C2")
                    + " - " + edit.Gastos.ToString("C2")
                    + ")";

            ;
        }

        private bool IsValidChange(Operacion operacion, int newStatus, ref string msg)
        {
            bool status = operacion.Estado == (int)Estados.eEstadosOp.Pendiente ||
                   operacion.Estado == (int)Estados.eEstadosOp.Pendiente24 ||
                   operacion.Estado == (int)Estados.eEstadosOp.Pendiente48;

            if (!status)
                return false;

            if (newStatus == (int)Estados.eEstadosOp.Aprobado)
            {
                bool faltaEvaluar = operacion.DetalleOperacion.Any(det => det.MontoAprobado == null);
                if (faltaEvaluar)
                {
                    msg = Resources.Resource.EvaluarCheques;
                    return false;
                }
            }

            return true;
        }

        public long CreateOperacion(DtoOperacion op, List<DtoDetalleOperacion> detalle, Usuario user, Configuracion configuracion)
        {

            using (var ctx = DataAccessManager.GetContext() as DCREntities)
            {
                using (var tx = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        Operacion operacion = new Operacion
                        {
                            IdCliente = op.IdCliente,
                            Fecha = DateTime.Now,
                            IdUsuario = user.IdUsuario,
                            Estado = (int)Estados.eEstadosOp.Pendiente,
                            Observacion = op.Observacion,
                            Monto = detalle.Sum(det => det.Monto),
                            IdEmpresa = Convert.ToInt32(SessionContext.IdEmpresaActual),
                            IdSucursal = Convert.ToInt32(SessionContext.IdSucursalOperacionActual),
                            Sesion = (op.Sesion == null || op.Sesion == "")? decimal.Parse("0.00") : decimal.Parse(op.Sesion),
                            IdVendedor = op.IdVendedor
                        };

                        operacion.DetalleOperacion = new List<DetalleOperacion>();

                        foreach (var det in detalle)
                        {
                            var recepcion = new RecepcionCheque
                            {
                                IdCliente = op.IdCliente,
                                Estado = (int)Estados.eEstadosOpDetalle.Pendiente,
                                Fecha = DateTime.Now
                            };

                            int cp = 0;
                            int.TryParse(det.Cp, out cp);
                            var cheque = new Cheque
                            {
                                IdBanco = det.IdBanco,
                                IdEstado = (int)Estados.eEstadosCheque.EnOperacion,
                                FechaVencimiento = det.FechaVencimiento,
                                Monto = det.Monto,
                                NroCuenta = det.NumeroCuenta,
                                Numero = det.Numero,
                                TitularCuenta = det.TitularCuenta,
                                SucursalEmision = det.SucursalEmision,
                                DiasClearing = CalcularDiasClering(operacion.Fecha, det.FechaVencimiento),
                                RecepcionCheque = recepcion,
                                ImagenFront = det.ImagenFront,
                                ImagenBack = det.ImagenBack,
                                CP = cp != 0 ? (int?)cp : null,
                                CMC7 = det.CMC7,
                                Activo = true
                            };

                            var chequemovimiento = new ChequeMovimiento
                            {
                                Cheque = cheque,
                                ChequeEstado = ChequeEstadoBusiness.Instance.GetEntity((long)Utils.Enumeraciones.Estados.eEstadosCheque.EnOperacion),
                                Fecha = DateTime.Now
                            };

                            var detOp = new DetalleOperacion
                            {
                                Operacion = operacion,
                                Cheque = cheque,
                                Estado = (int)Estados.eEstadosOpDetalle.Pendiente,
                                DiasClering = CalcularDiasClering(operacion.Fecha, det.FechaVencimiento)
                            };

                            //var tasa = GetByConditions<Cliente>(p => p.IdCliente == op.IdCliente).FirstOrDefault().IdTasa;
                            var tasa = ClienteBusiness.Instance.GetTasaCliente(op.IdCliente);
                            detOp.TasaInteres = tasa;
                            detOp.Impuestos = CalcularImpuestoDiario(detOp.DiasClering.Value, cheque.Monto, tasa);
                            detOp.Gastos = CalcularImpustoPorGastos(detOp.Cheque.Monto, configuracion);
                            detOp.ImpuestosCheque = CalcularImpustoAlCheque(detOp.Cheque.Monto, configuracion);
                            detOp.MontoAprobado = (decimal)Math.Round(detOp.Cheque.Monto - detOp.ImpuestosCheque - detOp.Impuestos - detOp.Gastos, 2);

                            operacion.DetalleOperacion.Add(detOp);
                        }
                        ctx.Operacion.Add(operacion);
                        ctx.SaveChanges();
                        tx.Commit();
                        return operacion.Id;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        LogManager.Log.RegisterError(ex);
                        throw ex;
                    }
                }
            }
        }


        public static decimal CalcularImpustoPorGastosByGasto(decimal monto, decimal gasto)
        {
            return monto * gasto / 100.0M;
        }

        public static decimal CalcularImpustoPorGastos(decimal monto, Configuracion configuracion)
        {
            return monto * configuracion.Gastos / 100.0M;
        }

        public static decimal CalcularImpustoAlCheque(decimal monto, Configuracion configuracion)
        {
            return monto * configuracion.ImpuestoCheque / 100.0M;
        }

        public static decimal CalcularImpuestoDiario(int diasClering, decimal monto, decimal tasa)
        {
            return ((diasClering * tasa) / 30.0M) * monto / 100.0M;
        }

        public static decimal CalcularPorcentajeDiario(int diasClering)
        {
            return (diasClering * 4.0M) / 30.0M;
        }

        public PageResult<ViewOperaciones> GetBandejaAprobacionByFilters(DateTime? fecha, int? cliente, int? estado, int? nroop, int skip, int top)
        {
            try
            {
                return DataAccessManager.GetByConditions<ViewOperaciones, DateTime>(
                   op => (fecha == null || DbFunctions.TruncateTime(op.Fecha) == DbFunctions.TruncateTime(fecha))
                        &&
                        (cliente == null || op.IdCliente == cliente)
                        &&
                        (estado == null || op.Estado == estado)
                        &&
                        (nroop == null || op.Id == nroop)
                   , skip, top, op => op.Fecha, true);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public PageResult<ViewOperaciones> GetBandejaOperadorByFilters(DateTime? fecha, int? cliente, int? estado, int? nroop, Usuario user, int skip, int top)
        {
            try
            {
                return DataAccessManager.GetByConditions<ViewOperaciones, DateTime>(
                    op => (fecha == null || DbFunctions.TruncateTime(op.Fecha) == DbFunctions.TruncateTime(fecha))
                         &&
                         (cliente == null || op.IdCliente == cliente)
                         &&
                         (estado == null || op.Estado == estado)
                         &&
                         (nroop == null || op.Id == nroop)
                         &&
                         op.IdUsuario == user.IdUsuario
                         &&
                         op.IdSucursal == SessionContext.IdSucursalOperacionActual
                    , skip, top, op => op.Fecha, true);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public void CancelOperacion(long idOp)
        {
            int[] estados = new int[] { (int)Estados.eEstadosOp.Pendiente24, (int)Estados.eEstadosOp.Pendiente48, (int)Estados.eEstadosOp.Pendiente };
            try
            {
                var operacion = GetByConditions<Operacion>(op => op.Id == idOp && estados.Contains(op.Estado), new List<string> { { "DetalleOperacion" }, { "DetalleOperacion.Cheque" } }).FirstOrDefault();
                if (operacion == null)
                    return;
                var ctx = (DataAccessManager.GetContext() as DCREntities);
                using (var tx = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var det in operacion.DetalleOperacion)
                        {
                            det.MontoAprobado = 0;
                            det.Estado = (int)Estados.eEstadosOpDetalle.Cancelado;
                            det.Cheque.Activo = false;
                            DataAccessManager.Update(det);


                            ChequeBusiness.Instance.ChangeStatus(det.Cheque.IdCheque, Utils.Enumeraciones.Estados.eEstadosCheque.NoAceptado);

                        }

                        if (operacion.Estado == (int)Estados.eEstadosOp.Aprobado)
                        {
                            operacion.Cliente.CuentaCorriente.FirstOrDefault().OperacionCuentaCorriente.Add
                              (
                                   new OperacionCuentaCorriente
                                   {
                                       Monto = operacion.Saldo.Value,
                                       Descripcion = "Devolución por Cancelación Operación Nro: " + operacion.Id,
                                       Fecha = DateTime.Now,
                                       TipoMovimiento = 1
                                   }
                              );
                        }

                      


                        operacion.Saldo = 0;
                        operacion.Estado = (int)Estados.eEstadosOp.Cancelada;

                        DataAccessManager.Update(operacion);
                        DataAccessManager.SaveChanges();
                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        LogManager.Log.RegisterError(ex);
                        throw ex;
                    }

                }

            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public dynamic GetMotivosRechazo(long idDetOp)
        {
            try
            {
                var detOp = GetByConditions<DetalleOperacion>(det => det.Id == idDetOp, det => det.Cheque).FirstOrDefault();
                if (detOp != null)
                {
                    var result = GetByConditions<DetalleOperacion>
                        (det =>
                            det.Cheque.IdBanco == detOp.Cheque.IdBanco &&
                            det.Cheque.NroCuenta == det.Cheque.NroCuenta &&
                            det.Estado == (int)Estados.eEstadosOpDetalle.Rechazado, det => det.Operacion, det => det.Operacion.IdUsuarioEvaluo).Select(det => new { Usuario = det.Operacion.IdUsuarioEvaluo, Fecha = det.Operacion.FechaEvaluacion, Motivo = det.Observacion, Monto = det.Cheque.Monto });
                    
                    return result;
                }

                return null;
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public dynamic GetEstados(bool bandeja, bool aprobador, Estados.eEstadosOp status)
        {
            var result = Enum.GetValues(typeof(Estados.eEstadosOp)).Cast<Estados.eEstadosOp>()
                 .Where(en => (en != Estados.eEstadosOp.Pendiente && en != Estados.eEstadosOp.Pagada) || bandeja);

            if (aprobador)
            {
                if (status == Estados.eEstadosOp.Pendiente24 || status == Estados.eEstadosOp.Pendiente48)
                    result = result.Where(p => p != Estados.eEstadosOp.Pendiente24 && p != Estados.eEstadosOp.Pendiente48);
                else
                    result = new List<Estados.eEstadosOp> { { Estados.eEstadosOp.Pendiente24 }, { Estados.eEstadosOp.Pendiente48 }, { Estados.eEstadosOp.Aprobado }, { Estados.eEstadosOp.Rechazado }, { Estados.eEstadosOp.Cancelada } };
            }


            return result.Select(p => new KeyValuePair<int, string>((int)p, p.GetEnumeracionName()))
                .OrderBy(p => p.Value).ToList();
        }

        public decimal GetTasaCliente(string key)
        {
            var _idTasa = DataAccessManager.GetById<Cliente>(Convert.ToInt32(key)).IdTasa.Value;
            var _tasa = DataAccessManager.GetById<Tasa>(_idTasa);
            return _tasa.Value;
        }

        public decimal GetMaxTasa()
        {
            return (DataAccessManager.GetContext() as DCREntities).Tasa.Max(p => p.Value);
        }
    }
}
