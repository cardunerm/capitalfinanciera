﻿using System.ComponentModel.DataAnnotations;

namespace Business.Enumeraciones
{
    public  class Estados
    {      

        public enum eEstadosOp
        {
            [Display(Name = "Pendiente")]
            Pendiente = 1,           
            [Display(Name = "Aprobado")]
            Aprobado = 3,
            [Display(Name = "Rechazado")]
            Rechazado = 4,
            [Display(Name = "Aprobación en 24hs")]
            Pendiente24 = 5,
            [Display(Name = "Aprobación en 48hs")]
            Pendiente48 = 6
        }

        public enum eEstadosOpDetalle
        {
            [Display(Name = "Pendiente")]
            Pendiente = 1,
            [Display(Name = "Aprobado Parcial")]
            AprobadoParcial = 2,
            [Display(Name = "Aprobado")]
            Aprobado = 3,
            [Display(Name = "Rechazado")]
            Rechazado = 4
        }

    }
}
