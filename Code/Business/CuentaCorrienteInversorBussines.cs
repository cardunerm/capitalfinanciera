﻿using DataAccess;
using Domine;
using Enumerations;
using Luma.Utils.Logs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Enumeraciones;

namespace Business
{
    public  class CuentaCorrienteInversorBussines : GenericBusiness<CuentaCorrienteInversorBussines, CuentaCorrienteInversor>
    {

        #region Methods

        //public List<View_SaldoCtaCteInversor> GetCuentaCorrienteList(string inversor)
        //{
        //    try
        //    {
        //        IEnumerable<View_SaldoCtaCteInversor> query = DataAccessManager.GetByConditions<View_SaldoCtaCteInversor>(p => p.NombreInversor.Contains(inversor));

        //        query = query.OrderBy(p => p.IdCuentaCorrienteInversor);

        //        return (query).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void SaveOrUpdateCuentaCorriente(CuentaCorrienteInversor cuenta)
        {
            CuentaCorrienteInversor entity;

            if (cuenta.IdInversor > 0)
            {
                entity = GetEntity(cuenta.IdCuentaCorrienteInversor);
                entity.Estado = cuenta.Estado;
                entity.IdInversor = cuenta.IdInversor;
            }
            else
            {
                entity = new CuentaCorrienteInversor
                {
                    IdInversor = cuenta.IdInversor,
                    Estado = true
                };
            }

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaCuentaCorrienteInversor(cuenta.IdInversor > 0 ? AuditAction.Edit : AuditAction.Create, entity);
        }

        private bool ValidateSaveOrUpdate(CuentaCorrienteInversor entity)
        {
            //if (!ClienteNombreExist(entity))
            //{
            return true;
            //}
            //else
            //{
            //    throw new ValidationException("Ya existe un cliente con ese nombre");
            //}
        }

        //public bool ValidateDelete(CuentaCorrienteInversor entity)
        //{
        //    if (!ClienteHasOperaciones(entity))
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        throw new ValidationException(string.Format("No se puede eliminar el registro.", "La cuenta corriente tiene operaciones asociadas"));
        //    }
        //}

        //private bool ClienteHasOperaciones(CuentaCorrienteInversor entity)
        //{
        //    try
        //    {
        //        return entity.OperacionCuentaCorriente.Any();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Recupera las cuentas corrientes asociadas a un inversor.
        /// <param name="idInversor"></param>
        /// <returns></returns>
        public List<CuentaCorrienteInversor> GetCuentasCorrientesByIdInversor (int idInversor)
        {
            try
            {

                List<CuentaCorrienteInversor> cuentas = DataAccessManager.GetByConditions<CuentaCorrienteInversor>(c => c.IdInversor == idInversor);
                return cuentas;
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }


        public decimal? GetCtaCteSaldoDebito(DateTime? fechaDesde, DateTime? fechaHasta, int idCuentaCorrienteInversor)
        {
            List<OperacionInversor> query = DataAccessManager.GetByConditions<OperacionInversor>(p => p.TipoMovimiento == 1
                                                                                                               && p.Fecha >= fechaDesde
                                                                                                               && p.Fecha <= fechaHasta
                                                                                                               && p.IdCuentaCorrienteInversor == idCuentaCorrienteInversor);

            return query.Sum(op => op.Monto);
        }

        public decimal? GetCtaCteSaldoCredito(DateTime? fechaDesde, DateTime? fechaHasta, int idCuentaCorrienteInversor)
        {
            List<OperacionInversor> query = DataAccessManager.GetByConditions<OperacionInversor>(p => p.TipoMovimiento == 2
                                                                                                               && p.Fecha >= fechaDesde
                                                                                                               && p.Fecha <= fechaHasta
                                                                                                               && p.IdCuentaCorrienteInversor == idCuentaCorrienteInversor);

            return query.Sum(op => op.Monto);
        }

        public void DeleteLogicCuentaCorrienteInversortByInversor(long IdInversor)
        {
            try
            {
                List<CuentaCorrienteInversor> listCtaCte = DataAccessManager.GetByConditions<CuentaCorrienteInversor>(p =>
                                                p.IdInversor == IdInversor);

                foreach (var item in listCtaCte)
                {
                    CuentaCorrienteInversor ctaCte = this.GetEntity(item.IdCuentaCorrienteInversor);
                    ctaCte.Estado = false;
                    SaveOrUpdate(ctaCte, ValidateSaveOrUpdate);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int OperationInversor(int IdInversor, decimal importe, int tipo)
        {
            try
            {
                CuentaCorrienteInversor ctaCte = DataAccessManager.GetByConditions<CuentaCorrienteInversor>(p =>
                                              p.IdInversor == IdInversor).FirstOrDefault();
                if (ctaCte == null)
                {
                    var ex = new Exception("No hay cuenta corriente para el cliente");
                    LogManager.Log.RegisterError(ex);
                    throw ex;
                }

                OperacionInversor opCtaCte = new OperacionInversor()
                {
                    IdCuentaCorrienteInversor = ctaCte.IdCuentaCorrienteInversor,
                    Observacion = ((tipo == 1) ? "Extracción" : "Depósito") + " de efectivo por caja",
                    Estado = 1,
                    Fecha = DateTime.Now,
                    Monto = importe,
                    TipoMovimiento = (int)(tipo == 1 ? Estados.mTipoMovimientoCuentaCorriente.Debito : Estados.mTipoMovimientoCuentaCorriente.Credito)
                };

                using (DCREntities context = new DCREntities())
                {
                    using (var tx = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var imp = importe;
                            var operaciones = context.Operacion.Where(op => op.Saldo > 0 && op.Estado == (int)Estados.eEstadosOp.Aprobado).OrderBy(op => op.Fecha);
                            foreach (var op in operaciones)
                            {
                                if (op.Saldo <= imp)
                                {
                                    imp -= (op.Saldo ?? 0);
                                    op.Saldo = 0;
                                    op.Estado = (int)Estados.eEstadosOp.Pagada;
                                }
                                else
                                {
                                    op.Saldo -= imp;
                                    imp = 0;
                                }

                                if (imp <= 0)
                                    break;
                            }

                            var opCaja = new OperacionCaja
                            {
                                IdCaja = CajaBusiness.Instance.GetUltimaCaja().IdCaja,
                                TipoMovimiento = (int)(tipo == 1 ? Estados.mTipoMovimientoCuentaCorriente.Credito : Estados.mTipoMovimientoCuentaCorriente.Debito),
                                Monto = importe,
                                Fecha = DateTime.Now,
                                Descripcion = opCtaCte.Observacion
                            };

                            context.OperacionCaja.Add(opCaja);
                            context.OperacionInversor.Add(opCtaCte);
                            context.SaveChanges();
                            tx.Commit();

                            return opCtaCte.IdOperacionInversor;
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            throw ex;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal? GetSaldoByFecha(DateTime? fechaHasta, int idCuentaCorrienteInversor)
        {
            decimal? debito = 0.0M;
            decimal? credito = 0.0M;

            List<OperacionInversor> queryCredito = DataAccessManager.GetByConditions<OperacionInversor>(p => p.TipoMovimiento == 2
                                                                                                               && p.Fecha < fechaHasta
                                                                                                               && p.IdCuentaCorrienteInversor == idCuentaCorrienteInversor);

            List<OperacionInversor> queryDebito = DataAccessManager.GetByConditions<OperacionInversor>(p => p.TipoMovimiento == 1
                                                                                                               && p.Fecha < fechaHasta
                                                                                                               && p.IdCuentaCorrienteInversor == idCuentaCorrienteInversor);

            debito = queryDebito.Sum(op => op.Monto);

            credito = queryCredito.Sum(op => op.Monto);


            return credito - debito;
        }

        #endregion

    }
}
