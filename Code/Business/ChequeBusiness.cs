﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DataAccess.Pages;
using Luma.Utils.Logs;
using System.Transactions;
using Domine.Dto;
using static Utils.Enumeraciones.Estados;
using CurrentContext;
using Utils.Enumeraciones;
using Luma.Utils;

namespace Business
{
    public class ChequeBusiness : GenericBusiness<ChequeBusiness, Cheque>
    {
        #region Methods

        public Cheque GetCheque(int id)
        {
            var include = new List<string>();
            include.Add("Banco");
            var cheque = ChequeBusiness.Instance.GetByConditions<Cheque>(c => c.IdCheque == id, include).FirstOrDefault();

            return cheque;

            //return DataAccessManager.GetById<Cheque>(id);
        }

        public List<Cheque> GetByFilters(string numero, int idBanco, int idEstado, int skip, int top, ref int rowCount)
        {
            List<string> includes = new List<string>();
            includes.Add("Banco");
            includes.Add("ChequeEstado");


            try
            {
                IEnumerable<Cheque> query = DataAccessManager.GetByConditions<Cheque>(p =>
                                                p.Activo == true &&
                                                (idBanco == -1 || p.IdBanco == idBanco) &&
                                                (idEstado == -1 || p.IdEstado == idEstado) &&
                                                p.Numero.Contains(numero), includes);

                query = query.OrderBy(p => p.Numero);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Cheque> GetByFilters(string numero, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<Cheque> query = DataAccessManager.GetByConditions<Cheque>(p =>
                                                p.IdEstado == (int)EntityStatus.Active &&
                                                p.Numero.Contains(numero));

                query = query.OrderBy(p => p.Numero);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PageResult<ViewCheque> GetChequesByFilters(string numero, int skip, int top, ref int rowCount)
        {
            try
            {
                return DataAccessManager.GetByConditions<ViewCheque, int>(
                    cr => (cr.IdEstado == (int)Utils.Enumeraciones.Estados.eEstadosCheque.Cartera) &&
                    (cr.Numero.Contains(numero) || numero == "") &&
                    (cr.Activo == true),
                         skip, top, cr => cr.IdCheque, true);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public List<Cheque> GetDisponiblesArqueo(int skip, int top, ref int rowCount)
        {
            var context = ChequeBusiness.Instance.GetContext();
            try
            {

              //  IEnumerable<Cheque> query2 = DataAccessManager.MapView<Cheque>("Select * From Cheque where Not IdCheque In (Select IdCheque From ArqueoDetalle)", null);

               

                //var query = from arq in context.ArqueoDetalle               
                //            from cheque in context.Cheque
                //            join chequearq in context.Cheque on arq.IdCheque equals chequearq.IdCheque
                //            where cheque.IdCheque != chequearq.IdCheque
                //            select cheque;

                var query = (from cheq in context.Cheque.Include("Banco")
                            where !context.ArqueoDetalle.Any(p => p.IdCheque == cheq.IdCheque)
                            select cheq);


                //IEnumerable<Cheque> query = DataAccessManager.DataContextManager.GetByConditions<Cheque>();

                query = query.OrderBy(p => p.Numero);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                context.Dispose();
            }
        }

        public void SaveOrUpdateCheque(Cheque Cheque)
        {
            Cheque entity;
            try
            {
                if (Cheque.IdCheque > 0)
                {
                    entity = GetEntity(Cheque.IdCheque);
                    entity.DiasClearing = Cheque.DiasClearing;
                    entity.IdEstado = Cheque.IdEstado;
                    entity.FechaVencimiento = Cheque.FechaVencimiento;
                    entity.IdBanco = Cheque.IdBanco;
                    entity.IdRecepcionCheque = Cheque.IdRecepcionCheque;
                    entity.Monto = Cheque.Monto;
                    entity.NroCuenta = Cheque.NroCuenta;
                    entity.Numero = Cheque.Numero;
                    entity.SucursalEmision = Cheque.SucursalEmision;
                    entity.TitularCuenta = Cheque.TitularCuenta;
                }
                else
                {
                    entity = new Cheque
                    {
                        DiasClearing = Cheque.DiasClearing,
                        FechaVencimiento = Cheque.FechaVencimiento,
                        IdBanco = Cheque.IdBanco,
                        IdRecepcionCheque = Cheque.IdRecepcionCheque,
                        Monto = Cheque.Monto,
                        NroCuenta = Cheque.NroCuenta,
                        Numero = Cheque.Numero,
                        SucursalEmision = Cheque.SucursalEmision,
                        TitularCuenta = Cheque.TitularCuenta,
                        IdEstado = (int)Utils.Enumeraciones.Estados.eEstadosCheque.EnOperacion,
                        Activo = true
                    };
                }

                SaveOrUpdate(entity, ValidateSaveOrUpdate);

                AuditoriaBusiness.Instance.AuditoriaCheque(Cheque.IdCheque > 0 ? AuditAction.Edit : AuditAction.Create, entity);
            }

            catch (Exception ex)
            {

                throw ex;
            }

        }
        public bool ValidateSaveOrUpdate(Cheque entity)
        {
            if (!ChequeNumeroExist(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException("Ya se encuentra registrado en el sistema el Cheque");
            }
        }

        private bool ChequeNumeroExist(Cheque entity)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Cheque>(p =>
                                p.IdEstado == (int)EntityStatus.Active &&
                                p.Numero.Equals(entity.Numero) &&
                                p.IdBanco.Equals(entity.IdBanco) &&
                                p.IdCheque != entity.IdCheque);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(Cheque entity)
        {
            if (!ChequeHasCheques(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException(string.Format("No se puede eliminar el registro.", "El Cheque tiene cheques asociados"));
            }
        }

        private bool ChequeHasCheques(Cheque entity)
        {
            bool estado = true;

            try
            {
                if (entity.IdEstado > 0)
                {
                    estado = false;
                }
                return estado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void ChangeStatus(long idcheque, Utils.Enumeraciones.Estados.eEstadosCheque newstatus, DtoRechazoCheque rechazo = null,int rechazos = 0)
        {
            //using (TransactionScope tx = new TransactionScope())
            //{
                try
                {
                    Cheque entidad = GetEntity(idcheque);

                    if (entidad.IdEstado == 10)
                        return;
                        
                    entidad.IdEstado = (int)newstatus;
                    SaveOrUpdateCheque(entidad);

                    //Cargo en la Tabla ChequeMovimiento
                    ChequeMovimiento entidadMovimiento = new ChequeMovimiento();
                    entidadMovimiento.Cheque = entidad;
                    entidadMovimiento.ChequeEstado = ChequeEstadoBusiness.Instance.GetEntity((long)newstatus);
                    entidadMovimiento.Fecha = DateTime.Now;
                    ChequeMovimientoBusiness.Instance.SaveOrUpdate(entidadMovimiento);

                    //Registro el rechazo del cheque
                    if (rechazo != null)
                    {
                        ChequeRechazado chequeRechazado = new ChequeRechazado
                        {
                            IdCheque = rechazo.IdCheque,
                            Motivo = rechazo.Motivo,
                            Gasto = rechazo.Gasto,
                            Impuesto = rechazo.Impuesto,
                            Multa = rechazo.Multa,
                            Fecha = DateTime.Now                            
                        };

                       // DataAccessManager.SaveOrUpdate(chequeRechazado);
                    ChequeRechazadoBusiness.Instance.SaveOrUpdate(chequeRechazado);
                    }

                    DetalleOperacion detOpInv = DataAccessManager.GetByConditions<DetalleOperacion>(d => d.IdCheque == entidad.IdCheque, new List<string> {"Cheque"}).FirstOrDefault();

                    //Verifico que el detalle pertenezca a una operacion de deposito de inversor y que ya haya sido procesado, para revertir las acreditaciones hechas
                    if (!Equals(detOpInv, default(DetalleOperacion)) && detOpInv.IdOperacionInversor != null && detOpInv.IdResumen != null)

                    {
                        OperacionInversor op = DataAccessManager.GetByConditions<OperacionInversor>(o => o.IdOperacionInversor == detOpInv.IdOperacionInversor, new List<string> { "CuentaCorrienteInversor", "CuentaCorrienteInversor.Inversor" }).FirstOrDefault();

                        if (!Equals(op, default(OperacionInversor)))
                        {
                            DateTime fechaActual = DateTime.Now;

                            //Operacion de Debito del monto del chque rechazado
                            OperacionInversor debitOperationMonto = new OperacionInversor
                            {
                                IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                                Observacion = "Débito por monto de cheque rechazado N°: " + entidad.Numero,
                                ClasificacionMovimiento = (int)ClasificacionMovimiento.Egreso,
                                Condicion = (int)Condicion.Efectivo,
                                IdCuentaCorrienteInversor = op.IdCuentaCorrienteInversor,
                                Estado = (int)eEstadosOp.Aprobado,
                                Fecha = fechaActual,
                                IdHistorialTasaInteresInversorAplicado = op.IdHistorialTasaInteresInversorAplicado,
                                Monto = entidad.Monto,
                                TipoMovimiento = (int)mTipoMovimientoCuentaCorriente.Debito
                            };

                            DataAccessManager.Save(debitOperationMonto);

                            //Operación de Débito de los intereses ganados desde que el cheque rechazado fue acreditado en cuenta
                            decimal montoInteresADebitar = CalcularInteresesPorDebitar(fechaActual.Date, entidad.FechaVencimiento.AddDays(1).Date, entidad.Monto, op.CuentaCorrienteInversor.Inversor.IdInversor, op.CuentaCorrienteInversor.TipoMoneda);
                            OperacionInversor debitOperationIntereses = new OperacionInversor
                            {
                                IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                                Observacion = "Débito por intereses acreditados de cheque rechazado N°: " + entidad.Numero,
                                ClasificacionMovimiento = (int)ClasificacionMovimiento.Egreso,
                                Condicion = (int)Condicion.Efectivo,
                                IdCuentaCorrienteInversor = op.IdCuentaCorrienteInversor,
                                Estado = (int)eEstadosOp.Aprobado,
                                Fecha = fechaActual,
                                IdHistorialTasaInteresInversorAplicado = op.IdHistorialTasaInteresInversorAplicado,
                                Monto = montoInteresADebitar,
                                TipoMovimiento = (int)mTipoMovimientoCuentaCorriente.Debito
                            };

                            DataAccessManager.Save(debitOperationIntereses);

                            if (rechazo.Gasto != 0)
                            {
                                OperacionInversor debitOperationGasto = new OperacionInversor
                                {
                                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                                    Observacion = "Débito por gasto de cheque rechazado N°: " + entidad.Numero,
                                    ClasificacionMovimiento = (int)ClasificacionMovimiento.Gastos,
                                    Condicion = (int)Condicion.Efectivo,
                                    IdCuentaCorrienteInversor = op.IdCuentaCorrienteInversor,
                                    Estado = (int)eEstadosOp.Aprobado,
                                    Fecha = fechaActual,
                                    IdHistorialTasaInteresInversorAplicado = op.IdHistorialTasaInteresInversorAplicado,
                                    Monto = Convert.ToDecimal(rechazo.Gasto),
                                    TipoMovimiento = (int)mTipoMovimientoCuentaCorriente.Debito
                                };
                                DataAccessManager.Save(debitOperationGasto);
                            }

                            if (rechazo.Impuesto != 0)
                            {
                                OperacionInversor debitOperationImpuesto = new OperacionInversor
                                {
                                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                                    Observacion = "Débito por impuesto sobre cheque rechazado N°: " + entidad.Numero,
                                    ClasificacionMovimiento = (int)ClasificacionMovimiento.Impuestos,
                                    Condicion = (int)Condicion.Efectivo,
                                    IdCuentaCorrienteInversor = op.IdCuentaCorrienteInversor,
                                    Estado = (int)eEstadosOp.Aprobado,
                                    Fecha = fechaActual,
                                    IdHistorialTasaInteresInversorAplicado = op.IdHistorialTasaInteresInversorAplicado,
                                    Monto = Convert.ToDecimal(rechazo.Impuesto),
                                    TipoMovimiento = (int)mTipoMovimientoCuentaCorriente.Debito
                                };
                                DataAccessManager.Save(debitOperationImpuesto);
                            }

                            if (rechazo.Multa != 0)
                            {
                                OperacionInversor debitOperationMulta = new OperacionInversor
                                {
                                    IdUsuario = SessionContext.UsuarioActual.IdUsuario,
                                    Observacion = "Débito por multa sobre cheque rechazado N°: " + entidad.Numero,
                                    ClasificacionMovimiento = (int)ClasificacionMovimiento.Multas,
                                    Condicion = (int)Condicion.Efectivo,
                                    IdCuentaCorrienteInversor = op.IdCuentaCorrienteInversor,
                                    Estado = (int)eEstadosOp.Aprobado,
                                    Fecha = fechaActual,
                                    IdHistorialTasaInteresInversorAplicado = op.IdHistorialTasaInteresInversorAplicado,
                                    Monto = Convert.ToDecimal(rechazo.Multa),
                                    TipoMovimiento = (int)mTipoMovimientoCuentaCorriente.Debito
                                };

                                DataAccessManager.Save(debitOperationMulta);
                            }
                        }
                    }


                    if(rechazos == 1)
                    {
                        DateTime fechaActual = DateTime.Now;
                        DetalleOperacion _detalleOperacion = DetalleOperacionBusiness.Instance.GetByConditions(p => p.IdCheque == (int)entidad.IdCheque)[0];
                        CuentaCorriente _cuentaCorriente = CuentaCorrienteBusiness.Instance.GetByConditions(p => p.IdCliente == _detalleOperacion.Operacion.IdCliente)[0];
                        

                        //Operacion de Debito del monto del chque rechazado
                        OperacionCuentaCorriente _operactioncuentacorriente = new OperacionCuentaCorriente
                        {
                             Monto = entidad.Monto,
                             Fecha = fechaActual,
                             Descripcion = "Débito por monto de cheque rechazado N°: " + entidad.Numero.Trim(),
                             IdCuentaCorriente = _cuentaCorriente.IdCuentaCorriente,
                             Estado = 0,
                             TipoMovimiento = (int)mTipoMovimientoCuentaCorriente.Debito
                        };                    
                        OperacionCuentaCorrienteBusiness.Instance.SaveOrUpdate(_operactioncuentacorriente);
                                              
                        if (rechazo.Gasto != 0)
                        {                           
                            OperacionCuentaCorriente _operactioncuentacorrienteGasto = new OperacionCuentaCorriente
                            {
                                Monto = Convert.ToDecimal(rechazo.Gasto),
                                Fecha = fechaActual,
                                Descripcion = "Débito por gasto de cheque rechazado N°: " + entidad.Numero.Trim(),
                                IdCuentaCorriente = _cuentaCorriente.IdCuentaCorriente,
                                Estado = 0,
                                TipoMovimiento = (int)mTipoMovimientoCuentaCorriente.Debito
                            };
                            OperacionCuentaCorrienteBusiness.Instance.SaveOrUpdate(_operactioncuentacorrienteGasto);                          
                        }

                        if (rechazo.Impuesto != 0)
                        {                           
                            OperacionCuentaCorriente _operactioncuentacorrienteImpuesto = new OperacionCuentaCorriente
                            {
                                Monto = Convert.ToDecimal(rechazo.Impuesto),
                                Fecha = fechaActual,
                                Descripcion = "Débito por impuesto sobre cheque rechazado N°: " + entidad.Numero.Trim(),
                                IdCuentaCorriente = _cuentaCorriente.IdCuentaCorriente,
                                Estado = 0,
                                TipoMovimiento = (int)mTipoMovimientoCuentaCorriente.Debito
                            };

                            OperacionCuentaCorrienteBusiness.Instance.SaveOrUpdate(_operactioncuentacorrienteImpuesto);
                            
                        }

                        if (rechazo.Multa != 0)
                        {                           
                            OperacionCuentaCorriente _operactioncuentacorrienteMulta = new OperacionCuentaCorriente
                            {
                                Monto = Convert.ToDecimal(rechazo.Multa),
                                Fecha = fechaActual,
                                Descripcion = "Débito por multa sobre cheque rechazado N°: " + entidad.Numero.Trim(),
                                IdCuentaCorriente = _cuentaCorriente.IdCuentaCorriente,
                                Estado = 0,
                                TipoMovimiento = (int)mTipoMovimientoCuentaCorriente.Debito
                            };

                            OperacionCuentaCorrienteBusiness.Instance.SaveOrUpdate(_operactioncuentacorrienteMulta);                         
                        }
                    }
                    
                  //  tx.Complete();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
           // }
        }

        private decimal CalcularInteresesPorDebitar(DateTime fechaActual, DateTime fechaCheque, decimal capital, int idInversor, int TipoMoneda)
        {
            List<HistorialTasaInteresInversor> listaHistorialTasa = DataAccessManager.GetByConditions<HistorialTasaInteresInversor>(h => h.IdInversor == idInversor && h.TipoMonedaTasa == TipoMoneda &&
                                                                                                                         ((h.FechaHasta < fechaCheque) ||
                                                                                                                          (h.FechaDesde < fechaActual) ||
                                                                                                                         (h.FechaDesde >= fechaCheque && h.FechaHasta <= fechaActual))).ToList();
            decimal montoAcumulado = 0;

            foreach (var historial in listaHistorialTasa)
            {
                int cantDias = 0;
                if (historial.FechaDesde < fechaCheque && historial.FechaHasta == null)
                {
                    cantDias = (fechaActual - fechaCheque).Days;
                    
                }

                if(historial.FechaDesde >= fechaCheque && historial.FechaHasta == null)
                {
                    cantDias = (fechaActual - historial.FechaDesde).Days;
                }

                if (historial.FechaDesde >= fechaActual && historial.FechaHasta != null && historial.FechaHasta <= fechaActual)
                {
                    cantDias = (historial.FechaHasta.Value - historial.FechaDesde).Days;
                }

                if (historial.FechaHasta != null && historial.FechaHasta > fechaCheque)
                {
                    cantDias = (historial.FechaHasta.Value - fechaCheque).Days;
                }

                montoAcumulado = montoAcumulado + Convert.ToDecimal(UtilHelper.CalcularDescuento(capital, historial.ValorTasa.Value / 100, cantDias));
            }

            return montoAcumulado;

        }
        #endregion
    }
}