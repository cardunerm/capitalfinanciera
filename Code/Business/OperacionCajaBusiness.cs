﻿using Domine;
using System.Linq;
using System.Collections.Generic;
using Domine.Dto;
using System;
using DataAccess.Pages;
using System.Data.Entity;
using Luma.Utils.Logs;
using Utils.Enumeraciones;
using System.ComponentModel.DataAnnotations;

namespace Business
{
    public class OperacionCajaBusiness : GenericBusiness<OperacionCajaBusiness, OperacionCaja>
    {
        public List<OperacionCaja> GetMovimientos(int idCaja, int skip, int top)
        {
            try
            {
                IEnumerable<OperacionCaja> query = DataAccessManager.GetByConditions<OperacionCaja>(
                    p => p.IdCaja == idCaja);

                query = query.OrderBy(p => p.IdOperacionCaja);

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OperacionCaja SaveOrUpdateOperacionCaja(OperacionCaja operacion)
        {
            OperacionCaja entity;
            Caja caja = CajaBusiness.Instance.GetUltimaCaja();

            if (caja.Total < operacion.Monto && operacion.TipoMovimiento == 2)
            {
                throw new Exception("El monto del egreso es superior al saldo de caja");
            }
            entity = new OperacionCaja
            {
                IdCaja = CajaBusiness.Instance.GetUltimaCaja().IdCaja,
                TipoMovimiento = operacion.TipoMovimiento,
                Monto = operacion.Monto,
                Fecha = DateTime.Now,
                Descripcion = operacion.Descripcion,
                Caja = caja
            };

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            //AuditoriaBusiness.Instance.AuditoriaCliente(cliente.IdCliente > 0 ? AuditAction.Edit : AuditAction.Create, entity);

            return entity;
        }

        private bool ValidateSaveOrUpdate(OperacionCaja entity)
        {
            int idUltimaCaja = CajaBusiness.Instance.GetUltimaCaja().IdCaja;

            if (idUltimaCaja == entity.IdCaja)
            {
                return true;
            }
            else
            {
                throw new ValidationException("La caja se encuentra cerrada");
            }
        }
    }
}
