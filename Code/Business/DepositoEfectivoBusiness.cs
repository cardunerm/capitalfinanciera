﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Business
{
    public class DepositoEfectivoBusiness : GenericBusiness<DepositoEfectivoBusiness, OperacionInversor>
    {

        public List<OperacionInversor> GetByFilters(string nombre, int skip, int top, ref int rowCount)
        {
            try
            {                                

                IEnumerable<OperacionInversor> query = DataAccessManager.GetByConditions<OperacionInversor>( p => p.CuentaCorrienteInversor.Inversor.Nombre.Contains(nombre) ,p => p.CuentaCorrienteInversor, p => p.CuentaCorrienteInversor.Inversor);


                query = query.OrderByDescending(p => p.Fecha);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OperacionInversor> GetInversoresList()
        {
            try
            {
                IEnumerable<OperacionInversor> query = DataAccessManager.GetByConditions<OperacionInversor>(p => p.CuentaCorrienteInversor.Inversor.IdInversor <= 0);


                query = query.OrderBy(p => p.Fecha);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public OperacionInversor SaveOrUpdateInversores(OperacionInversor inversor)
        {
           // InversorEfectivo entity;
            var n = (DataAccessManager.GetContext() as DCREntities).Inversor.Max(p => p.Nombre);
            var name = GetByConditions<Inversor>(p => p.Nombre == n).FirstOrDefault();

            //if (inversor.IdInversorEfectivo >= 0)
            //{
            //entity = GetEntity(inversor.IdInversorEfectivo);
            //entity.Fecha = inversor.Fecha;
            //entity.TipoMonto = inversor.TipoMonto;
            //entity.Monto = inversor.Monto;
        
                SaveOrUpdate(inversor, ValidateSaveOrUpdate);
                return inversor;
            //}

            //return null;

            //entity = new InversorEfectivo
            //{
            //    Fecha = inversor.Fecha,
            //    TipoMonto=inversor.TipoMonto,
            //    Monto=inversor.Monto,
            //    IdInversor=inversor.IdInversor
            //};

            //entity.CuentaCorrienteInversor.Add
            //    (
            //        new CuentaCorrienteInversor
            //        {
            //            IdInversor = 1,
            //            Estado = true

            //        }
            //    );



            //AuditoriaBusiness.Instance.AuditoriaInversores(inversor.IdInversores > 0 ? AuditAction.Edit : AuditAction.Create, entity);


        }

        private bool ValidateSaveOrUpdate(OperacionInversor entity)
        {
            return true;
        }

        public View_OperacionesInversor GetOperacion(int id)
        {
            return DataAccessManager.GetById<View_OperacionesInversor>(id);
        }

    }
}
