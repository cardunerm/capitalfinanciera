﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class MayoristaBusiness : GenericBusiness<MayoristaBusiness, Mayorista>
    {
        #region Methods

       

        public List<Mayorista> GetMayoristaList()
        {
            try
            {
                IEnumerable<Mayorista> query = DataAccessManager.GetByConditions<Mayorista>(p =>
                                                p.Activo == true);

                query = query.OrderBy(p => p.Nombre);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

        #endregion
    }
}
