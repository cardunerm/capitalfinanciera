﻿using Domine;
using System.Linq;
using System.Collections.Generic;
using Domine.Dto;
using System;
using DataAccess.Pages;
using System.Data.Entity;
using Luma.Utils.Logs;
using Utils.Enumeraciones;
using System.ComponentModel.DataAnnotations;
using CurrentContext;

namespace Business
{
    public class CajaBusiness : GenericBusiness<CajaBusiness, Caja>
    {
        public PageResult<ViewCaja> GetCajasByFilters(DateTime? fecha, Usuario user, int skip, int top)
        {
            try
            {
                return DataAccessManager.GetByConditions<ViewCaja, DateTime>(
                    op => (fecha == null || DbFunctions.TruncateTime(op.FechaApertura) == DbFunctions.TruncateTime(fecha))
                         &&
                         op.IdUsuario == user.IdUsuario
                         &&
                         op.IdEmpresa == SessionContext.IdEmpresaActual
                         &&
                         op.IdSucursal == SessionContext.IdSucursalOperacionActual
                    , skip, top, op => op.FechaApertura, true);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public void CrearCaja(Usuario user)
        {
            try
            {
                Caja ultimaCaja = this.GetUltimaCaja();
                Caja nuevaCaja = new Caja();
                if (ultimaCaja != null)
                {
                    if (ultimaCaja.FechaCierre == null)
                    {
                        throw new Exception("Se debe cerrar la ultima caja abierta");
                    }
                    else
                    {
                        nuevaCaja.FechaApertura = DateTime.Now;
                        nuevaCaja.FechaCierre = null;
                        nuevaCaja.MontoInicial = Convert.ToDecimal(ultimaCaja.MontoFinal);
                        nuevaCaja.MontoFinal = 0;
                        nuevaCaja.IdUsuario = user.IdUsuario;
                        nuevaCaja.IdEmpresa = Convert.ToInt32(SessionContext.IdEmpresaActual);
                        nuevaCaja.IdSucursal = Convert.ToInt32(SessionContext.IdSucursalOperacionActual);
                        SaveOrUpdate(nuevaCaja);
                        DataAccessManager.SaveChanges();
                    }
                }
                else
                {
                    nuevaCaja.FechaApertura = DateTime.Now;
                    nuevaCaja.FechaCierre = null;
                    nuevaCaja.MontoInicial = 0;
                    nuevaCaja.MontoFinal = 0;
                    nuevaCaja.IdUsuario = user.IdUsuario;
                    nuevaCaja.IdEmpresa = Convert.ToInt32(SessionContext.IdEmpresaActual);
                    nuevaCaja.IdSucursal = Convert.ToInt32(SessionContext.IdSucursalOperacionActual);

                    SaveOrUpdate(nuevaCaja);
                    DataAccessManager.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public void CerrarCaja(Usuario user)
        {
            try
            {
                Caja ultimaCaja = this.GetUltimaCaja();

                ultimaCaja.MontoFinal = ultimaCaja.Total;
                ultimaCaja.FechaCierre = DateTime.Now;

                SaveOrUpdate(ultimaCaja);
                DataAccessManager.SaveChanges();

            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public Caja GetUltimaCaja()
        {
            try
            {
                return DataAccessManager.GetByConditions<Caja>(ca => ca.IdEmpresa == SessionContext.IdEmpresaActual.Value
                                                                  && ca.IdSucursal == SessionContext.IdSucursalOperacionActual.Value).OrderByDescending(c => c.FechaApertura).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

    }
}
