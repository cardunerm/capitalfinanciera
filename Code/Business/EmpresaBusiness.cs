﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Luma.Utils.Logs;

namespace Business
{
    public class EmpresaBusiness : GenericBusiness<EmpresaBusiness, Empresa>
    {
        #region Methods

        public List<Empresa> GetEmpresasByNombreUsuario(string nombreUsuario)
        {
            try
            {
                IEnumerable<Empresa> query = DataAccessManager.GetByConditions<Empresa>(p =>
                                                p.Usuario.Any(q =>
                                                    q.NombreUsuario.Equals(nombreUsuario) &&
                                                    q.Estado == (int)EntityStatus.Active &&
                                                    q.SucursalUsuario.Any(r =>
                                                        r.Sucursal.IdEmpresa == p.Id)));

                query = query.OrderBy(p => p.Nombre);

                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Empresa> GetAllEmpresas()
        {
            try
            {
                IEnumerable<Empresa> query = DataAccessManager.GetAll<Empresa>();

                query = query.OrderBy(p => p.Nombre);

                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
