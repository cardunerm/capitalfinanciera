﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Model
{
    public class ProyeccionFinancieraModel
    {
        public int IdResumen { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Ingreso { get; set; }
        public decimal Egreso { get; set; }
        public decimal Intereses { get; set; }
        public decimal Impuestos { get; set; }
        public decimal Gastos { get; set; }
        public decimal Multas { get; set; }
        public decimal Total { get; set; }
        public decimal Saldo { get; set; }
        public decimal TasaInteres { get; set; }
        public bool EsCalculado { get; set; }
    }
}
