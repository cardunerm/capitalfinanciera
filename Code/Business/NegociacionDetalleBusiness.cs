﻿using DataAccess.Pages;
using Domine;
using Luma.Utils.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class NegociacionDetalleBusiness : GenericBusiness<NegociacionDetalleBusiness, NegociacionDetalle>
    {


        public List<NegociacionDetalle> GetByIdNegociacion(int idNegociacion)
        {
            try
            {               
                return DataAccessManager.GetByConditions<NegociacionDetalle>(p => p.IdNegociacion == idNegociacion && p.IdEstado != (int)Enumerations.EstadoNegocion.Cancelado, p=> p.Cheque ,p=>p.Cheque.Banco);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }


    }
}
