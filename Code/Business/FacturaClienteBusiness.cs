﻿using Domine;
using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class FacturaClienteBusiness : GenericBusiness<FacturaClienteBusiness,FacturaCliente>
    {

        #region methods

        public List<FacturaCliente> GetByFilters(string nombre, string cuit, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<FacturaCliente> query = DataAccessManager.GetByConditions<FacturaCliente>(p =>
                                                p.Activo == true &&
                                                p.Nombre.Contains(nombre) &&
                                                p.CUIT.Contains(cuit));

                query = query.OrderBy(p => p.Nombre).ThenBy(x => x.CUIT);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FacturaCliente> GetClientes()
        {
            var query = DataAccessManager.GetByConditions<FacturaCliente>(p => p.Activo == true);
            return query;
        }

        #endregion 
    }
}
