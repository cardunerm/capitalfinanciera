﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Business
{
    public class SociedadBusiness : GenericBusiness<SociedadBusiness, Sociedad>
    {
        #region Methods

        public List<Sociedad> GetByFilters(string nombre, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<Sociedad> query = DataAccessManager.GetByConditions<Sociedad>(p =>
                                                p.Estado == (int)EntityStatus.Active &&
                                                p.Nombre.Contains(nombre));

                query = query.OrderBy(p => p.Nombre).ThenBy(x => x.CUIT);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Sociedad> GetSociedadsList()
        {
            try
            {
                IEnumerable<Sociedad> query = DataAccessManager.GetByConditions<Sociedad>(p =>
                                                p.Estado == (int)EntityStatus.Active);

                query = query.OrderBy(p => p.Nombre).ThenBy(x => x.CUIT);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveOrUpdateSociedad(Sociedad Sociedad)
        {
            Sociedad entity;

            if (Sociedad.IdSociedad > 0)
            {
                entity = GetEntity(Sociedad.IdSociedad);
                entity.Nombre = Sociedad.Nombre;
                entity.CUIT = Sociedad.CUIT;
                entity.IdCliente = Sociedad.IdCliente;
            }
            else
            {
                entity = new Sociedad
                {
                    Nombre = Sociedad.Nombre,
                    CUIT = Sociedad.CUIT,
                    IdCliente = Sociedad.IdCliente,
                    Estado = (int)EntityStatus.Active,
                };
            }

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaSociedad(Sociedad.IdSociedad > 0 ? AuditAction.Edit : AuditAction.Create, entity);
        }

        private bool ValidateSaveOrUpdate(Sociedad entity)
        {
            if (!SociedadNombreExist(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException("Ya existe un Sociedad con ese nombre");
            }
        }

        private bool SociedadNombreExist(Sociedad entity)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Sociedad>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Nombre.Equals(entity.Nombre) &&
                                p.CUIT.Equals(entity.CUIT) &&
                                p.IdSociedad != entity.IdSociedad);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(Sociedad entity)
        {
           
                return true;
            
        }              

        public Sociedad SociedadExist(string nombre, string cuit)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Sociedad>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Nombre.Equals(nombre) &&
                                p.CUIT.Equals(cuit));

                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}