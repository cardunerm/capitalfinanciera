﻿using CurrentContext;
using DataAccess.Pages;
using Domine;
using Domine.Dto;
using Domine.PartialModel;
using Enumerations;
using Luma.Utils.Logs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils.Enumeraciones;

namespace Business
{
    public class OperacionInversoresBussines : GenericBusiness<OperacionInversoresBussines, OperacionInversor>
    {


        #region OldMethod
        public DtoSaldoInversor GetSaldoInversor(int inversor)
        {
            var saldos = GetByConditions<ViewSaldoInversor>(view => view.IdInversor == inversor).FirstOrDefault() ?? new ViewSaldoInversor { Credito = 0, Debito = 0 };
            return new DtoSaldoInversor
            {
                Saldo = saldos.Credito.Value - saldos.Debito.Value,
                CantidadRechazos = saldos.CantRechazados ?? 0,
                MontoRechazos = saldos.Rechazado ?? 0
            };

        }

        public static int CalcularDiasClering(DateTime fechaLiquidacion, DateTime fechaPago)
        {
            fechaPago = CalcularFeriados(fechaPago, GetFeriados(fechaPago, fechaPago.AddDays(15)));
            var dias = fechaPago.Date.Subtract(fechaLiquidacion.Date).Days + 2; //dias de diferencia + 48hs del deposito      
            return dias;
        }

        private static List<Feriado> GetFeriados(DateTime desde, DateTime hasta)
        {
            return DataAccessManager.GetByConditions<Feriado>(f => f.Estado == 1 && DbFunctions.TruncateTime(f.Fecha) >= desde && DbFunctions.TruncateTime(f.Fecha) <= hasta).ToList();
        }

        public static DateTime CalcularFeriados(DateTime fechaPago, List<Feriado> feriados)
        {
            if (fechaPago.DayOfWeek == DayOfWeek.Thursday
                || fechaPago.DayOfWeek == DayOfWeek.Friday
                || fechaPago.DayOfWeek == DayOfWeek.Saturday
                || fechaPago.DayOfWeek == DayOfWeek.Sunday) //feriados.Count(f => f.Fecha.Date.CompareTo(fechaPago.Date) == 0) > 0)
            {
                fechaPago = fechaPago.AddDays(2);
                CalcularFeriados(fechaPago, feriados);
            }
            else
                return fechaPago;

            return fechaPago;
        }

        public List<ViewResumenDetalleOperacionInversor> GetChequesByOperacion(int id)
        {
            try
            {
                return DataAccessManager.GetByConditions<ViewResumenDetalleOperacionInversor>
                    (
                        det => det.IdOperacionInversor == id
                    );
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public void UpdateDetalleOperacion(DtoDetalleOperacionAprobacion detalle, short? newStatus, Usuario user, Configuracion config)
        {
            try
            {
                var detalleOperacion = DataAccessManager.GetById<DetalleOperacion>(detalle.Id);
                if (detalleOperacion != null)
                {
                    detalleOperacion.TasaInteres = decimal.Parse(detalle.TasaStr); // detalle.Tasa;
                    detalleOperacion.MontoAprobado = (newStatus ?? 0) == (int)Estados.eEstadosOpDetalle.Rechazado ? 0 : CalcularMontoAPagar(detalle.Monto, detalle.FechaVencimiento, detalleOperacion.OperacionInversor.Fecha, decimal.Parse(detalle.TasaStr), decimal.Parse(detalle.GastoStr), config);
                    detalleOperacion.IdUsuario = user.IdUsuario;
                    detalleOperacion.Observacion = detalle.Observacion;
                    detalleOperacion.Gastos = Math.Round((decimal.Parse(detalle.GastoStr) * detalle.Monto) / 100, 2);
                    detalleOperacion.Impuestos = CalcularImpuestoDiario(detalle.DiasClearing.Value, detalle.Monto, decimal.Parse(detalle.TasaStr));
                    if (detalle.Numero != null)
                    {
                        int cp = 0;
                        detalleOperacion.Cheque.IdBanco = detalle.IdBanco;
                        detalleOperacion.Cheque.FechaVencimiento = detalle.FechaVencimiento;
                        detalleOperacion.Cheque.Monto = detalle.Monto;
                        detalleOperacion.Cheque.NroCuenta = detalle.NumeroCuenta;
                        detalleOperacion.Cheque.TitularCuenta = detalle.TitularCuenta;
                        detalleOperacion.Cheque.Numero = detalle.Numero;
                        detalleOperacion.Cheque.SucursalEmision = detalle.SucursalEmision;
                        detalleOperacion.Cheque.CP = int.TryParse(detalle.Cp, out cp) ? (int?)cp : null;
                    }

                    if (newStatus != null)
                        detalleOperacion.Estado = newStatus.Value;

                    DataAccessManager.Update(detalleOperacion);



                    DataAccessManager.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        private decimal CalcularMontoAPagar(decimal monto, DateTime fechaVencimiento, DateTime fecha, decimal? tasa, decimal gasto, Configuracion configuracion)
        {

            var diasClering = CalcularDiasClering(fecha, fechaVencimiento);
            var impuestoDiario = CalcularImpuestoDiario(diasClering, monto, tasa.Value);
            var impuestoAlCheque = CalcularImpustoAlCheque(monto, configuracion);
            var impuestoPorGastos = CalcularImpustoPorGastosByGasto(monto, gasto);
            return (decimal)Math.Round(monto - impuestoAlCheque - impuestoDiario - impuestoPorGastos, 2);

        }

        //public void Pagar(long idOp, Usuario user)
        //{
        //var operacion = GetByConditions<OperacionInversor>(op => op.IdOperacionInversor == idOp, new List<string> { { "DetalleOperacion" } }).FirstOrDefault();

        //if (operacion == null)
        //    return;
        //try
        //{
        //    if (operacion.Estado == (int)Estados.eEstadosOp.Aprobado)
        //    {
        //        operacion.IdUsuarioPago = user.IdUsuario;
        //        operacion.FechaPago = DateTime.Now;
        //        operacion.Estado = (int)Estados.eEstadosOp.Pagada;
        //        operacion.Inversores.CuentaCorrienteInversor.FirstOrDefault().OperacionCtaCteInversor.Add
        //            (
        //                 new OperacionCtaCteInversor
        //                 {
        //                     Monto = operacion.Saldo.Value,
        //                     Descripcion = "Pago Operación Nro: " + operacion.IdOperacionInversor,
        //                     Fecha = DateTime.Now,
        //                     TipoMovimiento = 1
        //                 }
        //            );

        //        operacion.Saldo = 0;
        //        SaveOrUpdate(operacion);
        //        DataAccessManager.SaveChanges();


        //        //Una vez guardada la operacion , guardo el movimiento en la caja
        //        OperacionCaja opeCaja = new OperacionCaja();
        //        opeCaja.Descripcion = "Pago Operacion Nro: " + operacion.IdOperacionInversor;
        //        opeCaja.Fecha = DateTime.Now;
        //        opeCaja.Monto = (decimal)operacion.MontoAprobado;
        //        opeCaja.TipoMovimiento = 2;
        //        OperacionCajaBusiness.Instance.SaveOrUpdateOperacionCaja(opeCaja);

        //        //Cambio en estado de los cheques
        //        foreach (var item in operacion.DetalleOperacion)
        //        {
        //            ChequeBusiness.Instance.ChangeStatus(item.Cheque.IdCheque, Utils.Enumeraciones.Estados.eEstadosCheque.Cartera);
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //    LogManager.Log.RegisterError(ex);
        //    throw ex;
        //}

        //}

        //public bool Finalizar(DtoOperacionAprobacion dtoOp, Usuario user, ref string msg)
        //{
        //var operacion = GetByConditions<OperacionInversor>(op => op.IdOperacionInversor == dtoOp.Id, new List<string> { { "DetalleOperacion" }, { "DetalleOperacion.Cheque" } }).FirstOrDefault();

        //if (operacion == null)
        //    return false;

        //var ctx = (DataAccessManager.GetContext() as DCREntities);

        //using (var tx = ctx.Database.BeginTransaction())
        //{
        //    dtoOp.IdUser = operacion.IdUsuario;

        //    try
        //    {
        //        if (operacion.Estado != dtoOp.IdEstado)
        //        {
        //            if (IsValidChange(operacion, dtoOp.IdEstado, ref msg))
        //            {
        //                operacion.IdUsuarioEvaluo = user.IdUsuario;
        //                operacion.FechaEvaluacion = DateTime.Now;
        //                if (dtoOp.IdEstado == (int)Estados.eEstadosOp.Aprobado || dtoOp.IdEstado == (int)Estados.eEstadosOp.Rechazado)
        //                {
        //                    operacion.MontoAprobado = dtoOp.IdEstado == (int)Estados.eEstadosOp.Aprobado ? operacion.DetalleOperacion.Sum(det => (decimal)(det.MontoAprobado ?? 0)) : 0;
        //                    operacion.Saldo = operacion.MontoAprobado;

        //                    //EvaluarCheques(operacion,ctx);

        //                    foreach (var det in operacion.DetalleOperacion)
        //                    {
        //                        if (det.Estado == (int)Estados.eEstadosOpDetalle.Pendiente)
        //                        {
        //                            det.Estado = (short)dtoOp.IdEstado;
        //                            det.Observacion = dtoOp.ObservacionEvaluacion;
        //                            if (det.Estado == (int)Estados.eEstadosOpDetalle.Rechazado)
        //                            {
        //                                det.Cheque.Activo = false;
        //                            }
        //                            else if (det.Estado == (int)Estados.eEstadosOpDetalle.Aprobado)
        //                            {
        //                                //if (det.Cheque.IdEstado == (int)Utils.Enumeraciones.Estados.eEstadosCheque.EnOperacion)
        //                                //{
        //                                //    det.Cheque.IdEstado = (int)Utils.Enumeraciones.Estados.eEstadosCheque.Aprobado;
        //                                //}
        //                            }
        //                            DataAccessManager.Update(det);
        //                        }
        //                    }

        //                    GenerarMovimientoCuentaCorriente(operacion);
        //                }

        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }

        //        operacion.ObservacionEvaluacion = dtoOp.ObservacionEvaluacion;
        //        operacion.Estado = (short)dtoOp.IdEstado;

        //        DataAccessManager.Update(operacion);
        //        DataAccessManager.SaveChanges();
        //        tx.Commit();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        tx.Rollback();
        //        LogManager.Log.RegisterError(ex);
        //        throw ex;
        //    }
        //}

        //}

        //private void GenerarMovimientoCuentaCorriente(OperacionInversor operacion)
        //{
        //var detallesAprobados = operacion.DetalleOperacion.Where(cheque => cheque.Estado == (int)Estados.eEstadosOpDetalle.Aprobado || cheque.Estado == (int)Estados.eEstadosOpDetalle.AprobadoParcial);
        //var montoAprobado = detallesAprobados.Sum(p => p.Cheque.Monto);
        //var gastos = detallesAprobados.Sum(p => p.Gastos);
        //var impuestos = detallesAprobados.Sum(p => p.Impuestos);
        //var impuestoCheque = detallesAprobados.Sum(p => p.ImpuestosCheque);

        //var cuentaCorrientInversor = GetByConditions<CuentaCorrienteInversor>(p => p.IdInversor == operacion.CuentaCorrienteInversor.Ope).FirstOrDefault();

        //if (cuentaCorrientInversor == null)
        //    throw new Exception("No hay cuenta corriente");

        //DataAccessManager.Save(
        //    new OperacionInversor
        //    {
        //        Monto = montoAprobado,
        //        CuentaCorrienteInversor = cuentaCorrientInversor,
        //        Observacion = "Operación Nro: " + operacion.IdOperacionInversor + " - Fecha: " + operacion.Fecha.ToString("dd/MM/yyyy HH:mm"),
        //        Fecha = DateTime.Now,
        //        TipoMovimiento = 2
        //    });

        //DataAccessManager.Save(
        //    new OperacionInversor
        //    {
        //        Monto = impuestos,
        //        CuentaCorrienteInversor = cuentaCorrientInversor,
        //        Observacion = "Intereses Operación Nro: " + operacion.IdOperacionInversor + " - Fecha: " + operacion.Fecha.ToString("dd/MM/yyyy HH:mm"),
        //        Fecha = DateTime.Now,
        //        TipoMovimiento = 1
        //    });

        //DataAccessManager.Save(
        //   new OperacionCtaCteInversor
        //   {
        //       Monto = gastos + impuestoCheque,
        //       CuentaCorrienteInversor = cuentaCorrientInversor,
        //       Descripcion = "Comision y Gastos Operación Nro: " + operacion.IdOperacionInversor + " - Fecha: " + operacion.Fecha.ToString("dd/MM/yyyy HH:mm"),
        //       Fecha = DateTime.Now,
        //       TipoMovimiento = 1
        //   });
        //}

        public static string DescripcionCalculo(DetalleOperacion edit)
        {
            return
                "Monto - Inteeres - Imp. Cheque - Gastos </br> (" + edit.Cheque.Monto.ToString("C2")
                    + " - " + edit.Impuestos.ToString("C2")
                    + " - " + edit.ImpuestosCheque.ToString("C2")
                    + " - " + edit.Gastos.ToString("C2")
                    + ")";

            ;
        }

        private bool IsValidChange(OperacionInversor operacion, int newStatus, ref string msg)
        {
            bool status = operacion.Estado == (int)Estados.eEstadosOp.Pendiente ||
                   operacion.Estado == (int)Estados.eEstadosOp.Pendiente24 ||
                   operacion.Estado == (int)Estados.eEstadosOp.Pendiente48;

            if (!status)
                return false;

            if (newStatus == (int)Estados.eEstadosOp.Aprobado)
            {
                bool faltaEvaluar = operacion.DetalleOperacion.Any(det => det.MontoAprobado == null);
                if (faltaEvaluar)
                {
                    msg = Resources.Resource.EvaluarCheques;
                    return false;
                }
            }

            return true;
        }

        public int CreateOperacion(DtoOperacionInversor op, List<DtoDetalleOperacion> detalle, Usuario user, Configuracion configuracion)
        {

            using (var ctx = DataAccessManager.GetContext() as DCREntities)
            {
                using (var tx = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        //Busco la cuenta para obtener el tipo de moneda.
                        CuentaCorrienteInversor ctacte = CuentaCorrienteBusiness.DataAccessManager.GetById<CuentaCorrienteInversor>(op.IdCuentaCorrienteInversor);

                        //Buscar Id de la Tasa de interés vigente al momento de la operación

                        int IdTasaAplicable = HistorialTasaInteresBusiness.DataAccessManager.GetByConditions<HistorialTasaInteresInversor>(t => t.IdInversor == op.IdInversor && t.TipoMonedaTasa == ctacte.TipoMoneda && t.FechaHasta == null).FirstOrDefault().IdHistorial;

                        OperacionInversor operacion = new OperacionInversor
                        {
                            IdCuentaCorrienteInversor = op.IdCuentaCorrienteInversor,
                            Fecha = DateTime.Now,
                            IdUsuario = user.IdUsuario,
                            Estado = (int)Estados.eEstadosOp.Pendiente,
                            Observacion = op.Observacion,
                            Monto = detalle.Sum(det => det.Monto),
                            Condicion = 1,
                            ClasificacionMovimiento = 1,
                            TipoMovimiento =2,
                            IdHistorialTasaInteresInversorAplicado = IdTasaAplicable,
                            IdEmpresa = Convert.ToInt32(SessionContext.IdEmpresaActual),
                            IdSucursal = Convert.ToInt32(SessionContext.IdSucursalOperacionActual),
                            EsOperacionGarantia = ctacte.ClasificacionInversor == (int)ClasificacionInversor.BBB ? true : false
                        };

                        operacion.DetalleOperacion = new List<DetalleOperacion>();
                        
                        foreach (var det in detalle)
                        {
                            var recepcion = new RecepcionCheque
                            {
                                IdInversor = op.IdInversor,
                                Estado = (int)Estados.eEstadosOpDetalle.Pendiente,
                                Fecha = DateTime.Now
                            };

                            int cp = 0;
                            int.TryParse(det.Cp, out cp);
                            var cheque = new Cheque
                            {
                                IdBanco = det.IdBanco,
                                IdEstado = (int)Estados.eEstadosCheque.Cartera,
                                FechaVencimiento = det.FechaVencimiento,
                                Monto = det.Monto,
                                NroCuenta = det.NumeroCuenta,
                                Numero = det.Numero,
                                TitularCuenta = det.TitularCuenta,
                                SucursalEmision = det.SucursalEmision,
                                DiasClearing = CalcularDiasClering(operacion.Fecha, det.FechaVencimiento),
                                RecepcionCheque = recepcion,
                                ImagenFront = det.ImagenFront,
                                ImagenBack = det.ImagenBack,
                                CP = cp != 0 ? (int?)cp : null,
                                CMC7 = det.CMC7,
                                Activo = true,
                                Clasificacion = ClasificarCheque(det, ctacte) 
                            };

                            var chequemovimiento = new ChequeMovimiento
                            {
                                Cheque = cheque,
                                ChequeEstado = ChequeEstadoBusiness.Instance.GetEntity((long)Utils.Enumeraciones.Estados.eEstadosCheque.EnOperacion),
                                Fecha = DateTime.Now
                            };

                            var detOp = new DetalleOperacion
                            {
                                OperacionInversor = operacion,
                                Cheque = cheque,
                                Estado = (int)Estados.eEstadosOpDetalle.Pendiente,
                                DiasClering = CalcularDiasClering(operacion.Fecha, det.FechaVencimiento)
                            };

                            var tasa = Instance.GetTasa(op.IdCuentaCorrienteInversor);

                            detOp.TasaInteres = tasa;

                            detOp.Impuestos = CalcularImpuestoDiario(detOp.DiasClering.Value, cheque.Monto, tasa);
                            detOp.Gastos = CalcularImpustoPorGastos(detOp.Cheque.Monto, configuracion);
                            detOp.ImpuestosCheque = CalcularImpustoAlCheque(detOp.Cheque.Monto, configuracion);
                            detOp.MontoAprobado = (decimal)Math.Round(detOp.Cheque.Monto - detOp.ImpuestosCheque - detOp.Impuestos - detOp.Gastos, 2);

                            operacion.DetalleOperacion.Add(detOp);
                        }
                        ctx.OperacionInversor.Add(operacion);
                        ctx.SaveChanges();
                        tx.Commit();

                        //Llamo al metodo para calcular que cheques quedan como Garantia y cuales pasan a operativos.
                        CalcularChequeGarantia((int)operacion.IdCuentaCorrienteInversor);


                        return operacion.IdOperacionInversor;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        LogManager.Log.RegisterError(ex);
                        throw ex;
                    }
                }
            }
        }

        public void CalcularChequeGarantia(int idInversor)
        {

                var context = DataAccessManager.GetContext() as DCREntities;
                
                try
                {
                    //Recupero las cuentas de clase BBB
                    var cuentasBBB = from ctacte in  context.CuentaCorrienteInversor
                                     join inversor in context.Inversor on ctacte.IdInversor equals inversor.IdInversor
                                     where ctacte.Estado == true && ctacte.ClasificacionInversor == (int)ClasificacionInversor.BBB && ctacte.IdCuentaCorrienteInversor == idInversor
                                     select ctacte;

                    var _IdCuentaCorrienteInversor = cuentasBBB.ToList()[0].IdCuentaCorrienteInversor;
                    //Proceso los cheque de cada cuenta
                    //foreach (var ctaBBB in cuentasBBB)
                    //{
                        //join ch in context.Cheque on det.IdCheque equals ch.IdCheque
                       
                        var listaChequesEnCartera = from ch in context.Cheque
                                                    join detOp in context.DetalleOperacion on ch.IdCheque equals detOp.IdCheque
                                                    join opInv in context.OperacionInversor on detOp.IdOperacionInversor equals opInv.IdOperacionInversor
                                                    join ctacte in context.CuentaCorrienteInversor on opInv.IdCuentaCorrienteInversor equals ctacte.IdCuentaCorrienteInversor
                                                    where
                                                    ctacte.IdCuentaCorrienteInversor == _IdCuentaCorrienteInversor &&
                                                    //ch.FechaVencimiento.Date > fechaDeCorrida.Date
                                                    ch.FechaVencimiento > DateTime.Now
                                                    select ch;

                        List<Cheque> chequesOperativos = new List<Cheque>();
                        List<Cheque> chequesEnGarantia = new List<Cheque>();

                        decimal porcentajeBalance = cuentasBBB.ToList()[0].Inversor.PorcentajeBalanceGarantiaOperativo;

                        decimal montoOperativo = 0;
                        decimal montoGarantia = 0;

                        foreach (var chequeEnCartera in listaChequesEnCartera)
                        {
                            if (chequeEnCartera.Clasificacion == (int)ClasificacionCheque.Garantia)
                            {
                                montoGarantia = montoGarantia + chequeEnCartera.Monto;
                                chequesEnGarantia.Add(chequeEnCartera);
                            }
                            else
                            {
                                montoOperativo = montoOperativo + chequeEnCartera.Monto;
                                chequesOperativos.Add(chequeEnCartera);
                            }
                        }

                        //Ordeno las lista de cheques en garantia por Fecha de Cobro
                        chequesEnGarantia = chequesEnGarantia.OrderBy(c => c.FechaVencimiento).ToList();

                        var totalgarantia = chequesEnGarantia.Sum(p => p.Monto);
                        var montogarantia = ((totalgarantia * porcentajeBalance) / 100) * decimal.Parse("1.20");

                        foreach (var chequeGarantia in chequesEnGarantia)
                        {
                            if (totalgarantia > montogarantia)
                            {
                                chequeGarantia.Clasificacion = (int)ClasificacionCheque.Activo;

                                DataAccessManager.Update<Cheque>(chequeGarantia);
                                DataAccessManager.SaveChanges();     
                            }

                            totalgarantia -= chequeGarantia.Monto;
                        }                      

                    //}
                }
                catch (Exception ex)
                {

                }
            
        }

        private int ClasificarCheque(DtoDetalleOperacion det, CuentaCorrienteInversor ctacte)
        {
            if (ctacte.ClasificacionInversor == (int)ClasificacionInversor.BBB)
            {
                return (int)ClasificacionCheque.Garantia;
                //if (det.EsChequeGarantia)
                //{
                //    return (int)ClasificacionCheque.Garantia;
                //}
                //else
                //{
                //    return (int)ClasificacionCheque.Activo;
                //}
            }
            else
            {
                return (int)ClasificacionCheque.Comun;
            }
        }

        public static decimal CalcularImpustoPorGastosByGasto(decimal monto, decimal gasto)
        {
            return monto * gasto / 100.0M;
        }

        public static decimal CalcularImpustoPorGastos(decimal monto, Configuracion configuracion)
        {
            return monto * configuracion.Gastos / 100.0M;
        }

        public static decimal CalcularImpustoAlCheque(decimal monto, Configuracion configuracion)
        {
            return monto * configuracion.ImpuestoCheque / 100.0M;
        }

        public static decimal CalcularImpuestoDiario(int diasClering, decimal monto, decimal tasa)
        {
            return ((diasClering * tasa) / 30.0M) * monto / 100.0M;
        }

        public static double CalcularPorcentajeDiario(int diasClering)
        {
            return (diasClering * 4.0) / 30.0;
        }

        public PageResult<View_OperacionesInversor> GetBandejaAprobacionByFilters(DateTime? fecha, int? inversor, int? estado, int? nroop, int skip, int top)
        {
            try
            {
                return DataAccessManager.GetByConditions<View_OperacionesInversor, DateTime>(
                   op => (fecha == null || DbFunctions.TruncateTime(op.Fecha) == DbFunctions.TruncateTime(fecha))
                        &&
                        (inversor == null || op.IdInversor == inversor)
                        &&
                        (estado == null || op.Estado == estado)
                        &&
                        (nroop == null || op.IdOperacionInversor == nroop)
                   , skip, top, op => op.Fecha, true);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public PageResult<View_OperacionesInversor> GetBandejaOperadorByFilters(DateTime? fecha, int? inversor, int? estado, int? nroop, Usuario user, int skip, int top)
        {
            try
            {
                return DataAccessManager.GetByConditions<View_OperacionesInversor, DateTime>(
                    op => (fecha == null || DbFunctions.TruncateTime(op.Fecha) == DbFunctions.TruncateTime(fecha))
                         &&
                         (inversor == null || op.IdInversor == inversor)
                         &&
                         (estado == null || op.Estado == estado)
                         &&
                         (nroop == null || op.IdOperacionInversor == nroop)
                    //&&
                    //op.IdUsuario == user.IdUsuario
                    //&&
                    //op.IdSucursal == SessionContext.IdSucursalOperacionActual
                    , skip, top, op => op.Fecha, true);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public void CancelOperacion(long idOp)
        {
            //int?[] estados = new int?[] { (int)Estados.eEstadosOp.Pendiente24, (int)Estados.eEstadosOp.Pendiente48, (int)Estados.eEstadosOp.Pendiente };
            //try
            //{
            //    var operacion = GetByConditions<OperacionInversor>(op => op.IdOperacionInversor == idOp && estados.Contains(op.Estado), new List<string> { { "DetalleOperacion" }, { "DetalleOperacion.Cheque" } }).FirstOrDefault();
            //    if (operacion == null)
            //        return;
            //    var ctx = (DataAccessManager.GetContext() as DCREntities);
            //    using (var tx = ctx.Database.BeginTransaction())
            //    {
            //        try
            //        {
            //            foreach (var det in operacion.DetalleOperacion)
            //            {
            //                det.MontoAprobado = 0;
            //                det.Estado = (int)Estados.eEstadosOpDetalle.Cancelado;
            //                det.Cheque.Activo = false;
            //                DataAccessManager.Update(det);


            //                ChequeBusiness.Instance.ChangeStatus(det.Cheque.IdCheque, Utils.Enumeraciones.Estados.eEstadosCheque.NoAceptado);

            //            }

            //            if (operacion.Estado == (int)Estados.eEstadosOp.Aprobado)
            //            {
            //                operacion.Inversor.CuentaCorrienteInversor.FirstOrDefault().OperacionCtaCteInversor.Add
            //                  (
            //                       new OperacionInversor
            //                       {
            //                           Monto = operacion..Value,
            //                           Observacion = "Devolución por Cancelación Operación Nro: " + operacion.IdOperacionInversor,
            //                           Fecha = DateTime.Now,
            //                           TipoMovimiento = 1
            //                       }
            //                  );
            //            }




            //            operacion.Saldo = 0;
            //            operacion.Estado = (int)Estados.eEstadosOp.Cancelada;

            //            DataAccessManager.Update(operacion);
            //            DataAccessManager.SaveChanges();
            //            tx.Commit();
            //        }
            //        catch (Exception ex)
            //        {
            //            tx.Rollback();
            //            LogManager.Log.RegisterError(ex);
            //            throw ex;
            //        }

            //    }

            //}
            //catch (Exception ex)
            //{
            //    LogManager.Log.RegisterError(ex);
            //    throw ex;
            //}
        }

        public dynamic GetMotivosRechazo(long idDetOp)
        {
            //try
            //{
            //    var detOp = GetByConditions<DetalleOperacion>(det => det.Id == idDetOp, det => det.Cheque).FirstOrDefault();
            //    if (detOp != null)
            //    {
            //        var result = GetByConditions<DetalleOperacion>
            //            (det =>
            //                det.Cheque.IdBanco == detOp.Cheque.IdBanco &&
            //                det.Cheque.NroCuenta == det.Cheque.NroCuenta &&
            //                det.Estado == (int)Estados.eEstadosOpDetalle.Rechazado, det => det.OperacionInversor, det => det.OperacionInversor.IdUsuarioEvaluo).Select(det => new { Usuario = det.OperacionInversor.IdUsuarioEvaluo, Fecha = det.OperacionInversor.FechaEvaluacion, Motivo = det.Observacion, Monto = det.Cheque.Monto });

            //        return result;
            //    }

            //    return null;
            //}
            //catch (Exception ex)
            //{
            //    LogManager.Log.RegisterError(ex);
            //    throw ex;
            //}
            return null;
        }

        public dynamic GetEstados(bool bandeja, bool aprobador, Estados.eEstadosOp status)
        {
            var result = Enum.GetValues(typeof(Estados.eEstadosOp)).Cast<Estados.eEstadosOp>()
                 .Where(en => (en != Estados.eEstadosOp.Pendiente && en != Estados.eEstadosOp.Pagada) || bandeja);

            if (aprobador)
            {
                if (status == Estados.eEstadosOp.Pendiente24 || status == Estados.eEstadosOp.Pendiente48)
                    result = result.Where(p => p != Estados.eEstadosOp.Pendiente24 && p != Estados.eEstadosOp.Pendiente48);
                else
                    result = new List<Estados.eEstadosOp> { { Estados.eEstadosOp.Pendiente24 }, { Estados.eEstadosOp.Pendiente48 }, { Estados.eEstadosOp.Aprobado }, { Estados.eEstadosOp.Rechazado }, { Estados.eEstadosOp.Cancelada } };
            }


            return result.Select(p => new KeyValuePair<int, string>((int)p, p.GetEnumeracionName()))
                .OrderBy(p => p.Value).ToList();
        }



        public decimal GetMaxTasa()
        {
            return (DataAccessManager.GetContext() as DCREntities).Tasa.Max(p => p.Value);
        }

        public decimal GetTasa(int IdCuentaCorrienteInversorHidden)
        {

            try
            {
                var ctacte = DataAccessManager.GetByConditions<CuentaCorrienteInversor>(p => p.IdCuentaCorrienteInversor == IdCuentaCorrienteInversorHidden).FirstOrDefault();

                if (ctacte.TipoMoneda == 0)
                {
                    return ctacte.Inversor.TasaUSD;
                }

                if (ctacte.TipoMoneda == 1)
                {
                    return ctacte.Inversor.Tasa;
                }

                return 0M;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public decimal GetLimiteByIdCtaCteInversor(int idCuentaCorrienteInversor)
        {
            try
            {
                var ctacte = DataAccessManager.GetByConditions<CuentaCorrienteInversor>(c => c.IdCuentaCorrienteInversor == idCuentaCorrienteInversor).FirstOrDefault();
                if (ctacte.TipoMoneda == 0)
                {
                    return ctacte.Inversor.LimiteDescubiertoUSD;
                }
                else
                {
                    return ctacte.Inversor.LimiteDescubierto;     
                }      
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        public bool NuevoDepositoEfectivo(DtoOperacionInversor dtoOperacion, Usuario usuario, ref DtoReporteEfectivo dtoReporte)
        {
            try
            {
                //Busco la cuenta para obtener el tipo de moneda.
                CuentaCorrienteInversor ctacte = CuentaCorrienteBusiness.DataAccessManager.GetById<CuentaCorrienteInversor>(dtoOperacion.IdCuentaCorrienteInversor);

                //Buscar Id de la Tasa de interés vigente al momento de la operación

                int IdTasaAplicable = HistorialTasaInteresBusiness.DataAccessManager.GetByConditions<HistorialTasaInteresInversor>(t => t.IdInversor == dtoOperacion.IdInversor && t.TipoMonedaTasa == ctacte.TipoMoneda && t.FechaHasta == null).FirstOrDefault().IdHistorial;

                //Crear Operacion
                OperacionInversor opInversor = new OperacionInversor
                {

                    IdUsuario = usuario.IdUsuario,
                    Fecha = dtoOperacion.Fecha, // DateTime.Now,
                    Observacion = dtoOperacion.Observacion,
                    //Estado = (int)Estados.eEstadosOp.Aprobado,
                    Estado = (int)Estados.eEstadosOp.Pendiente,
                    Monto = dtoOperacion.Monto,
                    IdCuentaCorrienteInversor = dtoOperacion.IdCuentaCorrienteInversor,
                    TipoMovimiento = (int)Estados.mTipoMovimientoCuentaCorriente.Credito,
                    IdHistorialTasaInteresInversorAplicado = IdTasaAplicable,
                    Condicion = (int)Condicion.Efectivo,
                    ClasificacionMovimiento = (int)ClasificacionMovimiento.Ingreso
                };

                SaveOrUpdate(opInversor);

                dtoReporte.NombreUsuario = SessionContext.UsuarioActual.NombreUsuario;
                dtoReporte.Fecha = opInversor.Fecha;
                dtoReporte.NombreInversor = ctacte.Inversor.Nombre;
                dtoReporte.CuitInversor = ctacte.Inversor.CUIT;
                dtoReporte.Monto = opInversor.Monto;
                dtoReporte.CuentaCorrienteStr = ctacte.NumeroCtaCteStr;
                dtoReporte.NumeroOperacion = opInversor.IdOperacionInversor;
                dtoReporte.TipoMoneda = ((TipoMoneda)ctacte.TipoMoneda).ToString();

                return true;
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                return false;
            }
        }

        public bool ExtraccionFondos(DtoOperacionInversor dtoOperacion, Usuario usuario, ref DtoReporteEfectivo dtoReporte)
        {
            try
            {
                //Busco la cuenta para obtener el tipo de moneda.
                CuentaCorrienteInversor ctacte = CuentaCorrienteBusiness.DataAccessManager.GetById<CuentaCorrienteInversor>(dtoOperacion.IdCuentaCorrienteInversor);

                if (dtoOperacion.IdInversor == 0)
                    dtoOperacion.IdInversor = (int)ctacte.IdInversor;

                //Buscar Id de la Tasa de interés vigente al momento de la operación

                int IdTasaAplicable = HistorialTasaInteresBusiness.DataAccessManager.GetByConditions<HistorialTasaInteresInversor>(t => t.IdInversor == dtoOperacion.IdInversor && t.TipoMonedaTasa == ctacte.TipoMoneda && t.FechaHasta == null).FirstOrDefault().IdHistorial;

                //Crear Operacion
                OperacionInversor opInversor = new OperacionInversor
                {

                    IdUsuario = usuario.IdUsuario,
                    Fecha = DateTime.Now,
                    Observacion = dtoOperacion.Observacion,
                    Estado = (int)Estados.eEstadosOp.Aprobado,
                    Monto = dtoOperacion.Monto,
                    IdCuentaCorrienteInversor = dtoOperacion.IdCuentaCorrienteInversor,
                    TipoMovimiento = (int)Estados.mTipoMovimientoCuentaCorriente.Debito,
                    IdHistorialTasaInteresInversorAplicado = IdTasaAplicable,
                    Condicion = (int)Condicion.Efectivo,
                    ClasificacionMovimiento = (int)ClasificacionMovimiento.Egreso
                };

                SaveOrUpdate(opInversor);

                dtoReporte.NombreUsuario = SessionContext.UsuarioActual.NombreUsuario;
                dtoReporte.Fecha = opInversor.Fecha;
                dtoReporte.NombreInversor = ctacte.Inversor.Nombre;
                dtoReporte.CuitInversor = ctacte.Inversor.CUIT;
                dtoReporte.Monto = opInversor.Monto;
                dtoReporte.CuentaCorrienteStr = ctacte.NumeroCtaCteStr;
                dtoReporte.NumeroOperacion = opInversor.IdOperacionInversor;
                dtoReporte.TipoMoneda = ((TipoMoneda)ctacte.TipoMoneda).ToString();

                return true;
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                return false;
            }
        }

        public void SaveOrUpdateOperacion(OperacionInversor operacion)
        {
            try
            {
                SaveOrUpdate(operacion);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public void MarcarOperacionLiquidada()
        {

            try
            {
                var resul = DataAccessManager.GetByConditions<OperacionInversor>(p => p.Fecha.Year == DateTime.Now.Year && p.Fecha.Month == DateTime.Now.Month
                && p.Fecha.Day == DateTime.Now.Day);

                foreach (var item in resul)
                {
                    item.Estado = 9;
                    DataAccessManager.SaveOrUpdate<OperacionInversor>(item);
                }

                DataAccessManager.SaveChanges();
               
            }
            catch (Exception)
            {

                throw;
            }


        }

        public bool BalanceCancelar(int IdOperacionInversor)
        {
            try
            {

                var operacion = OperacionInversoresBussines.Instance.GetByConditions<OperacionInversor>(o => o.IdOperacionInversor == IdOperacionInversor).FirstOrDefault();

                CuentaCorrienteInversor ctacte = CuentaCorrienteBusiness.DataAccessManager.GetByConditions<CuentaCorrienteInversor>(c => c.IdCuentaCorrienteInversor == operacion.IdCuentaCorrienteInversor).FirstOrDefault();

                int IdTasaAplicable = HistorialTasaInteresBusiness.DataAccessManager.GetByConditions<HistorialTasaInteresInversor>(t => t.IdInversor == operacion.CuentaCorrienteInversor.IdInversor && t.TipoMonedaTasa == ctacte.TipoMoneda && t.FechaHasta == null).FirstOrDefault().IdHistorial;

                OperacionInversor opInversor = new OperacionInversor

                {

                    IdUsuario = CurrentContext.SessionContext.IdUsuarioActual,
                    Fecha = DateTime.Now,
                    Observacion = "Débito por cancelación de la operación N°: " + operacion.IdOperacionInversor,
                    Estado = (int)Estados.eEstadosOp.Aprobado,
                    Monto = operacion.Monto,
                    IdCuentaCorrienteInversor = operacion.IdCuentaCorrienteInversor,
                    TipoMovimiento = (int)Estados.mTipoMovimientoCuentaCorriente.Debito,
                    IdHistorialTasaInteresInversorAplicado = IdTasaAplicable,
                    Condicion = (int)Condicion.Efectivo,
                    ClasificacionMovimiento = (int)ClasificacionMovimiento.Cancelacion                    
                };

                SaveOrUpdate(opInversor);

                return true;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //public void NuevoDepositoCheque(DtoOperacionInversor operacion, Configuracion configuracion)
        //{
        //    try
        //    {
        //        RecepcionCheque recepcionCheque = new RecepcionCheque
        //        {
        //            Fecha = DateTime.Now,
        //            Estado = 1,
        //            IdInversor = operacion.IdInversor
        //        };

        //        Cheque cheque = new Cheque
        //        {
        //            Activo = true,
        //            Numero = operacion.ChequeNumero,
        //            IdBanco = operacion.ChequeIdBanco,
        //            TitularCuenta = operacion.ChequeTitularCuenta,
        //            NroCuenta = operacion.ChequeNroCuenta,
        //            SucursalEmision = operacion.ChequeSucursalEmision,
        //            FechaVencimiento = operacion.ChequeFechaVencimiento,
        //            Monto = operacion.ChequeMonto,
        //            IdEstado = 6
        //        };


        //        cheque.RecepcionCheque = recepcionCheque;
        //        ChequeBusiness.Instance.SaveOrUpdate(cheque);

        //        DetalleOperacion detalle = new DetalleOperacion
        //        {
        //            Estado = 1,
        //            Observacion = operacion.Observacion,
        //            Gastos = CalcularImpustoPorGastos(cheque.Monto, configuracion),
        //            Impuestos = 0M,
        //            ImpuestosCheque = 0M,
        //            IdOperacionInversor = null
        //         };
        //    }

        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }


        //}


        public decimal GetSaldoActual(int idcuentacorrienteinversor)
        {

            decimal _saldo = 0;
            decimal _saldopositivo = 0;
            decimal _saldonegativo = 0;
            DateTime _fechaconsulta = DateTime.Now.AddDays(-1);
            try
            {
                //Obtento el Saldo Inicial a la Fecha de la Consulta
                List<ResumenDiarioOperacionInversor> resulRDOI = DataAccessManager.GetByConditions<ResumenDiarioOperacionInversor>(p => p.IdCuentaCorrienteInversor == idcuentacorrienteinversor && p.Fecha.Year == _fechaconsulta.Year && p.Fecha.Month == _fechaconsulta.Month && p.Fecha.Day == _fechaconsulta.Day).ToList();
               

                if (resulRDOI.Count > 0)
                    _saldo = (decimal)resulRDOI.Sum(p => p.Saldo);

                //Obtengo los movimientos negativos a la fecha
                List<OperacionInversor> resulOI = DataAccessManager.GetByConditions<OperacionInversor>(p=>p.CuentaCorrienteInversor.IdCuentaCorrienteInversor == idcuentacorrienteinversor 
                    && p.CuentaCorrienteInversor.Estado == true
                    && (p.Fecha.Year == DateTime.Now.Year && p.Fecha.Month == DateTime.Now.Month && p.Fecha.Day == DateTime.Now.Day || p.IdResumen == null)
                    && p.Condicion == (int)Condicion.Efectivo
                    ).ToList();


                //List<OperacionInversor> operaciones = (from op in context.OperacionInversor
                //                                       where op.IdCuentaCorrienteInversor == idCtaCteInversor
                //                                       && ((op.Fecha >= fechaLimiteInferior && op.Fecha < fechaLimiteSuperior) || op.IdResumen == null)
                //                                       && op.Estado != (int)eEstadosOp.Cancelada
                //                                       && op.Estado != (int)eEstadosOp.Rechazado
                //                                       && op.ClasificacionMovimiento != (int)ClasificacionMovimiento.Cancelacion
                //                                       select op).ToList();




                _saldopositivo = resulOI.Where(p=>p.TipoMovimiento == 2 && p.IdUsuario != null).Sum(p => p.Monto);
                _saldonegativo = resulOI.Where(p => p.TipoMovimiento == 1).Sum(p => p.Monto);
                _saldo = _saldo + _saldopositivo - _saldonegativo;
                return _saldo;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        
        public int GetTipoCuentaByIdCtaCte(int? idctacteinversor)
        {
            var cuenta = this.GetByConditions<CuentaCorrienteInversor>(c => c.IdCuentaCorrienteInversor == idctacteinversor).FirstOrDefault();

            if (cuenta.TipoMoneda == 0)
            {
                return 0;
            }
            if (cuenta.TipoMoneda == 1)
            {
                return 1;
            }
            return 1;
        }

    }
}
