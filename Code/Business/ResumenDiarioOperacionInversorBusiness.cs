﻿using Domine;
using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using static Utils.Enumeraciones.Estados;

namespace Business
{
    public class ResumenDiarioOperacionInversorBusiness : GenericBusiness<ResumenDiarioOperacionInversorBusiness, ResumenDiarioOperacionInversor>
    {
        public decimal GetSaldoByIdCuenta(int idCtaCteInversor, int tipoMoneda)
        {
            using (DCREntities context = new DCREntities())
            {
                //Defino acumuladores por clasificacion de movimiento
                decimal ingresos = 0;
                decimal egresos = 0;
                decimal intereses = 0;
                decimal impuestos = 0;
                decimal gastos = 0;
                decimal multas = 0;

                decimal saldoHoy = 0;
                DateTime fechaLimiteInferior = DateTime.Now.Date;
                DateTime fechaLimiteSuperior = DateTime.Now.Date.AddDays(1).AddMilliseconds(-1);

                ProcesoGeneracionResumen ultimoProcesoEjecutado = (from upe in context.ProcesoGeneracionResumen select upe).ToList().OrderByDescending(p => p.IdProceso).FirstOrDefault();

                if (ultimoProcesoEjecutado == null || (ultimoProcesoEjecutado != null && ultimoProcesoEjecutado.FechaCorrida < fechaLimiteInferior))
                {

                    //Recupero todas las operaciones con fechas anteriores a la de la corrida cuyos cheques vencen el día actual de corrida
                    List<DetalleOperacion> detallesAnterioresCheques = new List<DetalleOperacion>();
                    detallesAnterioresCheques = (from det in context.DetalleOperacion
                                                 join ch in context.Cheque on det.IdCheque equals ch.IdCheque
                                                 join op in context.OperacionInversor on det.IdOperacionInversor equals op.IdOperacionInversor
                                                 where (op.IdCuentaCorrienteInversor == idCtaCteInversor &&
                                                        op.Fecha < fechaLimiteInferior &&
                                                        op.ClasificacionMovimiento == (int)ClasificacionMovimiento.Ingreso && op.Condicion == (int)Condicion.Cheque &&
                                                        det.IdResumen == null &&
                                                        (op.Estado != (int)eEstadosOp.Cancelada && op.Estado != (int)eEstadosOp.Rechazado) &&
                                                        (det.Estado != (int)eEstadosOpDetalle.Cancelado && det.Estado != (int)eEstadosOpDetalle.Rechazado && det.Estado != (int)eEstadosOpDetalle.Pagada) &&
                                                        ch.FechaVencimiento == fechaLimiteInferior)
                                                 select det).ToList();

                    //Recorro cada detalle anterior para acreditar el monto
                    foreach (var detalle in detallesAnterioresCheques)
                    {
                        ingresos = ingresos + detalle.Cheque.Monto;
                    }


                    List<OperacionInversor> operaciones = (from op in context.OperacionInversor
                                                           where op.IdCuentaCorrienteInversor == idCtaCteInversor 
                                                           && ((op.Fecha >= fechaLimiteInferior && op.Fecha < fechaLimiteSuperior) || op.IdResumen == null) 
                                                           && op.Estado != (int)eEstadosOp.Cancelada 
                                                           && op.Estado != (int)eEstadosOp.Rechazado 
                                                           && op.ClasificacionMovimiento != (int)ClasificacionMovimiento.Cancelacion
                                                           select op).ToList();

                    foreach (var operacion in operaciones)
                    {
                        List<DetalleOperacion> detallesOp = new List<DetalleOperacion>();

                        //Recupero los detalles si los tiene, a partir de saber si la operacion es ingreso de cheque
                        if (operacion.ClasificacionMovimiento == (int)ClasificacionMovimiento.Ingreso && operacion.Condicion == (int)Condicion.Cheque)
                        {
                            detallesOp = (from det in context.DetalleOperacion
                                          join ch in context.Cheque on det.IdCheque equals ch.IdCheque
                                          where det.IdOperacionInversor == operacion.IdOperacionInversor && det.Estado != (int)eEstadosOpDetalle.Cancelado && det.Estado != (int)eEstadosOpDetalle.Rechazado && det.Estado != (int)eEstadosOpDetalle.Pagada
                                          select det).ToList();

                            foreach (var detalle in detallesOp)
                            {
                                if (detalle.Cheque.FechaVencimiento.Date <= fechaLimiteInferior && detalle.IdResumen == null)
                                {
                                    ingresos = ingresos + detalle.Cheque.Monto;
                                }
                            }
                        }
                        else
                        {
                            switch (operacion.ClasificacionMovimiento)
                            {
                                case (int)ClasificacionMovimiento.Ingreso:
                                    ingresos = ingresos + operacion.Monto;
                                    break;
                                case (int)ClasificacionMovimiento.Egreso:
                                    egresos = egresos + operacion.Monto;
                                    break;
                                case (int)ClasificacionMovimiento.Impuestos:
                                    impuestos = impuestos + operacion.Monto;
                                    break;
                                case (int)ClasificacionMovimiento.Gastos:
                                    gastos = gastos + operacion.Monto;
                                    break;
                                case (int)ClasificacionMovimiento.Multas:
                                    multas = multas + operacion.Monto;
                                    break;
                            }
                        }
                    }

                    saldoHoy = ingresos - egresos + intereses - impuestos - gastos - multas;
                }

                ResumenDiarioOperacionInversor resumen = ((from rd in context.ResumenDiarioOperacionInversor
                                                           where rd.IdCuentaCorrienteInversor == idCtaCteInversor && rd.TipoMoneda == tipoMoneda
                                                           select rd).OrderByDescending(r => r.IdResumen).FirstOrDefault());
                decimal saldoActual = 0;

                if (resumen != default(ResumenDiarioOperacionInversor))
                {
                    saldoActual = resumen.Saldo.Value + saldoHoy;
                }
                else
                {
                    saldoActual = saldoHoy;
                }

                return saldoActual;

            }

        }
    }
}
