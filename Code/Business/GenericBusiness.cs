﻿using DataAccess;
using DataAccess.Interface;
using Enumerations;
using Domine;
using Luma.Utils.Logs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Reflection;
using Luma.Utils.Logs;

namespace Business
{
    public abstract class GenericBusiness<TBusiness, TModel>
        where TBusiness : new()
        where TModel : class
    {
        #region Singleton

        private static readonly TBusiness instance = new TBusiness();
        public static TBusiness Instance { get { return instance; } }

        internal static IDataAccess DataAccessManager
        {
            get
            {
                return DataAccessManager<DCREntities>.Instance;
            }
        }

        public DCREntities GetContext()
        {
            return DataAccessManager<DCREntities>.Instance.GetContext() as DCREntities;
        }

        #endregion

        #region Common Methods

        public virtual void DeleteList(IEnumerable<TModel> list)
        {
            try
            {
                DataAccessManager.DeleteList<TModel>(list);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public virtual void Remove(TModel entity)
        {
            DataAccessManager.Remove(entity);
        }

        /// <summary>
        /// Obtiene todas la entidades
        /// </summary>
        /// <returns></returns>
        public virtual List<TModel> GetAll()
        {
            return DataAccessManager.GetAll<TModel>();
        }

        /// <summary>
        /// Obtiene una entidad específica
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TModel GetEntity(long id)
        {
            try
            {
                return DataAccessManager.GetById<TModel>(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene una entidad específica
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T GetEntity<T>(long id)
            where T : class
        {
            try
            {
                return DataAccessManager.GetById<T>(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene una entidad específica
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual List<TModel> GetByConditions(Expression<Func<TModel, bool>> where, params Expression<Func<TModel, object>>[] includes)
        {
            try
            {
                return DataAccessManager.GetByConditions(where, includes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene una entidad específica
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual List<T> GetByConditions<T>(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes)
            where T : class
        {
            try
            {
                return DataAccessManager.GetByConditions(where, includes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Obtiene una entidad específica
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual List<T> GetByConditions<T>(Expression<Func<T, bool>> where, List<string> includes)
            where T : class
        {
            try
            {
                return DataAccessManager.GetByConditions(where, includes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual bool Any<T>(Expression<Func<T, bool>> predicate)
           where T : class
        {
            try
            {
                return DataAccessManager.Any<T>(predicate);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina lógicamente una entidad por su ID
        /// </summary>
        /// <param name="id">ID de la entidad a eliminar lógicamente</param>
        public virtual TModel DeleteLogic(long id, Func<TModel, bool> fnDeleteValidation = null,
                                            Action<AuditAction, TModel> auditAction = null)
        {
            try
            {
                return DeleteLogic(GetEntity(id), fnDeleteValidation, auditAction);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Elimina lógicamente una entidad
        /// </summary>
        /// <param name="entity"></param>
        public virtual TModel DeleteLogic(TModel entity, Func<TModel, bool> fnDeleteValidation = null,
                                            Action<AuditAction, TModel> auditAction = null)
        {
            try
            {
                if (entity != null)
                {
                    if (fnDeleteValidation != null && !fnDeleteValidation(entity))
                    {
                        return entity;
                    }

                    Type type = typeof(TModel);

                    PropertyInfo propUpdateDate = type.GetProperty("FechaEdicion");
                    PropertyInfo propStatus = type.GetProperty("Estado");

                    if (propUpdateDate != null)
                    {
                        propUpdateDate.SetValue(entity, DateTime.Now, null);
                    }

                    if (propStatus != null)
                    {
                        propStatus.SetValue(entity, (int)EntityStatus.Deleted, null);
                    }

                    DataAccessManager.Update(entity);
                    DataAccessManager.SaveChanges();

                    if (auditAction != null)
                    {
                        auditAction(AuditAction.Delete, entity);
                    }

                    return entity;
                }
                else
                {
                    throw new Exception("Error al eliminar");
                }
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene todas la entidades activas
        /// </summary>
        /// <param name="propertyNombre">Es la propiedad que define si la entidad está o no activa</param>
        /// <returns></returns>
        public virtual List<TModel> GetAllActives(Func<TModel, TModel> columns = null, string propertyNombre = "Estado")
        {
            try
            {
                Type type = typeof(TModel);
                var item = Expression.Parameter(type, "item");
                var equal = Expression.Equal(Expression.Property(item, type.GetStatusPropertyOfMetaType() ?? propertyNombre), Expression.Constant(1));
                var lambda = Expression.Lambda<Func<TModel, bool>>(equal, item);

                return DataAccessManager.GetByConditions<TModel>(columns, lambda);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw;
            }
        }

        public virtual void SaveOrUpdate(TModel entity, Func<TModel, bool> fnValidate = null)
        {
            try
            {
                if (fnValidate != null && !fnValidate(entity))
                {
                    return;
                }

                var propertyID = entity.GetType().GetIDPropertyOfMetaType();

                if (propertyID == null)
                {
                    throw new NotImplementedException("MustImplementIDAttribute");
                }

                //TODO: Implementar para un futuro que pueda tomar diferentes typos el ID y que no solo sean Int32
                if (Convert.ToInt64(entity.GetType().InvokeMember(propertyID, BindingFlags.GetProperty, null, entity, null)) == default(Int64))
                {
                    DataAccessManager.Save<TModel>(entity);
                }
                else
                {
                    DataAccessManager.Update<TModel>(entity);
                }

                DataAccessManager.SaveChanges();
            }
            catch (ValidationException exv)
            {
                throw exv;
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public virtual void SaveOrUpdate(IEnumerable<TModel> entities, Func<TModel, bool> fnValidate = null)
        {
            try
            {
                string propertyID;

                foreach (var entity in entities)
                {
                    if (fnValidate != null && !fnValidate(entity))
                    {
                        return;
                    }

                    propertyID = entity.GetType().GetIDPropertyOfMetaType();

                    if (string.IsNullOrEmpty(propertyID))
                    {
                        throw new NotImplementedException("MustImplementIDAttribute");
                    }

                    //TODO: Implementar para un futuro que pueda tomar diferentes typos el ID y que no solo sean Int32
                    if (Convert.ToInt64(entity.GetType().InvokeMember(propertyID, BindingFlags.GetProperty, null, entity, null)) == default(Int64))
                    {
                        DataAccessManager.Save<TModel>(entity);
                    }
                    else
                    {
                        DataAccessManager.Update<TModel>(entity);
                    }
                }

                DataAccessManager.SaveChanges();
            }
            catch (ValidationException exv)
            {
                throw exv;
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        #endregion
    }
}