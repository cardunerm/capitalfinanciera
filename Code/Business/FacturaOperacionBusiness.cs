﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class FacturaOperacionBusiness : GenericBusiness<FacturaOperacionBusiness,FacturaOperacion>
    {

        #region methods

        public List<FacturaOperacion> GetByFilters(string numero,string cliente, string deudor,string estado,  int skip, int top, ref int rowCount)
        {

            int _cliente = 0;
            int _deudor = 0;
            int _estado = 0;

            try
            {
                var include = new List<string>();
                include.Add("FacturaCliente");
                include.Add("FacturaDeudo");
                include.Add("FacturaDesembolso");
                include.Add("FacturaCobro");

                if (cliente != "0" && cliente != "")
                    _cliente = int.Parse(cliente);

                if (deudor != "0" && deudor != "")
                    _deudor = int.Parse(deudor);

                if (estado != "0" && estado != "")
                    _estado = int.Parse(estado);


                IEnumerable<FacturaOperacion> query = DataAccessManager.GetByConditions<FacturaOperacion>(p =>p.FacturaActivo == true 
                                                && p.FacturaNumero.Contains(numero)  
                                                && (_cliente == 0 || p.IdFacturaCliente == _cliente)
                                                && (_deudor == 0 || p.IdFacturaDeudo == _deudor)
                                                && (_estado == 0 || p.FacturaIdEstado == _estado)
                                                , include
                                               );

                query = query.OrderByDescending(p => p.FacturaFecha);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        


        #endregion 
    }
}
