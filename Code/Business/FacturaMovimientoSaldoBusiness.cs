﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class FacturaMovimientoSaldoBusiness : GenericBusiness<FacturaMovimientoSaldoBusiness,FacturaMovimientoSaldo>
    {

        public void CalcularSaloActualizado(int idFacturaOperacion, int idTipoMovimiento,int? idFacturaCobro, int? IdFacturaesembolso, int? IdFacturaTasa, DateTime fecha,decimal monto)
        {
            decimal _saldoinicial = 0;
            DateTime _fechainicial = DateTime.Now;
            int _diferenciadias = 0;
            decimal _saldonuevo = 0;
            decimal _tasaanterior = 0;
            decimal _interesnuevo = 0;
            double _potencia = 0;
            decimal _ivanuevo = 0;
            decimal _gastootorgamientonuevo = 0;
            decimal _impuestodebcrednuevo = 0;
            decimal _ivagastonuevo = 0;
            try
            {
                //Obtengo el ultimo movimiento de saldo de la operacion
                var _facturasaldoactualizado = this.GetByConditions(p => p.IdFacturaOperacion == idFacturaOperacion && p.Activo == true).OrderBy(p => p.IdFacturaMovimientoSaldo);
                //Sino hay movimiento busco la operacion
                if (_facturasaldoactualizado.Count() > 0)
                {
                    FacturaMovimientoSaldo _facturaMovimientoSaldo = _facturasaldoactualizado.Last();

                    _fechainicial = (DateTime)_facturaMovimientoSaldo.Fecha;
                    _saldoinicial = (decimal)_facturaMovimientoSaldo.SaldoActualizado;
                    _tasaanterior = (decimal)_facturaMovimientoSaldo.TasaActualizada;
                }
                else
                {
                    var facturaoperacion = FacturaOperacionBusiness.Instance.GetEntity(long.Parse(idFacturaOperacion.ToString()));
                    _fechainicial = (DateTime)facturaoperacion.FacturaFecha;
                    _saldoinicial = monto;// (decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p=>p.IdFacturaOperacion == facturaoperacion.IdFacturaOperacion && p.IdFacturaParametro == 15)[0].Valor;
                    _tasaanterior =  (decimal)FacturaParametroOperacionBusiness.Instance.GetByConditions(p => p.IdFacturaOperacion == facturaoperacion.IdFacturaOperacion && p.IdFacturaParametro == 29)[0].Valor;
                }


                //if (IdFacturaTasa != null)
                //    _tasaanterior = (decimal)FacturaTasaBusiness.Instance.GetEntity(long.Parse(IdFacturaTasa.ToString())).Tasa;


                if (_facturasaldoactualizado.Count() > 0)
                {
                    _diferenciadias = fecha.Subtract(_fechainicial).Days;
                    _potencia = Math.Pow(1 + ((double)_tasaanterior / 100), _diferenciadias);
                    _interesnuevo = _saldoinicial * (decimal)(_potencia - 1);
                    _ivanuevo = _interesnuevo * (decimal)(0.21);

                    if (idTipoMovimiento == 1)
                    {                      

                        if (_saldoinicial > 0)
                        {
                            _interesnuevo = 0;
                            _ivanuevo = 0;
                        }

                        _saldoinicial += monto;
                    }

                    if (idTipoMovimiento == 2)
                    {                      
                            //Gasto Otorgamiento
                            var gastootorgamiento = this.GetByConditions<FacturaParametroOperacion>(p => p.IdFacturaOperacion == idFacturaOperacion && p.IdFacturaParametro == 9);
                            var _gastootorgamiento = (decimal)gastootorgamiento.Last().Valor / 100;
                            _gastootorgamientonuevo = (-1) * monto * _gastootorgamiento;


                            //Impuesto a los deb y cred
                            var gastoimpcheque = this.GetByConditions<FacturaParametroOperacion>(p => p.IdFacturaOperacion == idFacturaOperacion && p.IdFacturaParametro == 8);
                            var _gastoimpcheque = (decimal)gastoimpcheque.Last().Valor / 100;
                            _impuestodebcrednuevo = (-1) * monto * _gastoimpcheque;

                            //Iva Gasto
                            _ivagastonuevo = (_gastootorgamientonuevo + _impuestodebcrednuevo) * (decimal)0.21;

                        if (_saldoinicial > 0)
                        {
                            _interesnuevo = 0;
                            _ivanuevo = 0;
                        }

                        _saldoinicial += monto;




                    }

                    if (idTipoMovimiento == 3)
                        _tasaanterior = monto;

                }
                else
                {
                    _interesnuevo = 0;
                }

              

                FacturaMovimientoSaldo _facturaMovimientoSaldoEnt = new FacturaMovimientoSaldo();
                _facturaMovimientoSaldoEnt.IdFacturaOperacion = idFacturaOperacion;
                _facturaMovimientoSaldoEnt.IdTipoMovimiento = idTipoMovimiento;
                _facturaMovimientoSaldoEnt.IdFacturaCobro = idFacturaCobro;
                _facturaMovimientoSaldoEnt.IdFacturaDesembolso = IdFacturaesembolso;
                _facturaMovimientoSaldoEnt.IdFacturaTasa = IdFacturaTasa;
                _facturaMovimientoSaldoEnt.Fecha = fecha;
                _facturaMovimientoSaldoEnt.Diferenciadia = _diferenciadias;
                _facturaMovimientoSaldoEnt.Interes = _interesnuevo;
                _facturaMovimientoSaldoEnt.SaldoActualizado = _interesnuevo + _ivanuevo + _gastootorgamientonuevo + _impuestodebcrednuevo + _ivagastonuevo + _saldoinicial;
                _facturaMovimientoSaldoEnt.TasaActualizada = _tasaanterior;
                _facturaMovimientoSaldoEnt.Activo = true;

                _facturaMovimientoSaldoEnt.Iva = _ivanuevo;
                _facturaMovimientoSaldoEnt.GastoOtorgamiento = _gastootorgamientonuevo;
                _facturaMovimientoSaldoEnt.ImpuestoDebCred = _impuestodebcrednuevo;
                _facturaMovimientoSaldoEnt.IvaGasto = _ivagastonuevo;

                this.SaveOrUpdate(_facturaMovimientoSaldoEnt);

            }
            catch (Exception ex)
            {
                throw ex;
            }





        }

        public void EliminarSaldoMovimiento(string id)
        {
            int _id = int.Parse(id);
            try
            {
                FacturaMovimientoSaldo _facturaMovimientoSaldo = this.GetByConditions(p => p.IdFacturaDesembolso == _id || p.IdFacturaCobro == _id || p.IdFacturaTasa == _id)[0];
                _facturaMovimientoSaldo.Activo = false;
                this.SaveOrUpdate(_facturaMovimientoSaldo);
            }
            catch (Exception ex) 
            {

                throw ex;
            }



        }



    }
}
