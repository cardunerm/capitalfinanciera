﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Luma.Utils.Logs;

namespace Business
{
    public class SucursalBusiness : GenericBusiness<SucursalBusiness, Sucursal>
    {
        #region Methods

        public List<View_Sucursal> GetByFilters(string numero, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<View_Sucursal> query = DataAccessManager.GetByConditions<View_Sucursal>(p => p.Estado == (int)EntityStatus.Active &&
                                                p.Numero.Contains(numero));

                query = query.OrderBy(p => p.Numero);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Sucursal> GetSucursalesList()
        {
            try
            {
                IEnumerable<Sucursal> query = DataAccessManager.GetAll<Sucursal>();

                query = query.OrderBy(p => p.Numero);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SucursalUsuario> GetSucursaleUsuarioList(long idUsuario)
        {
            try
            {
                IEnumerable<SucursalUsuario> query = DataAccessManager.GetByConditions<SucursalUsuario>(su => su.IdUsuario == idUsuario);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveOrUpdateSucursal(Sucursal sucursal)
        {
            Sucursal entity;

            if (sucursal.IdSucursal > 0)
            {
                entity = GetEntity(sucursal.IdSucursal);
                entity.IdEmpresa = sucursal.IdEmpresa;
                entity.Numero = sucursal.Numero;
                entity.Descripcion = sucursal.Descripcion;
            }
            else
            {
                entity = new Sucursal
                {
                    IdEmpresa = DataAccessManager.GetAll<Empresa>().OrderBy(e => e.Id).FirstOrDefault().Id,
                    Numero = sucursal.Numero,
                    Descripcion = sucursal.Descripcion,
                };
            }

            entity.Estado = 1;

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaSucursal(sucursal.IdSucursal > 0 ? AuditAction.Edit : AuditAction.Create, entity);
        }

        private bool ValidateSaveOrUpdate(Sucursal sucursal)
        {
            if (!SucursalNumeroExist(sucursal))
            {
                return true;
            }
            else
            {
                throw new ValidationException("Ya existe una sucursal con ese numero");
            }
        }

        private bool ValidateSaveOrUpdateSucursalUsuario(SucursalUsuario sucursalUsuario)
        {
            return true;
        }

        private bool SucursalNumeroExist(Sucursal entity)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Sucursal>(p =>
                                p.Numero.Equals(entity.Numero) &&
                                p.IdEmpresa != entity.IdEmpresa);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(Sucursal entity)
        {
            return true;
            //if (!BancoHasChequesOrCBUCliente(entity))
            //{
            //    return true;
            //}
            //else
            //{
            //    throw new ValidationException(string.Format("No se puede eliminar el registro.", "El banco tiene cheques asociados y/o CBU asociados"));
            //}
        }

        public Sucursal SucursalExist(string numero)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Sucursal>(p =>
                                p.Numero.Equals(numero));

                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateSucursalUsuario(Usuario user, List<Sucursal> sucursales)
        {
            using (var ctx = DataAccessManager.GetContext() as DCREntities)
            {
                using (var tx = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        List<SucursalUsuario> listSucursalesUsuario = this.GetSucursaleUsuarioList(user.IdUsuario);

                        DataAccessManager.DeleteList<SucursalUsuario>(listSucursalesUsuario);

                        SucursalUsuario entity;

                        foreach (var suc in sucursales)
                        {
                            entity = new SucursalUsuario
                            {
                                IdSucursal = suc.IdSucursal,
                                IdUsuario = user.IdUsuario,
                            };

                            ctx.SucursalUsuario.Add(entity);
                        }

                        ctx.SaveChanges();
                        tx.Commit();

                    }
                    catch (Exception ex)
                    {
                        LogManager.Log.RegisterError(ex);
                        throw ex;
                    }
                }
            }
        }

        public List<Sucursal> GetSucursalByUsuarioEmpresas(Usuario usuario)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Sucursal>(p => p.IdEmpresa == usuario.IdEmpresa && p.Estado == 1);

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Sucursal> GetSucursalByNombreUsuario(string nombre)
        {
            try
            {
                Usuario user = UsuarioBusiness.Instance.GetByConditions(u => u.NombreUsuario == nombre).FirstOrDefault();

                //var query = DataAccessManager.GetByConditions<Sucursal>(p => p.SucursalUsuario.);

                var query = SucursalUsuarioBusiness.Instance.GetByConditions(us => us.IdUsuario == user.IdUsuario)
                                        .Select(p => p.Sucursal).ToList();
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}

        #endregion