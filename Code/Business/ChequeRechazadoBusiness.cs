﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DataAccess.Pages;
using Luma.Utils.Logs;
using CurrentContext;
using Utils.Enumeraciones;
namespace Business
{
    public class ChequeRechazadoBusiness : GenericBusiness<ChequeRechazadoBusiness, ChequeRechazado>
    {
        #region Methods

        public PageResult<ViewChequeRechazado> GetByFilters(string numero, int banco, int skip, int top, ref int rowCount)
        {
            try
            {
                return DataAccessManager.GetByConditions<ViewChequeRechazado, DateTime>(
                    cr => (numero == "" || cr.Numero.Contains(numero))
                         &&
                         (banco == 0 || cr.IdBanco == banco),
                         skip, top, cr => cr.Fecha, true);
            }
            catch (Exception ex)
            {
                LogManager.Log.RegisterError(ex);
                throw ex;
            }
        }

        public void SaveOrUpdateCheque(ChequeRechazado ChequeRechazado)
        {
            ChequeRechazado entity;

            if (ChequeRechazado.IdCheque > 0)
            {
                entity = GetEntity(ChequeRechazado.IdCheque);
                entity.Estado = ChequeRechazado.Estado;
                entity.Motivo = ChequeRechazado.Motivo;
                entity.Fecha = ChequeRechazado.Fecha;
                entity.Gasto = ChequeRechazado.Gasto;
                entity.IdCheque = ChequeRechazado.IdCheque;
            }
            else
            {
                entity = new ChequeRechazado
                {
                    Estado = (int)EntityStatus.Active,
                    Motivo = ChequeRechazado.Motivo,
                    Fecha = ChequeRechazado.Fecha,
                    Gasto = ChequeRechazado.Gasto,
                    IdCheque = ChequeRechazado.IdCheque
                };
            }

            SaveOrUpdate(entity, ValidateSaveOrUpdate);

            AuditoriaBusiness.Instance.AuditoriaChequeRechazado(ChequeRechazado.IdCheque > 0 ? AuditAction.Edit : AuditAction.Create, entity);
        }

        private bool ValidateSaveOrUpdate(ChequeRechazado entity)
        {
            if (!ChequeNumeroExist(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException("Ya se encuentra registrado en el sistema el Cheque");
            }
        }

        private bool ChequeNumeroExist(ChequeRechazado entity)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<ChequeRechazado>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Cheque.Numero.Equals(entity.Cheque.Numero) &&
                                p.Cheque.IdBanco.Equals(entity.Cheque.IdBanco) &&
                                p.IdCheque != entity.IdCheque);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(ChequeRechazado entity)
        {
            return true;
        }

        public int CreateChequeRechazado(List<Cheque> listCheques, ChequeRechazado chequeRechazadoModel)
        {
            using (var ctx = DataAccessManager.GetContext() as DCREntities)
            {
                using (var tx = ctx.Database.BeginTransaction())
                {
                    try
                    {
                        //Cambio de estado del cheque
                        Cheque chequeBD = ChequeBusiness.Instance.GetCheque(listCheques.FirstOrDefault().IdCheque);

                        chequeBD.IdEstado = (int)Estados.eEstadosCheque.Rechazado;

                        DetalleOperacion detop = OperacionBusiness.Instance.GetByConditions<DetalleOperacion>(p => p.IdCheque == chequeBD.IdCheque).FirstOrDefault();

                        if (detop != null)
                            detop.Estado = (short)Utils.Enumeraciones.Estados.eEstadosOpDetalle.Rechazado;

                        ChequeBusiness.Instance.SaveOrUpdate(chequeBD);

                        //Creacion Nota de debito al cliente
                        RecepcionCheque rc = RecepcionChequeBusiness.Instance.GetEntity(chequeBD.IdRecepcionCheque);
                        CuentaCorriente cc = CuentaCorrienteBusiness.Instance.GetByConditions(c => c.IdCliente == rc.IdCliente).FirstOrDefault();


                        OperacionCuentaCorriente OpCc = new OperacionCuentaCorriente
                        {
                            Monto = chequeBD.Monto,
                            Fecha = DateTime.Now,
                            Descripcion = "El cheque N°: " + chequeBD.Numero.Trim() + " ha sido rechazado",
                            Estado = 1,
                            IdCuentaCorriente = cc.IdCuentaCorriente,
                            TipoMovimiento = 1
                        };
                        ctx.OperacionCuentaCorriente.Add(OpCc);

                        //Gasto por el cheque rechazado
                        OperacionCuentaCorriente OpComisionCc = new OperacionCuentaCorriente
                        {
                            Monto = decimal.Parse(chequeRechazadoModel.Gasto.ToString()),
                            Fecha = DateTime.Now,
                            Descripcion = "Gastos generados por el rechazo del cheque N°: " + chequeBD.Numero.Trim() + "",
                            Estado = 1,
                            IdCuentaCorriente = cc.IdCuentaCorriente,
                            TipoMovimiento = 1
                        };

                        
                        ctx.OperacionCuentaCorriente.Add(OpComisionCc);

                        //Creacion Cheque Rechazado
                        ChequeRechazado chequeRechazado = new ChequeRechazado
                        {
                            Motivo = chequeRechazadoModel.Motivo,
                            Gasto = chequeRechazadoModel.Gasto,
                            Fecha = DateTime.Now,
                            Estado = (int)Estados.eEstadosCheque.Rechazado,
                            IdCheque = listCheques.FirstOrDefault().IdCheque,
                        };

                        //SaveOrUpdate(chequeRechazado, ValidateSaveOrUpdate);

                        ctx.ChequeRechazado.Add(chequeRechazado);
                        ctx.SaveChanges();
                        tx.Commit();
                        return chequeRechazado.IdCheque;
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        LogManager.Log.RegisterError(ex);
                        throw ex;
                    }
                }
            }
        }

        #endregion
    }
}
