﻿using Enumerations;
using Domine;
using Luma.Utils;
using Luma.Utils.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CurrentContext;

namespace Business
{
    public class UsuarioBusiness : GenericBusiness<UsuarioBusiness, Usuario>, IMembershipValidator
    {
        #region Methods

        public Usuario GetUsuarioByEmail(string email)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Usuario>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Email.Equals(email));

                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UsuarioPerfil GetUsuarioById(long IdUsuario)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<UsuarioPerfil>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.IdUsuario == IdUsuario);

                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Usuario GetUsuario(string nombreUsuario, string contrasenia, long empresa)
        {
            try
            {
                contrasenia = UtilHelper.GetMD5String(contrasenia);

                DataAccessManager.SetLazy(false);
                //DataAccessManager.UseProxy(false);               
                //return DataAccessManager.GetById<Usuario>(1);
                //var ret = DataAccessManager.GetByConditions<Usuario>(
                //                u =>
                //                new Usuario
                //                {
                //                    Nombre = u.Nombre,
                //                    IdUsuario = u.IdUsuario,
                //                    UsuarioPerfil = u.UsuarioPerfil
                //                                    .Where(p => p.Estado == (int)EstadoEntidad.Active)
                //                                    .ToList()
                //                },
                //                p =>
                //                    p.Estado == (int)EstadoEntidad.Active &&
                //                    p.NombreUsuario.Equals(nombreUsuario) &&
                //                    p.Contrasenia.Equals(contrasenia) &&
                //                    p.UsuarioEmpresa.Any(q =>
                //                        q.Estado == (int)EstadoEntidad.Active &&
                //                        q.Empresa.IdEmpresa == empresa &&
                //                        p.UsuarioSucursalOperacion.Any(r =>
                //                            r.SucursalOperacion.IdEmpresa == q.IdEmpresa &&
                //                            r.SucursalOperacion.Estado == (int)EstadoEntidad.Active &&
                //                            r.Estado == (int)EstadoEntidad.Active)),
                //                p => p.UsuarioPerfil,
                //                p => p.UsuarioPerfil.Select(y => y.Perfil),
                //                p => p.UsuarioPerfil.Select(y => y.Perfil.PerfilModulo),
                //                p => p.UsuarioPerfil.Select(y => y.Perfil.PerfilModulo.Select(z => z.Modulo)),
                //                p => p.UsuarioPerfil.Select(y => y.Perfil.PerfilModulo.Select(z => z.Modulo.Pagina))).FirstOrDefault();

                var user = DataAccessManager.GetByConditions<Usuario>(
                    //u =>
                    //new Usuario
                    //{
                    //    Nombre = u.Nombre,
                    //    IdUsuario = u.IdUsuario,
                    //    UsuarioPerfil = u.UsuarioPerfil
                    //                    .Where(p => p.Estado == (int)EstadoEntidad.Active)
                    //                    .ToList()
                    //},
                                p =>
                                    p.Estado == (int)EntityStatus.Active &&
                                    p.NombreUsuario.Equals(nombreUsuario) &&
                                    p.Contrasenia.Equals(contrasenia),p=>p.Empresa).SingleOrDefault();

                //ret.Paginas = PaginaBusiness.Instance.GetPaginasByUsuario(ret);
                return (user != null && user.NombreUsuario.Equals(nombreUsuario) && user.Contrasenia.Equals(contrasenia) ? user : null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Usuario GetUsuario(string nombreUsuario, long empresa)
        {
            try
            {
                DataAccessManager.SetLazy(false);
                //DataAccessManager.UseProxy(false);               
                //return DataAccessManager.GetById<Usuario>(1);
                //var ret = DataAccessManager.GetByConditions<Usuario>(
                //                u =>
                //                new Usuario
                //                {
                //                    Nombre = u.Nombre,
                //                    IdUsuario = u.IdUsuario,
                //                    UsuarioPerfil = u.UsuarioPerfil
                //                                    .Where(p => p.Estado == (int)EstadoEntidad.Active)
                //                                    .ToList()
                //                },
                //                p =>
                //                    p.Estado == (int)EstadoEntidad.Active &&
                //                    p.NombreUsuario.Equals(nombreUsuario) &&
                //                    p.Contrasenia.Equals(contrasenia) &&
                //                    p.UsuarioEmpresa.Any(q =>
                //                        q.Estado == (int)EstadoEntidad.Active &&
                //                        q.Empresa.IdEmpresa == empresa &&
                //                        p.UsuarioSucursalOperacion.Any(r =>
                //                            r.SucursalOperacion.IdEmpresa == q.IdEmpresa &&
                //                            r.SucursalOperacion.Estado == (int)EstadoEntidad.Active &&
                //                            r.Estado == (int)EstadoEntidad.Active)),
                //                p => p.UsuarioPerfil,
                //                p => p.UsuarioPerfil.Select(y => y.Perfil),
                //                p => p.UsuarioPerfil.Select(y => y.Perfil.PerfilModulo),
                //                p => p.UsuarioPerfil.Select(y => y.Perfil.PerfilModulo.Select(z => z.Modulo)),
                //                p => p.UsuarioPerfil.Select(y => y.Perfil.PerfilModulo.Select(z => z.Modulo.Pagina))).FirstOrDefault();

                var user = DataAccessManager.GetByConditions<Usuario>(
                    //u =>
                    //new Usuario
                    //{
                    //    Nombre = u.Nombre,
                    //    IdUsuario = u.IdUsuario,
                    //    UsuarioPerfil = u.UsuarioPerfil
                    //                    .Where(p => p.Estado == (int)EstadoEntidad.Active)
                    //                    .ToList()
                    //},
                                p =>
                                    p.Estado == (int)EntityStatus.Active &&
                                    p.NombreUsuario.Equals(nombreUsuario)).SingleOrDefault();

                //ret.Paginas = PaginaBusiness.Instance.GetPaginasByUsuario(ret);
                return (user != null && user.NombreUsuario.Equals(nombreUsuario) ? user : null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Usuario> GetByFilters(string nombre, string apellido, string nombreUsuario, long idPerfil, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<Usuario> query = DataAccessManager.GetByConditions<Usuario>(p =>
                                                p.Estado == (int)EntityStatus.Active &&
                                                p.Nombre.ToLower().Contains(nombre) &&
                                                p.Apellido.ToLower().Contains(apellido) &&
                                                p.NombreUsuario.ToLower().Contains(nombreUsuario) &&
                                                (idPerfil > 0 ? p.UsuarioPerfil.Any(up => up.IdPerfil == idPerfil &&
                                                                                          up.Estado == (int)EntityStatus.Active) : true));

                query = query.OrderBy(p => p.Apellido);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Usuario> GetAllByEmpresaActual()
        {
            try
            {
                IEnumerable<Usuario> query = DataAccessManager.GetByConditions<Usuario>(p =>
                                                p.Estado == (int)EntityStatus.Active );

                query = query.OrderBy(p => p.Apellido);

                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public List<Usuario> GetVendedores()
        //{
        //    try
        //    {
        //        IEnumerable<Usuario> query = DataAccessManager.GetByConditions<Usuario>(p =>
        //                                        p.Estado == (int)EntityStatus.Active &&
        //                                        p.UsuarioVendedorEmpresa.Any(q => q.IdEmpresa == SessionContext.EmpresaActual.IdEmpresa) &&
        //                                        p.UsuarioEmpresa.Any(q => q.IdEmpresa == SessionContext.EmpresaActual.IdEmpresa &&
        //                                                                  q.Estado == (int)EntityStatus.Active) &&
        //                                        p.UsuarioSucursalOperacion.Any(q => q.SucursalOperacion.IdEmpresa == SessionContext.EmpresaActual.IdEmpresa &&
        //                                                                            q.SucursalOperacion.Estado == (int)EntityStatus.Active &&
        //                                                                            q.Estado == (int)EntityStatus.Active),
        //                                                    p => p.UsuarioEmpresa,
        //                                                    p => p.UsuarioSucursalOperacion.Select(q => q.SucursalOperacion),
        //                                                    p => p.UsuarioVendedorEmpresa);

        //        query = query.OrderBy(p => p.Apellido);

        //        return query.ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public List<Usuario> GetVendedoresByEmpresa(long idEmpresa)
        //{
        //    try
        //    {
        //        IEnumerable<Usuario> query = DataAccessManager.GetByConditions<Usuario>(p =>
        //                                        p.Estado == (int)EntityStatus.Active &&
        //                                        p.UsuarioVendedorEmpresa.Any(q => q.IdEmpresa == SessionContext.EmpresaActual.IdEmpresa) &&
        //                                        p.UsuarioEmpresa.Any(q => q.IdEmpresa == idEmpresa &&
        //                                                                  q.Estado == (int)EntityStatus.Active) &&
        //                                        p.UsuarioSucursalOperacion.Any(q => q.SucursalOperacion.IdEmpresa == idEmpresa &&
        //                                                                            q.SucursalOperacion.Estado == (int)EntityStatus.Active &&
        //                                                                            q.Estado == (int)EntityStatus.Active),
        //                                                    p => p.UsuarioEmpresa,
        //                                                    p => p.UsuarioSucursalOperacion.Select(q => q.SucursalOperacion),
        //                                                    p => p.UsuarioVendedorEmpresa);

        //        query = query.OrderBy(p => p.Apellido);

        //        return query.ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public List<Usuario> GetUsuariosByValidacion(Validacion validacion)
        //{
        //    try
        //    {
        //        IEnumerable<Usuario> query = DataAccessManager.GetByConditionsNoSQL<Usuario>(p =>
        //                                        p.Estado == (int)EntityStatus.Active);

        //        return query.ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void SaveOrUpdateUsuario(Usuario usuario, bool changeContrasenia)
        {
            if (changeContrasenia)
            {
                usuario.Contrasenia = UtilHelper.GetMD5String(usuario.Contrasenia);
            }
            if (usuario.IdUsuario == 0 && usuario.IdEmpresa == 0)
            {
                usuario.IdEmpresa = DataAccessManager.GetAll<Empresa>().OrderBy(e => e.Id).FirstOrDefault().Id;
            }

            SaveOrUpdate(usuario, ValidateSaveOrUpdate);
        }

        public void DeletePerfilesUsuarios(long idUsuario)
        {
            try
            {
                IEnumerable<UsuarioPerfil> query = DataAccessManager.GetByConditions<UsuarioPerfil>(p =>
                                                p.IdUsuario == idUsuario);

                DataAccessManager.DeleteList<UsuarioPerfil>(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeContrasenia(string newContrasenia)
        {
            Usuario entity = GetEntity(SessionContext.UsuarioActual.IdUsuario);

            entity.Contrasenia = UtilHelper.GetMD5String(newContrasenia);

            SaveOrUpdate(entity, null);

            AuditoriaBusiness.Instance.AuditoriaUsuario(AuditAction.Edit, entity);
        }

        public void RestablecerContrasenia(string email)
        {
            Usuario entity = GetUsuarioByEmail(email);

            if (entity != null)
            {
                string newContrasenia = Guid.NewGuid().ToString();

                entity.Contrasenia = UtilHelper.GetMD5String(newContrasenia);

                SaveOrUpdate(entity, null);

                AuditoriaBusiness.Instance.AuditoriaUsuario(AuditAction.Edit, entity);

                SendEmailBusiness.Instance.NotifyPasswordChanged(entity, newContrasenia);
            }
            else
            {
                throw new Exception("No se encontro email del usuario");
            }
        }

        private bool ValidateSaveOrUpdate(Usuario usuario)
        {
            if (!NombreUsuarioExist(usuario))
            {
                if (!EmailExist(usuario))
                {
                    return true;
                }
                else
                {
                    throw new ValidationException("Email ya existe");
                }
            }
            else
            {
                throw new ValidationException("El usuario no existe");
            }
        }

        private bool NombreUsuarioExist(Usuario usuario)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Usuario>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.NombreUsuario.Equals(usuario.NombreUsuario) &&
                                p.IdUsuario != usuario.IdUsuario);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool EmailExist(Usuario usuario)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Usuario>(p =>
                                p.Estado == (int)EntityStatus.Active &&
                                p.Email.Equals(usuario.Email) &&
                                p.IdUsuario != usuario.IdUsuario);

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public dynamic IsValid(string nombre, string contrasenia, params object[] metadata)
        {
            long empresa = 0;

            //if (metadata.Length == 0 || !long.TryParse(metadata[0].ToString(), out empresa))
            //    return null;

            return GetUsuario(nombre, contrasenia, empresa);
        }

        public dynamic IsValidDirect(string nombre, params object[] metadata)
        {
            long empresa = 0;

            if (metadata.Length == 0 || !long.TryParse(metadata[0].ToString(), out empresa))
                return null;

            return GetUsuario(nombre, empresa);
        }

        public bool HasPermission(System.Web.HttpContextBase context, CustomMembershipUser usuario, params object[] metadata)
        {
            return HasPermission(metadata);
        }

        //public bool HasPermission(Validacion validacion)
        //{
        //    var usuarioPerfiles = DataAccessManager.GetByConditions<UsuarioPerfil>(p =>
        //                            p.IdUsuario == SessionContext.UsuarioActual.IdUsuario &&
        //                            p.Estado == (int)EntityStatus.Active);

        //    var query = from up in usuarioPerfiles
        //                from pv in up.Perfil.PerfilValidacion
        //                where pv.Estado == (int)EntityStatus.Active &&
        //                        up.Perfil.Estado == (int)EntityStatus.Active &&
        //                        pv.Validacion == (int)validacion
        //                select pv;

        //    return query.Any();
        //}

        public bool HasPermission(params object[] metadata)
        {
            if (metadata.Length > 0)
            {
                var usuarioPerfiles = DataAccessManager.GetByConditions<UsuarioPerfil>(p =>
                                       p.IdUsuario == SessionContext.UsuarioActual.IdUsuario &&
                                       p.Estado == (int)EntityStatus.Active);

                var query = from up in usuarioPerfiles
                            from pm in up.Perfil.PerfilModulo
                            where pm.Estado == (int)EntityStatus.Active &&
                                  pm.Permiso == (int)metadata[2] &&
                                  pm.Perfil.Estado == (int)EntityStatus.Active &&
                                  pm.Modulo.Estado == (int)EntityStatus.Active &&
                                  pm.Modulo.Pagina.Any(p => p.Url.Equals(metadata[1]) && p.Estado == (int)EntityStatus.Active)
                            select pm;

                return query.Any();
            }

            return false;
        }

        public bool HasPermiso(string url)
        {
            var reqPermiso = DataAccessManager.GetByConditions<PaginaPermiso>(p =>
                                    p.Pagina.Url.Equals(url))
                                    .Select(p => p.PermisoRequerido);

            var usuarioPerfiles = DataAccessManager.GetByConditions<UsuarioPerfil>(p =>
                                    p.IdUsuario == SessionContext.UsuarioActual.IdUsuario &&
                                    p.Estado == (int)EntityStatus.Active);

            var query = from up in usuarioPerfiles
                        from pm in up.Perfil.PerfilModulo
                        where pm.Estado == (int)EntityStatus.Active &&
                                reqPermiso.Contains(pm.Permiso) &&
                                pm.Perfil.Estado == (int)EntityStatus.Active &&
                                pm.Modulo.Estado == (int)EntityStatus.Active &&
                                pm.Estado == (int)EntityStatus.Active &&
                                pm.Modulo.Pagina.Any(p => p.Url.Equals(url) && p.Estado == (int)EntityStatus.Active)
                        select pm;

            return query.Any();
        }

        //public bool UsuarioRealizaValidacion(long idUsuario, Validacion validacion)
        //{
        //    try
        //    {
        //        var perfilesUsuario = UsuarioPerfilBusiness.Instance.GetUsuarioPerfiles(idUsuario);

        //        return perfilesUsuario.Any(p => p.Perfil.PerfilValidacion.Any(q =>
        //                        q.Estado == (int)EntityStatus.Active &&
        //                        q.Validacion == (int)validacion));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public bool UsuarioActualRealizaValidacion(Validacion validacion)
        //{
        //    try
        //    {
        //        return UsuarioRealizaValidacion(SessionContext.UsuarioActual.IdUsuario, validacion);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        #endregion
    }
}