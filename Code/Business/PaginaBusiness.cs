﻿using System;
using System.Collections.Generic;
using System.Linq;
using Enumerations;
using Domine;

namespace Business
{
    public class PaginaBusiness : GenericBusiness<PaginaBusiness, Pagina>
    {
        #region Methods

        public bool IsPagina(string url)
        {
            try
            {
                var query = DataAccessManager.GetByConditions<Pagina>(p =>
                                    p.Url.Equals(url));

                return query.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Pagina> GetPaginasByUsuario(Usuario usuarioLog)
        {
            try
            {
                Pagina pagPadre = new Pagina();

                var queryUsuario = DataAccessManager.GetQueryable<Usuario>();

                var query = (from usuario in queryUsuario
                             from up in usuario.UsuarioPerfil
                             from pm in up.Perfil.PerfilModulo
                             from p in pm.Modulo.Pagina
                             from pp in p.PaginaPermiso
                             where usuario.IdUsuario == usuarioLog.IdUsuario &&
                                   pm.Estado == (int)EntityStatus.Active &&
                                   pm.Permiso == (int)Permission.View &&
                                   up.Perfil.Estado == (int)EntityStatus.Active &&
                                   up.Estado == (int)EntityStatus.Active &&
                                   pm.Modulo.Estado == (int)EntityStatus.Active &&
                                   p.Estado == (int)EntityStatus.Active &&
                                   (pp.PermisoRequerido == pm.Permiso )
                             orderby p.OrdenPagina
                             select p);

                //List<Pagina> listPag = PaginaBusiness.Instance.GetByConditions(p => p.IdPagina == 200 || p.IdPagina == 300 || p.IdPagina == 400 || p.IdPagina == 500 || p.IdPagina == 600);
                List<Pagina> listPag = new List<Pagina>();
                List<Pagina> listPagPadre = new List<Pagina>();

                foreach (var item in query)
                {
                    if (item.IdPagina != 100 && item.IdPagina != 200 && item.IdPagina != 300 && item.IdPagina != 400 && item.IdPagina != 500 && item.IdPagina != 600 && item.IdPagina != 700 && item.IdPagina != 800 && item.IdPagina != 709 && item.IdPagina != 714)
                    {
                        listPag.Add(item);

                        if (item.IdPadre != null)
                        {
                            pagPadre = PaginaBusiness.Instance.GetEntity(item.IdPadre.Value);

                            listPagPadre.Add(pagPadre);
                        }
                    }                   
                }
                foreach (var item in listPagPadre.Distinct())
                {
                    listPag.Add(item);
                }
                //return query.ToList();
                return listPag.OrderBy(p  => p.IdPagina).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}