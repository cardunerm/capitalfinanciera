﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ProveedorBusiness : GenericBusiness<ProveedorBusiness, Proveedor>
    {
        #region Methods



        public List<Proveedor> GetProveedorList()
        {
            try
            {
                IEnumerable<Proveedor> query = DataAccessManager.GetByConditions<Proveedor>(p =>
                                                p.Activo == true);

                query = query.OrderBy(p => p.Nombre);

                return (query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #endregion
    }

}
