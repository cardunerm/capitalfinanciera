﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ParametroBusiness : GenericBusiness<ParametroBusiness, Parametro>
    {

        public List<Parametro> GetByFilters(string nombre, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<Parametro> query = DataAccessManager.GetByConditions<Parametro>(p => p.Nombre.Contains(nombre));

                query = query.OrderBy(p => p.Nombre);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
    
}
