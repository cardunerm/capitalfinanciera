﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class FacturaDeudoBusiness : GenericBusiness<FacturaDeudoBusiness,FacturaDeudo>
    {


        #region methods

        public List<FacturaDeudo> GetByFilters(string nombre, string cuit, int skip, int top, ref int rowCount)
        {
            try
            {
                IEnumerable<FacturaDeudo> query = DataAccessManager.GetByConditions<FacturaDeudo>(p =>
                                                p.Activo == true &&
                                                p.Nombre.Contains(nombre) &&
                                                p.CUIT.Contains(cuit));

                query = query.OrderBy(p => p.Nombre).ThenBy(x => x.CUIT);
                rowCount = query.Count();

                return (top > 0 ? query.Skip(skip).Take(top) : query).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FacturaDeudo> GetDeudos()
        {
            var query = DataAccessManager.GetByConditions<FacturaDeudo>(p => p.Activo == true);
            return query;
        }


        #endregion 
    }
}
