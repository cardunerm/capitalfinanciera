﻿using Enumerations;
using Domine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class RecepcionChequeBusiness : GenericBusiness<RecepcionChequeBusiness, RecepcionCheque>
    {
        public RecepcionCheque GetRecepcionCheque(int id)
        {
            return DataAccessManager.GetById<RecepcionCheque>(id);
        }

        public List<RecepcionCheque> GetRecepcionCheques(DateTime FechaHasta, DateTime FechaDesde, string nombreCliente, int jtStartIndex, int jtPageSize, ref int rowCount)
        {
            try
            {
                IQueryable<RecepcionCheque> query = DataAccessManager.GetByConditions<RecepcionCheque>(p => p.Cliente.Nombre.Contains(nombreCliente)).AsQueryable();

                query = query.Where(u => u.Fecha >= FechaDesde);

                query = query.Where(u => u.Fecha <= FechaHasta);

                query = query.OrderByDescending(b => b.Fecha);

                rowCount = query.Count();

                return query.Skip(jtStartIndex).Take(jtPageSize).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateRecepcionCheques(RecepcionCheque RecepcionCheques, Cheque[] chequesList)
        {

            if (chequesList != null && chequesList.Any())
            {
                RecepcionCheque entidad = new RecepcionCheque
                {
                    Fecha = RecepcionCheques.Fecha,
                    IdCliente = RecepcionCheques.IdCliente
                };

                foreach (var detalle in chequesList)
                {
                    Cheque cheq = new Cheque() { Activo = true };

                    cheq.RecepcionCheque = entidad;
                    cheq.FechaVencimiento = detalle.FechaVencimiento;
                    cheq.IdRecepcionCheque = entidad.IdRecepcionCheque;
                    cheq.Monto = detalle.Monto;
                    cheq.NroCuenta = detalle.NroCuenta;
                    cheq.Numero = detalle.Numero;
                    cheq.IdEstado = (int)Utils.Enumeraciones.Estados.eEstadosCheque.Cartera;
                    cheq.SucursalEmision = detalle.SucursalEmision;
                    cheq.TitularCuenta = detalle.TitularCuenta;
                     
                    ChequeBusiness.Instance.SaveOrUpdateCheque(cheq);
                }

                SaveOrUpdate(entidad, ValidateSaveOrUpdate);

                AuditoriaBusiness.Instance.AuditoriaRecepcionCheque(AuditAction.Create, entidad);
            }
        }

        private bool ValidateSaveOrUpdate(RecepcionCheque entity)
        {
            if (true)
            {
                return true;
            }
            else
            {
                throw new ValidationException("No se puede aceptar la recepcion del cheque.");
            }
        }

        public void UpdateRecepcionCheques(RecepcionCheque RecepcionCheques)
        {
            try
            {
                RecepcionCheque RecepcionChequesDB = this.GetRecepcionCheque(RecepcionCheques.IdRecepcionCheque);

                if (RecepcionChequesDB != null)
                {
                    RecepcionChequesDB.Fecha = RecepcionCheques.Fecha;
                    RecepcionChequesDB.IdCliente = RecepcionCheques.IdCliente;

                    SaveOrUpdate(RecepcionChequesDB, ValidateSaveOrUpdate);
                }
                else
                {
                    throw new Exception("La Recepcion Cheque no existe");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateDelete(RecepcionCheque entity)
        {
            if (!RecepcionHasCheques(entity))
            {
                return true;
            }
            else
            {
                throw new ValidationException(string.Format("No se puede eliminar el registro.", "El registro tiene cheques asociados"));
            }
        }

        private bool RecepcionHasCheques(RecepcionCheque entity)
        {
            try
            {
                return entity.Cheque.Any();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void CreateDetallesCheques(SGFIN_RecepcionChequesDetalles[] detallesList, int idRecepcion)
        //{
        //    try
        //    {
        //        SGFIN_RecepcionCheques recepcionCheque = DataContextManager.Instance.AccessRepository.GetById<SGFIN_RecepcionCheques>(idRecepcion);

        //        if (recepcionCheque == null)
        //            throw new Exception("No se encontro la recepcion del cheque");

        //        if (detallesList.Any())
        //        {

        //            foreach (var detalle in detallesList)
        //            {
        //                SGFIN_RecepcionChequesDetalles recepcionChequesDetalle = new SGFIN_RecepcionChequesDetalles();

        //                recepcionChequesDetalle.IdRecepcionCheques = idRecepcion;
        //                recepcionChequesDetalle.IdBanco = detalle.IdBanco;
        //                recepcionChequesDetalle.NumeroCheque = detalle.NumeroCheque;
        //                recepcionChequesDetalle.Importe = detalle.Importe;
        //                recepcionChequesDetalle.FechaEmision = detalle.FechaEmision;
        //                recepcionChequesDetalle.FechaCobro = detalle.FechaCobro;
        //                recepcionChequesDetalle.DescuentoDetalle = detalle.DescuentoDetalle;
        //                recepcionChequesDetalle.Estado = (int)EstadoChequeDetalle.EnCartera;
        //                recepcionChequesDetalle.FechaIngreso = detalle.FechaIngreso;

        //                DataContextManager.Instance.AccessRepository.AddObject(recepcionChequesDetalle);
        //            }
        //            DataContextManager.Instance.AccessRepository.SaveChanges();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
