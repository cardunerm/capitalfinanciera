﻿using Domine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class FacturaParametroOperacionBusiness : GenericBusiness<FacturaParametroOperacionBusiness, FacturaParametroOperacion>
    {


        public void SaveOperacionParametro(List<FacturaParametroOperacion> list)
        {
            try
            {
                int _idFacturacionOperacion = (int)list[0].IdFacturaOperacion;
                List<FacturaParametroOperacion> _list = this.GetByConditions(p => p.IdFacturaOperacion == _idFacturacionOperacion);
                this.DeleteList(_list);

                foreach (var item in list)
                {
                    this.SaveOrUpdate(item);
                }               
                
            }
            catch (Exception ex)
            {

                throw ex;
            }



        }



    }
}
