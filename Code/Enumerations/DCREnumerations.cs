﻿
using System.ComponentModel.DataAnnotations;

namespace Enumerations
{
    public enum AuditAction
    {
        Create = 1,
        Edit = 2,
        Delete = 3,
        Login = 4,
        Logout = 5
    }

    public enum DocumentType
    {
        DNI = 1,
        LC = 2,
        LE = 3,
        Passport = 4,
        CUIT = 5
    }

    public enum EntityStatus
    {
        Deleted = 0,
        Active = 1
    }

    public enum LogSeverity
    {
        Info = 0,
        Warning = 1,
        Error = 2,
        Fatal = 3
    }

    public enum Permission
    {
        View = 1,
        Create = 2,
        Edit = 3,
        Delete = 4
    }

    public enum ReportFormat
    {
        PDF = 1,
        Excel = 2
    }

    public enum YesNo
    {
        Yes = 1,
        No = 2
    }

    public enum ModuloSistema
    {
        Principal = 1,
        Seguridad = 100,
        Perfiles = 110,
        ValidacionesAutorizadasPerfil = 112,
        Usuarios = 120,        
        Auditoria = 130,
        Administrar = 300,
        Clientes = 220,       
        Configuracion = 200,        
        Bancos = 210,       
        Informes = 400,
        Cheques = 190,
        Sociedades = 260,
        ChequesRechazados = 210,
        Liquidacion = 320,
        RecepcionCheques = 310,
        EmisoresDeValores = 240,
        CuentaCorriente = 230,
        Feriados = 250,
        RecepcionChequesDetalles = 270,
        LiquidacionDetalle = 280,
        Inversores = 400
    }
    public enum Validacion
    {
        AutorizarSobreventaGC = 1,
        AutorizarImporteExcedente = 2,
        AutorizarImporteMenor = 3,
        AutorizarBonificacion = 4,
        AutorizarCierrePauta = 5,
        AutorizarLiberacionPauta = 6,
        CargarPautaDeEjecutivo = 7,
        AutorizarSobreventaGG = 8,
        AutorizarDetallePautaProduccion = 9,
        EliminarLotePagoProvisionado = 100,
        ReprocesarPauta = 101,
        RefacturarPauta = 102,
        VerBotonesPauta = 103,
        CargarPautasFechaAnterior = 104,
        ReabrirPauta = 105,
        ReasignarProveedor = 106,
        VerDevengadoVentasVendedores = 107,
        GestionarGruposElementosStock = 108,
        GestionarGruposElementosVentas = 109,
        FacturarPautasExhibicion = 110,
        FacturarPautasProduccion = 111
    }

    public enum EstadoCheque
    {         
        EnOperacion = 1,
        Aprobado = 2,
        NoAprobado = 3,
        Aceptado = 4,      
        NoAceptado = 5,
        Cartera = 6,
        Depositado = 7,
        Negociado = 8,
        Cobrado = 9,
        Rechazado = 10,
        Cancelado = 11
    }

    public enum ClasificacionCheque
    {
        Comun = 1,
        Activo = 2,
        Garantia = 3
    }

    public enum EstadoNegocion
    {
        Negociado = 1,        
        Cobrado = 2,
        Cancelado = 3
    }

    public enum TipoNegociacion
    {
        Bancaria = 1,
        Proveedor = 2,
        Mayorista = 3
    }

    public enum TipoMoneda
    {

        [Display(Name = "Dolar")]
        Dolar = 0,
        [Display(Name = "Peso")]
        Peso = 1

    }

    public enum Condicion
    {
        Efectivo = 0,
        Cheque = 1
    }

    public enum ClasificacionMovimiento
    {
        Ingreso = 1,
        Egreso = 2,
        Intereses = 3,
        Impuestos = 4,
        Gastos = 5,
        Multas = 6,
        Cancelacion = 7
    }

    public enum EntidadesAdjuntos
    {
        Inversor = 1
    }

    public enum ClasificacionInversor
    {
        AAA = 1, //Inversores normales
        BBB = 2  //Inversores con garantías
    }

    public enum FacturaOperacionEstado
    {
        Pendiente = 1, 
        Aprobada = 2,
        Terminada = 3
    }

    public enum FacturaParametro
    {

        Escribania = 1,
        Transcripcion = 2,
        Protocolizacion = 3,
        Sello = 4,
        TotalGasto = 5,
        DiasDemora = 6,
        GastoFija = 7,
        ImpCheque = 8,
        GastoOtorgamiento = 9,
        TadaDemora = 10,
        Demora = 11,
        IncidenciaGasto = 12,
        IncidenciaInteres = 13,
        TotalAforo = 14,
        SaldoInicial = 15,
        InteresFecha = 16,
        IvaInteres = 17,
        Impuesto = 18,
        GastoCesion = 19,
        GastoOtorgamientoFicha = 20,
        IvaGasto = 21,
        InteresAdicional1Pago = 22,
        IvaInteresAdicional1Pago = 23,
        InteresAdicional2Pago = 24,
        IvaInteresAdicional2Pago = 25,
        SalgoCuenta = 26,
        importeFactura = 27,
        Iva = 28,
        TasaDiaria = 29,
        Garantia1 = 30,
        Garantia2 = 31,
        Garantia3 = 32




    }

}