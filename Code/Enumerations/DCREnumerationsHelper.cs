﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Enumerations
{
    public class DCREnumerationsHelper
    {
        public static List<SelectListItem> ObtenerValidaciones(bool insertarIndistinto = false)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();

                if (insertarIndistinto)
                {
                    list.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }

                list.Add(new SelectListItem { Value = ((int)Validacion.AutorizarSobreventaGC).ToString(), Text = "AutorizarSobreventaGC" });
                list.Add(new SelectListItem { Value = ((int)Validacion.AutorizarSobreventaGG).ToString(), Text = "AutorizarSobreventaGG" });
                list.Add(new SelectListItem { Value = ((int)Validacion.AutorizarImporteExcedente).ToString(), Text = "AutorizarImporteExcedente" });
                list.Add(new SelectListItem { Value = ((int)Validacion.AutorizarImporteMenor).ToString(), Text = "AutorizarImporteMenor" });
                list.Add(new SelectListItem { Value = ((int)Validacion.AutorizarBonificacion).ToString(), Text = "AutorizarBonificacion" });
                list.Add(new SelectListItem { Value = ((int)Validacion.AutorizarCierrePauta).ToString(), Text = "AutorizarCierrePauta" });
                list.Add(new SelectListItem { Value = ((int)Validacion.AutorizarLiberacionPauta).ToString(), Text = "AutorizarLiberacionPauta" });
                list.Add(new SelectListItem { Value = ((int)Validacion.CargarPautaDeEjecutivo).ToString(), Text = "CargarPautaDeEjecutivo" });
                list.Add(new SelectListItem { Value = ((int)Validacion.AutorizarDetallePautaProduccion).ToString(), Text = "AutorizarDetallePautaProduccion" });
                list.Add(new SelectListItem { Value = ((int)Validacion.EliminarLotePagoProvisionado).ToString(), Text = "EliminarLotePagoProvisionado" });
                list.Add(new SelectListItem { Value = ((int)Validacion.ReprocesarPauta).ToString(), Text = "ReprocesarPauta" });
                list.Add(new SelectListItem { Value = ((int)Validacion.RefacturarPauta).ToString(), Text = "RefacturarPauta" });
                list.Add(new SelectListItem { Value = ((int)Validacion.VerBotonesPauta).ToString(), Text = "VerBotonesPauta" });
                list.Add(new SelectListItem { Value = ((int)Validacion.CargarPautasFechaAnterior).ToString(), Text = "CargarPautasFechaAnterior" });
                list.Add(new SelectListItem { Value = ((int)Validacion.ReabrirPauta).ToString(), Text = "ReabrirPauta" });
                list.Add(new SelectListItem { Value = ((int)Validacion.ReasignarProveedor).ToString(), Text = "ReasignarProveedor" });
                list.Add(new SelectListItem { Value = ((int)Validacion.VerDevengadoVentasVendedores).ToString(), Text = "VerDevengadoVentasVendedores" });
                list.Add(new SelectListItem { Value = ((int)Validacion.GestionarGruposElementosStock).ToString(), Text = "GestionarGruposElementosStock" });
                list.Add(new SelectListItem { Value = ((int)Validacion.GestionarGruposElementosVentas).ToString(), Text = "GestionarGruposElementosVentas" });
                list.Add(new SelectListItem { Value = ((int)Validacion.FacturarPautasExhibicion).ToString(), Text = "FacturarPautasExhibicion" });
                list.Add(new SelectListItem { Value = ((int)Validacion.FacturarPautasProduccion).ToString(), Text = "FacturarPautasProduccion" });

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        #region Get Name

        public static string GetAuditActionName(AuditAction item)
        {
            try
            {
                switch (item)
                {
                    case AuditAction.Create:
                        return "Crear";
                    case AuditAction.Edit:
                        return "Editar";
                    case AuditAction.Delete:
                        return "Eliminar";
                    case AuditAction.Login:
                        return "Inicio Sesion";
                    case AuditAction.Logout:
                        return "Cerrar Sesion";
                    default:
                        return string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetDocumentTypeName(DocumentType item)
        {
            try
            {
                switch (item)
                {
                    case DocumentType.DNI:
                        return "DNI";
                    case DocumentType.LC:
                        return "LC";
                    case DocumentType.LE:
                        return "LE";
                    case DocumentType.Passport:
                        return "Passport";
                    case DocumentType.CUIT:
                        return "CUIT";
                    default:
                        return string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetPermissionName(Permission item, bool tionEnding = false)
        {
            try
            {
                switch (item)
                {
                    case Permission.View:
                        return (!tionEnding ? "Ver" : "visualización");
                    case Permission.Create:
                        return (!tionEnding ? "Crear" : "creación");
                    case Permission.Edit:
                        return (!tionEnding ? "Editar" : "edición");
                    case Permission.Delete:
                        return (!tionEnding ? "Eliminar" : "eliminación");
                    default:
                        return string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Get Value

        public static string GetPermissionValue(Permission item)
        {
            try
            {
                return ((int)item).ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region List Items

        public static List<SelectListItem> GetClasificacionInversorTypes(bool insertIndistinct = false)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    list.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }

                list.Add(new SelectListItem { Value = ((int)ClasificacionInversor.AAA).ToString(), Text = "AAA" });
                list.Add(new SelectListItem { Value = ((int)ClasificacionInversor.BBB).ToString(), Text = "BBB" });

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetDocumentTypes(bool insertIndistinct = false)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    list.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }

                list.Add(new SelectListItem { Value = ((int)DocumentType.DNI).ToString(), Text = "DNI" });
                list.Add(new SelectListItem { Value = ((int)DocumentType.LC).ToString(), Text = "LC" });
                list.Add(new SelectListItem { Value = ((int)DocumentType.LE).ToString(), Text = "LE" });
                list.Add(new SelectListItem { Value = ((int)DocumentType.Passport).ToString(), Text = "Passport" });
                list.Add(new SelectListItem { Value = ((int)DocumentType.CUIT).ToString(), Text = "CUIT" });

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetTipoNegociacion(bool insertIndistinct = false)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    list.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }

                list.Add(new SelectListItem { Value = ((int)TipoNegociacion.Bancaria).ToString(), Text = "BANCARIA" });
                list.Add(new SelectListItem { Value = ((int)TipoNegociacion.Mayorista).ToString(), Text = "MAYORISTA" });
                list.Add(new SelectListItem { Value = ((int)TipoNegociacion.Proveedor).ToString(), Text = "PROVEEDOR" });                

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public static List<SelectListItem> GetAuditActions(bool insertIndistinct = false)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    list.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }

                list.Add(new SelectListItem { Value = ((int)AuditAction.Create).ToString(), Text = "Crear" });
                list.Add(new SelectListItem { Value = ((int)AuditAction.Edit).ToString(), Text = "Editar" });
                list.Add(new SelectListItem { Value = ((int)AuditAction.Delete).ToString(), Text = "Eliminar" });
                list.Add(new SelectListItem { Value = ((int)AuditAction.Login).ToString(), Text = "Inicio sesión" });
                list.Add(new SelectListItem { Value = ((int)AuditAction.Logout).ToString(), Text = "Cierre sesión" });

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetYesNoList(bool insertIndistinct = false)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    list.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }

                list.Add(new SelectListItem { Value = ((int)YesNo.Yes).ToString(), Text = "Si" });
                list.Add(new SelectListItem { Value = ((int)YesNo.No).ToString(), Text = "No" });

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetEstadoCheques(bool insertIndistinct = false)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    list.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }

                list.Add(new SelectListItem { Value = "6", Text = "En Cartera" });
                list.Add(new SelectListItem { Value = "9", Text = "Cobrado" });
                list.Add(new SelectListItem { Value = "10", Text = "Rechazado" });

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SelectListItem> GetEstadoCtaCte(bool insertIndistinct = false)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();

                if (insertIndistinct)
                {
                    list.Add(new SelectListItem { Value = "-1", Text = "Indistinto" });
                }

                list.Add(new SelectListItem { Value = "1", Text = "Activa" });
                list.Add(new SelectListItem { Value = "0", Text = "Inactiva" });

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
