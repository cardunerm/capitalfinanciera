﻿using System;
using System.Web;
using Domine;
using static Luma.Utils.Enumeraciones.Perfil;

namespace CurrentContext
{
    public static class SessionContext
    {
        public static Configuracion Configuracion
        {
            get
            {
                return (Configuracion)HttpContext.Current.Session["Configuracion_Empresa"];
            }
            set
            {
                HttpContext.Current.Session["Configuracion_Empresa"] = value;
            }
        }

        public static Empresa Empresa
        {
            get
            {
                return (Empresa)HttpContext.Current.Session["Entidad_Empresa"];
            }
            set
            {
                HttpContext.Current.Session["Entidad_Empresa"] = value;
            }
        }

        public static Usuario UsuarioActual
        {
            get
            {
                if (HttpContext.Current.Session["PS_UsuarioActual"] != null)
                {
                    return (Usuario)HttpContext.Current.Session["PS_UsuarioActual"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    HttpContext.Current.Session["PS_UsuarioActual"] = value;

                    IdUsuarioActual = value.IdUsuario;
                }
                else
                {
                    HttpContext.Current.Session["PS_UsuarioActual"] = null;

                    IdUsuarioActual = null;
                }
            }
        }

        public static long? IdUsuarioActual
        {
            get
            {

                if (HttpContext.Current.Session["PS_IdUsuarioActual"] != null)
                    return (long?)HttpContext.Current.Session["PS_IdUsuarioActual"];
                else if (HttpContext.Current.Request.Cookies["PS_IdUsuarioActual"] != null)
                    return Convert.ToInt64(HttpContext.Current.Request.Cookies["PS_IdUsuarioActual"].Value);
                else
                    return null;

            }
            set
            {
                if (value != null)
                {
                    HttpCookie cookie = new HttpCookie("PS_IdUsuarioActual");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddHours(1);
                    System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
                }
                else
                {
                    if (System.Web.HttpContext.Current.Request.Cookies["PS_IdUsuarioActual"] != null)
                    {
                        System.Web.HttpContext.Current.Response.Cookies["PS_IdUsuarioActual"].Expires = DateTime.Now.AddDays(-1);
                    }
                }

                System.Web.HttpContext.Current.Session["PS_IdUsuarioActual"] = value;
            }
        }

        //public static Empresa EmpresaActual
        //{
        //    get
        //    {
        //        if (HttpContext.Current.Session["PS_EmpresaActual"] != null)
        //        {
        //            return (Empresa)HttpContext.Current.Session["PS_EmpresaActual"];
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    set
        //    {
        //        if (value != null)
        //        {
        //            HttpContext.Current.Session["PS_EmpresaActual"] = new Empresa
        //            {
        //                IdEmpresa = value.IdEmpresa,
        //                Nombre = value.Nombre,
        //                EstadoPautaInicio = value.EstadoPautaInicio,
        //                UsaPautaExhibicion = value.UsaPautaExhibicion,
        //                PrefijoExhibicionNormal = value.PrefijoExhibicionNormal,
        //                PrefijoExhibicionRefactura = value.PrefijoExhibicionRefactura,
        //                UsaPautaProduccion = value.UsaPautaProduccion,
        //                PrefijoProduccionNormal = value.PrefijoProduccionNormal,
        //                PrefijoProduccionRefactura = value.PrefijoProduccionRefactura,
        //                ServidorBAS = value.ServidorBAS,
        //                BaseDatosBAS = value.BaseDatosBAS,
        //                UsuarioBAS = value.UsuarioBAS,
        //                ContraseniaBAS = value.ContraseniaBAS,
        //                ServidorICR = value.ServidorICR,
        //                BaseDatosICR = value.BaseDatosICR,
        //                UsuarioICR = value.UsuarioICR,
        //                ContraseniaICR = value.ContraseniaICR
        //            };

        //            IdEmpresaActual = value.IdEmpresa;
        //        }
        //        else
        //        {
        //            HttpContext.Current.Session["PS_EmpresaActual"] = null;

        //            IdEmpresaActual = null;
        //        }
        //    }
        //}

        public static long? IdEmpresaActual
        {
            get
            {

                if (HttpContext.Current.Session["PS_IdEmpresaActual"] != null)
                    return (long?)HttpContext.Current.Session["PS_IdEmpresaActual"];
                else if (HttpContext.Current.Request.Cookies["PS_IdEmpresaActual"] != null)
                    return Convert.ToInt64(HttpContext.Current.Request.Cookies["PS_IdEmpresaActual"].Value);
                else
                    return null;

            }
            set
            {
                if (value != null)
                {
                    HttpCookie cookie = new HttpCookie("PS_IdEmpresaActual");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddHours(1);
                    System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
                }
                else
                {
                    if (System.Web.HttpContext.Current.Request.Cookies["PS_IdEmpresaActual"] != null)
                    {
                        System.Web.HttpContext.Current.Response.Cookies["PS_IdEmpresaActual"].Expires = DateTime.Now.AddDays(-1);
                    }
                }

                System.Web.HttpContext.Current.Session["PS_IdEmpresaActual"] = value;
            }
        }

        //public static SucursalOperacion SucursalOperacionActual
        //{
        //    get
        //    {
        //        if (HttpContext.Current.Session["PS_SucursalOperacionActual"] != null)
        //        {
        //            return (SucursalOperacion)HttpContext.Current.Session["PS_SucursalOperacionActual"];
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    set
        //    {
        //        if (value != null)
        //        {
        //            HttpContext.Current.Session["PS_SucursalOperacionActual"] = new SucursalOperacion
        //            {
        //                IdSucursalOperacion = value.IdSucursalOperacion,
        //                IdEmpresa = value.IdEmpresa,
        //                Nombre = value.Nombre
        //            };

        //            IdSucursalOperacionActual = value.IdSucursalOperacion;
        //        }
        //        else
        //        {
        //            HttpContext.Current.Session["PS_SucursalOperacionActual"] = null;

        //            IdSucursalOperacionActual = null;
        //        }
        //    }
        //}

        public static long? IdSucursalOperacionActual
        {
            get
            {

                if (HttpContext.Current.Session["PS_IdSucursalOperacionActual"] != null)
                    return (long?)HttpContext.Current.Session["PS_IdSucursalOperacionActual"];
                else if (HttpContext.Current.Request.Cookies["PS_IdSucursalOperacionActual"] != null)
                    return Convert.ToInt64(HttpContext.Current.Request.Cookies["PS_IdSucursalOperacionActual"].Value);
                else
                    return null;

            }
            set
            {
                if (value != null)
                {
                    HttpCookie cookie = new HttpCookie("PS_IdSucursalOperacionActual");
                    cookie.Value = value.ToString();
                    cookie.Expires = DateTime.Now.AddHours(1);
                    System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
                }
                else
                {
                    if (System.Web.HttpContext.Current.Request.Cookies["PS_IdSucursalOperacionActual"] != null)
                    {
                        System.Web.HttpContext.Current.Response.Cookies["PS_IdSucursalOperacionActual"].Expires = DateTime.Now.AddDays(-1);
                    }
                }

                System.Web.HttpContext.Current.Session["PS_IdSucursalOperacionActual"] = value;
            }
        }

        public static string DireccionIPActual
        {
            get
            {
                return HttpContext.Current.Request.UserHostAddress;
            }
        }

        public static string UrlActual
        {
            get
            {
                return HttpContext.Current.Request.Url.Authority;
            }
        }

        public static string Key
        {
            get
            {
                return (HttpContext.Current.Session["Key"] ?? "").ToString();
            }
            set
            {
                HttpContext.Current.Session["Key"] = value;
            }
        }


        public static TipoPerfil Perfil
        {
            get
            {
                return (TipoPerfil)HttpContext.Current.Session["Perfil"];
            }
            set
            {
                HttpContext.Current.Session["Perfil"] = value;
            }
        }
    }
}