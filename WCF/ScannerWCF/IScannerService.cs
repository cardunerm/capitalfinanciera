﻿using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ScannerWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IScannerService
    {
        [OperationContract]
        [WebInvoke(Method = "OPTIONS", UriTemplate = "*")]
        Cheque Options();
        [WebGet(UriTemplate = "*",ResponseFormat = WebMessageFormat.Json)]            
        Cheque Get();

       
        // TODO: Add your service operations here
    }
    [DataContract]
    public class Cheque
    {
        Scanner.Model.Cheque _cheque;

        [DataMember]
        public Scanner.Model.Cheque ChequeResponse
        {
            get { return _cheque; }
            set { _cheque = value; }
        }
    }
}
